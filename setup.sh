mkdir data/raw
mkdir data/parsed
mkdir data/other

# Downloading Bashi
rm -r data/raw/bashi
mkdir data/raw/bashi
mkdir data/parsed/bashi
wget https://www.ims.uni-stuttgart.de/documents/team/alt-nicht-mehr-da/roesigia/bashi-bridging-column.zip -P data/raw/bashi
unzip -qq data/raw/bashi/bashi-bridging-column.zip -d data/raw/bashi

# Download ISNotes
rm -r data/raw/isnotes
mkdir data/raw/isnotes
mkdir data/parsed/isnotes
wget https://github.com/nlpAThits/ISNotes1.0/archive/master.zip -P data/raw/isnotes
unzip -qq data/raw/isnotes/master.zip -d data/raw/isnotes

# Make dir for ontonotes.
mkdir data/raw/ontonotes
#mkdir data/parsed/ontonotes
#touch data/parsed/ontonotes/processed.json    # Create an empty json file for the ontonotes parser
tar zxf data/raw/ontonotes/ontonotes-release-5.0_LDC2013T19.tgz -C data/raw/ontonotes/

# Unzip the Crac datasets
unzip -qq data/raw/crac-switchboard/Switchboard_3_dev.CONLL_LDC2021E05.zip -d data/raw/crac-switchboard
rm -r data/raw/crac-switchboard/__MACOSX
unzip -qq data/raw/crac-arrau/ARRAU2.0_UA_v3_LDC2021E05.zip -d data/raw/crac-arrau
mv data/raw/crac-arrau/v3/*.CONLL data/raw/crac-arrau
rm -r data/raw/crac-arrau/v3
rm -r data/raw/crac-arrau/__MACOSX
unzip -qq data/raw/crac-light/light_dev.CONLLUA.zip -d data/raw/crac-light
rm -r data/raw/crac-light/__MACOSX

# Make dir for storing wordtovec and other models
mkdir models/wordtovec
mkdir models/wordnet

# Download Wikitext103
echo "Moving on to Wikitext103"
mkdir data/raw/wikitext
wget https://s3.amazonaws.com/research.metamind.io/wikitext/wikitext-103-v1.zip -P ./data/raw/wikitext/
unzip ./data/raw/wikitext/wikitext-103-v1.zip -d ./data/raw/wikitext/

# Install python libraries
pip install -r requirements.txt

# Install spacy model
python -m spacy download en_core_web_sm
#python -m spacy download en_core_web_trf

# Update the submodules
git submodule update --init --recursive
git submodule update --remote --merge