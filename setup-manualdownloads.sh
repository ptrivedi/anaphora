# ###############################
# This script assumes that you have access to the Grid5000 servers, and that you are a part of the MAGNET INRIA Lab.
#
# # RESPONSIBILITY
# If not, please download these datasets responsibly from their respective sources, respecting their licenses.
#
# # REQUIREMENTS
# You should also ensure that your `~/.ssh/config` is set up as suggested on G5k Wiki - [Link](https://www.grid5000.fr/w/Getting_Started#Recommended_tips_and_tricks_for_an_efficient_use_of_Grid.275000)
# i.e., you can access the Lille center by simply executing `ssh lille.g5k` on your computer.
# If you have access to G5k but don't want to set up your ssh config as suggested, feel free to change the scp commands to your liking.
#
# You also need access to magnet's storage on G5k servers. To do so,
# 1. Go to https://api.grid5000.fr/stable/users/ and log in
# 2. Go to `Groups` on the sidebar and join a group called `sto-magnet`.
# 3. Wait for approval, and then yo can access the directories below.
#
# # ALTERNATIVES
# Alternatively you can also source these datasets as you see fit from the internet, or other sources. Ignore running this file, in such cases.
# ###############################


# Data
echo "Attempting to Manually download Ontonotes"
mkdir data
mkdir data/raw
mkdir data/raw/ontonotes
scp lille.g5k:/srv/storage/magnet@storage1.lille.grid5000.fr/ptrivedi/anaphora/data/raw/ontonotes/*tgz data/raw/ontonotes

echo "Attempting to Datasets for the CODI-CRAC 2021 Task"
mkdir data/raw/crac-arrau
scp lille.g5k:/srv/storage/magnet@storage1.lille.grid5000.fr/ptrivedi/anaphora/data/raw/crac-arrau/*.zip data/raw/crac-arrau

mkdir data/raw/crac-switchboard
scp lille.g5k:/srv/storage/magnet@storage1.lille.grid5000.fr/ptrivedi/anaphora/data/raw/crac-switchboard/*.zip data/raw/crac-switchboard

# Models
mkdir models
scp -r lille.g5k:/srv/storage/magnet@storage1.lille.grid5000.fr/ptrivedi/anaphora/models/wordtovec ./models
scp -r lille.g5k:/srv/storage/magnet@storage1.lille.grid5000.fr/ptrivedi/anaphora/models/wordnet ./models

# TODO: Add knowBERT stuff there as well.

