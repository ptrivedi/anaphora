# anaphora

A codebase containing data loaders, approaches, and test benches for Bridging Anaphora related tasks.

## Set Up

### Overview

The tree structure should look like:

```
.
├── data
│    ├── parsed
│    └── raw
│        ├── bashi
│        │   ├── bashi-bridging-column.conll
│        │   └── bashi-bridging-column.zip
│        ├── isnotes
│        │   ├── ISNotes1.0-master
│        │    └── master.zip
│        └── ontonotes
│            ├── ontonotes-release-5.0
│            └── ontonotes-release-5.0_LDC2013T19.tgz
│        └── ...
├── models
│    ├── wordtovec
│    └── wordnet
├── kb (submodule)
├── universal-anaphora-scorer (submodule)
├── README.md
├── setup.sh
└── src
    ├── config.py
    ├── run.py
    ├── preproc
    │   ├── base.py
    │   ├── bashi.py
    │   ├── __init__.py
    │   ├── isnotes.py
    │   └── ...
    └── utils
         ├── main.py
         └── ...
```

### Get pull
After pulling, you'll need to run `git submodule init` and `git submodule update` to get and then populate the submodules with the appropriate code. The two submodules are `kb` for the knowbert code and `universal-anaphora-scorer` for the official CRAC scoring code.

### 1. Manual Downloads

**If you are a member of MAGNET Lab, INRIA, and have access to Grid5000 servers, you do not need to manually download the following. Just read and run `./setup-manualdownloads.sh` to copy them from my folders on G5k. Ignore the rest of this section  and skip to "Enable/Create a Python venv/conda env" section.**

#### Ontonotes

Ontonotes is not a freely available corpora. Yes, yes... I know. I know. I hope you have access to it. 
So, in any case, you're going to have to get access to it somehow. All the best. Godspeed.

You want a file called `ontonotes-release-5.0_LDC2013T19.tgz`.

Put it in `./data/raw/ontonotes`, and extract it as well.
Finally, there should a directory: `data/raw/ontonotes/ontonotes-release-5.0/`

#### CODI CRAC 2021 Datasets

This is for a shared task whose resources we are not allowed to share openly. [Link.](https://competitions.codalab.org/competitions/30312)

If you have access to the files, place them in their respective `data/raw/crac-*` folders. E.g. ARRAU goes in `data/raw/crac-arrau` and so on. We will unzip them using our setup.sh file. 

#### WordToVec

TODO: which variant, what to do with it ...

Get the `GoogleNews-vectors-negative300.bin` file in `models/wordtovec/`

#### KnowBert

The pretrained knowbert archive can be downloaded at this link: https://allennlp.s3-us-west-2.amazonaws.com/knowbert/models/knowbert_wordnet_model.tar.gz

The archive should be extracted to `models/knowbert/`:

```bash
    mkdir models/knowbert
    tar -xf knowbert_wordnet_model.tar.gz -C models/knowbert/
```

### 2. Enable/Create a python venv/conda env

You know the drill.

We assume that the venv/ conda env is loaded by now, and we can proceed doing nice python things hereon.

### 3. Run the setup.sh file.

You might have to permit the shell file to be an executable.

```bash
    chmod +x ./setup.sh
    ./setup.sh
```

This will download some datasets, as well as install some libraries

**Note: NLTK data needs to be downloaded**: TODO how

**Note: HuggingFace cache dir**: 
We will be using huggingface's dataset library to download some datasets. 
It may be a good idea to specify the directory where library downloads datasets.
    
For me, I added ```export HF_HOME="$HOME/Dev/perm/huggingface"``` to `~/.bashrc` 
and updated the shell by entering `source ~/.bashrc`. You will have to reactive your venv/conda env after this again.

**Why**: when we download gigaword (and other) datasets using the library, they are saved somewhere on disk, locally.
In total, the download can be north of a gigabyte, 
and thus you might wanna specify where you want this heavy thing to be stored.
The library will automatically locate the dataset once downloaded, so nothing changes in the code, don't worry.

### 4. CoreNLP, Java Installation

We use CoreNLP for a very specific task -- finding the head of spans. While this can be done with spacy model, we couldn't find any information on _how_ spacy finds the span head/root. And thus we resort to the CoreNLP implementation.

Check if you have Java installed by `java -version`.
Open a python interpreter (within your conda environment) and exectue the following two commands:

```python
import stanza
stanza.install_corenlp(dir="YOUR_PREFERRED_DIRECTORY")
stanza.download_corenlp_models(model='english', version='4.1.0', dir="YOUR_PREFERRED_DIRECTORY")
```

You should check the latest model version. At the time of writing this, the latest version of models is `4.1.0`.

Afterwards, set up the path to this directory from your terminal by
```bash
export CORENLP_HOME=YOUR_PREFERRED_DIRECTORY
```
You should add the above command to your `~/.bashrc` or equivalent file.

### 5. Preprocess the datasets.

#### Real Datasets
```bash
python src/preproc/bashi.py
python src/preproc/isnotes.py
python src/preproc/crac.py switchboard
python src/preproc/crac.py arrau Trains_91.CONLL
python src/preproc/crac.py arrau Trains_93.CONLL
```

#### Faux Anaphor Pairs from Gigaword, Wikitext

We can mine fake anaphor pairs from Gigaword and Wikitext. 
This has been experimented with previously in [[Hou et al, 2018]](). 
To do this you run **either of**:

```bash
python src/preproc/text.py wikitext
python src/preproc/text.py gigaword
python src/preproc/text.py gigaword wikitext
```

**Warning: They takes north of ten hours**

## Approaches

All implemented approaches are available in the `./src/evaluate` dir. They all inherit from a base class - `BaseEvaluationBench` found in `src/evaluate/base.py`.

The parsed instances have two candidate spaces:
- spans which were real antecedents to other anaphors in this document
- all noun phrase spans.

The second one is the default one.

### Word Embedding Based

```python src/evaluate/wordembeddings.py -d bashi```

The goal here, is to compute a vector representing the anaphor, and another vector representing the antecedent.

There are some variations here:

1. Embed only headwords OR embed the entire span
2. If, entire span: How to aggregate multiple word vectors: mean/max per dim/pairwise similarity
3. Similarity: tanimoto distance? cosine distance?

### Sense Embedding Based

```python src/evaluate/senseembeddings.py```

Given: a synset vectors matrix corresponding to WordNet

#### Overview

The goal is to somehow use the sense vectors to estimate the similarity between the anaphor and the antecedent. The hope is that WordNet relations, captured by the vectors, estimate the tendencies of these phrases (or their head words) to co-occur which we use as a proxy to judge that they're being anaphorically bridged. The primary challenge: **how to use one (or more) of the available sense for a given word to get the vector repr.**

#### 1. Closest:

Given two words, $w_a$, $w_b$ corresponding to the headwords of the anaphor and the antecedent, get a set of potential senses: $S_{w_a}, S_{w_b}$. The similarity then is

$$Sim(w_a, w_b) = \underset{s_a \in S_{w_a}, s_b \in S_{w_b}}{\text{max}} S (\vec{s_a}, \vec{s_b})$$

That is, choose the most similar pair from the two sets.

#### Other Challenges

1. **No sense available:**
    1. Case A: word is a entity (PERSON/ORGANIZATION)
    2. Case B: word is not an entity: get the closest word in the word vector/contextual space.
        1. Static Vectors Space: just get the closest vector: `w2v-closest`
        2. Contextual Vector Space:
            1. `mlm-sent`: mask the headword and pass the SENTENCE  to a masked language model and get the predictions.
            2. `mlm-upto`: mask the headword and get all sentences BEFORE the current one (inclusive), and get words
            3. `mlm-from`: same as mlm-upto but from sentences AFTER the current one (inclusive)
            4. `mlm-mid`: get some sentences before, some after the current one.
        3. ??


### Self Attention Based:

```python src/evaluate/contextualmodels.py -d bashi -a bert-attn --use-heads```

#### Implemented Approaches:

1. BERT (bert-base-uncased) 
2. RoBERTa (roberta-base)
3. KnowBERT

#### Overview

There is a latent relation between anaphor and antecedent pairs. BERT based models have shown to contain an understanding of semantic relations between entities/noun phrases in text. → Maybe BERT already focuses some attention to the correct antecedent, for a given anaphor.

**Without** any training/fine tuning, estimate the "focus" that the anaphor span gives to the candidate antecedent spans.

Pass the document through the transformer. Extract the self attention tensor. The tensor is of ****`num. layers, num. heads, sequence length, sequence length` dimension. We want to turn this into a scalar score, based on the attention between the anaphor and antecedent pairs

So we have a scalar score corresponding to each anaphor-antecedent pairs. Can we simply base our candidate selection strategy on this attention score.

#### Scalar Score from a 4D Matrix

- Dim 1: Layers
    - `**select` choose a layer**
    - `select-k` choose multiple layers and do a mean
    - `max` choose maximum values across layers?
- Dim 2: Heads
    - `select` choose one head
    - `mean` do a mean across all heads
    - `max`
    - `select-best` choose the head whose (1, seq len, seq len) matrix has the highest activation
- Dim 3: Sequence - anaphor subset
    - `mean`
- Dim 4: Sequence - antecedent subset
    - `mean`

#### Challenge 1: Maximum length based problems

`bert-based-uncased` has a maximum length of 512 word-piece tokens. Our documents are often longer than that. Strategy —

- Case 1: doc is smaller than 512 words
    - use the entire doc
      `| ************ |`
- Case 2: the anaphor and all antecedents are within the **first** 512 words.
    - use all sentences from start upto the last relevant ones

      `| ********____ |`

- Case 3: the candidate and the anaphor lie within 512 words of each other
    - gather all sentences from the start of the first antecedent candidate to the last candidate
      `| __******____ |`
- Case 4: there are more than 512 words between the candidate/anaphor spans.
    - choose multiple discontinuous spans
      `| _**__*__**** |`
- Case 5: we can't even include all sentences with candidate/anaphor spans as the sentences are too big
    - Start dropping sentences with candidates (furthest from the anaphor span are dropped first), till the doc subset fits the BERT max length.

#### Challenge 2: Tokenization, and manipulating integer spans

Since BERT-based models used word-piece tokens, and ontonotes documents, all our anaphor antecedent spans are word tokens, there's a bit of challenge to convert a span like `[8, 13]` to a span in the word-piece tokenized sequence.

The problem gets worse if you consider Case 3 or 4 or 5 from above. In 3 we have to offset the entire sequence by some constant. In case 4, we have to offset multiple parts of the sequence in different ways so that the anaphor, antecedent spans still correspond with the word-piece tokenized sequence.

### BERT as QA

Based on [Hou, 2020].

### BERT Next Sentence Prediction for Bridging Anaphora Resolution

On going. 

## References

Hou, Yufang.
    "Enhanced word representations for bridging anaphora resolution."
    arXiv preprint arXiv:1803.04790 (2018).

Hou, Yufang. 
    "Bridging anaphora resolution as question answering." 
    arXiv preprint arXiv:2004.07898 (2020).
