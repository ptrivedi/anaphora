"""
    A handy script to run all the appendages, and fetch model results
"""
import json
import click
import warnings
import traceback
import fasteners
from pathlib import Path
from json import JSONDecodeError
from typing import Dict, List, Optional

# Local imports
from utils.misc import pprint
from evaluate.wordembeddings import WordEmbeddingBench
from dataloader import CrossValidationDataLoader, DataLoader
from evaluate.base import cross_validate, OracleEvaluationBench, RandomEvaluationBench
from evaluate.contextualmodels import BertSelfAttentionBench, KnowBertSelfAttentionBench, RoBertaAttentionBench, \
    BertNextSentenceBench, BertSelfAttentionPairwiseBench, DiabloGPTAttentionBench


def cross_validate_self_attn_models(cvdl: CrossValidationDataLoader, approach: str, params: dict,
                                    dumping_approach_name: Optional[str] = None) -> dict:
    """
        Invoke the specified approach and get the corresponding numbers.
        Dump the details of the run in parsed dir/.cv/<approach name.json>

        Return the best performing approach's metrics
    """

    params_: Dict[str, list] = {
        'max_len': [400],
        # 'pool_layers': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', 'mean'],
        'pool_layers': [ '3', '4', '5', '6', '7', '8', '9', '10', '11', 'mean'],
        # 'pool_layers': [ '6',  'mean'],
        'pool_heads': ['mean', 'max', 'best-mean', 'best-max'],
        # 'pool_heads': ['mean'],
        # 'pool_span': ['mean', 'max']
        'pool_span': ['mean', 'max']
    }
    # Superimpose those that have been passed to this fn.
    for k, v in params.items():
        params_[k] = v

    approach_ = {
        'BertSelfAttentionBench': BertSelfAttentionBench,
        'KnowBertSelfAttentionBench': KnowBertSelfAttentionBench,
        'RoBertaAttentionBench': RoBertaAttentionBench,
        'BertSelfAttentionPairwiseBench': BertSelfAttentionPairwiseBench,
        'DiabloGPTAttentionBench': DiabloGPTAttentionBench
    }[approach]

    results: Dict = cross_validate(approach=approach_, cvdl=cvdl, params=params_, select_based_on='metric_acc',
                                   dumping_approach_name=dumping_approach_name)

    # Dump these results in an appropriate dir.
    try:
        dump_dir: Path = cvdl.dir / '.cv'
        dump_dir.mkdir(exist_ok=True)

        with (dump_dir / f'{approach}_{params["candidate_reduction_method"][0]}.json').open('w+', encoding='utf8') as f:
            json.dump(results, f)

    except (ValueError, TypeError, RuntimeError, NotImplementedError):
        print("Could not print results")
        traceback.print_exc()

    del approach_

    return results['summary']


def update_metrics(metrics: Dict, key: str, dataset_dir: Path):
    lock = fasteners.InterProcessLock(dataset_dir / 'results.json')

    with lock:
        try:
            with (dataset_dir / 'results.json').open('r', encoding='utf8') as f:
                known_metrics = json.load(f)
            if key not in known_metrics:
                known_metrics[key] = {}
        except (JSONDecodeError, FileNotFoundError):
            warnings.warn(f"Could not load metrics from {dataset_dir}")
            traceback.print_exc()
            known_metrics = {key: {}}

        for k, v in metrics[key].items():
            known_metrics[key][k] = v

        with (dataset_dir / 'results.json').open('w+', encoding='utf8') as f:
            json.dump(known_metrics, f, indent=4)


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--candidates-src', '-cs', type=str, default='nounphrase',
              help="Either 'nounphrase' or 'antecedent'")
@click.option('--candidate-reduction-method', '-crm', default='none', type=str,
              help="The chosen method to reduce the number of candidates")
@click.option('--approaches', '-a', type=str, default=None, multiple=True,
              help="Multiple values accepted. 'oracle', 'random', 'bert-attn', 'knowbert-attn', 'diablo-attn', "
                   "'wordtovec', 'glove', 'fasttext', 'wordnet-synset'")
@click.option('--device', '-dv', type=str, default=None,
              help="The device to use: cpu, cuda, cuda:0, ...")
def run_all(dataset: str, dataset_domain: str, dataset_domain_filename: str, candidate_reduction_method: str,
            candidates_src: str, approaches: List[str] = None, device: str = None):
    """
        When called, it gets the result for every specified approach.
        The ones which need cross validation are hard coded to be cross validated over a large space.
        The other ones are run normally.

        For each approach run, we augment the results.json file present in the data/parsed/dataset dir directory,
            updating the dict with the current results.
    """
    # Populate the approaches list if not specified.
    if not approaches:
        approaches = ['oracle', 'random',
                      'bert-attn', 'spanbert-attn', 'knowbert-attn', 'roberta-attn', 'diablo-attn',
                      'wordtovec', 'glove', 'fasttext',
                      'wordnet-synset']
    approaches = list(approaches)

    # Declare a dict to store metrics
    metrics: dict = {candidate_reduction_method: {}}

    # Make a DL and a CVDL based on the specified dataset.
    dl = DataLoader(dataset, dataset_domain, dataset_domain_filename)
    # cvdl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename, folds=2)

    # Create a common set of parameters which needs to be added to every other
    common_params = {'candidates_src': candidates_src, 'candidate_reduction_method': candidate_reduction_method}
    common_params_cv = {k: [v] for k, v in common_params.items()}

    for approach in approaches:

        if approach == 'oracle':
            oracle = OracleEvaluationBench(data_loader=dl, candidates_src=candidates_src,
                                           candidate_reduction_method=candidate_reduction_method)
            metrics_: dict = {k: v for k, v in oracle.run(dump_predictions=True, approach_nm=approach).items()
                              if k.startswith('metric_')}
            metrics[candidate_reduction_method]['OracleEvaluationBench'] = metrics_
            del oracle

        elif approach == 'random':
            random = RandomEvaluationBench(data_loader=dl, candidates_src=candidates_src,
                                           candidate_reduction_method=candidate_reduction_method)
            metrics_: dict = {k: v for k, v in random.run(dump_predictions=True, approach_nm=approach).items()
                              if k.startswith('metric_')}
            metrics[candidate_reduction_method]['RandomEvaluationBench'] = metrics_
            del random

        elif approach == 'wordtovec':
            wordtovec = WordEmbeddingBench(data_loader=dl, candidates_src=candidates_src, source='wordtovec',
                                           candidate_reduction_method=candidate_reduction_method)
            metrics_: dict = {k: v for k, v in wordtovec.run(dump_predictions=True, approach_nm=approach).items()
                              if k.startswith('metric_')}
            metrics[candidate_reduction_method]['WordEmbeddingBench-wordtovec'] = metrics_
            del wordtovec

        elif approach == 'glove':
            glove = WordEmbeddingBench(data_loader=dl, candidates_src=candidates_src, source='glove',
                                       candidate_reduction_method=candidate_reduction_method)
            metrics_: dict = {k: v for k, v in glove.run(dump_predictions=True, approach_nm=approach ).items()
                              if k.startswith('metric_')}
            metrics[candidate_reduction_method]['WordEmbeddingBench-glove'] = metrics_
            del glove

        elif approach.startswith('bert-nsp'):

            # -h: use heads
            use_heads: bool = approach.endswith('-h')
            approach_name: str = 'BertNextSentenceBench-heads' if use_heads else 'BertNextSentenceBench'

            bert_nsp = BertNextSentenceBench(data_loader=dl, candidates_src=candidates_src, model='bert-base-uncased',
                                             candidate_reduction_method=candidate_reduction_method,
                                             use_heads=use_heads, device=device)
            metrics_: dict = {k: v for k, v in bert_nsp.run(dump_predictions=True, approach_nm=approach).items()
                              if k.startswith('metric_')}

            metrics[candidate_reduction_method][approach_name] = metrics_
            del bert_nsp

        elif approach.startswith('bert-attn'):

            approach_name = 'BertSelfAttentionPairwiseBench' if '-pairwise' in approach else 'BertSelfAttentionBench'
            use_heads = approach.endswith('-h')

            cvdl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename)  # Make new cvdl
            common_params_cv['model'] = ['bert-base-uncased']
            common_params_cv['device'] = [device]
            common_params_cv['use_heads'] = [use_heads]

            metrics_: dict = cross_validate_self_attn_models(approach=approach_name, cvdl=cvdl,
                                                             params=common_params_cv, dumping_approach_name=approach)

            if use_heads:
                approach_name += '-heads'

            metrics[candidate_reduction_method][approach_name] = metrics_

        elif approach.startswith('spanbert-attn'):

            approach_name = 'BertSelfAttentionPairwiseBench' if '-pairwise' in approach else 'BertSelfAttentionBench'
            use_heads = approach.endswith('-h')

            cvdl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename)  # Make new cvdl
            common_params_cv['model'] = ['SpanBERT/spanbert-base-cased']
            common_params_cv['device'] = [device]
            common_params_cv['use_heads'] = [use_heads]

            metrics_: dict = cross_validate_self_attn_models(approach=approach_name, cvdl=cvdl,
                                                             params=common_params_cv, dumping_approach_name=approach)

            if use_heads:
                approach_name += '-heads'

            # Change the approach name to not overwrite the results
            approach_name = approach_name.replace('Bert', 'SpanBert')

            metrics[candidate_reduction_method][approach_name] = metrics_

        elif approach.startswith('roberta-attn'):

            approach_name = 'RoBertaAttentionBench'
            use_heads = approach.endswith('-h')

            cvdl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename)  # Make new cvdl
            common_params_cv['model'] = ['roberta-base']
            common_params_cv['device'] = [device]
            common_params_cv['use_heads'] = [use_heads]

            metrics_: dict = cross_validate_self_attn_models(approach=approach_name, cvdl=cvdl,
                                                             params=common_params_cv, dumping_approach_name=approach)

            if use_heads:
                approach_name += '-heads'

            metrics[candidate_reduction_method][approach_name] = metrics_

        elif approach.startswith('diablo-attn'):
            approach_name = 'DiabloGPTAttentionBench'
            use_heads = approach.endswith('-h')
            cvdl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename)  # Make new cvdl
            common_params_cv['model'] = ['microsoft/DialoGPT-medium']
            common_params_cv['device'] = [device]
            common_params_cv['use_heads'] = [use_heads]

            metrics_: dict = cross_validate_self_attn_models(approach=approach_name, cvdl=cvdl,
                                                             params=common_params_cv, dumping_approach_name=approach)

            if use_heads:
                approach_name += '-heads'

            metrics[candidate_reduction_method][approach_name] = metrics_
        # These ones are still not plugged in
        elif approach.startswith('knowbert-attn'):
            approach_name = 'KnowBertSelfAttentionBench'
            use_heads = approach.endswith('-h')
            cvdl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename)  # Make new cvdl
            common_params_cv['model'] = ['knowbert-base']
            common_params_cv['device'] = [device]
            common_params_cv['use_heads'] = [use_heads]

            metrics_: dict = cross_validate_self_attn_models(approach=approach_name, cvdl=cvdl,
                                                             params=common_params_cv, dumping_approach_name=approach)

            if use_heads:
                approach_name += '-heads'

            metrics[candidate_reduction_method][approach_name] = metrics_

        update_metrics(metrics, key=candidate_reduction_method, dataset_dir=dl.dir)
        pprint(metrics)


if __name__ == "__main__":

    run_all()
