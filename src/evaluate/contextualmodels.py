"""
    This file contains "EvaluationBench" things which can evaluate a given dataset for a given BERT based model.

    TODO: put SEP at start and end of each span (anaphor or antecedent)
    TODO: only one sentence thing
"""
import os
import copy
import click
import torch
import warnings
import numpy as np
import transformers
from tqdm import tqdm
from copy import deepcopy
import numpy.random as npr
from functools import partial
import torch.nn.functional as F
from abc import ABC, abstractmethod
from typing import List, Callable, Union, Tuple, Dict, Iterable, Optional
from pathlib import Path
from transformers import BertTokenizer, BertModel, RobertaTokenizer, RobertaModel, BertForNextSentencePrediction, \
    BertTokenizerFast, GPT2Tokenizer, GPT2Model

# Local imports
try:
    import _pathfix
except ImportError:
    from . import _pathfix
from dataloader import DataLoader
from config import LOCATIONS as LOC
from utils.nlp import to_toks, to_str
from utils.bridging import BridgingInstance
from evaluate.base import BaseEvaluationBench
from utils.exceptions import NoCandidateLeft

# Clamp the randomness
np.random.seed(42)


class SelfAttentionBench(BaseEvaluationBench, ABC):
    """
        Attempts to look at self attention between antecedents' and anaphor's spans and figure out the best one.

        Two challenges:
            1. attention attention is: num layers, num heads, seq len, seq len.
                - how do we choose a layer
                - how do we choose heads (do we aggregate all, choose one, etc)
            1. Instead of using spacy/ontonotes tokenization, we have to tokenize using BertTokenizer,
                which imparts byte-pair based tokens. So all our "spans" are rendered useless.
                And we can't use anaphor/antecedent text either since context matters.
                So we need to juxtapose spans from one tokenization scheme to the other.

    """

    def __init__(self, data_loader: Union[DataLoader, Iterable], model: str, max_len: int,
                 candidates_src: str, candidate_reduction_method: str, use_heads: bool = False,
                 metrics: Tuple[str] = ('acc', 'mrr', 'mr', 'ent_f1'), tqdm: bool = True,
                 pool_heads: str = 'max', pool_span: str = 'mean', pool_layers: str = '5',
                 proximity_trim: bool = False, trim_words_before: int = 50, trim_words_after: int = 20,
                 device: str = None):
        """
        :param data_loader: DataLoader object
        :param model: a string indicating which huggingface model to use
        :param max_len: an int suggesting the max nr of words we can consider in a document.
            This is required because the model has a max len (often 512) and our docs are often longer that this.
        :param pool_heads: a str denoting which fn to use for aggregating the attention heads
        :param pool_span: a str denoting which fn to use for getting a scalar score from a 2D attn matrix of spans
        :param pool_layers: a str denoting which fn to use for aggregating the attn from diff. layers of transformers
            pass a digit (in str) if you want to just "select" one attn layer instead of using a aggregate fn.
        :param proximity_trim: Whether to trim the document to the (anaphor_start - 50, anaphor_end + 20) word indices.
            If False, will use normal trimming procedure in trim_document. Note that if true, it is possible that the
            correct antecedent is trimmed out of the document.
        :param trim_words_before: if proximity_trim is true (the document is trimmed based on proximity to anaphor),
            this parameter will control how many words before the anaphor are in the trimmed document.
        :param trim_words_after: if proximity_trim is true (the document is trimmed based on proximity to anaphor),
            this parameter will control how many words after the anaphor are in the trimmed document.
        :param use_heads: bool flag which if turned true will ensure that every model only uses spans' head word
            to compare the attention and not pool over the entire span. As a consequence, we ignore `pool_heads`
        :param device: str device to use (None, cpu, cuda, cuda:0, ...)
        """
        super().__init__(data_loader, candidates_src=candidates_src, tqdm=tqdm, metrics=metrics,
                         candidate_reduction_method=candidate_reduction_method)
        self.proximity_trim = proximity_trim
        self.trim_words_before = trim_words_before
        self.trim_words_after = trim_words_after

        self.device = device

        # Sanity checks
        if model not in ['bert-base-uncased', 'roberta-base', "SpanBERT/spanbert-base-cased", "knowbert-base",
                         'microsoft/DialoGPT-medium']:
            raise NotImplementedError(f"No way to vectorize text using {model}")

        if pool_heads not in ['mean', 'max', 'best-mean', 'best-max']:
            raise NotImplementedError(f"No way to pool attention values across multiple heads using {pool_heads}")

        if pool_span not in ['mean', 'max']:
            raise NotImplementedError(f"No way to pool attention values across multiple words using {pool_span}")

        if pool_layers not in ['mean'] and not pool_layers.isdigit():
            raise NotImplementedError(f"No way to pool attention values across multiple layers using {pool_layers}")

        if self.device and self.__class__.__name__ not in ['BertSelfAttentionBench', 'RoBertaAttentionBench',
                                                           "KnowBertSelfAttentionBench", "DiabloGPTAttentionBench"]:
            warnings.warn(f"Using {self.device} with {self.__class__.__name__} is not implemented yet", UserWarning)

        # Store the raw arg string.
        self.pool_span_: str = pool_span
        self.pool_heads_: str = pool_heads
        self.pool_layers_: str = pool_layers
        self.use_heads_: bool = use_heads

        # Config some pooling function
        self.pool_span: Callable = {'mean': partial(self._pool_mean_, dim=[2, 3]),
                                    'max': partial(self._pool_max_, dim=[2, 3])}[pool_span]
        self.pool_heads: Callable = {'mean': partial(self._pool_mean_, dim=1),
                                     'max': partial(self._pool_max_, dim=1),
                                     'best-mean': self._select_heads_mean_,
                                     'best-max': self._select_heads_max_}[pool_heads]
        if pool_layers.isdigit():
            self.pool_layers: Callable = partial(self._pool_layers_select_, k=int(pool_layers))
        else:
            self.pool_layers: Callable = {'mean': partial(self._pool_mean_, dim=0)}[pool_layers]

        # Config some more stuff
        self.max_len_toks: int = max_len
        self.max_len_bpts: int = 512 if max_len > 300 else 128

        # Init the model and tokenizer
        self._get_tokenizer(model)
        self._get_model(model)
        self.model_: str = model

    @abstractmethod
    def _get_tokenizer(self, name):
        """ Instantiates and assigns tokenizer object to self.tokenizer
        [jrenner 31/3/21] I'm making this method a non-static method that assigns the tokenizer attribute within because
        the knowbert object needs a tokenizer and a "batcher" (their version of a tokenizer combined with candidate
        generator). Since this super class needs the tokenizer to work as a normal huggingface tokenizer (in the
        _convert_span_to_bpt_ function), in the knowbert subclass I instantiate a hf tokenizer for this purpose and a
        batcher for getting model inputs from text.
        """
        self.tokenizer = None  # to suppress annoying pycharm warnings
        ...

    @abstractmethod
    def _get_model(self, name):
        """ Instantiates and assigns model object to self.model"""
        ...

    @abstractmethod
    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        ...

    @abstractmethod
    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        ...

    def _summarise_(self, metrics: List[Union[float, np.ndarray]], n_cans: List[int]) -> dict:
        """ Return a nice summary of the metrics """
        return {**super()._summarise_(metrics, n_cans), 'model': self.model_, 'max_len': self.max_len_toks,
                'pool_layers': self.pool_layers_, 'pool_heads': self.pool_heads_, 'pool_span': self.pool_span_,
                'proximity_trim': self.proximity_trim}

    @staticmethod
    def _pool_layers_select_(k: int, attention: torch.Tensor) -> torch.Tensor:
        """ mat is (n_layers, n_heads, seq len, seq len). Return (1, n_heads, seq len, seq len) """
        return attention[k].unsqueeze(0)

    @staticmethod
    def _select_heads_mean_(attention: torch.Tensor) -> torch.Tensor:
        """ mat is (1, n_heads, n_words, n_words). Return (1, 1, n_words, n_words). Choose by mean of all values """
        weight_per_head = torch.mean(attention, dim=(2, 3))
        biggest_head = torch.argmax(weight_per_head)
        return attention[:, biggest_head].unsqueeze(1)

    @staticmethod
    def _select_heads_max_(attention: torch.Tensor) -> torch.Tensor:
        """ mat is (1, n_heads, n_words, n_words). Return (1, 1, n_words, n_words). Choose by max of all values """
        weight_per_head = torch.max(torch.max(attention, dim=-1)[0], dim=-1)[0]
        biggest_head = torch.argmax(weight_per_head)
        return attention[:, biggest_head].unsqueeze(1)

    @staticmethod
    def _pool_mean_(dim: Union[int, List[int]], attention: torch.Tensor) -> torch.Tensor:
        """ Input tensor can be pooled along one or multiple dims. """
        pooled = torch.mean(attention, dim=dim)
        if type(dim) is list:
            for _dim in dim:
                pooled = pooled.unsqueeze(_dim)
        else:
            pooled = pooled.unsqueeze(dim)
        return pooled

    @staticmethod
    def _pool_max_(dim: Union[int, List[int]], attention: torch.Tensor) -> torch.Tensor:
        if type(dim) is list:
            for _dim in dim:
                attention = torch.max(attention, dim=_dim)[0].unsqueeze(_dim)
        else:
            attention = torch.max(attention, dim=dim)[0].unsqueeze(dim)
        return attention

    def _all_elements_under_(self, instance: BridgingInstance, src: str) -> bool:
        """ check if the anaphor and all candidates_src lie under the max_len """
        last_relevant_sent_nr: int = instance.last_relevant_sent(src=src)
        last_relevant_sent_span: Tuple[int, int] = instance.get_sentence_span(last_relevant_sent_nr)
        return last_relevant_sent_span[1] < self.max_len_toks

    def _all_elements_within_(self, instance: BridgingInstance, src: str) -> bool:
        """ check if the anaphor and all candidates_src lie under the max_len from each other (not from the start)"""
        last_relevant_sent_nr: int = instance.last_relevant_sent(src=src)
        last_relevant_sent_span: Tuple[int, int] = instance.get_sentence_span(last_relevant_sent_nr)
        first_relevant_sent_nr: int = instance.first_relevant_sent(src=src)
        first_relevant_sent_span: Tuple[int, int] = instance.get_sentence_span(first_relevant_sent_nr)

        # Get the nr. of words between the first word of first relevant sent and last word of last relevant sent
        diff_len = last_relevant_sent_span[1] - first_relevant_sent_span[0]

        # Is it more than the designated max_len
        return diff_len < self.max_len_toks

    def _create_offset_dict_(self, instance: BridgingInstance, sentences: List[List[int]]) -> Dict[int, int]:
        """
            Create a dict where key: current position of a token. Value: the offset (to be subtracted from it).
            E.g. { 30: 30, 45: 5} ->
                every token id including and above 30 needs to have 30 subtracted from it
                every token id including and above 45 needs to have ANOTHER 5 subtracted from it.
        """
        sentences = self._merge_sent_spans_(sentences)

        # Create a length aggregate list corresponding to instance.sentence_lengths_
        n_words_before_this_sent: List[int] = [sum(instance.sentence_lengths_[:sent_n])
                                               for sent_n in range(len(instance.sentence_lengths_))]

        offset: Dict[int, int] = {n_words_before_this_sent[sentences[0][0]]: n_words_before_this_sent[sentences[0][0]]}
        for i in range(1, len(sentences)):
            gap_start = sentences[i - 1][1] + 1
            gap_end = sentences[i][0]

            gap_len = n_words_before_this_sent[gap_end] - n_words_before_this_sent[gap_start]
            offset[n_words_before_this_sent[gap_end]] = gap_len

        return offset

    @staticmethod
    def _sq_norm_(query: List[int], center: List[int]) -> int:
        """ if query span left of center -> diff b/w query end, center start. if right -> query start, center end """
        if query[1] < center[0]:
            return (query[1] - center[0]) ** 2
        else:
            return (center[1] - query[0]) ** 2

    def _trim_document_with_omissions_(self, instance: BridgingInstance, candidate_span_ids: List[List[int]]) \
            -> (List[List[int]], List[List[int]]):
        """
            Invoked in case 5 in self.trim_document()
            We need to drop some candidates_src, in order to make the doc fit in transformers' max length.
            We also return a list of candidate_span_ids where the ones we omit are replaced with [-1, -1]

            # Algorithm
                1. sort all candidates with their distance to the anaphor.
                1. start adding sentences corresponding to the candidates,
                    starting from the closest to the anaphor to further away
                1. stop when either we've covered all candidates, or if the max length is reached.
                1. sorted_candidates[i:] is the list of omitted candidates.
                1. convert them all to [-1, -1]

            Returns: sentence_spans, modified candidate antecedent ids
        """
        anaphor: List[int] = instance.anaphor
        sorted_candidates: List[List[int]] = sorted([list(x) for x in candidate_span_ids],
                                                    key=partial(self._sq_norm_, center=anaphor))

        # Init the elem spans list
        sent_spans: List[List[int]] = [list(instance.get_sentence(anaphor))]
        i: int = -1

        # candidates_src are sorted from closest to furthest
        for i, candidate in enumerate(sorted_candidates):

            # Get sentences corresponding to this candidate span
            cand_sent_span: List[int] = list(instance.get_sentence(candidate))

            # Try merging
            sent_spans_temp: List[List[int]] = sent_spans + [cand_sent_span]

            # Merge the spans once so length estimation is more believable.
            sent_spans_temp: List[List[int]] = self._merge_sent_spans_(sent_spans_temp)

            # Estimate Length of this temp obj
            if self._estimate_length_(instance, sent_spans_temp) > self.max_len_toks:
                # Too long. Abort
                break
            else:
                # We're good. Let's commit this.
                sent_spans = sent_spans_temp

        # Now, i is the sent at which we break away.

        assert i < len(sorted_candidates), "We managed to fit all candidates_src. This should not be possible in case 5"

        if i == 0:
            # We are not letting any instance go.
            # Return an empty list and hope that the code below takes care of itself. i.e., fail silently.
            # warnings.warn(f"No candidates were selected when trimming the instance - {instance.anaphor_id}.")
            raise NoCandidateLeft(f"No candidates were selected when trimming the instance - {instance.anaphor_id}.")

        candidates = deepcopy([list(x) for x in candidate_span_ids])

        for leftover_index in range(i, len(sorted_candidates)):
            candidate_id = candidates.index(sorted_candidates[leftover_index])
            candidates[candidate_id] = [-1, -1]

        return sent_spans, candidates

    def trim_document_proximity(self, instance: BridgingInstance, candidate_span_ids: List[List[int]], src: str) \
            -> (List[List[int]], Dict[int, int], int, List[List[int]]):
        """
            # Logic
            We take the sentences containing the 50 words to the anaphor.

            Case 1: doc is smaller than 50 words
                - use the entire doc
            Case 2: we take the sentences containing 50 words closest to the anaphor

            After this,
                1. note down the offset of spans

            # Returns TODO: alter
                1. list of sentence spans e.g. [[1, 3], [6, 7]] indicating sent 1, 2, and 6
                2. int indicating which of the four cases were chosen (feel free to ignore)
                3. offset. This is used in case 3. There, we ignore first few sentences.
                    All anaphor, antecedent spans need to shift accordingly as well.
                4. Returns either an empty list, or a trimmed list of candidates (in case we're ignoring some; case 5)

                The start and end sent can be put on the doc like `d.document[start_sent: end_sent] to get a subset.
        """
        anaphor_start, anaphor_end = instance.anaphor
        start_index = max(anaphor_start - self.trim_words_before, 0)
        end_index = min(anaphor_end + self.trim_words_after, len(to_toks(instance.document)))
        start_sent, end_sent = instance.get_sentence([start_index, end_index])
        end_sent += 1
        sent_spans: List[List[int]] = [[start_sent, end_sent]]
        offset: Dict[int, int] = self._create_offset_dict_(instance=instance, sentences=sent_spans)
        candidates: List[List[int]] = []
        for start, end in candidate_span_ids:
            cand_sent_span = instance.get_sentence([start, end])
            if start_sent <= cand_sent_span[0] and cand_sent_span[1] < end_sent:
                candidates.append([start, end])
            else:
                candidates.append([-1, -1])
        return sent_spans, offset, 3, candidates

    def trim_document(self, instance: BridgingInstance, candidate_span_ids: List[List[int]], src: str,
                      ) -> (List[List[int]], Dict[int, int], int, List[List[int]]):
        """
            # Logic
            Since the document may contain more words than can fit inside our contextual model,
            we use the following logic to select the sentences.

            Case 1: doc is smaller than 512 words
                - use the entire doc
                | ************ |
            Case 2: the anaphor and all antecedents are within the **first** 512 words.
                - use all sentences from start upto the last relevant ones
                | ********____ |
            Case 3: the candidate and the anaphor lie within 512 words **of each other**
                - gather all sentences from the start of the first antecedent candidate to the last candidate
                | __*****____ |
            Case 4: there are more than 512 words between the candidate/anaphor spans.
                - choose multiple discontinuous spans
                | _**__*__**** |
            Case 5: we can't even include all sentences with candidate/anaphor spans as the sentences are too big
                - drop antecedents furthest away from the anaphor, till the maximum seq. is reached after case 4.

            After this,
                1. inflate the spans (to not only include all assets but also other sentences as much as len permits)
                1. note down the offset of spans

            # Returns TODO: alter
                1. list of sentence spans e.g. [[1, 3], [6, 7]] indicating sent 1, 2, and 6
                2. int indicating which of the four cases were chosen (feel free to ignore)
                3. offset. This is used in case 3. There, we ignore first few sentences.
                    All anaphor, antecedent spans need to shift accordingly as well.
                4. Returns either an empty list, or a trimmed list of candidates (in case we're ignoring some; case 5)

                The start and end sent can be put on the doc like `d.document[start_sent: end_sent] to get a subset.
        """
        candidates: List[List[int]] = []

        if len(to_toks(instance.document)) < self.max_len_toks:
            # The document is under the max len (most likely). Tokenize it all
            # Consider all sentences in the doc
            # First sent: 0, Last Sent: -1
            sent_spans: List[List[int]] = [[0, len(instance.document) - 1]]
            case_id: int = 1

        elif self._all_elements_under_(instance, src=src):
            # All the candidate spans and the anaphor lies under max_len (from the start)
            # Consider all sentences upto the max_len
            # First sent: 0, Last Sent: n
            sent_spans: List[List[int]] = [[0, instance.last_relevant_sent(src)]]
            case_id: int = 2

        elif self._all_elements_within_(instance, src=src):
            # All the candidate spans and the anaphor lies between max_len of each other (not from the start)
            # Consider all sentences beginning from the the one with the first antecedent,
            # #### ending with the one which has the last antecedent/the anaphor (or the max length)
            # First sent: n, Last Sent: m
            case_id: int = 3

            sent_spans: List[List[int]] = [[instance.first_relevant_sent(src), instance.last_relevant_sent(src)]]

        else:
            # The artefacts itself are more than max_len apart from each other.
            # Pick discontinuous spans. Try to bridge them/expand each discontinuous spans using some heuristics.
            case_id: int = 4

            # Collect all elements in this instance
            elem_spans: List[List[int]] = [instance.anaphor] + [list(x) for x in candidate_span_ids]

            # Sort this in ascending order
            elem_spans = sorted(elem_spans, key=lambda x: x[0])

            # Get sentences corresponding to each
            sent_spans: List[List[int]] = [list(instance.get_sentence(element)) for element in elem_spans]

            # Merge the spans once so length estimation is more believable.
            sent_spans: List[List[int]] = self._merge_sent_spans_(sent_spans)

        # If this is already greater than max_len, we need to start dropping some candidates_src,
        # #### making our task easier.
        if case_id == 4 and self._estimate_length_(instance, sent_spans) > self.max_len_toks:
            case_id = 5
            sent_spans, candidates = self._trim_document_with_omissions_(instance=instance,
                                                                         candidate_span_ids=candidate_span_ids)

        """
            1. Sort the spans in ascending order
            1. If a span starts, ends at the same position, offset the end with one.
                e.g. [[1, 4], [6,6]] -> [[1,4], [6, 7]]
            1. Add more sentences till the length exceeds self.max_len_toks
        """
        sent_spans: List[List[int]] = sorted(sent_spans, key=lambda x: x[0])
        # for i, span in enumerate(sent_spans):
        # if span[0] == span[1]:
        #     sent_spans[i][1] += 1
        sent_spans = self._add_more_sentences_(instance=instance, spans=sent_spans)

        """
            Since we're splitting the sentence, we also need to split the spans accordingly.
            In case 3: just have to increase all by a particular constant.
            In case 4: it depends. Just see the code or email me. Easier to explain that way. Sorry.
        """
        # offset: Dict[int, int] = {0: sum(instance.sentence_lengths_[:sent_spans[0][0]])}
        offset: Dict[int, int] = self._create_offset_dict_(instance=instance, sentences=sent_spans)

        return sent_spans, offset, case_id, candidates

    @staticmethod
    def _slice_attn_(attn: torch.Tensor, x: Tuple[int, int], y: Tuple[int, int]) -> torch.Tensor:
        """ Expect a (n_layers, n_heads, n, n) attention, Return a (n_layers, n_heads, nx, ny) attention """

        return attn[:, :, x[0]:x[1], y[0]:y[1]]

    @staticmethod
    def _merge_sent_spans_(spans: List[List[int]]) -> List[List[int]]:
        """
            Given one or more sentence spans, return their contiguous form.
            Examples:
                [[1, 2], [2, 3], [4, 5], [5, 9], [11, 13]]
                    to [[1, 9], [11, 13]]
                [[1, 3]]
                    to [[1, 3]]
                [[1, 2], [2, 3], [4, 5],[4, 6]]
                    to [[1, 6]]
                [[1, 2], [2, 2], [4, 5],[4, 6]]
                    to [[1, 2], [4, 6]]
                [[1, 2], [2, 5], [4, 5],[4, 6]]
                    to  [[1, 6]]
                [[1, 2], [2, 5], [6, 11],[7, 12]]
                    to [[1, 12]]
                [[1, 2], [2, 5], [6, 11],[7, 9]]
                    to  [[1, 11]]
        """

        # Edge case: only one sent
        if len(spans) == 1:
            return spans

        # Sort them in ascending order
        spans = sorted(spans, key=lambda x: x[0])

        # Declare a list of all merged spans, init with first sent span
        spans = deepcopy(spans)
        merged: List[List[int]] = [spans[0]][:]

        for span in spans[1:]:
            if merged[-1][1] + 1 >= span[0]:
                merged[-1][1] = max(span[1], merged[-1][1])
            else:
                merged.append(span)

        return merged

    @staticmethod
    def _estimate_length_(instance: BridgingInstance, spans: List[List[int]]):
        """ Expect something like[[1, 3], [4, 6]]. Count the length of sentences 1,2,3,4,5,6 and sum """
        sum_len: int = 0
        for span in spans:
            sum_len += sum(instance.sentence_lengths_[span[0]:span[1] + 1])
        return sum_len

    @staticmethod
    def _possible_adjacent_sentences_(spans: List[List[int]], num_sentences: int):
        """ Get a sent left, right of every span i.e., [[1, 4], [6, 7]] -> [[0, 1], [4, 5], [5, 6], [7, 8]] """
        adja_sents: List[List[int]] = []  # Pun intended :]

        for span in spans:
            if span[0] > 0:
                adja_sents.append([span[0] - 1, span[0] - 1])
            if span[1] < num_sentences - 1:
                adja_sents.append([span[1] + 1, span[1] + 1])

        return adja_sents

    def _add_more_sentences_(self, instance: BridgingInstance, spans: List[List[int]]) -> List[List[int]]:
        """
            Input spans: List of sentence spans like [[1, 4], [6, 7]]
                This implies that sentences 1,2,3 and 6 are going to be considered.
                Assume they're already merged and don't already exceed the limit

            Try to add more sentences into the mix till we are at the max_len_toks limit.

            Algorithm:
                - list down all possible permutations: [[0, 1], [5, 6], [5, 6], [6, 7]]
                    - for each span, add one sent left, one sent right (mindful of edge cases)
                - choose one at random (pop it out).
                - try adding it to the list. Merge again. Estimate length again. If it fits
        """

        while True:

            # Measure num of tokens already
            cur_len_toks: int = self._estimate_length_(instance=instance, spans=spans)

            # Get all possible permutations
            adjacent_sentences = self._possible_adjacent_sentences_(spans, num_sentences=len(instance.document))

            did_add_sent: bool = False
            while adjacent_sentences:
                adjacent_sentence: List[int] = adjacent_sentences.pop(npr.randint(0, len(adjacent_sentences)))

                if len(instance.document[adjacent_sentence[0]]) + cur_len_toks <= self.max_len_toks:
                    # Use this sentence
                    did_add_sent = True
                    spans = self._merge_sent_spans_(spans + [adjacent_sentence])

                    # Re do the whole thing, don't add more
                    break
                else:
                    # Try with a different adjacent sentence
                    continue

            # ####
            # Either we added a sentence, or we did not.
            #     if yes: re-do the loop
            #     if no: no more permutations are possible, break
            # ####
            if not did_add_sent:
                break

        return spans

    def _convert_span_to_bpt_(self, doc: List[List[str]], span: List[int], offset: Dict[int, int]) -> (int, int):
        """ Get the index of the span inside the doc. Used to find index of antecedent inside doc (hf tokenized) """

        # First, remove the offset from the span, and tokenize the doc
        doc: List[str] = to_toks(doc)
        span = self.do_offset_(span, offset)

        assert span[1] <= len(doc), "The offset was not properly done."

        txt_doc_upto_span_end = ' '.join(doc[:span[1]])
        tok_doc_upto_span_end = self.tokenizer(txt_doc_upto_span_end, return_tensors='pt', add_special_tokens=True,
                                               max_length=self.max_len_bpts, truncation=True)
        txt_span = ' '.join(doc[span[0]:span[1]])
        """
            EDGE CASE bug fix:
            RoBERTa tokenizes things in a weird way. So the tokenization of
                ```
                the neighborhood: [   133, 3757]
                neighborhood: [    858,  8774, 12514,  8489]
                ```
            So we change 'neighborhood' to ' neighborhood'
            
            Thus, when we're working with Roberta, and when span[1] - span[0] == 1, we add a space before the text span
        """
        if 'roberta' in self.__class__.__name__.lower() and span[1] - span[0] == 1 and ' ' not in txt_span:
            txt_span = ' ' + txt_span
        elif 'diablo' in self.__class__.__name__.lower():
            txt_span = ' ' + txt_span
        tok_span = self.tokenizer(txt_span, return_tensors='pt', add_special_tokens=False,
                                  max_length=self.max_len_bpts, truncation=True)

        end: int = tok_doc_upto_span_end['input_ids'].shape[1] - 1
        if 'diablo' in self.__class__.__name__.lower():
            end += 1    # to offset gp2 tokenizer activity
        start: int = end - tok_span['input_ids'].shape[1]

        return start, end

    def score(self, attention: torch.Tensor) -> torch.float:
        """
            The actual selection happens here.

            Input:
                A torch tensor of (n_layers, n_heads, seq_len anaphor, seq_len antecedent)
            Output:
                One scalar value representing, by some metric the compatibility between anaphor and antecedent
        """

        if (attention == -1).all():
            return attention

        # Pool across layers: -> torch.Tensor((1, n_heads, seq_len_anaphor, seq_len_antecedent))
        attention = self.pool_layers(attention=attention)

        # Pool across heads: -> torch.Tensor((1, 1, seq_len_anaphor, seq_len_antecedent))
        attention = self.pool_heads(attention=attention)

        # Pool across spans: -> torch.Tensor((1, 1, 1, 1))
        attention = self.pool_span(attention=attention)

        return attention.squeeze()

    @staticmethod
    def impose_sentence_span(document: List[List[str]], spans: List[List[int]]) -> List[List[str]]:
        """ Doc is list of tokenized sentences. spans is [[2, 5], [6, 7]] i.e. concat sent 2,3,4 and 6 and return """
        trimmed: List[List[str]] = []
        for span in spans:
            trimmed += document[span[0]: span[1] + 1]
        return trimmed

    @staticmethod
    def do_offset_(span: List[int], offset: Dict[int, int]) -> List[int]:
        """ [2, 30]; {1:1, 5:10; 29: 14} -> [2-1; 30-1-10-14]. i.e., sub val from span if span >= key """
        span = deepcopy(span)

        offset_k = [k for k in offset.keys() if k <= span[0]]
        offset_v = sum(offset[k] for k in offset_k)
        span[0] -= offset_v
        span[1] -= offset_v

        # for span_pos in range(len(span)):
        #     offset_k = [k for k in offset.keys() if k <= span[span_pos]]
        #     offset_v = sum(offset[k] for k in offset_k)
        #     span[span_pos] -= offset_v

        return span

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            1. Intelligently select a document subset (see comments on self._doc_sentence_selection_)
            1. Tokenize the document subset based on self.tokenizer.
            1. Pass the tokenized text through the model
            1. Get the attention matrix
            1. Figure out spans in tokenized text of the anaphor and each candidate antecedent
            1. Get a scalar score corresponding to the attention mat for each candidate antecedent
            1. Return a 1D numpy array of these scalar scores
        """

        # Selecting the spans. If the use_heads flag is true,
        # #### then we only use the anaphor and candidate heads span.
        # #### Otherwise, we use the entire anaphor and full candidate span
        _anaphor_span = datum.anaphor_h if self.use_heads_ else datum.anaphor
        _candidate_spans = candidate_span_h_ids if self.use_heads_ else candidate_span_ids

        if self.proximity_trim:
            sentence_spans, offset_dict, case_id, candidates = self.trim_document_proximity(datum,
                                                                                            _candidate_spans, src=src)
        else:
            sentence_spans, offset_dict, case_id, candidates = self.trim_document(datum, _candidate_spans, src=src)

        if not candidates:
            candidates: List[List[int]] = [list(x) for x in _candidate_spans]

        # Use the spans to select a doc subset, and flatten it to str
        raw_doc: List[List[str]] = self.impose_sentence_span(datum.document, sentence_spans)
        txt_doc: str = to_str(raw_doc)

        # Tokenize it
        tok_doc = self._get_model_inputs(txt_doc)

        # Get the span of the anaphor and other candidates_src
        span_ana: Tuple[int, int] = self._convert_span_to_bpt_(raw_doc, _anaphor_span, offset_dict)
        # span_ants will have empty slices in places where we're omitting candidates_src (due to trimming)
        span_ants: List[Tuple[int, int]] = [self._convert_span_to_bpt_(raw_doc, list(candidate), offset_dict)
                                            if not candidate[0] == candidate[1] == -1 else [-1, -1]
                                            for candidate in candidates]

        """ 
            Sanity checks
        """
        #  TODO: reimplement these sanity checks for subclasses
        # bp_ana = self.tokenizer.decode(tok_doc['input_ids'][0, span_ana[0]:span_ana[1]])
        # assert bp_ana.replace(' ', '') == ''.join(datum.anaphor_).lower()
        assert span_ana[0] != span_ana[1]
        for span_ant in span_ants:
            assert span_ant[0] != span_ant[1] or span_ant[0] == span_ant[1] == -1
        for span_org, span_mod in zip(_candidate_spans, candidates):
            assert span_mod == [-1, -1] or span_org == span_mod, "Spans do not match up after trimming doc."

        """
            We've probably done it correctly. 
            Now let's try to compute the relevant attention scores.
        """
        # Compute the attention, i.e., pass the tok_doc through the model, get attn weights
        attention: Tuple[torch.Tensor] = self._get_attention_weights(tok_doc)

        # From n_layers tensors of (1, n_heads, n_words, n_words). To 1 tensor of (n_layers, n_head, n_words, n_words)
        attention: torch.Tensor = torch.cat(attention, dim=0)

        # For each anaphor, candidate antecedent pair, slice the attention
        attention_slices = [self._slice_attn_(attention, span_ana, span_ant)
                            if not span_ant[0] == span_ant[1] == -1 else torch.tensor(-1, dtype=torch.float)
                            for span_ant in span_ants]

        # Convert attn to scalar values by pooling and choose the best one (highest).
        attention_scalars = torch.Tensor([self.score(attention_slice) for attention_slice in attention_slices])

        # Choose the highest value
        return attention_scalars.cpu().detach().numpy()

    def dump_attention_matrix(self, datum: BridgingInstance, path: Optional[Union[str, Path]] = None,
                              compress: bool = True):
        _anaphor_span = datum.anaphor_h if self.use_heads_ else datum.anaphor
        _candidate_spans = getattr(datum, f'candidate_{self.candidates_src}_h_ids') if \
            self.use_heads_ else getattr(datum, f'candidate_{self.candidates_src}_ids')
        if self.proximity_trim:
            sentence_spans, offset_dict, case_id, candidates = self.trim_document_proximity(datum,
                                                                                            _candidate_spans,
                                                                                            src=self.candidates_src)
        else:
            sentence_spans, offset_dict, case_id, candidates = self.trim_document(datum, _candidate_spans,
                                                                                  src=self.candidates_src)
        raw_doc: List[List[str]] = self.impose_sentence_span(datum.document, sentence_spans)
        txt_doc: str = to_str(raw_doc)
        tok_doc = self._get_model_inputs(txt_doc)
        attention = self._get_attention_weights(tok_doc)
        if path is None:
            dir_path = LOC.root / Path(f"data/features/{self.data_loader.dataset}")
            if self.data_loader.dataset_domain is not None:
                dir_path = dir_path / Path(self.data_loader.dataset_domain)
                if self.data_loader.dataset_domain_filename is not None:
                    dir_path = dir_path / Path(self.data_loader.dataset_domain_filename)
            dir_path = dir_path / Path(f"{self.__class__.__name__.lower()}_{self.candidates_src}_"
                                       f"{self.candidate_reduction_method_}")
            dir_path.mkdir(parents=True, exist_ok=True)
            anaphor_id = str(datum.anaphor_id).split("/")[-1]
            path = dir_path / Path(anaphor_id)
        attention = [ten.half() for ten in attention]
        if compress:
            np.savez_compressed(path, attention)
        else:
            np.savez(path, attention)


class NextSentenceBench(BaseEvaluationBench, ABC):
    """
        We concatenate the text span of the anaphor with the antecedent
            and hope that the next sentence prediction scores are indicative of the correct span.
    """

    def __init__(self, data_loader: Union[DataLoader, Iterable], model: str,
                 candidates_src: str, candidate_reduction_method: str,
                 metrics: Tuple[str] = ('acc', 'mrr', 'mr', 'ent_f1'),
                 use_heads: bool = False, tqdm: bool = True, device: str = None):
        """
        :param data_loader: DataLoader object
        :param model: a string indicating which huggingface model to use
        """
        super().__init__(data_loader, candidates_src=candidates_src, tqdm=tqdm, metrics=metrics,
                         candidate_reduction_method=candidate_reduction_method)

        self.max_len_bpts: int = 512
        self.use_heads_: bool = use_heads
        self.model_: str = model
        self.device: str = device

        # Init the model and tokenizer
        self._get_tokenizer(model)
        self._get_model(model)

    @abstractmethod
    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        ...

    @abstractmethod
    def _get_model(self, name):
        """ Instantiates and returns model object """
        ...

    @abstractmethod
    def _get_model_inputs(self, txt_a: str, txt_b: str) -> transformers.tokenization_utils_base.BatchEncoding:
        """ Get model inputs from tokenizer """
        ...

    @abstractmethod
    def _get_next_sentence_scores(self, txt_a: str, txt_b: str) -> float:
        """ Get attention weights from model for input document"""
        ...

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            Get the NSP prediction (class ID 0 from the softmax) for anaphor phrase and antecedent phrase.
            Simply store them in an array and return it back.
        """

        # Selecting the spans. If the use_heads flag is true,
        # #### then we only use the anaphor and candidate heads span.
        # #### Otherwise, we use the entire anaphor and full candidate span
        _anaphor_txt: List[str] = datum.anaphor_h_ if self.use_heads_ else datum.anaphor_
        _candidate_txt: List[List[str]] = candidate_span_h_txt if self.use_heads_ else candidate_span_txt

        # Declare an empty np array for scores
        scores = np.zeros(len(_candidate_txt))

        for i, candidate in enumerate(_candidate_txt):
            scores[i] = self._get_next_sentence_scores(' '.join(candidate), ' '.join(_anaphor_txt))

        return scores


# noinspection PyShadowingNames
class BertSelfAttentionBench(SelfAttentionBench):
    """
        Attempts to look at self attention between antecedents' and anaphor's spans and figure out the best one.

        Two challenges:
            1. attention attention is: num layers, num heads, seq len, seq len.
                - how do we choose a layer
                - how do we choose heads (do we aggregate all, choose one, etc)
            1. Instead of using spacy/ontonotes tokenization, we have to tokenize using BertTokenizer,
                which imparts byte-pair based tokens. So all our "spans" are rendered useless.
                And we can't use anaphor/antecedent text either since context matters.
                So we need to juxtapose spans from one tokenization scheme to the other.

    """

    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        if name == "SpanBERT/spanbert-base-cased":
            name = "bert-base-cased"
        self.tokenizer = BertTokenizer.from_pretrained(name, do_lower_case=True if "uncased" in name else False)

    def _get_model(self, name):
        """ Instantiates and returns model object """
        self.model = BertModel.from_pretrained(name, output_attentions=True)
        if self.device:
            self.model = self.model.to(self.device)

    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        model_inputs = self.tokenizer(txt, return_tensors='pt', add_special_tokens=True, max_length=self.max_len_bpts,
                                      truncation=True)
        if self.device:
            model_inputs = model_inputs.to(self.device)
        return model_inputs

    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        return self.model(tokenized_document['input_ids'], tokenized_document['attention_mask'])[-1]


class RoBertaAttentionBench(SelfAttentionBench):

    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        self.tokenizer = RobertaTokenizer.from_pretrained(name, do_lower_case=True)

    def _get_model(self, name):
        """ Instantiates and returns model object """
        self.model = RobertaModel.from_pretrained(name, output_attentions=True)
        if self.device:
            self.model = self.model.to(self.device)

    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        model_inputs = self.tokenizer(txt, return_tensors='pt', add_special_tokens=True, max_length=self.max_len_bpts,
                                      truncation=True)
        if self.device:
            model_inputs = model_inputs.to(self.device)
        return model_inputs

    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        return self.model(tokenized_document['input_ids'], tokenized_document['attention_mask'])[-1]


class DiabloGPTAttentionBench(SelfAttentionBench):

    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        self.tokenizer = GPT2Tokenizer.from_pretrained(name)

    def _get_model(self, name):
        """ Instantiates and returns model object """
        self.model = GPT2Model.from_pretrained(name, output_attentions=True)
        if self.device:
            self.model = self.model.to(self.device)

    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        model_inputs = self.tokenizer(txt, return_tensors='pt', add_special_tokens=True, max_length=self.max_len_bpts,
                                      truncation=True)
        if self.device:
            model_inputs = model_inputs.to(self.device)
        return model_inputs

    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        return self.model(tokenized_document['input_ids'], attention_mask=tokenized_document['attention_mask'],
                          return_dict=True)["attentions"]


class BertSelfAttentionPairwiseBench(SelfAttentionBench):
    """
        Similar to BertSelfAttentionBench but here, when calculating the attention matrix,
            we only consider the sentences of anaphor and antecedent.

        So we calculate multiple attn matrices and then choose the best one
    """

    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        self.tokenizer = BertTokenizer.from_pretrained(name, do_lower_case=True)

    def _get_model(self, name):
        """ Instantiates and returns model object """
        self.model = BertModel.from_pretrained(name, output_attentions=True)
        if self.device:
            self.model = self.model.to(self.device)

    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        model_inputs = self.tokenizer(txt, return_tensors='pt', add_special_tokens=True, max_length=self.max_len_bpts,
                                      truncation=True)
        if self.device:
            model_inputs = model_inputs.to(self.device)
        return model_inputs

    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        return self.model(tokenized_document['input_ids'], tokenized_document['attention_mask'])[-1]

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            For every candidate, get its sentence.
            Create a list of unique sentences, keeping score of which sentence corresponds to which candidate.
            Calculate the attention matrix of each sentence+anaphor pair. (experiment with NLI token types and not)
            Then, calculate "compatibility scores" of each span

            We assume every sentence pair fits inside bert. if not, then we would just throw an error.
        """

        # Selecting the spans. If the use_heads flag is true,
        # #### then we only use the anaphor and candidate heads span.
        # #### Otherwise, we use the entire anaphor and full candidate span
        _anaphor_span = datum.anaphor_h if self.use_heads_ else datum.anaphor
        _candidate_spans = candidate_span_h_ids if self.use_heads_ else candidate_span_ids

        # Get the sentence for the anaphor
        anaphor_sentence_span = list(datum.get_sentence(_anaphor_span))

        # Declare a scores array
        scores = np.zeros(len(_candidate_spans))

        # Group the candidates with their sentences
        candidates_grouped_by_sentences: dict = {}
        for _candidate in _candidate_spans:
            candidates_grouped_by_sentences.setdefault(datum.get_sentence(_candidate), []).append(_candidate)

        # For every sentence group
        for candidate_sentence_span, candidate_spans_this_sentence in candidates_grouped_by_sentences.items():

            # Append anaphor sentence to sentence spans
            relevant_sentences = [list(candidate_sentence_span), anaphor_sentence_span]

            # Create the offset dict
            offset_dict = self._create_offset_dict_(datum, relevant_sentences)

            # Create a snipped off raw document
            raw_document: List[List[str]] = self.impose_sentence_span(datum.document, relevant_sentences)
            txt_document: str = to_str(raw_document)

            # Get the span of the anaphor and other candidates_src
            span_ana = self._convert_span_to_bpt_(raw_document, _anaphor_span, offset_dict)
            # span_ants will have empty slices in places where we're omitting candidates_src (due to trimming)
            span_cans = [self._convert_span_to_bpt_(raw_document, list(candidate), offset_dict)
                         if not candidate[0] == candidate[1] == -1 else [-1, -1]
                         for candidate in candidate_spans_this_sentence]

            # Tokenize the document
            tok_doc = self._get_model_inputs(txt_document)

            # Compute the attention, i.e., pass the tok_doc through the model, get attn weights
            attention: Tuple[torch.Tensor] = self._get_attention_weights(tok_doc)

            # n_layers tensors of (1, n_heads, n_words, n_words) to 1 tensor of (n_layers, n_head, n_words, n_words)
            attention: torch.Tensor = torch.cat(attention, dim=0)

            # For each anaphor, candidate antecedent pair, slice the attention
            attention_slices = [self._slice_attn_(attention, span_ana, span_can)
                                if not span_can[0] == span_can[1] == -1 else torch.tensor(-1, dtype=torch.float)
                                for span_can in span_cans]

            # Convert attn to scalar values by predefined pooling strategies.
            attention_scalars = torch.Tensor([self.score(attention_slice) for attention_slice in attention_slices])

            # Now we have to put these scalars back to the global scores list
            for i, _can_span in enumerate(candidate_spans_this_sentence):
                actual_index: int = _candidate_spans.index(_can_span)
                scores[actual_index] = attention_scalars[i]

        return scores


# noinspection PyShadowingNames
class KnowBertSelfAttentionBench(SelfAttentionBench):
    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        # [jrenner 31.3.21] I put these imports here so that the Bert code wouldn't break if these libraries aren't
        # installed.
        from kb.knowbert_utils import KnowBertBatchifier
        self.tokenizer = BertTokenizerFast.from_pretrained('bert-base-uncased', do_lower_case=True)
        self.batcher = KnowBertBatchifier(str(LOC.knowbert), batch_size=1)

    def _get_model(self, name):
        """ Instantiates and returns model object """
        from kb.include_all import ModelArchiveFromParams
        from allennlp.common import Params
        params = Params({"archive_file": str(LOC.knowbert)})
        model = ModelArchiveFromParams.from_params(params=params)
        model.eval()
        if self.device:
            model = model.to(self.device)
        self.model = model

    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        batch = next(self.batcher.iter_batches([txt], verbose=False))
        if self.device:
            batch["tokens"]["tokens"] = batch["tokens"]["tokens"].to(self.device)
            batch["segment_ids"] = batch["segment_ids"].to(self.device)
            batch["candidates"]["wordnet"]["candidate_entity_priors"] = batch["candidates"]["wordnet"][
                "candidate_entity_priors"].to(self.device)
            batch["candidates"]["wordnet"]["candidate_entities"]["ids"] = \
            batch["candidates"]["wordnet"]["candidate_entities"]["ids"].to(self.device)
            batch["candidates"]["wordnet"]["candidate_spans"] = batch["candidates"]["wordnet"]["candidate_spans"].to(
                self.device)
            batch["candidates"]["wordnet"]["candidate_segment_ids"] = batch["candidates"]["wordnet"][
                "candidate_segment_ids"].to(self.device)
        return batch

    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        return self.model(**tokenized_document)["attentions"]

    @staticmethod
    def overlap_span(span1, span2):
        # return true if any of span1 is in span2 and all of span2 is in span1
        span1 = set(list(range(span1[0], span1[1] + 1)))
        span2 = set(list(range(span2[0], span2[1] + 1)))
        intersection = span1.intersection(span2)
        return len(intersection) > 0 and len(span2 - intersection) == 0

    @staticmethod
    def get_bpt_span(word_span, bp_offsets):
        # make work for multi word span
        seen_words = 0
        num_words = word_span[1] - word_span[0]
        bpt_span = []
        for j in range(len(bp_offsets)):
            if j == 0:  # skip special token
                continue
            if seen_words == word_span[0] and bp_offsets[j][0] == 0:
                bpt_span.append(j)
                span_seen_words = 1
                k = j + 1
                while bp_offsets[k][0] > 0 or span_seen_words < num_words:
                    k += 1
                    if bp_offsets[k][0] == 0:
                        span_seen_words += 1
                bpt_span.append(k - 1)
                break
            if bp_offsets[j][0] == 0:
                seen_words += 1
        return bpt_span

    def _init_wsd(self, ):
        from allennlp.data import DatasetReader, Vocabulary
        from allennlp.common import Params
        def find_key(d, func):
            ret = None
            stack = [d]
            while len(stack) > 0 and ret is None:
                s = stack.pop()
                for k, v in s.items():
                    if func(k, v):
                        ret = s
                        break
                    elif isinstance(v, dict):
                        stack.append(v)
            return ret

        config = Params.from_file(os.path.join(str(LOC.knowbert), 'config.json'))
        full_reader_params = copy.deepcopy(config['dataset_reader'].as_dict())
        reader_params = find_key(full_reader_params,
                                 lambda k, v: k == 'type' and v == 'wordnet_fine_grained')
        reader_params['is_training'] = False
        reader_params['should_remap_span_indices'] = True
        reader_params = Params(reader_params)
        reader = DatasetReader.from_params(reader_params)
        self.synset_to_lemmas = {}
        for lemma_id, synset_id in reader.mention_generator._lemma_to_synset.items():
            if synset_id not in self.synset_to_lemmas:
                self.synset_to_lemmas[synset_id] = []
            self.synset_to_lemmas[synset_id].append(lemma_id)
        vocab_params = config['vocabulary']
        self.vocab = Vocabulary.from_params(vocab_params)

    def get_wsd_predictions(self, txt):
        if not hasattr(self, 'vocab') or not hasattr(self, 'synset_to_lemmas'):
            self._init_wsd()
        batch = self._get_model_inputs(txt)
        outputs = self.model(**batch)
        # predicted entities is list of (batch_index, (start, end), entity_id)
        predicted_entities = self.model.soldered_kgs['wordnet'].entity_linker._decode(
            outputs['wordnet']['linking_scores'], batch['candidates']['wordnet']['candidate_spans'],
            batch['candidates']['wordnet']['candidate_entities']['ids']
        )
        batch_size = batch['tokens']['tokens'].shape[0]
        predicted_entities_batch_indices = []
        for k in range(batch_size):
            predicted_entities_batch_indices.append([])
        for b_index, start_end, eid in predicted_entities:
            try:
                synset_id = self.vocab.get_token_from_index(eid, 'entity')
            except KeyError:
                synset_id = self.vocab.get_token_from_index(eid, 'entity_wordnet')
            all_lemma_ids = self.synset_to_lemmas[synset_id]
            predicted_entities_batch_indices[b_index].append(all_lemma_ids)
        words = []
        spans = []
        tenses = []
        for k in range(batch_size):
            assert k == 0  # bs should be 1
            for i in range(len(predicted_entities_batch_indices[k])):
                span = batch["candidates"]["wordnet"]["candidate_spans"][k][i]
                token_id = batch['tokens']['tokens'][k][span[0]:span[1] + 1]
                word = self.tokenizer.convert_ids_to_tokens([token_id.tolist()][0])
                words.append(word)
                spans.append(span)
                tenses.append(predicted_entities_batch_indices[k][i])
        return spans, tenses, words

    def mine_wsd_dl(self, dl: DataLoader):
        """
        This function will predict the synsets for all the antecedent candidates and the anaphors in the dataloader.
        It returns a dict{(docname, span_start, span_end) -> [(word, synsets)]}}
        """
        iterator: Iterable = enumerate(dl)
        mention_synsets = {}    # (docname, span_start, span_end) -> [(word, synsets)]
        for i, instance in tqdm(iterator):
            candidate_spans = getattr(instance, f'candidate_{self.candidates_src}_ids')
            candidate_txt = getattr(instance, f'candidate_{self.candidates_src}_txt')
            sent_lens = [sum(instance.sentence_lengths_[:sent_n]) for
                         sent_n in range(len(instance.sentence_lengths_))]
            # candidates
            for k in range(len(candidate_spans)):
                candidate_span = candidate_spans[k]
                if (instance.docname, candidate_span[0], candidate_span[1]) not in mention_synsets:
                    ante_sentences = instance.get_sentence(candidate_span)
                    ante_offset = sent_lens[ante_sentences[0]]
                    ante_span = [x - ante_offset for x in candidate_span]
                    tokenized_sent = self.tokenizer(to_toks(instance.document[ante_sentences[0]:ante_sentences[1] + 1]),
                                                    return_tensors='pt', add_special_tokens=True, max_length=512,
                                                    truncation=True, is_split_into_words=True,
                                                    return_offsets_mapping=True)
                    offset_mapping = tokenized_sent["offset_mapping"].tolist()[0]
                    ante_bpt_span = self.get_bpt_span(ante_span, offset_mapping)
                    pred_spans, pred_tenses, pred_words = self.get_wsd_predictions(
                        to_str(instance.document[ante_sentences[0]:ante_sentences[1] + 1]))
                    mention_prediction = []
                    for j in range(len(pred_spans)):
                        if self.overlap_span(ante_bpt_span, pred_spans[j].tolist()):
                            prediction_tuple = (" ".join(pred_words[j]), pred_tenses[j])
                            mention_prediction.append(prediction_tuple)
                            assert any(
                                [[pred_word in ant.lower() for pred_word in pred_words[j]] for ant in
                                 candidate_txt[k]])
                    mention_synsets[(instance.docname, candidate_span[0], candidate_span[1])] = mention_prediction
            # anaphor
            if (instance.docname, instance.anaphor[0], instance.anaphor[1]) not in mention_synsets:
                anaphor_sentences = instance.get_sentence(instance.anaphor)
                ana_offset = sent_lens[anaphor_sentences[0]]
                ana_span = [x - ana_offset for x in instance.anaphor]
                tokenized_sent = self.tokenizer(to_toks(instance.document[anaphor_sentences[0]:anaphor_sentences[1]+1]),
                                                return_tensors='pt', add_special_tokens=True, max_length=512,
                                                truncation=True, is_split_into_words=True, return_offsets_mapping=True)
                offset_mapping = tokenized_sent["offset_mapping"].tolist()[0]
                anaphor_bpt_span = self.get_bpt_span(ana_span, offset_mapping)
                pred_spans, pred_tenses, pred_words = self.get_wsd_predictions(
                    to_str(instance.document[anaphor_sentences[0]:anaphor_sentences[1] + 1]))
                mention_prediction = []
                for j in range(len(pred_spans)):
                    if self.overlap_span(anaphor_bpt_span, pred_spans[j].tolist()):
                        prediction_tuple = (" ".join(pred_words[j]), pred_tenses[j])
                        mention_prediction.append(prediction_tuple)
                        assert any([[pred_word in ana.lower() for pred_word in pred_words[j]] for ana in
                                    instance.anaphor_])
                mention_synsets[(instance.docname, instance.anaphor[0], instance.anaphor[1])] = mention_prediction
        return mention_synsets


class BertNextSentenceBench(NextSentenceBench):

    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        self.tokenizer = BertTokenizer.from_pretrained(name)

    def _get_model(self, name):
        """ Instantiates and returns model object """
        self.model = BertForNextSentencePrediction.from_pretrained(name)
        if self.device:
            self.model = self.model.to(self.device)

    def _get_model_inputs(self, txt_a: str, txt_b: str) -> transformers.tokenization_utils_base.BatchEncoding:
        """ Get model inputs from tokenizer """
        model_inputs = self.tokenizer.encode_plus(txt_a, txt_b, return_tensors='pt', add_special_tokens=True,
                                                  max_length=self.max_len_bpts, truncation=True)
        if self.device:
            model_inputs = model_inputs.to(self.device)
        return model_inputs

    def _get_next_sentence_scores(self, txt_a: str, txt_b: str) -> float:
        """ Get attention weights from model for input document"""

        tokenized_data = self._get_model_inputs(txt_a, txt_b)
        outputs = self.model(**tokenized_data)[0]
        softmax = F.softmax(outputs, dim=1)
        return float(softmax[0][0])


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--approach', '-a', type=str, default='bert-attn',
              help="bert-attn for BERT, roberta-attn for ROBERTA based approach")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--candidates-src', '-cs', type=str, default='nounphrase',
              help="Either 'nounphrase' or 'antecedent'")
@click.option('--candidate-reduction-method', '-crm', default='none', type=str,
              help="The chosen method to reduce the number of candidates")
@click.option('--use-heads', '-h', is_flag=True, default=False,
              help='If true, we only use span heads when considering the spans.')
@click.option('--device', type=str, default='cpu', help="pass cuda if you want the approach to be run on GPUs")
def main(candidates_src: str, dataset: str, dataset_domain: str, dataset_domain_filename: str,
         candidate_reduction_method: str, approach: str, use_heads: bool, device: str):
    dl = DataLoader(dataset, dataset_domain, dataset_domain_filename)

    if approach == 'bert-attn':
        eval_bench = BertSelfAttentionBench(data_loader=dl, candidates_src=candidates_src, model='bert-base-uncased',
                                            max_len=400, pool_layers='11', pool_heads="max", pool_span="max",
                                            candidate_reduction_method=candidate_reduction_method, use_heads=use_heads)
    elif approach == 'bert-nsp':
        eval_bench = BertNextSentenceBench(data_loader=dl, candidates_src=candidates_src, model='bert-base-uncased',
                                           candidate_reduction_method=candidate_reduction_method, use_heads=use_heads)

    elif approach == 'roberta-attn':
        eval_bench = RoBertaAttentionBench(data_loader=dl, candidates_src=candidates_src, model='roberta-base',
                                           max_len=400, pool_layers='mean', pool_heads="mean", pool_span="mean",
                                           candidate_reduction_method=candidate_reduction_method, use_heads=use_heads)
    elif approach == 'bert-attn-pairwise':
        eval_bench = BertSelfAttentionPairwiseBench(data_loader=dl, candidates_src=candidates_src,
                                                    model='bert-base-uncased', max_len=400,
                                                    pool_layers='mean', pool_heads="mean", pool_span="mean",
                                                    candidate_reduction_method=candidate_reduction_method,
                                                    use_heads=use_heads, proximity_trim=False)
    elif approach =='spanbert-attn':
        eval_bench = BertSelfAttentionBench(data_loader=dl, candidates_src=candidates_src,
                                            model='SpanBERT/spanbert-base-cased',
                                            max_len=400, pool_layers='5', pool_heads="best-max", pool_span="max",
                                            candidate_reduction_method=candidate_reduction_method, use_heads=use_heads,
                                            device=device)
    elif approach =='diablo-attn':
        eval_bench = DiabloGPTAttentionBench(data_loader=dl, candidates_src=candidates_src,
                                            model="microsoft/DialoGPT-medium",
                                            max_len=400, pool_layers='5', pool_heads="best-max", pool_span="max",
                                            candidate_reduction_method=candidate_reduction_method, use_heads=use_heads,
                                            device=device)
    else:
        raise ValueError(f"Unknown model name: {approach}.")

    print(eval_bench.run(dump_predictions=True, approach_nm=approach))


if __name__ == "__main__":
    _pathfix.suppress_unused_import()

    main()

    """
    import pickle
    dl = DataLoader('crac', 'arrau', 'Trains_91.CONLL', shuffle=False)
    model = KnowBertSelfAttentionBench(dl, model='knowbert-base', max_len=400,
                                       candidates_src="nounphrase", candidate_reduction_method="none")
    sense_dict = model.mine_wsd_dl(dl)  # (docname, span_start, span_end) -> [(word, synsets)]
    with open("trains91_knowbert_wsd.pkl", 'wb') as f:
        pickle.dump(sense_dict, f)
    """

