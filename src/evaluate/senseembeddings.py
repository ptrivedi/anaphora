"""
    This file contains "EvaluationBench" things which can evaluate a given dataset using wordnet embeddings.
"""
# Global Imports
import nltk

nltk.data.path.append("/home/priyansh/Dev/perm/nltk/nltk_data")  # TODO: look for a permanent fix
import json
import torch
import click
import numpy as np
from typing import List, Dict, Union, Iterable, Tuple

# Local Imports
try:
    import _pathfix
except ImportError:
    from . import _pathfix
from dataloader import DataLoader
from config import LOCATIONS as LOC
from utils.bridging import BridgingInstance
from evaluate.base import BaseStaticEmbeddingBench
from utils.wordsense import PairwiseSynsetDisambiguation


class SenseEmbeddingBench(BaseStaticEmbeddingBench):

    def __init__(self, data_loader: Union[DataLoader, Iterable], candidates_src: str, candidate_reduction_method: str,
                 only_heads: bool = True, tqdm: bool = True, pooling: str = 'mean', wsd_strategy: str = 'closest',
                 always_fallback: bool = False, similarity: str = 'cos',
                 metrics: Tuple[str] = ('acc', 'mrr', 'mr', 'ent_f1'),
                 fallbacks: Iterable[str] = ('spacy-lemmatize', 'w2v-closest', 'unk')):
        """
            The SenseEmbeddingBench class aims to link synsets to the headwords of anaphor and antecedent spans.
            Then using WordNet embeddings, we aim to estimate the similarity between these spans
                using their sense vectors.

            Challenges:
                - linking sense:
                    - try to simply find synsets using NLTK
                    - if none are found, rely on functions specified in fallbacks tuple. Apply them one after one.

            Example Usage:
                test_data: List[BridgingInstance] = ...
                bench = SenseEmbeddingBench(test_data)
                bench.run()

        :param data_loader: A dataloader obj
            We report numbers over all these, and thus they're to be treated as 'test instances'
        :param only_heads: a bool which indicates whether we only use the headwords or the entire span.
            For now, always use headwords
        :param pooling: a string indicating the method which with we will pool vectors if we're using the entire span
        :param similarity: a string indicating the method to compare two vectors.
        :param wsd_strategy: a str indicating the method we use to select the correct sense, or a combination thereof,
            given a set of senses for a pair of words
        :param fallbacks: a string indicating the functions to be used in case no sense is found by a direct NLTK lookup
            These all change the given word, hoping that NLTK can find a sense for the new word.
        :param always_fallback: bool indicating whether we use fallback fns only when no sense is found via NLTK
        """
        super().__init__(data_loader=data_loader, similarity=similarity, tqdm=tqdm, metrics=metrics,
                         candidates_src=candidates_src, candidate_reduction_method=candidate_reduction_method)

        """
            Sanity check and store all parameters
        """
        if not only_heads:
            raise NotImplementedError("Have not written code to do this without heads")

        self.only_heads_: bool = only_heads

        if pooling not in ['mean', 'max']:
            raise NotImplementedError(f"No way to pool vectors using {pooling}")
        else:
            self.pooling_: str = pooling

        """ 
            Load the enttoid, idtovec of WordNet embeddings. Each vector corresponds to a synset, here.
        """
        with (LOC.wordnetdir / 'entoid.json').open('r', encoding='utf8') as fp:
            self.enttoid: Dict[str, int] = json.load(fp)
            self.idtoent: Dict[int, str] = {v: k for k, v in self.enttoid.items()}

        self.idtovec: np.ndarray = torch.load(LOC.wordnetdir / 'model.torch',
                                              map_location=torch.device('cpu'))['init_embed'].cpu().detach().numpy()

        self.wsd: PairwiseSynsetDisambiguation = PairwiseSynsetDisambiguation(sense_vectors=self.idtovec,
                                                                              sense_vocab=self.enttoid,
                                                                              wsd_strategy=wsd_strategy,
                                                                              always_fallback=always_fallback,
                                                                              similarity=self.similarity_,
                                                                              fallbacks=fallbacks)

    def embed(self, key: str) -> np.ndarray:

        """ Get id from enttoid and get vec from idtovec """
        try:
            return self.idtovec[self.enttoid[key]].reshape(1, -1)
        except KeyError:
            # warnings.warn(f"No embedding for found {key}")
            return self.idtovec[self.enttoid['__na__']].reshape(1, -1)

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            Assuming heads have only one word.
            Simply give the anaphor head and candidate's head positions to PairwiseSenseDisambiguation.wsd()
                and store the scores in a np array. Return it when done.
        """
        assert len(datum.anaphor_h_) == 1
        for candidate in candidate_span_h_txt:
            assert len(candidate) == 1

        # Get the head word of anaphor
        ana_pos: int = datum.anaphor_h[0]
        antecedents_pos: List[int] = [x[0] for x in candidate_span_h_ids]

        # Init the scores array where each index corresponds to candidates_src in candidate_span_*
        scores: np.ndarray = np.zeros(len(antecedents_pos))

        for i, (can_pos) in enumerate(antecedents_pos):
            # Get the best sense pair (str)
            sense_ana, sense_can = self.wsd.wsd(position_a=ana_pos, position_b=can_pos,
                                                context=datum.document, context_pos=datum.pos)

            # Compute the similarity between them
            score = self.similarity(self.embed(sense_ana), self.embed(sense_can))

            scores[i] = score

        return scores


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--candidates-src', '-cs', type=str, default='nounphrase',
              help="Either 'nounphrase' or 'antecedent'")
@click.option('--candidate-reduction-method', '-crm', default='none', type=str,
              help="The chosen method to reduce the number of candidates")
def main(candidates_src: str, dataset: str, dataset_domain: str, dataset_domain_filename: str,
         candidate_reduction_method: str):
    dl = DataLoader(dataset, dataset_domain, dataset_domain_filename)

    eval_bench = SenseEmbeddingBench(data_loader=dl, candidates_src=candidates_src, fallbacks=('spacy-lemmatize', 'unk'),
                                     candidate_reduction_method=candidate_reduction_method)

    print(eval_bench.run())


if __name__ == "__main__":
    _pathfix.suppress_unused_import()
    main()
