"""

    Base evaluation class with functionality to be used by other evaluation benches

"""
# Global Imports
import copy
import click
import torch
import pickle
import warnings
import numpy as np
import torch.nn as nn
from pathlib import Path
from tqdm.auto import tqdm
from itertools import product
from abc import abstractmethod, ABC
from typing import List, Union, Tuple, Optional, Callable, Dict, Iterable

# Local Imports
try:
    import _pathfix
except ImportError:
    from . import _pathfix
from utils.misc import sim_cos, sim_tanimoto, pop
from utils.bridging import BridgingInstance, to_toks
from dataloader import DataLoader, CrossValidationDataLoader, WrappedCVDL, TrainableInstanceIterator
from config import CRAC_RAWDATA_FILES as RAW_DATA_FILES
from config import LOCATIONS as LOC
from scorer import evaluate_ua_corpus, crac_save_predictions_jsons_to_ua
from utils.exceptions import NoCandidateLeft

# Clamp the randomness
np.random.seed(42)


def cross_validate(approach, cvdl: CrossValidationDataLoader, params: Dict[str, list],
                   select_based_on: str = 'metric_acc', dumping_approach_name: Optional[str] = None) -> (dict, dict):
    """
        1. approach is a class reference, subclass of BaseEvaluationBench
        1. Divide the data in validation, test based on folds.
        1. Params is a dict like {'pool_layers': ['5', 'max', 'max'] ... }
            1. Get the results on the valid split for each param value (grid search)
            1. Report the of the best param combination on the test split
        1. Repeat this num. of folds time
    """

    # Declare a dict to hold the final results
    results_final: List[dict] = []
    logs: List[List[dict]] = []

    folds: int = cvdl.total_folds

    for i in range(folds):
        print(f"Starting Grid search for fold nr. {i}")

        # Use the train split, iterate over all parameters
        param_names = [k for k in params.keys()]
        param_values = [v for v in params.values()]

        # Declare a list to store all summaries, corresponding to each combination
        results_for_this_fold: List[dict] = []

        # Permute all parameter combinations
        param_combos: list = [comb for comb in product(*param_values)]

        # Iterate through all of them
        for param_combo in tqdm(param_combos, desc='Cross Validation'):
            # Init the eval bench based on this parameter combination
            eval_bench = approach(**{k: v for k, v in zip(param_names, param_combo)},
                                  data_loader=WrappedCVDL(cvdl.train, cvdl), tqdm=False)

            # Calculate the metrics on the train split
            train_result_for_this_combination: dict = eval_bench.run()

            # Add them to the declared list
            results_for_this_fold.append(train_result_for_this_combination)

        # Store all of these in logs
        logs.append(results_for_this_fold)

        # Choose the best combination base on train combinations
        best_param_combo = int(np.argmax([result[select_based_on] for result in results_for_this_fold]))

        # Instantiate another eval bench, based on the best performing param combination
        best_approach = approach(**{k: v for k, v in zip(param_names, param_combos[best_param_combo])},
                                 data_loader=WrappedCVDL(cvdl.valid, cvdl))

        # Get the results
        best_bench_results: dict = best_approach.run()

        # Add them to the declared list
        results_final.append(best_bench_results)
        print(f"For fold {i}, the final results are: {best_bench_results}")

    # Time to summarise things across splits.
    # Assuming: splits have different sizes

    # Declare summarised metrics
    assert len(results_final) > 0
    summarised_metrics: Dict[str, Union[float, np.float]] = {k: 0 for k in results_final[0].keys()
                                                             if k.startswith('metric_')}

    for metric in summarised_metrics.keys():
        value, instances = 0, 0
        for split in results_final:
            instances += split['instances']
            value += split[metric] * split['instances']

        summarised_metrics[metric] = float(value) / instances

    return {'params': params,
            'detailed': logs,
            'results': results_final,
            'summary': summarised_metrics,
            'folds': folds,
            'training_includes': [str(x) for x in cvdl.dirs_aux]
            }


class BaseEvaluationBench(ABC):

    def __init__(self, data_loader: Union[DataLoader, Iterable], metrics: Tuple[str] = ('acc', 'mrr', 'mr', 'ent_f1'),
                 tqdm: bool = True, candidates_src: str = 'nounphrase', candidate_reduction_method: str = 'none'):
        """
            Metrics determine which things are to be calculated.
            For now, we only support accuracy.

        :param data_loader: a data loader obj
        :param metrics: tuple of str indicating which metrics to use
        """

        assert candidates_src in ['antecedent', 'nounphrase'], f"Candidate src: '{candidates_src}' not understood"

        # TODO: can we add a streamer here instead, so that we can deal with millions of data later?
        self.data_loader: DataLoader = data_loader
        if not data_loader.dataset == "crac":
            metrics = tuple([m for m in metrics if not m == "ent_f1"])
        self.metrics: List[Callable] = [getattr(self, 'metric_' + m_name) for m_name in metrics
                                        if not m_name == "ent_f1"]
        self.metrics_: Tuple[str] = metrics
        self.candidates_src = candidates_src

        self.tqdm: bool = tqdm

        # Set up fn for candidate reduction
        self.candidate_reduction_method_: str = candidate_reduction_method
        self.candidate_reduction_method: Callable = {
            'first_and_two_sents': self._candidate_reduction_first_and_two_sents_,
            'first_and_one_sent': self._candidate_reduction_first_and_one_sent_,
            'first_and_current_sent': self._candidate_reduction_first_and_current_sent_,
            'two_sents': self._candidate_reduction_two_sents_,
            'fifty_words': self._candidate_reduction_fifty_words_,
            'none': self._candidate_reduction_none_
        }[self.candidate_reduction_method_]

    def _candidate_reduction_select_candidates_(self, instance: BridgingInstance, sentences: List[int]) -> np.array:
        # Iterate through all candidates and create a boolean 0/1 for each candidate
        # 0: ignore
        # 1: keep
        selected_candidates = np.zeros(len(getattr(instance, f'candidate_{self.candidates_src}_ids')))
        for i, can in enumerate(getattr(instance, f'candidate_{self.candidates_src}_ids')):
            can_sents = instance.get_sentence(can)
            if can_sents[0] in sentences and can_sents[1] in sentences:
                selected_candidates[i] = 1

        return selected_candidates

    def _candidate_reduction_none_(self, instance: BridgingInstance) -> Union[np.ndarray, List[int]]:
        """ Return a [1,1,1 ...] w length of specified candidate source """
        selected_candidates = np.ones(len(getattr(instance, f'candidate_{self.candidates_src}_ids')))
        return selected_candidates

    def _candidate_reduction_two_sents_(self, instance: BridgingInstance) -> Union[np.ndarray, List[int]]:
        """ Keep the candidates from the first instance and two sentences before the anaphor """
        anaphor_sentence: List[int] = instance.get_sentence(instance.anaphor)
        sentences: List[int] = list({anaphor_sentence[0], anaphor_sentence[1],
                                     anaphor_sentence[0] - 1, anaphor_sentence[0] - 2})

        return self._candidate_reduction_select_candidates_(instance=instance, sentences=sentences)

    def _candidate_reduction_first_and_two_sents_(self, instance: BridgingInstance) -> Union[np.ndarray, List[int]]:
        """ Keep the candidates from the first instance and two sentences before the anaphor """
        anaphor_sentence: List[int] = instance.get_sentence(instance.anaphor)
        sentences: List[int] = list({0, anaphor_sentence[0], anaphor_sentence[1],
                                     anaphor_sentence[0] - 1, anaphor_sentence[0] - 2})

        return self._candidate_reduction_select_candidates_(instance=instance, sentences=sentences)

    def _candidate_reduction_first_and_one_sent_(self, instance: BridgingInstance) -> Union[np.ndarray, List[int]]:
        """ Keep the candidates from the first instance and two sentences before the anaphor """
        anaphor_sentence: List[int] = instance.get_sentence(instance.anaphor)
        sentences: List[int] = list({0, anaphor_sentence[0], anaphor_sentence[1], anaphor_sentence[0] - 1})

        return self._candidate_reduction_select_candidates_(instance=instance, sentences=sentences)

    def _candidate_reduction_first_and_current_sent_(self, instance: BridgingInstance) -> Union[np.ndarray, List[int]]:
        """ Keep the candidates from the first instance and two sentences before the anaphor """
        anaphor_sentence: List[int] = instance.get_sentence(instance.anaphor)
        sentences: List[int] = list({0, anaphor_sentence[0], anaphor_sentence[1]})

        return self._candidate_reduction_select_candidates_(instance=instance, sentences=sentences)

    def _candidate_reduction_fifty_words_(self, instance: BridgingInstance) -> Union[np.ndarray, List[int]]:
        """
            Only keep the instances which appear in sentences that are found in about fifty words around the anaphor.
                Both sides.
        """
        anaphor_start, anaphor_end = instance.anaphor
        start_index = max(anaphor_start - 50, 0)
        end_index = min(anaphor_end + 50, len(to_toks(instance.document)))
        start_sent, end_sent = instance.get_sentence([start_index, end_index])
        end_sent += 1
        sentences: List[int] = list(range(start_sent, end_sent))

        return self._candidate_reduction_select_candidates_(instance=instance, sentences=sentences)

    @staticmethod
    def _to_numpy_(data: List[np.ndarray], max_len: Optional[int] = None, pad: Union[int, float] = -1,
                   dtype: np.dtype = np.float64) -> np.ndarray:
        """ Convert a list of np arrays into one nd array. If max len not provided, calc it.  """

        if not max_len:
            max_len: int = max([d.shape[0] for d in data])

        op: np.ndarray = pad * np.ones((len(data), max_len), dtype=dtype)
        for i, d in enumerate(data):
            op[i, :d.shape[0]] = d

        return op

    def run(self, save_output: bool = False, output_dir: Union[str, Path] = None,
            dump_predictions: bool = False, approach_nm: str = None, testing: bool = False):
        """
            Treat the data as an iterator and only store true and predicted indices

            Can optionally save the predictions in UA format to upload for shared task. As such, this only applies to
            the crac datasets. If the output_dir is None, will save to
            "(root)/predictions/submission/{dl.dataset_domain}"

            If dump_predictions flag is turned on, it will dump the numpy matrix to disk
                (leaving you to calc. metrics etc)

            testing: if we are running for a test set in which we don't have the correct antecedent, skip the metrics
                and just return the predictions
        """
        true: List[int] = []
        pred_list: List[np.ndarray] = []

        max_n_candidates: int = -1

        if self.tqdm:
            # Print the progress for this run
            iterator: Iterable = enumerate(tqdm(self.data_loader, leave=True, total=len(self.data_loader),
                                                desc=f"{self.__class__.__name__} over {self.data_loader.dir}"))
        else:
            # Don't print the progress for this run, usually when doing cross validation.
            iterator: Iterable = enumerate(self.data_loader)

        num_candidates: List[int] = []
        # dict with keys: (doc_name, (ana_start, ana_end)) tuples, values ((ant_start, ante_end), words) tuples
        # this info is needed to map prediction on to scorer's gold annotations
        universal_anaphora_predictions = {}
        output_dict = {}
        instances = []      # keep all instances copied here

        for i, datum in iterator:

            if save_output:
                # if outputting predictions, we don't want to output (-1, -1) in the case where there are no candidates
                # after trimming, we might as well take a random guess. To do so, we need the original candidate spans,
                # so we save a copy here.
                full_candidate_set = copy.deepcopy(getattr(datum, f'candidate_{self.candidates_src}_ids'))
            else:
                full_candidate_set = None

            # Reduce candidates as specified.
            # This reduces the amount of candidates and stores them back (in nounphrase or antecedent as specified)
            self.reduce_candidates(instance=datum)
            num_candidates.append(len(getattr(datum, f'candidate_{self.candidates_src}_ids')))

            try:
                if num_candidates[-1] == 0:
                    # if the candidate reduction results in zero candidates, create dummy prediction, which will count
                    # as a wrong prediction because correct_{self.candidates_src}_id = -1 in this case
                    raise NoCandidateLeft
                else:
                    # Call predict
                    _pred = self.predict(datum=datum, src=self.candidates_src,
                                         candidate_span_ids=getattr(datum, f'candidate_{self.candidates_src}_ids'),
                                         candidate_span_txt=getattr(datum, f'candidate_{self.candidates_src}_txt'),
                                         candidate_span_h_ids=getattr(datum, f'candidate_{self.candidates_src}_h_ids'),
                                         candidate_span_h_txt=getattr(datum, f'candidate_{self.candidates_src}_h_txt'))
            except NoCandidateLeft:
                warnings.warn(f"No candidate left error was raised for - {datum.anaphor_id}")
                _pred = np.array([0], dtype=np.float)
            pred_list.append(_pred)
            if not testing:
                true.append(getattr(datum, f'correct_{self.candidates_src}_id'))
                if "ent_f1" in self.metrics_:
                    if num_candidates[-1] == 0:
                        # if the filtering ends with 0 candidates, create dummy prediction
                        pred_ante_span = (-1, -1)
                        pred_antecedent_words = []
                    else:
                        predicted_index = np.argmax(_pred, axis=0)
                        pred_ante_span = getattr(datum, f'candidate_{self.candidates_src}_ids')[predicted_index]
                        pred_antecedent_words = getattr(datum, f'candidate_{self.candidates_src}_txt')[predicted_index]
                    universal_anaphora_predictions[(datum.docname, tuple(datum.anaphor))] = \
                        (pred_ante_span, pred_antecedent_words)

            max_n_candidates = _pred.shape[0] if _pred.shape[0] > max_n_candidates else max_n_candidates

            if save_output:
                if datum.docname not in output_dict:
                    output_dict[datum.docname] = {}
                    output_dict[datum.docname]["sentences"] = datum.document
                    output_dict[datum.docname]["doc_key"] = datum.docname
                    output_dict[datum.docname]["bridging_pairs"] = []
                    output_dict[datum.docname]["clusters"] = []
                if num_candidates[-1] == 0:
                    # if the filtering ends with 0 candidates, just use the first candidate before trimming
                    # TODO: should we have a different fall back plan here?
                    print("random pred")
                    pred_ante_span = full_candidate_set[0]
                else:
                    predicted_index = np.argmax(_pred, axis=0)
                    pred_ante_span = getattr(datum, f'candidate_{self.candidates_src}_ids')[predicted_index]
                output_dict[datum.docname]["bridging_pairs"].append([(datum.anaphor[0], datum.anaphor[1] - 1),
                                                                     (pred_ante_span[0], pred_ante_span[1] - 1)])

            if dump_predictions:
                instances.append(copy.deepcopy(datum))

        if save_output:
            if self.data_loader.dataset == "crac":
                if output_dir is None:
                    output_dir: Path = LOC.parsed / "submission" / self.data_loader.dataset_domain
                else:
                    output_dir = Path(output_dir)

                # Figure out the file name
                file_name = None
                if self.data_loader.dataset_domain == "switchboard":
                    file_name = 'Switchboard_3_dev.CONLL'
                elif self.data_loader.dataset_domain == "light":
                    file_name = 'light_dev.CONLLUA'
                elif self.data_loader.dataset_domain == 'persuasion':
                    file_name = 'Persuasion_dev.CONLLUA'
                elif self.data_loader.dataset_domain == 'ami':
                    file_name = 'AMI_dev.CONLLUA'
                elif self.data_loader.dataset_domain == "arrau":
                    file_name = self.data_loader.dataset_domain_filename
                else:
                    warnings.warn("Unknown dataset, skipping prediction saving")
                if file_name is not None:
                    crac_save_predictions_jsons_to_ua(output_dict=output_dict, output_file=output_dir / file_name)
            else:
                warnings.warn("Saving predictions is only implemented for crac datasets currently.")


        # Convert True and Pred to np mats/arrays
        pred: np.ndarray = self._to_numpy_(pred_list, max_len=max_n_candidates)  # 2D mat: n_data, n_candidates
        if testing:
            return pred

        true: np.ndarray = np.asarray(true)
        if dump_predictions:

            # Prep to dump the np array
            dump_dir = (LOC.predictions / self.data_loader.dataset)
            if self.data_loader.dataset_domain:
                dump_dir = dump_dir / self.data_loader.dataset_domain
            if self.data_loader.dataset_domain_filename:
                dump_dir = dump_dir / self.data_loader.dataset_domain_filename
            dump_dir = dump_dir / self.candidate_reduction_method_
            dump_dir.mkdir(parents=True, exist_ok=True)

            if not approach_nm:
                raise ValueError("Approach name not provided when dumping.")
            dump_dir = dump_dir / (approach_nm+'.pkl')

            with dump_dir.open('wb+') as f:
                pickle.dump({'instances': instances, 'pred': pred, 'true': true}, f)

        # Get metrics out.
        metrics: List[Union[float, np.ndarray]] = [metric_callable(true=true, pred=pred)
                                                   for metric_callable in self.metrics]
        if "ent_f1" in self.metrics_:
            raw_file = ""
            if self.data_loader.dataset_domain == "switchboard":
                raw_file = RAW_DATA_FILES.switchboard
            elif self.data_loader.dataset_domain == "light":
                raw_file = RAW_DATA_FILES.light
            elif self.data_loader.dataset_domain == "arrau":
                if "91" in self.data_loader.dataset_domain_filename:
                    raw_file = RAW_DATA_FILES.trains91
                elif "93" in self.data_loader.dataset_domain_filename:
                    raw_file = RAW_DATA_FILES.trains93
            elif self.data_loader.dataset_domain == "ami":
                raw_file = RAW_DATA_FILES.ami
            elif self.data_loader.dataset_domain == "persuasion":
                raw_file = RAW_DATA_FILES.persuasion
            if raw_file == "":
                warnings.warn("Raw data file not found, skipping ent_f1 metric.")
            else:
                metrics.insert(self.metrics_.index("ent_f1"), evaluate_ua_corpus(raw_file,
                                                                                 universal_anaphora_predictions))
        summary: dict = self._summarise_(metrics, num_candidates)
        return summary

    def _summarise_(self, results: List[Union[float, np.ndarray]], n_cans: List[int]) -> dict:
        """ Return a nice summary of the metrics """
        # noinspection PyStringFormat
        return {'instances': len(self.data_loader), 'candidates_src': self.candidates_src,
                'candidate_reduction_method': self.candidate_reduction_method_,
                'candidates_num': '%(mean)1.3f +- %(std)1.3f' % {'mean': np.mean(n_cans), 'std': np.std(n_cans)},
                **{'metric_' + m_name: m_val for m_name, m_val in zip(self.metrics_, results)}}

    @abstractmethod
    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            Take an instance. do whatever you want to.
            Return a 1D array containing scores corresponding to every candidate (logits).
        """
        ...

    # TODO: make many metrics and wire them in diff. eval benches.
    @staticmethod
    def metric_acc(true: np.ndarray, pred: np.ndarray) -> np.ndarray:
        """
            Get accuracy between a list of true and preds. True is 1D. Pred is 2D.
            If true is -1, return 0: dont have to write an edge case. Function does this automatically.
        """
        return np.mean((np.argmax(pred, axis=1) == true) * 1.0)

    def metric_lenient_acc(self, true: np.ndarray, pred: np.ndarray) -> np.ndarray:
        """ If there is a overlap between a list of true and pred, return 1. """
        # TODO
        ...

    @staticmethod
    def metric_mrr(true: np.ndarray, pred: np.ndarray) -> np.ndarray:
        """
            Calculate the mean reciprocal rank

            if true is -1, we add 1/(len(candidates) + 1) to mrr for that instance

            # Detailed Example
            > pred = np.random.randn(2, 3)
                [[ 0.36092338,  0.91540212,  0.32875111],
                 [-0.5297602 ,  0.51326743,  0.09707755]]
            > true = np.random.randint(0, 3, (2,))
                [1, 2]
            # argsort -pred gives us the indices: highest to lowest.
            # In the first instance, candidate 1 (correct one) has the highest score and thus rank = 1.
            # In the second instance, candidate 2 (correct one) has the second highest score and thus rank = 2.
            > pred_sorted = np.argsort(-pred, axis=1)
                [[1, 0, 2],
                 [1, 2, 0]]
            # We tile the true arr to make comparison easy
            > true_tiled = np.tile(true, (pred.shape[1],1)).transpose((1, 0))
                [[1, 1, 1],
                 [2, 2, 2]]
            # Now, the rank is the position where pred_sorted == true_tiled
            > rank = np.where(pred_sorted == true_tiled)[1]+1
                [1, 2]
            # Inverse it, take the mean and return it. super easy.
        """
        # First, remove all invalid instances, i.e. where true = -1
        valid_instances = np.where(true != -1)[0]
        pred_ = pred[valid_instances]
        true_ = true[valid_instances]

        # Compute the ranks for remaining instances
        pred_sorted = np.argsort(-pred_, axis=1)
        true_tiled = np.tile(true_, (pred_.shape[1], 1)).transpose((1, 0))
        rank = np.where(pred_sorted == true_tiled)[1] + 1

        # Now, to this rank, add max rank for every invalid instance
        rank = np.concatenate((rank, np.array([pred.shape[1]] * (true.shape[0] - valid_instances.shape[0]))))
        return np.mean(1 / rank)

    @staticmethod
    def metric_mr(true: np.ndarray, pred: np.ndarray) -> np.ndarray:
        """
            Calculate the mean rank for the instances

            if true = -1 at any instance, we add len(candidates) + 1 for that instance

            # Detailed Example
            > pred = np.random.randn(2, 3)
                [[ 0.36092338,  0.91540212,  0.32875111],
                 [-0.5297602 ,  0.51326743,  0.09707755]]
            > true = np.random.randint(0, 3, (2,))
                [1, 2]
            # argsort -pred gives us the indices: highest to lowest.
            # In the first instance, candidate 1 (correct one) has the highest score and thus rank = 1.
            # In the second instance, candidate 2 (correct one) has the second highest score and thus rank = 2.
            > pred_sorted = np.argsort(-pred, axis=1)
                [[1, 0, 2],
                 [1, 2, 0]]
            # We tile the true arr to make comparison easy
            > true_tiled = np.tile(true, (pred.shape[1],1)).transpose((1, 0))
                [[1, 1, 1],
                 [2, 2, 2]]
            # Now, the rank is the position where pred_sorted == true_tiled
            > rank = np.where(pred_sorted == true_tiled)[1]+1
                [1, 2]
            # now we just take th mean of the rank and return it.
        """
        # First, remove all invalid instances, i.e. where true = -1
        valid_instances = np.where(true != -1)[0]
        pred_ = pred[valid_instances]
        true_ = true[valid_instances]

        # Compute the ranks for remaining instances
        pred_sorted = np.argsort(-pred_, axis=1)
        true_tiled = np.tile(true_, (pred_.shape[1], 1)).transpose((1, 0))
        rank = np.where(pred_sorted == true_tiled)[1] + 1

        # Now, to this rank, add max rank for every invalid instance
        rank = np.concatenate((rank, np.array([pred.shape[1]] * (true.shape[0] - valid_instances.shape[0]))))
        return np.mean(rank)

    def reduce_candidates(self, instance: BridgingInstance) -> None:
        """
            This does not affect the 'antecedent' part of things.

            Based on the candidate reduction method specified during init,
                - pop out the candidates from all four candidate lists
                    - candidate_nounphrase_ids, candidate_nounphrase_text, candidate_nounphrase_h_ids ...
                - adjust the correct candidate ID. If the correct candidate is no longer included, mark as -1
        """

        reduced_candidate_idx_raw: Union[np.ndarray, List[int]] = self.candidate_reduction_method(instance)
        ignore_candidate_idx: np.ndarray = np.asarray(np.array(reduced_candidate_idx_raw) == 0).nonzero()[0]

        self.pop_candidates(instance, ignore_candidate_idx=ignore_candidate_idx, candidates_src=self.candidates_src)

    @staticmethod
    def pop_candidates(instance: BridgingInstance, ignore_candidate_idx: np.ndarray, candidates_src: str) -> None:
        """ Pop the candidates based on src and the ignore array. Also invoke find_correct_candidate fn """

        candidate_ids = getattr(instance, f'candidate_{candidates_src}_ids')
        candidate_txt = getattr(instance, f'candidate_{candidates_src}_txt')
        candidate_h_ids = getattr(instance, f'candidate_{candidates_src}_h_ids')
        candidate_h_txt = getattr(instance, f'candidate_{candidates_src}_h_txt')

        pop(candidate_ids, ignore_candidate_idx)
        pop(candidate_txt, ignore_candidate_idx)
        pop(candidate_h_ids, ignore_candidate_idx)
        pop(candidate_h_txt, ignore_candidate_idx)

        setattr(instance, f'candidate_{candidates_src}_ids', candidate_ids)
        setattr(instance, f'candidate_{candidates_src}_txt', candidate_txt)
        setattr(instance, f'candidate_{candidates_src}_h_ids', candidate_h_ids)
        setattr(instance, f'candidate_{candidates_src}_h_txt', candidate_h_txt)

        # Finally, re calculate the correct ID
        instance.find_correct_candidate_id(fail_silently=True)


class BaseStaticEmbeddingBench(BaseEvaluationBench, ABC):

    def __init__(self, data_loader: DataLoader, candidates_src: str, candidate_reduction_method: str,
                 metrics: Tuple[str] = ('acc',), similarity: str = 'cos', tqdm: bool = True):
        super().__init__(data_loader=data_loader, metrics=metrics, tqdm=tqdm,
                         candidates_src=candidates_src, candidate_reduction_method=candidate_reduction_method)

        self.similarity_: str = similarity
        self.similarity: Callable = {'cos': sim_cos, 'tanimoto': sim_tanimoto}[self.similarity_]

    @staticmethod
    def _pool_mean_(arr: np.ndarray) -> np.ndarray:
        """ input is (n, D), output is (1, D) """
        return np.mean(arr, axis=0).reshape(1, -1)

    @staticmethod
    def _pool_sum_(arr: np.ndarray) -> np.ndarray:
        """ input is (n, D), output is (1, D) """
        return np.sum(arr, axis=0).reshape(1, -1)


class OracleEvaluationBench(BaseEvaluationBench):
    """
        This class always "knows" the correct answer if it has not been reduced based on candidate reduction.
        i.e. if candidate reduction method is None, the accuracy should be 1.0
    """

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            Return a 1-hot vector, len: # candidates. 1 at candidate ID. 0 otherwise.
            If the correct candidate is in the filtered list, it should be selected.
            If not, then the predicted array won't matter.
        """

        op: np.ndarray = np.zeros(len(candidate_span_ids))
        op[getattr(datum, f'correct_{self.candidates_src}_id')] = 1

        return op


class RandomEvaluationBench(BaseEvaluationBench):
    """
        Return a random list back.
    """

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        op = np.arange(0, len(candidate_span_ids))
        np.random.shuffle(op)
        return op


class ParametricEvaluationBench(BaseEvaluationBench):

    def __init__(self, model: nn.Module, model_predict: Callable, vocabularizer: Callable, use_heads: bool,
                 device: Union[torch.device, str], data_loader: Union[DataLoader, Iterable], batchsize: int,
                 metrics: Tuple[str] = ('acc', 'mrr', 'mr', 'ent_f1'), tqdm: bool = True, mode: str = 'pairwise',
                 candidates_src: str = 'antecedent', candidate_reduction_method: str = 'none',
                 salience_feat_extractor: Union[Callable] = None):
        """
           The vocabularizer is a function that takes an List[str] and returns a List[int]

           E.g.
               data: List[Dict[str, torch.tensor]] = converter(instance, spans_query, spans_candidates, source=None)
               # One anaphor, candidate pair at a time
               for datum in data:
                   scores.append(predict_fn(**datum))

           See base's comments for more information
        """

        super().__init__(data_loader=data_loader, metrics=metrics, tqdm=tqdm, candidates_src=candidates_src,
                         candidate_reduction_method=candidate_reduction_method)

        self.use_heads = use_heads
        self.model = model
        self.model_predict = model_predict
        self.vocabularizer = vocabularizer
        self.salience_feat_extractor = salience_feat_extractor
        self.device = device
        self.batchsize = batchsize
        self.mode = mode

        # Disable dropouts
        self.model.eval()

    def _predict_(self, anaphor: torch.Tensor, candidates: torch.tensor,
                  candidates_feat: torch.tensor) -> torch.tensor:
        """ Based on batchsize, pass the things to the model and collect predictions """

        with torch.no_grad():

            results = []

            # Make np arrays
            anaphor_ = np.array(anaphor, dtype=np.long)

            maxlen_candidates: int = max(len(candidate) for candidate in candidates)
            candidates_ = np.zeros((len(candidates), maxlen_candidates), dtype=np.long)
            for i, candidate in enumerate(candidates):
                candidates_[i, :len(candidate)] = candidate

            maxlen_feat: int = max(len(feat) for feat in candidates_feat)
            candidates_feat_ = np.zeros((len(candidates_feat), maxlen_feat), dtype=np.long)
            for i, candidate_feat in enumerate(candidates_feat):
                candidates_feat_[i, :len(candidate_feat)] = candidate_feat

            # Convert them to torch tensors
            anaphor_ = torch.from_numpy(anaphor_).to(self.device)
            candidates_ = torch.from_numpy(candidates_).to(self.device)
            candidates_feat_ = torch.from_numpy(candidates_feat_).to(self.device)

            # Iterate over these tensors to collect results
            for i in range(0, len(candidates), self.batchsize):
                # number of instances in this batch is either i - i+bs; or i + <whatever is left>
                _from = i
                _to = min(i + self.batchsize, len(candidates))

                # Repeat the anaphor tensor
                x_ana = anaphor_.unsqueeze(0).repeat(_to - _from, 1)
                x_can = candidates_[_from: _to]
                x_feats = candidates_feat_[_from: _to]

                # Put them in a dict
                x = {'anaphor': x_ana, 'candidate': x_can, 'features': x_feats}

                # Pass it through the model
                _results = self.model_predict(x)

                results.append(_results)

            # Make one tensor out of results
            return torch.cat(results, dim=0)

    def predict(self, datum: BridgingInstance, src: str, candidate_span_ids: List[List[int]],
                candidate_span_txt: List[List[str]], candidate_span_h_ids: List[List[int]],
                candidate_span_h_txt: List[List[str]]) -> Union[List[int], np.ndarray]:

        """ Extract artefacts to process based on use_heads and candidate_src flags."""
        anaphor = datum.anaphor_ if not self.use_heads else datum.anaphor_h_
        anaphor_span = datum.anaphor if not self.use_heads else datum.anaphor_h
        candidates = candidate_span_txt if not self.use_heads else candidate_span_h_txt
        candidates_span = candidate_span_ids if not self.use_heads else candidate_span_h_ids

        anaphor, candidates, candidates_feat = TrainableInstanceIterator.prepare_one_instance(
            instance=datum, anaphor=anaphor, candidates=candidates, anaphor_span=anaphor_span,
            candidates_span=candidates_span, vocabularize=self.vocabularizer,
            salience_feat_extractor=self.salience_feat_extractor)

        results: torch.Tensor = self._predict_(anaphor=anaphor, candidates=candidates, candidates_feat=candidates_feat)

        # Interpreting the results. This is the contentious part. We are assuming that the label=1 is the correct label
        return results.cpu().detach().squeeze().numpy()


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--approach', '-a', type=str, default='oracle',
              help="Either 'oracle', or 'random'.")
@click.option('--candidates-src', '-cs', type=str, default='nounphrase',
              help="Either 'nounphrase' or 'antecedent'")
@click.option('--candidate-reduction-method', '-crm', default='none', type=str,
              help="The chosen method to reduce the number of candidates")
def main(approach: str, candidates_src: str, dataset: str, dataset_domain: str, dataset_domain_filename: str,
         candidate_reduction_method: str):
    dl = DataLoader(dataset, dataset_domain, dataset_domain_filename)

    if approach == 'oracle':
        eval_bench = OracleEvaluationBench(data_loader=dl, candidates_src=candidates_src,
                                           candidate_reduction_method=candidate_reduction_method)
    else:
        eval_bench = RandomEvaluationBench(data_loader=dl, candidates_src=candidates_src,
                                           candidate_reduction_method=candidate_reduction_method)

    print(eval_bench.run(save_output=True))


if __name__ == '__main__':
    main()
