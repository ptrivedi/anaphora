"""
    This file contains "EvaluationBench" things which can evaluate a given dataset for a given set of embeddings.
"""
import click
import numpy as np
import gensim.downloader as api
from gensim.models import KeyedVectors
from typing import List, Callable, Union, Iterable, Tuple

# Local imports
try:
    import _pathfix
except ImportError:
    from . import _pathfix
from dataloader import DataLoader
from config import LOCATIONS as LOC
from utils.bridging import BridgingInstance
from evaluate.base import BaseStaticEmbeddingBench


class WordEmbeddingBench(BaseStaticEmbeddingBench):

    def __init__(self, data_loader: Union[DataLoader, Iterable], source: str, candidates_src: str,
                 candidate_reduction_method: str, pooling: str = 'mean', similarity: str = 'cos',
                 metrics: Tuple[str] = ('acc', 'mrr', 'mr', 'ent_f1')):
        """
        :param data_loader: a dataloader object
        :param source: a string indicating which embedding to use. Can be 'wordtovec', or 'glove' or something else
        :param pooling: the method to get a fixed len (1, D) vector from a (n, D) vector.
        :param similarity: a way to compute a scalar (1, ) from two (1, D), (1, D) vectors.
        """
        super().__init__(data_loader=data_loader, candidates_src=candidates_src, metrics=metrics,
                         candidate_reduction_method=candidate_reduction_method)

        if source not in ['wordtovec', 'random', 'glove']:
            raise NotImplementedError(f"No way to vectorize text using {source}")

        if pooling not in ['mean', 'sum']:
            raise NotImplementedError(f"No way to pool_heads vectors using {pooling}")

        if similarity not in ['cos']:
            raise NotImplementedError(f"No way to compute similarity between two vectors using {similarity}")

        # self.ABC_ indicates the raw string, self.ABC indicates the callable
        self.embed_: str = source
        self.pooling_: str = pooling
        self.similarity_: str = similarity

        if self.embed_ == 'wordtovec':
            self.vectors = KeyedVectors.load_word2vec_format(LOC.wordtovec, binary=True)
            self.embed: Callable = self._gensim_embeddings_
        elif self.embed_ == 'glove':
            self.vectors = api.load("glove-wiki-gigaword-50")
            self.embed: Callable = self._gensim_embeddings_
        elif self.embed_ == 'random':
            self.vectors = None

        self.pool: Callable = {'mean': self._pool_mean_, 'sum': self._pool_sum_}[self.pooling_]

    def _gensim_embeddings_(self, tokens: List[str]) -> (np.ndarray, List[str]):
        """ Input is list of tokens, output is a list of ndarrays, and a set of strings (indicating the unknowns)"""
        vecs: List[np.ndarray] = []
        unks: List[str] = []
        for token in tokens:
            try:
                if self.embed_ == 'glove':
                    vecs.append(self.vectors[token.lower()])
                elif self.embed_ == 'wordtovec':
                    vecs.append(self.vectors[token])
            except KeyError:
                unks.append(token)
                if self.embed_ == 'wordtovec':
                    vecs.append(self.vectors['UNK'])
                elif self.embed_ == 'glove':
                    vecs.append(np.zeros_like(self.vectors['the']))

        return np.asarray(vecs), set(unks)

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            use self.embed to get vectors and unknowns
            get (n, D) vector for src
            pool_heads to get a (1, D) for src
            get (n, D) vector for each candidate
            pool_heads to get a (1, D) vector for each candidate
            compute similarity for each.
            return similarity (as logits)

        :return: similarity array (one float for each candidate)
        """

        anaphor, _ = self.embed(datum.anaphor_)  # (nw, D)
        anaphor = self.pool(anaphor)  # (1, D)
        candidate_vecs = [self.embed(candidate)[0] for candidate in candidate_span_txt]     # [ (nw, D), ... ]
        candidate_vecs = [self.pool(candidate) for candidate in candidate_vecs]             # [ (1, D), ... ]

        similarity = np.asarray([self.similarity(anaphor, can) for can in candidate_vecs])
        return similarity


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--candidates-src', '-cs', type=str, default='nounphrase',
              help="Either 'nounphrase' or 'antecedent'")
@click.option('--candidate-reduction-method', '-crm', default='none', type=str,
              help="The chosen method to reduce the number of candidates")
def main(candidates_src: str, dataset: str, dataset_domain: str, dataset_domain_filename: str,
         candidate_reduction_method: str):
    dl = DataLoader(dataset, dataset_domain, dataset_domain_filename)

    source = 'glove'

    eval_bench = WordEmbeddingBench(data_loader=dl, candidates_src=candidates_src, source=source,
                                    candidate_reduction_method=candidate_reduction_method)

    print(eval_bench.run(dump_predictions=True, approach_nm=source))


if __name__ == "__main__":
    _pathfix.suppress_unused_import()
    main()
