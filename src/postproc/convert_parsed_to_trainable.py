"""
    This file takes preprocessed datasets, and transformers them like so
    - specific to a candidate reduction method
    - specific to a ranking scheme: pointwise/pairwise
    - store individual data points in a file which can be easily called and iterated over;

    # Pointwise
    One data point is:
        <anaphor text, tokenized> <antecedent text, tokenized> <correct/incorrect label - 0/1>

    # Pairwise
    One data point is:
        <anaphor text, tokenized> <correct antecedent text, tokenized> <incorrect antecedent text, tokenized>

    # Usage
    by command line -
        python src/postproc.py -d crac -dd arrau -ddf Trains_91.CONLL -m pointwise -crm none

    by function call
    convert(dataloader=dl, method='pointwise', candidate_reduction_method='first_and_two_sents')
    // this will dump it to disk
"""
from typing import Union, List

# Local imports
try:
    import _pathfix
except (ModuleNotFoundError, ImportError):
    from . import _pathfix
from dataloader import DataLoader, WrappedCVDL

def convert(dataloader: Union[DataLoader, WrappedCVDL], method: str, candidate_reduction_method: str = None):
    """

    :param dataloader:
    :param method:
    :param candidate_reduction_method:
    :return:
    """


if __name__ == '__main__':
    _pathfix.suppress_unused_import()