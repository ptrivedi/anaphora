"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    This file, given a particular dataset will mine wordnet edges from it using WSD methods in utils.word_sense
    The mined edges are then stored as triples.
"""
import click
from pathlib import Path
from tqdm.auto import tqdm
from typing import Dict, Set, List, Optional

try:
    import _pathfix
except (ModuleNotFoundError, ImportError):
    from . import _pathfix
from dataloader import DataLoader
from config import LOCATIONS as LOC
from utils.wordsense import PairwiseSynsetDisambiguation


def write_to_disk(data: Dict[str, set], directory: Path, filename: Optional[str] = None):
    """
        Write our data (repr. as a dict where subject is key and objects are a set of its value) to something like:

            spruce_bark_beetle.n.01,member_holonyms,dendroctonus.n.01
            lobster.n.02,member_holonyms,reptantia.n.01

    """

    if filename:
        path: Path = directory / filename
    else:
        path: Path = directory / 'triples.txt'

    try:
        assert path.exists()
    except AssertionError:
        if not path.parent.exists():
            path.parent.mkdir(parents=True)
            print(f"Created a new directory: {path.parent}")
        path.touch()
        print(f"Created a new file: {path}")

    dumping_data: List[str] = []

    for subj, objs in data.items():
        for obj in objs:
            triple = ','.join([subj.strip(), 'customrel', obj.strip()])
            dumping_data.append(triple)

    # Push it to disk
    with open(path, mode='a+', encoding='utf8') as f:
        f.writelines('\n')
        f.writelines('\n'.join(dumping_data))


def read_processed_file(outputdir: Path, ignore_upto: bool = False) -> (Dict[str, Set[str]], int):
    """ Read the file that was dumped to disk and create a dumped_edges file """
    edges: Dict[str, Set[str]] = {}

    with (outputdir / 'triples.txt').open('r', encoding='utf8') as f:
        for line in f.readlines():

            if line.strip() == '':
                continue

            s, _, o = line.strip().split(',')
            edges.setdefault(s, set()).add(o)

    if ignore_upto:
        upto = -1
    else:
        with (outputdir / 'upto.txt').open('r', encoding='utf8') as f:
            upto = int(f.read().strip())

    return edges, upto


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--fallbacks', '-f', type=str, multiple=True, default=('spacy-lemmatize', 'w2v-closest', 'unk'),
              help="The fallback strategies. Sequence matters. Check src/utils/wordsense.py for all options.")
@click.option('--resume', is_flag=True,
              help="In case you want to continue from where we left off, raise this flag.")
def run(fallbacks: List[str], dataset: str, dataset_domain: str = None, dataset_domain_filename: str = None,
        resume: bool = False):
    """
        For now, we do not have code to include vanilla wordnet. Will add this later.
    """

    print(dataset, dataset_domain, dataset_domain_filename, resume, fallbacks)

    dataloader = DataLoader(dataset, dataset_domain, dataset_domain_filename)
    outputdir: Path = LOC.other / dataloader.dir_suffix

    # Make a wsd object
    wsd = PairwiseSynsetDisambiguation(fallbacks=fallbacks)

    # Declare the list where we keep mined edges.
    edges: Dict[str, Set[str]] = {}

    if resume:
        dumped_edges, upto = read_processed_file(outputdir)
        print(f"Resuming the process of mining at {outputdir}, on iteration {upto}")
    else:
        # Remove the data we have mined so far
        (outputdir / 'triples.txt').unlink(missing_ok=True)
        (outputdir / 'upto.txt').unlink(missing_ok=True)
        # Init these two vars
        dumped_edges: Dict[str, Set[str]] = {}
        upto = 0

    for i, instance in enumerate(tqdm(dataloader, total=len(dataloader))):

        if i <= upto:
            continue

        w_ana = instance.anaphor_h[0]
        w_ant = instance.antecedent_h[0]
        s_ana, s_ant = wsd.wsd(w_ana, w_ant, instance.document, instance.pos)

        if s_ana not in dumped_edges or w_ana not in dumped_edges[s_ana]:
            edges.setdefault(s_ana, set()).add(s_ant)

        # Dump to disk at every 1000 instances
        if i % 1000 == 0:

            for s, objs in edges.items():

                dumped_objs = dumped_edges.get(s, set())
                dumped_objs.update(objs)
                dumped_edges[s] = dumped_objs

            # Do the dump; also note the # instance to which we did the mining.
            write_to_disk(edges, directory=outputdir)
            with (outputdir / 'upto.txt').open('w+', encoding='utf8') as f:
                f.write(str(i))

            # Reset edges
            edges: Dict[str, Set[str]] = {}

            print(f'Dumped edges at iteration: {i}')

    # Finally dump the rest of the edges to disk as well
    write_to_disk(edges, directory=outputdir)


if __name__ == '__main__':
    _pathfix.suppress_unused_import()

    run()
