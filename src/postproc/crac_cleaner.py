"""
    A collection of things which can clean up files from CRAC datasets.
    We have limited functionalities here. Keep in mind that we only expect to use this AT runtime,
        not while preprocessing since the thing isn't cent percent correct yet. Some spans will be gone.
        Other things will be fishy. C'est la vie.
"""
import re

import numpy as np
import spacy
from tqdm.auto import tqdm
from pathlib import Path
from typing import Optional, List, Union

try:
    import _pathfix
except (ModuleNotFoundError, ImportError):
    from . import _pathfix
from utils.nlp import NullTokenizer
from evaluate.base import BaseEvaluationBench
from utils.bridging import BridgingInstance, to_toks

SWITCHBOARD_STOPWORDS: List[List[str]] = [
    ['uh', ','],
    [',', 'uh'],
    ['you', 'know'],
    ['you', 'know', ','],
    ['Yeah', '.'],
    ['.', 'Yeah'],
    ['Yeah', ','],
    ['Uh', ','],
    ['Uh-huh'],
    ['the', ',']
]
SWITCHBOARD_STOPWORDS: List[str] = [" ".join(k) for k in SWITCHBOARD_STOPWORDS]


def clean_document(tokens: List[str], repetitive_word_2: List[List[str]] = None) -> Union[str, List[List[str]]]:
    """
        document: List[List[str]] = format_input(instance.document, return_sentences=True)
        document: str = format_input(instance.document, return_sentences = False)
    """
    text = " ".join(tokens)
    current_text = ''
    flag = True  # it flag is True the string would be manipulated
    while flag:
        for word in SWITCHBOARD_STOPWORDS:
            text = text.replace(word.lower(), "")
            text = text.replace(', ,', ',')
            text = text.replace(', .', '.')
            text = text.replace('. ,', '.')
            text = re.sub("\s\s+", " ", text)  # removes extra spaces
        if current_text == text:
            flag = False
        else:
            current_text = text

    text = text.strip()
    text_length = len(text.split(" "))

    # now comes replacing repetative words

    # case 1: Hi how how are you doing
    repetative_word_pattern = []
    split_text = text.split(" ")
    split_text_len = len(split_text)
    for index, word in enumerate(split_text):
        current_word = word
        if split_text_len > index + 1:
            next_word = split_text[index + 1]
        else:
            next_word = "***"
        if current_word == next_word:
            repetative_word_pattern.append([f"{current_word} {next_word}", f"{word}"])

    for w_pattern, w_replace in repetative_word_pattern:
        text = text.replace(w_pattern, w_replace)

    # case 2: Hi how are how are you doing -> Hi (test for this specific input)
    if not repetitive_word_2:
        repetative_word_pattern = []
        split_text = text.split(" ")
        split_text_len = len(split_text)
        for index, word in enumerate(split_text):
            current_word = word
            if split_text_len > index + 1:
                next_word = split_text[index + 1]
            else:
                next_word = "***"
            if split_text_len > index + 2:
                next_next_word = split_text[index + 2]
            else:
                next_next_word = "***"

            if current_word == next_next_word:
                repetative_word_pattern.append(
                    [f"{current_word} {next_word} {next_next_word}", f"{current_word} {next_word}"])

    else:
        repetative_word_pattern = repetitive_word_2

    #     print(repetative_word_pattern)
    for w_pattern, w_replace in repetative_word_pattern:
        text = text.replace(w_pattern, w_replace)

    text = re.sub("\s\s+", " ", text)
    return text.strip(), repetative_word_pattern


def find_span(instance, current_span_location, debug: int = 0, check_for_assertion: bool = False) -> List[int]:
    """
        instance: object containing all kind of information
        current_span_location: [100,110]
    """

    # basic setup
    o_instance_tokens = to_toks(instance.document)

    format_o_instance, r_p = clean_document(o_instance_tokens)
    format_o_instance = format_o_instance.split(" ")

    o_start, o_end = current_span_location[0], current_span_location[1]
    o_span = o_instance_tokens[o_start:o_end]

    transformed_span, _ = clean_document(o_span, r_p)
    transformed_span = transformed_span.split(" ")

    if debug >= 1:
        print(f"original span: {o_span}")
        print(f"transformed span: {transformed_span}")

    tranformed_doc_till_transformed_span, _ = clean_document(o_instance_tokens[:o_end], r_p)
    tranformed_doc_till_transformed_span = tranformed_doc_till_transformed_span.split(" ")

    if debug >= 2:
        print(f"doc_till_transformed: {tranformed_doc_till_transformed_span}")

    u_start_index = len(tranformed_doc_till_transformed_span) - len(transformed_span)
    u_end_index = len(tranformed_doc_till_transformed_span)

    if check_for_assertion:
        assert format_o_instance[:u_end_index] == tranformed_doc_till_transformed_span

    if format_o_instance[u_start_index:u_end_index] != transformed_span:
        # back off one instance:
        # for instance cand: some sort of --> whole document just some , some sort of --> just , sort of
        transformed_span, _ = clean_document(o_instance_tokens[o_start - 3:o_end], r_p)
        transformed_span = transformed_span.split(" ")[2:]
    #         print("**")
    #         print(transformed_span)
    #         print(format_o_instance[u_start_index:u_end_index])

    if format_o_instance[u_start_index:u_end_index] != transformed_span:
        # back off one instance:
        # for instance cand: some sort of --> whole document just some , some sort of --> just , sort of
        transformed_span, _ = clean_document(o_instance_tokens[o_start - 2:o_end], r_p)
        transformed_span = transformed_span.split(" ")[1:]
    #         print("**")
    #         print(transformed_span)
    #         print(format_o_instance[u_start_index:u_end_index])

    if format_o_instance[u_start_index:u_end_index] != transformed_span:
        # back off one instance:
        # for instance cand: some sort of --> whole document just some , some sort of --> just , sort of
        transformed_span, _ = clean_document(o_instance_tokens[o_start - 1:o_end], r_p)
        transformed_span = transformed_span.split(" ")
    #         print("**")
    #         print(transformed_span)
    #         print(format_o_instance[u_start_index:u_end_index])

    if check_for_assertion:
        assert format_o_instance[u_start_index:u_end_index] == transformed_span

    if transformed_span != format_o_instance[u_start_index:u_end_index]:
        return [-1, -1]

    return [u_start_index, u_end_index]


def split_by_full_stop(tokens: List[str]):
    indices = [i for i, x in enumerate(tokens) if x == "."]
    new_sentences = []
    for index, i in enumerate(indices):
        if index == 0:
            new_sentences.append(tokens[:i + 1])
        elif index == len(indices) - 1:
            new_sentences.append(tokens[i:])
        else:
            new_sentences.append(tokens[i + 1:indices[index + 1] + 1])
    return new_sentences


def split_by_full_stop(tokens: List[str]):
    # temp_list = [i + '.' for i in "%%**".join(tokens).split('.') if i != '']

    temp_list = []
    for index, i in enumerate("%%**".join(tokens).split('.')):
        if index == len("%%**".join(tokens).split('.')) - 1:
            temp_list.append(i)
        elif i != '':
            temp_list.append(i + '.')

    temp_list = [i.split('%%**') for i in temp_list]
    new_sentence = []
    for t in temp_list:
        _temp = []
        for i in t:
            if i != '':
                _temp.append(i)
        new_sentence.append(_temp)
    new_sentence = [i for i in new_sentence if i]
    return new_sentence


def get_heads(document, span) -> List[int]:
    root = document[span[0]: span[1]].root
    assert root.i >= 0
    return [root.i, root.i + 1]


def get_text(document, span) -> List[str]:
    return to_toks(document)[span[0]: span[1]]


def clean(instance: BridgingInstance, nlp, dataset: str, dataset_domain: Optional[str] = None):
    """
        Return instance untouched if some other dataset than the ones we've implemented is called.
    """

    if dataset not in ['crac']:
        return instance

    if dataset_domain and dataset_domain not in ['switchboard']:
        return instance

    instance.anaphor = find_span(instance, instance.anaphor)
    instance.antecedent = find_span(instance, instance.antecedent)
    instance.candidate_nounphrase_ids = [find_span(instance, span) for span in instance.candidate_nounphrase_ids]
    instance.candidate_antecedent_ids = [find_span(instance, span) for span in instance.candidate_antecedent_ids]
    document, _ = clean_document(to_toks(instance.document))
    instance.document = split_by_full_stop(tokens=document.split(" "))

    # First, drop the candidate spans which have not been found, and re-find the correct span
    ignore_candidate_nounphrase_idx = [i for i, candidate in enumerate(instance.candidate_nounphrase_ids)
                                       if -1 in candidate]
    ignore_candidate_antecedent_idx = [i for i, candidate in enumerate(instance.candidate_antecedent_ids)
                                       if -1 in candidate]
    BaseEvaluationBench.pop_candidates(instance, np.asarray(ignore_candidate_nounphrase_idx), 'nounphrase')
    BaseEvaluationBench.pop_candidates(instance, np.asarray(ignore_candidate_antecedent_idx), 'antecedent')

    # Now we need heads
    # NLPify the thing
    document_ = nlp(to_toks(instance.document))
    instance.anaphor_h = get_heads(document_, instance.anaphor)
    instance.antecedent_h = get_heads(document_, instance.antecedent)
    instance.candidate_nounphrase_h_ids = [get_heads(document_, span) for span in instance.candidate_nounphrase_ids]
    instance.candidate_antecedent_h_ids = [get_heads(document_, span) for span in instance.candidate_antecedent_ids]

    # Get Txt of things
    instance.anaphor_ = get_text(instance.document, instance.anaphor)
    instance.anaphor_h_ = get_text(instance.document, instance.anaphor_h)
    instance.antecedent_ = get_text(instance.document, instance.antecedent)
    instance.antecedent_h_ = get_text(instance.document, instance.antecedent_h)
    instance.candidate_nounphrase_txt = [get_text(instance.document, span) for span in
                                         instance.candidate_nounphrase_ids]
    instance.candidate_nounphrase_h_txt = [get_text(instance.document, span) for span in
                                           instance.candidate_nounphrase_h_ids]
    instance.candidate_antecedent_txt = [get_text(instance.document, span) for span in
                                         instance.candidate_antecedent_ids]
    instance.candidate_antecedent_h_txt = [get_text(instance.document, span) for span in
                                           instance.candidate_antecedent_h_ids]

    # Finalise this instance to take care of other weirdness that we may have induced
    instance.finalise()

    return instance


if __name__ == '__main__':
    from dataloader import DataLoader

    _pathfix.suppress_unused_import()
    dl = DataLoader('crac', 'switchboard')
    nlp = spacy.load('en_core_web_sm')
    nlp.tokenizer = NullTokenizer(nlp.vocab)
    # for instance_no, instance in enumerate(tqdm(dl)):
    #     print(len(instance.candidate_nounphrase_ids))
    #     cleaned = clean(instance=instance, nlp=nlp, dataset='crac')
    #     print(len(cleaned.candidate_nounphrase_ids))

    class WrappedInstances(list):
        """
        Overwriting a dict to give it metadata enough to pass though the evaluation classes without throwing errors.
            The usage is tricky.
            Do this and nothing else:

            dl = DataLoader(...whatever...)
            dataset = WrappedInstances(dl.dataset, dl.dir, dl.fnames, dl.dataset_domain, dl.dataset_domain_filename,
                                       split_instances['test'])
        """

        def __init__(self, dataset: str, datasetdir: Path, fnames: List[str], dataset_domain: Optional[str] = None,
                     dataset_domain_filename: Optional[str] = None, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.dataset: str = dataset
            self.dataset_domain: Optional[str] = dataset_domain
            self.dataset_domain_filename: Optional[str] = dataset_domain_filename
            self.dir: Path = datasetdir
            self.fnames: List[str] = fnames

    cleaned_instances = []
    # How to iterate through the dataset
    for instance in tqdm(dl):
        cleaned_instances.append(clean(instance, nlp=nlp, dataset=dl.dataset, dataset_domain=dl.dataset_domain))

    # Wrap the instances in the wrapped dl thing
    wdl = WrappedInstances(dl.dataset, dl.dir, dl.fnames, dl.dataset_domain, dl.dataset_domain_filename, cleaned_instances)

    from evaluate.base import OracleEvaluationBench, RandomEvaluationBench
    from evaluate.wordembeddings import WordEmbeddingBench
    from evaluate.contextualmodels import BertSelfAttentionBench

    candidates_src = 'nounphrase'
    candidate_reduction_method = 'none'
    use_heads = False
    device = 'cpu'

    # Prep an approach
    oracle = OracleEvaluationBench(data_loader=wdl, candidates_src='nounphrase', candidate_reduction_method=candidate_reduction_method)
    print(oracle.run())

    random = RandomEvaluationBench(wdl, candidates_src='nounphrase', candidate_reduction_method=candidate_reduction_method)
    print(random.run())

    wordtovec = WordEmbeddingBench(wdl, candidates_src='nounphrase', source='wordtovec',
                                   candidate_reduction_method=candidate_reduction_method)
    print(wordtovec.run())
    del wordtovec

    glove = WordEmbeddingBench(wdl, candidates_src='nounphrase', source='glove',
                               candidate_reduction_method=candidate_reduction_method)
    print(glove.run())
    del glove

    spanbert = BertSelfAttentionBench(wdl, candidates_src=candidates_src,
                                      model='SpanBERT/spanbert-base-cased',
                                      max_len=400, pool_layers='5', pool_heads="best-max", pool_span="max",
                                      candidate_reduction_method=candidate_reduction_method, use_heads=use_heads,
                                      device=device)
    print(spanbert.run())
    del spanbert

    bert = BertSelfAttentionBench(wdl, candidates_src=candidates_src, model='bert-base-uncased',
                                  max_len=400, pool_layers='11', pool_heads="max", pool_span="max",
                                  candidate_reduction_method=candidate_reduction_method, use_heads=use_heads)
    print(bert.run())
    del bert