import click
import numpy as np
from pathlib import Path
from typing import List, Dict, Set

try:
    import _pathfix
except (ModuleNotFoundError, ImportError):
    from . import _pathfix
from config import LOCATIONS as LOC
from postproc.mine_wordnet_edges import read_processed_file


def write_to_disk(data: List[str], filename: Path):

    path: Path = filename

    # Push it to disk
    with open(path, mode='a+', encoding='utf8') as f:
        f.writelines('\n'.join(data))


@click.command()
@click.option('--dataset', '-d', type=str, multiple=True, help="the name of a folder inside ~/data/other e.g. bashi.")
def split(dataset: List[str]):
    """
        For each passed dataset, pull the triples file.
        Interpret the triples back to dict.
        Combine the dicts.

        Linearize the dict into list of triples
            Ignore all the __na__ things
        Randomly dump 0.7 as train, 0.15 as valid and 0.15 as test

    :param dataset: iterable of strings ['bashi', 'wikitext']
    :return:
    """

    mined: Dict[str, Set[str]] = {}

    for _dataset in dataset:
        _processed, _ = read_processed_file(Path(LOC.other / _dataset), ignore_upto=True)

        # Combine it into the `mined` dict
        for s, objs in _processed.items():

            known_objs_for_this_s = mined.get(s, set())
            known_objs_for_this_s.update(objs)
            mined[s] = known_objs_for_this_s

    # Now we have a merged dict. Linearize it into train, test and valid
    mined_list: List[str] = []

    for subj, objs in mined.items():
        for obj in objs:
            triple = ','.join([subj.strip(), 'customrel', obj.strip()])
            mined_list.append(triple)

    # Split it into train, test, valid
    outputdir: Path = LOC.other / '_'.join(dataset)
    outputdir.mkdir(exist_ok=True)
    dl = len(mined_list)
    np.random.shuffle(mined_list)
    train, test, valid = mined_list[: int(dl*0.7)], mined_list[int(dl*0.7): int(dl*0.85)], mined_list[int(dl*0.85):]

    write_to_disk(train, outputdir / 'train.txt')
    write_to_disk(valid, outputdir / 'valid.txt')
    write_to_disk(test, outputdir / 'test.txt')


if __name__ == '__main__':
    split()
