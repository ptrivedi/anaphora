"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    This file is responsible for marking senses for a given span in text.
    A common use case is to disambiguate pairwise, i.e. given anaphor and antecedent spans,
        find a combination of senses most likely to fit both. - class PairwiseSenseDisambiguation
"""
import json
import torch
import spacy
import pickle
import warnings
import numpy as np
from os import path
from tqdm import tqdm
from pathlib import Path
from spacy import tokens
from nltk.corpus import wordnet
from gensim.models import KeyedVectors
from mytorch.utils.goodies import Timer
from nltk.corpus.reader.wordnet import WordNetError
from typing import Iterable, Dict, Callable, Optional, List, Tuple, Union, Set

# Local imports
try:
    import _pathfix
except (ModuleNotFoundError, ImportError):
    from . import _pathfix
from utils.misc import sim_tanimoto, sim_cos
from config import LOCATIONS as LOC, NLTK_PATH
from utils.bridging import get_dataset_from_docname
from utils.nlp import NullTokenizer, to_toks, to_hash

import nltk

nltk.data.path.append(NLTK_PATH)  # TODO: look for a permanent fix


def pos_conv(tag: str) -> Optional[str]:
    """ Conv a pos tag from the spacy nomenclature to the wordnet nomenclature. Return none if no equivalent pos tag."""

    # Some hacks
    if tag in ['PROPN']:
        tag = 'NOUN'

    try:
        sense = getattr(wordnet, tag)
    except AttributeError:
        sense = None

    return sense


class KnowbertSynsetDisambiguation:
    def __init__(self, sense_vectors: Optional[np.ndarray] = None, sense_vocab: Optional[Dict[str, int]] = None,
                 use_heads: bool = False):
        """
        if use_heads is set to True, will return synsets from the mention's head
        """
        if not path.exists(LOC.knowbert_wsd) or not path.isdir(LOC.knowbert_wsd):
            raise AssertionError("Knowbert wsd pkl files are not cached")
        # keys are dataset, values are dicts w/ keys (docname, span_start, span_end) (spans are for full mentions),
        # synsets are in (mention substring, synset list) tuples
        self.use_heads = use_heads
        self.knowbert_synsets = {}
        for dataset in ["ami", "isnotes", "bashi", "light", "persuasion", "switchboard", "trains91", "trains93"]:
            with open(LOC.knowbert_wsd / Path(f"{dataset}_knowbert_wsd.pkl"), 'rb') as f:
                self.knowbert_synsets[dataset] = pickle.load(f)

        if sense_vectors is None or sense_vocab is None:
            # Load the enttoid, idtovec of WordNet embeddings. Each vector corresponds to a synset, here.
            with (LOC.wordnetdir / 'entoid.json').open('r', encoding='utf8') as fp:
                self.enttoid: Dict[str, int] = json.load(fp)
                self.idtoent: Dict[int, str] = {v: k for k, v in self.enttoid.items()}

            self.idtovec: np.ndarray = torch.load(LOC.wordnetdir / 'model.torch',
                                                  map_location=torch.device('cpu'))['init_embed'].cpu().detach().numpy()
        else:
            self.idtovec: np.ndarray = sense_vectors
            self.enttoid: Dict[str, int] = sense_vocab

    def embed(self, key: str) -> np.ndarray:
        """ Get id from enttoid and get vec from idtovec """
        try:
            return self.idtovec[self.enttoid[key]].reshape(1, -1)
        except KeyError:
            # warnings.warn(f"No embedding for found {key}")
            return self.idtovec[self.enttoid['__na__']].reshape(1, -1)

    def embed_average(self, keys: List[str]) -> np.ndarray:
        """ takes a list of synsets and returns the average of their embeddings"""
        if len(keys) == 0:
            return self.embed('__na__')
        avg_vector = None
        num_valid = 0
        for i in range(len(keys)):
            if keys[i] in self.enttoid:
                if avg_vector is None:
                    avg_vector = self.embed(keys[i])
                else:
                    avg_vector += self.embed(keys[i])
                num_valid += 1
        if num_valid == 0:
            return self.embed('__na__')
        return avg_vector / num_valid

    def wsd(self,
            docname: str,
            span: List[int],
            dataset: Optional[str] = None,
            bashi_docnames: Optional[Union[Set, List]] = None,
            head_words: Optional[List[str]] = None) \
            -> List[str]:
        """
        :param docname: the document name of the instance
        :param span: the span of the full mention
        :param dataset: optional, the name of the dataset. if not passed, tries to figure it out from the docname, but
            if so, the names of the bashi docnames must be passed to be able to differentiate isnotes and bashi.
            (in ["ami", "isnotes", "bashi", "lights", "persuasion", "switchboard", "trains91", "trains93"])
        :param bashi_docnames: optional, the names of the bashi docname (in order to differentiate isnotes and bashi
            docnames. Only needed if no dataset param passed.
        :param head_words: optional, the words in the head of the mention (if only using head synsets)
        :return: the list of str corresponding to a senses
        """
        # retrieve knowbert synsets
        if dataset is None:
            dataset = get_dataset_from_docname(docname, bashi_docnames)
            if dataset == "arrau":
                if docname.startswith("Trains_91"):
                    dataset = "trains91"
                else:
                    dataset = "trains93"
        synsets = self.knowbert_synsets[dataset][(docname, span[0], span[1])]
        # filter by head, if needed
        if self.use_heads:
            head_words = [word.lower() for word in head_words]
            head_synsets = []
            for synset_words, ss in synsets:
                if any([word in head_words for word in synset_words.split(' ')]):
                    head_synsets += ss
            lemma_keys = [ss for ss in head_synsets]
        else:
            lemma_keys = [ss for tup in synsets for ss in tup[1]]
        final_synsets = []
        for lemma_key in lemma_keys:
            try:
                synset = wordnet.lemma_from_key(lemma_key).synset().name()
                final_synsets.append(synset)
            except WordNetError:
                warnings.warn(f"No lemma found for key {lemma_key}")
        return list(set(final_synsets))  # remove duplicates


# noinspection PyUnusedLocal
class PairwiseSynsetDisambiguation:

    # TODO: if there is a determiner in the phrase, restrict to only noun based pos.

    def __init__(self, wsd_strategy: str = 'closest', similarity: str = 'cos', nlp: Optional = None, pos: bool = False,
                 sense_vectors: Optional[np.ndarray] = None, sense_vocab: Optional[Dict[str, int]] = None,
                 always_fallback: bool = False, fallbacks: Iterable[str] = ('spacy-lemmatize', 'w2v-closest', 'unk')):
        """
        :param nlp: a spacy NLP object.
        :param similarity: a string indicating the method to compare two vectors.
        :param wsd_strategy: a str indicating the method we use to select the correct sense, or combination thereof,
            given a set of senses for a pair of words
        :param fallbacks: a string indicating the functions to be used in case no sense is found by a direct NLTK lookup
            These all change the given word, hoping that NLTK can find a sense for the new word.
        :param always_fallback: bool indicating whether we use fallback fns only when no sense is found via NLTK
        """
        # All implemented fallback mechanisms should be
        self._directory_fallbacks_: Dict[str, Callable] = {'w2v-closest': self._fallback_w2v_closest_,
                                                           'spacy-ner': self._fallback_spacy_ner_,
                                                           'spacy-lemmatize': self._fallback_spacy_lemmatize_,
                                                           'unk': self._fallback_unk_}

        '''
            Sanity check and store the arguments
        '''
        if wsd_strategy not in ['closest']:
            raise NotImplementedError(f"No way to select senses based on {wsd_strategy}")
        else:
            self.wsd_: str = wsd_strategy

        self.always_fallback_: bool = always_fallback
        self.filter_pos: bool = pos

        for fallback in fallbacks:
            if fallback not in self._directory_fallbacks_.keys():
                raise NotImplementedError(f"No way to fall back sense selection using using {fallback}")
        self.fallbacks_: List[str] = list(fallbacks)

        self.similarity_: str = similarity

        '''
            Store the vocab and vectors for synsets (needed for similarity)
        '''
        if sense_vocab is None or sense_vocab is None:
            # Load the enttoid, idtovec of WordNet embeddings. Each vector corresponds to a synset, here.
            with (LOC.wordnetdir / 'entoid.json').open('r', encoding='utf8') as fp:
                self.enttoid: Dict[str, int] = json.load(fp)
                self.idtoent: Dict[int, str] = {v: k for k, v in self.enttoid.items()}

            self.idtovec: np.ndarray = torch.load(LOC.wordnetdir / 'model.torch',
                                                  map_location=torch.device('cpu'))['init_embed'].cpu().detach().numpy()
        else:
            self.idtovec: np.ndarray = sense_vectors
            self.enttoid: Dict[str, int] = sense_vocab

        '''
            Setting up callables
        '''
        self.similarity: Callable = {'cos': sim_cos, 'tanimoto': sim_tanimoto}[self.similarity_]

        # Setting up resources for fallback mechanisms
        fallback_prefixes = [x.split('-')[0] for x in self.fallbacks_]
        if 'w2v' in fallback_prefixes:
            # We need w2v to find nearest words
            self.wordtovec = KeyedVectors.load_word2vec_format(LOC.wordtovec, binary=True)
        else:
            self.wordtovec = None

        fallback_prefixes = [x.split('-')[0] for x in self.fallbacks_]

        if 'spacy' in fallback_prefixes:
            # We need spacy to lemmatize, to do NER etc
            if not nlp:
                self.nlp = spacy.load('en_core_web_sm')
                self.nlp.tokenizer = NullTokenizer(self.nlp.vocab)
            else:
                self.nlp = nlp
        else:
            self.nlp = None

        # Init and then warm up cache
        self.cache_sync_at: int = 1000
        self.cache = {}
        self.cache_fid = None
        self.cache_n_updates = 0
        self.pull_cache()

    def pull_cache(self, loc: Path = LOC.pairwisewsdcache):
        """
            Try finding cache file with the same args as of this function.
            If directory does not exist:
                Create cache folder, create 0.pkl (empty dict), 0.meta (args of this class)
            If directory exists:
                Find all *meta files and choose the one with the same args as of this class.
                If found:
                    return fname, and its update self.cache with its contents
                If not found:
                    create <last fname>+1.pkl (empty dict), <last_fname>+1.meta (args of this class)

        :param loc: Path to folder with all caches
        :return: None
        """
        meta = {
            'wsd_strategy': self.wsd_,
            'similarity': self.similarity_,
            'pos': self.filter_pos,
            'always_fallback': self.always_fallback_,
        }

        if not loc.exists():
            # Make dir, make empty pickle file and make a meta file with same name.
            loc.mkdir(parents=True)

            with (loc / '0.json').open('w+') as f:
                json.dump(self.cache, f)

            with (loc / '0.meta').open('w+') as f:
                json.dump(meta, f)

            self.cache_fid = '0'

        else:
            # Get all meta files and find the one with the correct name

            correct_cache_fileid = -1
            last_cache_fileid = -1
            for fname in loc.iterdir():
                if fname.stem.isdigit():
                    last_cache_fileid = int(fname.stem) if int(fname.stem) > last_cache_fileid else last_cache_fileid

                if correct_cache_fileid < 0 and fname.suffix == '.meta':
                    # Try to check if this is the correct one
                    with fname.open('r') as f:
                        old_meta = json.load(f)

                    is_same = True
                    for k in meta:
                        if k in old_meta and meta[k] == old_meta[k]:
                            continue
                        else:
                            is_same = False

                    if is_same:
                        # We found the correct cache
                        correct_cache_fileid = int(fname.stem)

            # If we found the cache, great. Else create a new one
            if correct_cache_fileid >= 0:

                with open(str(loc / f'{correct_cache_fileid}.json'), 'r') as f:
                    self.cache = json.load(f)
                    self.cache_fid = correct_cache_fileid
                    print(f"Loaded {len(self.cache)} WSD instances from disk.")
            else:

                with (loc / (str(last_cache_fileid + 1) + '.json')).open('w+') as f:
                    json.dump(self.cache, f)

                with (loc / (str(last_cache_fileid + 1) + '.meta')).open('w+') as f:
                    json.dump(meta, f)

                self.cache_fid = last_cache_fileid

    def sync_cache(self, loc: Path = LOC.pairwisewsdcache):
        """ Assume pickle file to be outdated and just overwrite the file"""

        with (loc / (str(self.cache_fid) + '.json')).open('w') as f:
            json.dump(self.cache, f)

        print(f"WSD: Dumped {len(self.cache)} instances to disk.")

        self.cache_n_updates = 0

    @staticmethod
    def hash(position_a: int, position_b: int, context: List[List[str]],
             context_pos: List[List[str]] = None, n: int = 1) -> str:
        """ Return a string uniquely representing this data"""

        hashed_doc = to_hash(context)
        if context_pos is not None:
            hashed_pos = to_hash(context_pos)
        else:
            hashed_pos = 'None'

        return str(position_a) + str(position_b) + str(n) + hashed_doc + hashed_pos

    def _nltk_synsets_(self, word: str, pos: str) -> List[str]:
        """ if __na__: return ['__na__'] else return the name of NLTK synsets"""
        if word == '__na__':
            return ['__na__']

        if self.filter_pos:
            # Conv the tag to the nltk wordnet vocab
            pos = pos_conv(pos)
            if pos:
                return [synset.name() for synset in wordnet.synsets(word, pos=pos)]
            else:
                return [synset.name() for synset in wordnet.synsets(word)]
        else:
            return [synset.name() for synset in wordnet.synsets(word)]

    def _get_senses_(self, word: str, word_position: int, word_postag: str, context: List[List[str]]) \
            -> List[nltk.corpus.reader.wordnet.Synset]:
        """ Return a list of synset names using NLTK interface for this word, or its variants (based on fallbacks)"""
        synsets: List[str] = self._nltk_synsets_(word, pos=word_postag)

        """
            Fallbacks: if no sense is found, or if self.always_fallback
                we employ the fallbacks mechanism as indicated by self.fallbacks_
        """
        old_words: List[str] = [word]
        if self.always_fallback_ or len(synsets) == 0:
            for fn_name in self.fallbacks_:
                new_word: Optional[str] = self._directory_fallbacks_[fn_name](word=word,
                                                                              word_id=word_position,
                                                                              doc=context,
                                                                              synsets=synsets)

                # If the new word is None, continue
                if new_word is None:
                    continue

                # If the new word was already a word we've hunted synsets for, continue
                if new_word in old_words:
                    continue

                # Find the synsets for this word and append them to the list
                # NOTE: in doing so, we use the POS tag of the old word assuming that the new word would also have it.
                old_words.append(new_word)
                synsets: List[str] = list(set(synsets + self._nltk_synsets_(new_word, pos=word_postag)))

                # If we found at least one synsets, quit; unless always_fallback is ON, in which case, don't break
                if not self.always_fallback_ and len(synsets) > 0:
                    break

        # By this point, we've exhausted all fallbacks. We hope to have at least some synsets here.
        # In the worst case, there would be a '__na__' in the bin

        return synsets

    def embed(self, key: str) -> np.ndarray:
        """ Get id from enttoid and get vec from idtovec """
        try:
            return self.idtovec[self.enttoid[key]].reshape(1, -1)
        except KeyError:
            # warnings.warn(f"No embedding for found {key}")
            return self.idtovec[self.enttoid['__na__']].reshape(1, -1)

    # noinspection PyTypeChecker
    def _choose_closest_(self, sa: List[str], sb: List[str], n: int, *args, **kwargs) \
            -> List[Tuple[np.float, str, str]]:
        """
            sa <- set of synsets for word_main
            sb <- set of synsets for wb

            _sa* = max(similarity(_sa, _sb)) for all _sa in sa, _sb in sb

        TODO: If both sa and sb have a __na__ sense, and both of them ALSO have other senses,
            well then drop the __na__ sense from both?

        :param wa: str whose sense we need
        :param wb: str whose sense may be used
        :param sa: list of senses of wa
        :param sb: list of sense of sb
        :return: key representing the synset name for wa(key for self.entoid),
                key representing the synset name for wb
        """

        assert sa and sb, "No sense found for one or the other head."

        # Get vectors for all candidates_src
        sa_v: List[np.ndarray] = [self.embed(_sa) for _sa in sa]
        sb_v: List[np.ndarray] = [self.embed(_sb) for _sb in sb]

        # Get the scores for all candidates_src
        sim = np.zeros((len(sa), len(sb)), dtype=np.float)
        for i, _sa in enumerate(sa_v):
            for j, _sb in enumerate(sb_v):
                sim[i, j] = self.similarity(_sa, _sb)

        # Argsort it in descending order and get the first n instances.
        ranked_sim = np.argsort(-sim, axis=None)
        results: List[Tuple[np.float, str, str]] = []
        for i in range(min(n, len(ranked_sim))):
            i_, j_ = np.unravel_index(ranked_sim[i], shape=sim.shape)
            results.append([sim[i_, j_], sa[i_], sb[j_]])

        # Get the argmax; i -> index in sa, j -> index in sb
        # i_, j_ = np.unravel_index(np.argmax(sim, axis=None), shape=sim.shape)

        # Return the highest i (corresponding to word a
        return results

    def _choose_senses_(self, wa: str, wb: str, sa: List[str], sb: List[str], n: int) -> \
            (np.float, Optional[str], Optional[str]):
        """ Get a str corresponding with wa, and wb words based on which sense combination works better """
        if self.wsd_ == 'closest':
            return self._choose_closest_(wa=wa, wb=wb, sa=sa, sb=sb, n=n)
        else:
            raise NotImplementedError

    def _fallback_w2v_closest_(self, word: str, *args, **kwargs) -> Optional[str]:
        """ Give the nearest word for a given word. If word not in vocab, return None """
        if word not in self.wordtovec.vocab:
            return None
        return self.wordtovec.most_similar(word)[0][0]

    def _fallback_spacy_ner_(self, word_id: int, doc: List[List[str]], *args, **kwargs) -> Optional[str]:
        """ If the token is a entity (using default spacy ner), replace it with entity type """
        doc_: tokens.Doc = self.nlp(to_toks(doc))
        token: tokens.Token = doc_[word_id]
        if token.ent_type_:
            return token.ent_type_.lower()
        return None

    @staticmethod
    def _fallback_unk_(word: str, synsets: List[str], *args, **kwargs) -> Optional[str]:
        if len(synsets) > 0:
            return None
        return '__na__'

    def _fallback_spacy_lemmatize_(self, word_id: int, doc: List[List[str]], *args, **kwargs):
        """ Replace the word with its spacy lemma """
        doc_: tokens.Doc = self.nlp(to_toks(doc))
        token: tokens.Token = doc_[word_id]
        return token.lemma_

    def wsd(self,
            position_a: int,
            position_b: int,
            context: List[List[str]],
            context_pos: List[List[str]] = None,
            n: int = 1,
            return_scores: bool = False) \
            -> (List[Union[Tuple[str, float], str]], List[Union[Tuple[str, float], str]]):
        """
            Check if the args exist in cache.
                If so, return it.
                If not, do WSD, update cache, new counter and sync cache if needed before returning.
        """
        hashed_args = self.hash(position_a=position_a, position_b=position_b, context=context,
                                context_pos=context_pos, n=n)

        try:
            candidate_pairs = self.cache[hashed_args]
        except KeyError:

            candidate_pairs = self._wsd_(position_a=position_a, position_b=position_b, context=context,
                            context_pos=context_pos, n=n)
            self.cache[hashed_args] = candidate_pairs
            self.cache_n_updates += 1

            if self.cache_n_updates > self.cache_sync_at:
                self.sync_cache()

        # Process the op to return scores or not.
        sa, sb = [], []
        if not return_scores:
            for candidate_pair in candidate_pairs:
                if candidate_pair[1] not in sa:
                    sa.append(candidate_pair[1])
                if candidate_pair[2] not in sb:
                    sb.append(candidate_pair[2])
        else:
            for candidate_pairs in candidate_pairs:
                if not sa or (sa and candidate_pairs[1] not in [_s[1] for _s in sa]):
                    sa.append((candidate_pairs[0], candidate_pairs[1]))
                if not sb or (sb and candidate_pairs[2] not in [_s[1] for _s in sb]):
                    sb.append((candidate_pairs[0], candidate_pairs[2]))

        return sa, sb

    def _wsd_(self,
              position_a: int,
              position_b: int,
              context: List[List[str]],
              context_pos: List[List[str]] = None,
              n: int = 1) \
            -> (List[Union[Tuple[str, float], str]], List[Union[Tuple[str, float], str]]):
        """
            For a pair of words, we find all possible senses (individually),
                and compare them in a pairwise manner to see which two sense pairs fit the pair of words best.

        NOTE: while we need to context to be a list of sentences (list of words), the position should be a global span.
        e.g. context: [ [3 worded sentence], [11 worded sentence], [ w_14, WSD THIS WORD ] ...]; pos: 15

        :param n: number of candidates to return
        :param position_a: first word (str) whose sense we want to find.
        :param position_b: second  word (str) whose sense we want to find.
        :param context: the List of sentences (list of words) where both positions belong.
        :param context_pos: the pos tag corresponding to the list of sentences
        :return: the str corresponding to a sense for wa and another for wb
        """

        if self.filter_pos and context_pos is None:
            raise AssertionError("POS tags were expected but not passed")

        # Flatten the sentence structure
        flattened_context: List[str] = to_toks(context)
        flattened_postags: List[str] = to_toks(context_pos)
        if len(flattened_context) != len(flattened_postags):
            raise AssertionError("Length mismatch between postags and the document. "
                                 "Maybe some other instances' POS tags were passed?")

        word_a, word_b = flattened_context[position_a], flattened_context[position_b]
        postag_a, postag_b = flattened_postags[position_a], flattened_postags[position_b]

        # Get senses for both words individually
        senses_a: List[str] = self._get_senses_(word=word_a, word_position=position_a,
                                                context=context, word_postag=postag_a)
        senses_b: List[str] = self._get_senses_(word=word_b, word_position=position_b,
                                                context=context, word_postag=postag_b)

        # Find the most compatible pair
        candidate_pairs = self._choose_senses_(wa=word_a, wb=word_b, sa=senses_a, sb=senses_b, n=n)

        return candidate_pairs


if __name__ == '__main__':
    """ Demonstrate the usage here"""
    _pathfix.suppress_unused_import()

    from dataloader import DataLoader

    dl = DataLoader('crac', 'switchboard')
    wsd = PairwiseSynsetDisambiguation(always_fallback=False, pos=True)

    # Only return one candidate per span. Do not return scores
    for x in dl:
        print(wsd.wsd(x.anaphor_h[0], x.antecedent_h[0], x.document, x.pos, n=1))
        break
    # > (['plant.n.01'], ['fabrication.n.03'])

    # Only return one candidate per span. Return scores.
    # (The scores might be same for both sa, sb since the scores are assigned to a _pair_ of synsets
    for x in dl:
        print(wsd.wsd(x.anaphor_h[0], x.antecedent_h[0], x.document, x.pos, n=1, return_scores=True))
        break
    # > ([(0.21452459692955017, 'plant.n.01')], [(0.21452459692955017, 'fabrication.n.03')])

    # Return upto two candidates per span. Do not return scores
    for x in dl:
        print(wsd.wsd(x.anaphor_h[0], x.antecedent_h[0], x.document, x.pos, n=2))
        break
    # > (['plant.n.01', 'plant.n.02'], ['fabrication.n.03'])

    # Return upto two candidates per span. Return scores.
    for x in dl:
        print(wsd.wsd(x.anaphor_h[0], x.antecedent_h[0], x.document, x.pos, n=2, return_scores=True))
        break
    # > ([(0.21452459692955017, 'plant.n.01'), (0.12381801009178162, 'plant.n.02')],
    # >  [(0.21452459692955017, 'fabrication.n.03')])

    # What happens when no synsets are found
    for x in dl:
        if x.anaphor_id == 31:
            print(wsd.wsd(x.anaphor_h[0], x.antecedent_h[0], x.document, x.pos, n=2, return_scores=True))
        else:
            continue
    # > ([(0.11627403646707535, '__na__')],
    # >  [(0.11627403646707535, 'apply.v.03'), (0.10591042786836624, 'practice.v.04')])

    # How to get embeddings for a synset?
    print(wsd.embed("majority.n.03"))
    # or
    output = []
    for x in dl:
        output = wsd.wsd(x.anaphor_h[0], x.antecedent_h[0], x.document, x.pos, n=2, return_scores=True)
        break
    anaphor_synsets, antecedent_synsets = output[0], output[1]
    print(wsd.embed(anaphor_synsets[0][1]))
    # > <a numpy array of 200 dims>

    # Seeing how the cache works
    for i, x in enumerate(tqdm(dl)):
        for can in x.candidate_nounphrase_h_ids:
            wsd.wsd(x.anaphor_h[0],can[0], x.document, x.pos, n=1, return_scores=False)

