"""
    This is an assortment of frequently used functions, which use text/operate over text in some form.
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr
"""
import re
from hashlib import md5
from spacy.tokens import Doc
from typing import Optional, List, Tuple
from dataclasses import dataclass, field

try:
    from spacy.util import DummyTokenizer
except ImportError:
    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    class DummyTokenizer:
        def __call__(self, text):
            raise NotImplementedError

        def pipe(self, texts, **kwargs):
            for text in texts:
                yield self(text)

        # add dummy methods for to_bytes, from_bytes, to_disk and from_disk to
        # allow serialization (see #1557)
        def to_bytes(self, **kwargs):
            return b""

        def from_bytes(self, _bytes_data, **kwargs):
            return self

        def to_disk(self, _path, **kwargs):
            return None

        def from_disk(self, _path, **kwargs):
            return self

from . import _pathfix
from utils.misc import pop, NoRegexMatchError, MultipleRegexMatchError, check_overlap


@dataclass
class AnnotationBlock:
    """
        Dataclass used exclusively for searching for a pos tag in raw ONF ontonotes file.
        This dataclass represents one annotation / pair of brackets.

        E.g. when searching for NPs in
            (NP (CD six)
                (NNS years))

        an onf block may look like
        start = _, end = _+2, w

    """

    start: int
    words: List[str]
    tag: str

    end: int = field(default_factory=int)

    open_inside: int = 0
    closed_inside: int = 0

    # Optional field for more text
    metadata: str = field(default_factory=str)

    def finalise(self):
        assert self.open_inside == self.closed_inside, \
            f"Bracket Mismatch. Open: {self.open_inside}, Closed: {self.closed_inside}"

        assert self.end >= self.start
        if self.start == self.end:
            self.end += 1

    @property
    def span(self) -> List[int]:
        return [self.start, self.end]


class AnnotationBlockStack:
    """
        A collection of aforementioned OnfBlocks
    """

    def __init__(self):
        self.blocks: List[AnnotationBlock] = []

    def append(self, block: AnnotationBlock) -> None:
        self.blocks.append(block)

    def register_word(self, word: str) -> None:
        for block in self.blocks:
            block.words.append(word)

    def add(self, n: int = 1) -> None:
        for block in self.blocks:
            block.open_inside += n

    def sub(self, n: int, span: int) -> Optional[List[AnnotationBlock]]:
        """
            Note that n bracket ended.
            Iteratively add one closed brackets to all blocks.
            If at any point open brackets == closed brackets,
                mark that block as finished and pop it
        """

        to_pop: List[int] = []

        for i in range(n):

            # Close one bracket in all blocks
            for block_id, block in enumerate(self.blocks):

                # If the block is already marked as finished, don't do anything to it
                if block_id in to_pop:
                    continue

                assert block.open_inside >= block.closed_inside

                block.closed_inside += 1

                if block.open_inside == block.closed_inside:
                    # All open brackets are closed
                    block.end = span + 1
                    to_pop.append(block_id)

        if not to_pop:
            return None

        # Pop these elements and return
        return pop(data=self.blocks, ids=to_pop)


def find_one(pattern: re.Pattern, data: str) -> str:
    """ Syntactic sugar to use a compiled re expression over a string. """

    raw = pattern.findall(data)

    if len(raw) > 1:
        raise MultipleRegexMatchError(f"{len(raw)} matches. Pattern: `{pattern.pattern}` Src: {data}")

    if len(raw) < 1:
        raise NoRegexMatchError(f"{len(raw)} matches. Pattern: `{pattern.pattern}` Src: {data}")

    return raw[0]


def find_all(pattern: str, data: str):
    """Yields all the positions of
    the pattern p in the string s."""
    return [a.start() for a in list(re.finditer(pattern, data))]


def to_toks(doc: List[List[str]]) -> List[str]:
    """ [ ['a', 'sent'], ['another' 'sent'] ] -> ['a', 'sent', 'another', 'sent'] """
    return [word for sent in doc for word in sent]


def to_str(raw: List[List[str]]) -> str:
    sents = [' '.join(sent) for sent in raw]
    return ' '.join(sents)


def to_hash(raw: List[List[str]]) -> str:
    """ Return md5 for a doc"""
    return md5(to_str(raw).encode('ascii', 'ignore')).hexdigest()


class NullTokenizer(DummyTokenizer):
    """
        Use it when the text is already tokenized but the doc's gotta go through spacy.
        Usage: `nlp.tokenizer = CustomTokenizer(nlp.vocab)`
    """
    def __init__(self, vocab):
        self.vocab = vocab

    def __call__(self, words):
        return Doc(self.vocab, words=words)


NUM_SALIENCY_FEATURES = 3


def salience_features(span_a: List[int], span_b: List[int], instance) -> Tuple[int, int, int]:
    """
        Given two spans of text (along with the entire text),
        This class returns a set of features from it like
            - number of words between the spans
            - number of sentences between the spans
            - IF speakers are provided: 0 - same speaker; 1 - different speaker.
                IF speaker info is not provided, we return feat_speaker as 0, i.e. same speaker.
    """

    # Number of words between spans
    if check_overlap(span_a, span_b, False):
        feat_words = 0
    else:
        feat_words = min(span_a[0]-span_b[1], span_b[0]-span_a[1])

    feat_words = abs(feat_words)

    # Number of sentences between spans
    sent_a = instance.get_sentence(span_a)[0]
    sent_b = instance.get_sentence(span_b)[0]

    feat_sentences = abs(sent_a-sent_b)

    # Find Speaker information if available
    if hasattr(instance, 'speaker_info') and instance.speaker_info is not None:
        speaker_a = instance.speaker_info[sent_a]
        speaker_b = instance.speaker_info[sent_b]
        if speaker_a == speaker_b:
            feat_speakers = 0
        else:
            feat_speakers = 1
    else:
        feat_speakers = 0

    # return feat_words, feat_sentences, feat_speakers
    return 0, 0, 0


if __name__ == '__main__':
    _pathfix.suppress_unused_import()
