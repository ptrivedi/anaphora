"""
    Trust me, it is a good idea to keep all unique expressions in one file.
"""


class NoCandidateLeft(Exception):
    """
        Usage: src/evaluate/contextualmodels.py; src/evaluate/base.py

        When: We try trimming sentences (and candidates) at multiple places.
        Sometimes, we might run into an edge case where while trimming, we are left with no sentences or no candidates.
        As an example, consider the following:
            - maximum length for the model - 84
            - length of the sentence with anaphor - 150ish
            - number of candidate in the anaphor's sentence - 0

        So we not even
            - allow one sentence (larger than the allowed length but okay, max len is conservative) because even then,
                we get no candidate in our selected span.
            - can not increase the max len because that may lead to other problems

        So raise ths error, it is caught by `def run` in `class BaseEvaluationBench` in `evaluate/base.py`
            and we return a all negative prediction,
            knowing full well that this instance will be counted as wrongfully predicted.
    """
    pass

class InvalidStateCVDL(Exception):
    """
        Usage: src/dataloader.py

        In the CrossValidationDataLoader, we maintain an internal state to ensure that the object is used properly.
        For instance,

        trying to include a new dataset in the mix
            while we already started iterating over the initially provided data

        starting to iterate over validation split without completely iterating over the train split.

        We are very strongly enforcing this state thing because the CVDL is designed for a very narrow use case but
            we might be tempted to exploit it, inadvertently causing bugs in the process. Better to fail loudly.
    """
    ...


class RowNotFoundError(Exception):
    """
        Usage: src/utils/notion.py

        This thing is raised when we try to find a row/approach which does not exist in the table.
        This is expected to be caught and dealt with (create a new page manually e.g.)
    """
    ...


class ColumnNotFoundError(Exception):
    """
        Usage: src/utils/notion.py

        This one is raised when we're looking for a property but it does not exist in the table.
        We could have passed a dataset we've not included, or a metric whose column does not exist.

        Again, we expect this to be caught and handled
            rather than stopping the entire rube goldberg machine that is our notion table updater.
    """
    ...
