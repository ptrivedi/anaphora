"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    A true assortment of random functions.
"""

import os
import numpy as np
from pathlib import Path
from typing import List, Union
from sklearn.metrics.pairwise import cosine_similarity

# Local imports
from . import _pathfix


class NoRegexMatchError(ValueError):
    ...


class MultipleRegexMatchError(ValueError):
    ...


class BadParameters(ValueError):
    ...


def get_uniques(data: list) -> List[int]:
    """ Return the index of uniques from a list """
    uniques: list = []
    uniques_index: List[int] = []

    # Edge case, if items of data is a list, we need to turn it to a tuple
    to_tuple: bool = type(data[0]) is list

    for i, item in enumerate(data):
        if to_tuple:
            item = tuple(item)
        if item in uniques:
            continue
        else:
            uniques.append(item)
            uniques_index.append(i)

    return uniques_index


def pop(data: list, ids: Union[np.ndarray, List[int]]) -> list:
    """ Pop multiple elements from a list """
    if len(ids) == 0:
        return []

    popped: list = []

    ids = sorted(ids, reverse=True)
    # Fix dtype of ids
    ids = [int(x) for x in ids]

    assert ids[0] < len(data), f"Data has only {len(data)} elements, but we have to pop {ids[0]}."

    for _id in ids:
        popped.append(data.pop(_id))

    return popped


def count_existing_files(dir_name: Path, prefix: str = '', suffix: str = '') -> int:
    """ In this dir, how many files have this prefix and this suffix """
    file_names: List[str] = os.listdir(dir_name)
    matches: int = 0

    for file_name in file_names:
        if file_name.startswith(prefix) and file_name.endswith(suffix):
            matches += 1

    return matches


def sim_cos(a: np.ndarray, b: np.ndarray) -> np.float:
    """ a: (1, D), b: (1, D), output is scalar """
    assert a.shape[0] == b.shape[0] == 1
    assert a.shape[1] == b.shape[1]
    return cosine_similarity(a, b)[0, 0]


def sim_tanimoto(a: np.ndarray, b: np.ndarray) -> np.float:
    """ a: (1, D), b: (1, D), output is scalar -> a.b / ||a||^2 + ||b||^2 - a.b """
    assert a.shape[0] == b.shape[0] == 1
    assert a.shape[1] == b.shape[1]
    a = a.reshape(-1)
    b = b.reshape(-1)

    return np.dot(a, b) / (np.linalg.norm(a) + np.linalg.norm(b) - np.dot(a, b))


def pprint(data: dict) -> None:
    for k, v in data.items():
        print(k, ':', v)


class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def check_overlap(current_span, correct_span, include_ends: bool = True):
    """
        current_span: [24,26]
        correct_span: [26,28]

        Note that: Both ends are included
    """
    current_span_range = [i for i in range(current_span[0], current_span[1] + 1 if include_ends else current_span[1])]
    correct_span_range = [i for i in range(correct_span[0], correct_span[1] + 1 if include_ends else correct_span[1])]
    if bool(set(current_span_range) & set(correct_span_range)):
        return True
    else:
        return False


if __name__ == '__main__':
    _pathfix.suppress_unused_import()
