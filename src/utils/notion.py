"""
    This file contains constants and classes to help interact with Notion.
    So far we're working with the main tables in the experiments page to report results to it.

    Later, we can add interfaces to also interact with other classes.
"""
import git
import json
import copy
import requests
import datetime
import warnings
from alive_progress import alive_bar
from typing import Dict, List, Union, Optional
from mytorch.utils.goodies import FancyDict
from ratelimit import limits, sleep_and_retry


# Local imports
try:
    import _pathfix
except ImportError:
    from . import _pathfix
from config import LOCATIONS as LOC
from utils.exceptions import RowNotFoundError, ColumnNotFoundError

NOTION_APPROACHES = {
    'OracleEvaluationBench': 'Oracle',
    'RandomEvaluationBench': 'Random',
    'WordEmbeddingBench-wordtovec': 'Word2Vec',
    'WordEmbeddingBench-glove': 'GloVe',
    'BertNextSentenceBench': 'BERT NSP - spans',
    'BertNextSentenceBench-heads': 'BERT NSP - heads',
    'BertSelfAttentionBench': 'BERT Attn - spans',
    'BertSelfAttentionBench-heads': 'BERT Attn - heads',
    'BertSelfAttentionPairwiseBench': 'BERT Attn Pairwise - spans',
    'BertSelfAttentionPairwiseBench-heads': 'BERT Attn Pairwise - spans',
    'SpanBertSelfAttentionBench': 'SpanBert Attn - spans',
    'SpanBertSelfAttentionBench-heads': 'SpanBert Attn - heads',
    'SpanBertSelfAttentionPairwiseBench': 'SpanBert Attn Pairwise - spans',
    'SpanBertSelfAttentionPairwiseBench-heads': 'SpanBert Attn Pairwise - spans',
    'RoBertaAttentionBench': 'RoBERTa Attn - spans',
    'RoBertaAttentionBench-heads': 'RoBERTa Attn - heads',
    'KnowBertSelfAttentionBench': 'KnowBERT Attn - spans',
    'KnowBertSelfAttentionBench-heads': 'KnowBERT Attn - heads',
    'BertQuestionAnswering': 'BERT QA',
    'SpanBertQuestionAnswering': 'SpanBERT QA',
    'DiabloGPTAttentionBench': "Diablo Attn - spans",
    'DiabloGPTAttentionBench-heads': "Diablo Attn - heads",
    'BERT higher-order': "BERT higher-order",
    'BERT higher-order - alldata': 'BERT higher-order - alldata',
    'BERT higher-order +wsd': "BERT higher-order +wsd",
    'BERT higher-order - alldata +wsd': 'BERT higher-order - alldata +wsd',
    'Test': 'New Page'
}
NOTION_DATASETS: Dict[str, str] = {
    'isnotes': 'IS',
    'bashi': 'BA',
    'crac/arrau/Trains_91.CONLL': 'CT91',
    'crac/arrau/Trains_93.CONLL': 'CT93',
    'crac/switchboard': 'CSB',
    'crac/light': 'CL',
    'crac/persuasion': 'CP',
    'crac/ami': 'CA'
}
NOTION_CRM: Dict[str, str] = {
    # 'first_and_two_sents': '0c7c66ec17ca4993853690be9d25785c',
    'first_and_two_sents': '671f1a76e5de4637b4db106a64cbe2d9',
    'first_and_one_sent': '2b581ae5b1014ed19fe6e12f56595719',
    'first_and_current_sent': '2e7065f67c514d69886baabf09447eed',
    'two_sents': '00e7b60f68ad44fd95abe25354049ef0',
    'none': '5577a8f2f7094260a32e0cb9a34ba93e'
}
NOTION_RATE_FRAME: int = 2  # requests
NOTION_RATE_LIMIT: int = 1  # seconds


def leaves(d, length=0) -> int:
    # Recursively calculate dict length
    for k, v in d.items():
        if isinstance(v, dict):
            length = leaves(v, length)
        else:
            length += 1
    return length


class NotionInterfaceMainTables:
    """
        This class abstracts out some functionalities of notion,
            respects the rate limits of requests,
            and provides a hassle free mechanism to interact with tables.

        It also manages the nomenclature of approaches, metrics, and datasets.
    """

    def __init__(self, key_metric: str):
        """

        :param key_metric: The metric whose value if higher in a given run, is treated as SOTA. e.g. 'acc'
        """
        try:
            with (LOC.root / 'notion-api-access.secret').open('r', encoding='ascii') as f:
                self.secret: str = f.read().strip()
        except FileNotFoundError:
            raise FileNotFoundError(
                f"The Notion API access key was not found at {LOC.root / 'notion-api-access.secret'}. "
                f"Please contact the admin for it.")

        # Some constants that are going to help us
        self.urls: FancyDict = FancyDict(
            query_table=lambda table_id: f'https://api.notion.com/v1/databases/{table_id}/query',
            update_page=lambda page_id: f'https://api.notion.com/v1/pages/{page_id}',
            create_page=lambda: "https://api.notion.com/v1/pages",
            append_block=lambda page_id: f"https://api.notion.com/v1/blocks/{page_id}/children",
            get_page_children=lambda page_id: f"https://api.notion.com/v1/blocks/{page_id}/children?page_size=100"
        )
        self.headers: dict = {
            'Authorization': f'Bearer {self.secret}',
            'Content-Type': 'application/json',
            'Notion-Version': '2021-05-13'
        }
        self.column = lambda dataset, metric: NOTION_DATASETS[dataset] + ' ' + metric.lower().replace('metric_', '')

        # Some flags
        self.key_metric: str = key_metric

        # Run update once
        self.tables: Dict[str, dict] = {}
        self.pull()

    @staticmethod
    def new_cell(value, title: bool):
        """ Return a dict mimicking the cell in the table. If the cell is the key of the DB, change it a bit. """
        type_: str = 'title' if title else 'rich_text'
        value = str(value)
        return {'type': type_,
                type_: [{'type': 'text',
                         'text': {'content': value, 'link': None},
                         'annotations': {'bold': False,
                                         'italic': False,
                                         'strikethrough': False,
                                         'underline': False,
                                         'code': False,
                                         'color': 'default'},
                         'plain_text': value,
                         'href': None}]}

    def pull(self):
        """
            Fetch the content of tables mentioned in NOTION_CRM
        """
        for crm, table_id in NOTION_CRM.items():
            self.tables[crm] = self._request_get_table_content_(table_id)

    @staticmethod
    def _find_row_(table: dict, approach: str) -> dict:
        """ Return the page whose approach property matches the given str. Fail loudly. """
        rows: List[dict] = table['results']
        for page in rows:

            if len(page['properties']['Approach']['title']) == 0:
                # This is an empty row
                continue

            if page['properties']['Approach']['title'][0]['plain_text'] == approach:
                return page

        raise RowNotFoundError(f"No row/page titled {approach} found in the Table.")
    
    @staticmethod
    def _find_val_(row: dict, column_nm: str) -> Union[str, float, None]:
        """ Return the 'value' inside a given cell based on row and column. Return None if something goes wrong."""
        properties = row['properties']
        try:
            cell = properties[column_nm]
        except KeyError:
            # The column does not exist
            return None

        try:
            if cell['type'] == 'rich_text':
                content = cell['rich_text'][0]['text']['content']
            elif cell['type'] == 'title':
                content = cell['title'][0]['text']['content']
            else:
                raise ValueError(f"Cell (row: {row['id']}, col: {column_nm}) has unexpected type - {cell['type']}")
        except IndexError:
            # The cell is empty
            return None

        try:
            return float(content)
        except ValueError:
            # Content is not a number
            return str(content)

    @staticmethod
    def _prepare_cell_(row: dict, column_name: str, value: Union[str, float]) -> Optional[dict]:
        """
            Expect the cell to look like
                {'id': 'Mzl^',
                 'type': 'rich_text',
                 'rich_text': [{'type': 'text',
                   'text': {'content': '0.2851', 'link': None},
                   'annotations': {'bold': False,
                    'italic': False,
                    'strikethrough': False,
                    'underline': False,
                    'code': False,
                    'color': 'default'},
                   'plain_text': '0.2851',
                   'href': None}]}

            But the cell could also be empty. In this case, we create a new one based on the specification above.
        """
        try:
            cell: dict = row['properties'][column_name]
            cell: dict = copy.deepcopy(cell)
        except KeyError:
            # Column does not exist. Create a new column
            cell = {'type': 'rich_text',
                    'rich_text': [{'type': 'text',
                                   'text': {'content': '0.0', 'link': None},
                                   'annotations': {'bold': False,
                                                   'italic': False,
                                                   'strikethrough': False,
                                                   'underline': False,
                                                   'code': False,
                                                   'color': 'default'},
                                   'plain_text': '0.0',
                                   'href': None}]}

        try:
            rich_text = cell['rich_text'][0]
        except IndexError:
            # The cell could be empty
            cell['rich_text'].append({'type': 'text', 'text': {'content': '0.0'}})
            rich_text = cell['rich_text'][0]

        # Continue with the update
        cell['plain_text'] = str(value)
        rich_text['text']['content'] = str(value)
        cell['rich_text'][0] = rich_text

        return cell

    @staticmethod
    def _prepare_block_(base: dict, is_sota: bool = True) -> dict:
        """
            Fail Silently

                - get current git repo
                - get time stamp (upto seconds)
                - get value being pushed/updated

            Make given dict resemble the following
            {'object': 'block',
               'type': 'bulleted_list_item',
               'bulleted_list_item': {'text': [{'type': 'text',
                  'text': {'content': 'key: value\nkey:value\nkey: value',
                   'link': None}}]}}
        """

        # The value being updated
        content = {k: str(v) for k, v in base.items()}

        # The git commit
        try:
            repo = git.Repo(search_parent_directories=True)
            sha = repo.head.object.hexsha
            content['commit'] = sha
        except git.InvalidGitRepositoryError:
            # Do nothing
            ...

        # The timestamp
        now = datetime.datetime.now()
        f_now = now.strftime("%d/%m/%Y, %H:%M:%S")
        content['timestamp'] = f_now

        # If not updated
        content['sota'] = is_sota

        # Put in the expected format and send off
        content = '\n'.join([f"{k}: {v}" for k, v in content.items()])
        return {'object': 'block',
                'type': 'bulleted_list_item',
                'bulleted_list_item': {'text': [{'type': 'text',
                                                 'text': {'content': content,
                                                          'link': None}}]}}

    def _request_get_table_content_(self, table_id: str) -> dict:
        """ Send request to fetch the table with the given ID """

        url = self.urls.query_table(table_id)
        payload = "{}"
        response = self.request(method="POST", url=url, headers=self.headers, data=payload)
        return response.json()

    def _request_get_page_content_(self, row_id):
        """ Get all the children of the page"""

        url = self.urls.get_page_children(row_id)
        response = self.request("GET", url=url, headers=self.headers, data="")
        return response.json()

    def _request_update_page_(self, row_id: str, column_names: List[str], cells: List[dict]) -> None:
        """ Send a PATCH request with the payload as the updated property """
        url = self.urls.update_page(row_id)
        payload = {'properties': {column_name: cell for column_name, cell in zip(column_names, cells)}}
        payload = json.dumps(payload)
        self.request("PATCH", url=url, headers=self.headers, data=payload)

    def _request_create_page_(self, parent: str, properties: dict, children: List[dict]) -> None:
        """ Specify the database as the parent. Initialise it with the given property dicts """

        url = self.urls.create_page()
        payload = {
            "parent": {"database_id": parent},
            "properties": properties,
            "children": children
        }
        self.request("POST", url=url, headers=self.headers, data=json.dumps(payload))

    def _request_append_block_(self, page_id: str, content: List[dict]):
        """ We're only prepping bulleted list for now """

        url = self.urls.append_block(page_id)
        payload = {"children": content}

        self.request("PATCH", url=url, headers=self.headers, data=json.dumps(payload))

    @staticmethod
    @sleep_and_retry
    @limits(calls=NOTION_RATE_LIMIT, period=NOTION_RATE_FRAME)
    def request(method: str, url: str, headers: dict, data: dict):
        response = requests.request(method, url=url, headers=headers, data=data)

        if response.status_code != 200:
            print(response.content)
            raise Exception('API response: {}'.format(response.status_code))
        return response

    @staticmethod
    def _parse_log_blocks_(page_content: dict) -> List[dict]:
        result = []
        for block in page_content['results']:
            try:
                content = block['bulleted_list_item']['text'][0]['text']['content']
            except KeyError:
                continue
            content_ = {tok.split(':')[0].strip(): tok.split(':')[1].strip() for tok in content.split('\n')}

            # Try to turn things into float if possible
            for k, v in content_.items():
                try:
                    content_[k] = float(v)
                except ValueError:
                    continue
            result.append(content_)
        return result

    def _is_better_(self, row: dict, dataset: str, metrics: dict) -> bool:
        """
            Check if the values in the param dict is better than the one stored in the row

            - key metric is provided in self
                - key metric exists in params
                    - if both old and new values are the same, do update the page
                    - check if the key metric's value is higher in param
                    TODO: what if we want the value to be lower
                - key metric doesn't exist in params
                    - return True (yes the new value should go in the property field)
            - key metric is not provided
                compare all metrics and if more than 50% are better, update it.
        """

        # Try to fetch comparable values for those metrics which are present in the page
        old_metrics = {metric_nm: self._find_val_(row, self.column(dataset, metric_nm))
                       for metric_nm in metrics.keys()}

        if self.key_metric:

            try:
                new_val = metrics[self.key_metric]
            except KeyError:
                raise KeyError(f"The provided 'key' metric - {self.key_metric} was not found in the current run.")

            old_val = self._find_val_(row, self.column(dataset, self.key_metric))
            if old_val is None or type(old_val) is str:
                return True

            if new_val > old_val:
                return True
            elif new_val == old_val:
                if None in old_metrics.values():
                    # Do update the page
                    return True
                else:
                    return False
            else:
                # new val < old_val
                return False

        else:

            # Filter out the None and the str s in the old metrics
            old_metrics = {k: v for k, v in old_metrics.items() if v is not None and type(v) is float}

            # Compare them
            return sum([metrics[k] > old_metrics[k] for k in old_metrics.keys()])/len(old_metrics.keys()) > 0.5

    def _run_exists_(self, row: dict, dataset: str, metrics: dict) -> bool:
        """
            - fetch and parse all the blocks within the given row/page
            - check if the metrics dict is represented in any of the columns.
                - if yes: True
                - else: False
        """
        page_raw = self._request_get_page_content_(row['id'])
        page_parsed = self._parse_log_blocks_(page_raw)

        for block in page_parsed:
            all_equal = True
            for metric_nm, metric_vl in metrics.items():
                if not block.get(self.column(dataset, metric_nm), None) == metric_vl:
                    all_equal = False
                    break

            # If we still think that the existing block is equal to the given one (param metrics), return True
            if all_equal:
                return True

        # No block is equal to the given one
        return False

    def update_run(self, crm: str, approach: str, dataset: str, metrics: Dict[str, float]):
        """
            A wrapper over _update_. It expects values and metrics corresponding to one run.
                i.e. multiple floats corresponding to multiple metrics over one CRM, one Dataset and one Approach.

            If a best metric is defined in self, and if the metric exists here, we first compare to check if the
                value provided in params is better than the one already in the table,
                we merely log the numbers in a block and don't update the page's properties.

            Concretely
                -> page exists
                    -> check values

                -> page does not exist
                    -> create a new page with numbers in properties and blocks
        """

        # Get the table
        table = self.tables[crm]

        try:
            approach = NOTION_APPROACHES[approach]
        except KeyError:
            # The approach variable is no longer hooked up.
            warnings.warn(f"The approach named - {approach} is not registered in NOTION_APPROACHES dict.")
            return

        # Try to get the row
        try:
            row: dict = self._find_row_(table, approach)

            # Set a var to track whether the current run was chosen as SOTA
            updated: bool = False

            # Check if the current run is "better" than the old ones
            if self._is_better_(row=row, dataset=dataset, metrics=metrics):
                updated = True
                # For each metric, create a corresponding cell
                cells = [self._prepare_cell_(row, self.column(dataset, column_name), value)
                         for column_name, value in metrics.items()]

                # Update the page with these cells
                self._request_update_page_(row_id=row['id'], cells=cells,
                                           column_names=[self.column(dataset, cm) for cm in metrics.keys()])

            if self._run_exists_(row=row, dataset=dataset, metrics=metrics):
                # We didn't choose to update the properties.
                # And we also found out that the given run is logged in one of the blocks on the page/row
                # In this case, don't even update the blocks. Just do nothing. let's not spam the page.
                return

            # Add the current run as a block
            block_content = {self.column(dataset, metric_nm): metric_vl for
                             metric_nm, metric_vl in metrics.items()}
            block_content['dataset'] = dataset
            children = [self._prepare_block_(base=block_content, is_sota=updated)]
            self._request_append_block_(page_id=row['id'], content=children)

        except RowNotFoundError:

            # Create a new page/row.
            # First create a list of properties (column) namely (i): approach, (ii) current column (dataset + metric)
            properties: dict = {'Approach': self.new_cell(approach, title=True)}
            for metric_nm, metric_vl in metrics.items():
                properties[self.column(dataset, metric_nm)] = self.new_cell(metric_vl, title=False)

            # Create a logging block and use it as "page content"/children field of the page.
            block_content = {self.column(dataset, metric_nm): metric_vl for
                             metric_nm, metric_vl in metrics.items()}
            block_content['dataset'] = dataset
            children = [self._prepare_block_(base=block_content, is_sota=True)]
            self._request_create_page_(parent=NOTION_CRM[crm], properties=properties, children=children)

            # Crucially, update the understanding of the current state of tables
            self.pull()

    def update(self):
        """ Pull dictionaries, all available. Push values, all available"""

        # Get all the exp values
        results = {}
        for dataset_dir in NOTION_DATASETS.keys():
            try:
                with (LOC.parsed / dataset_dir / 'results.json').open('r') as f:
                    dataset_specific_results = json.load(f)
                    results[dataset_dir] = dataset_specific_results
            except FileNotFoundError:
                warnings.warn(f"No results.json found in {str(LOC.parsed / dataset_dir)}")

        # Flip Results from dataset -> crm -> approach to crm -> dataset -> approach
        # #### based on the known crm tables
        results_ = {}
        for crm in NOTION_CRM.keys():
            results_[crm] = {}
            for dataset_name, dataset_specific_results in results.items():
                results_[crm][dataset_name] = dataset_specific_results.get(crm, {})

        with alive_bar(leaves(results_)) as bar:
            for crm_nm in results_:
                for ds_nm in results_[crm_nm]:
                    for appr_nm in results_[crm_nm][ds_nm]:
                        print("Onto ", crm_nm, " - ", ds_nm, " - ", appr_nm)
                        self.update_run(crm=crm_nm, approach=appr_nm, dataset=ds_nm,
                                        metrics={metric_nm.replace('metric_', ''): round(metric_vl, 4)
                                                 for metric_nm, metric_vl in results_[crm_nm][ds_nm][appr_nm].items()})

                        for _ in results_[crm_nm][ds_nm][appr_nm].items():
                            bar()

    def bold_best(self):
        """
            This just reads all the content and emphasizes the best values in bold based on the key metric.
            If the key metric is not available, we do not bold anything for that set of cells.
        :return: None
        """

        # Assume that the tables are all updated
        raise NotImplementedError


if __name__ == '__main__':
    _pathfix.suppress_unused_import()
    na = NotionInterfaceMainTables(key_metric='acc')
    # na._update_(crm='first_and_two_sents', approach='OracleEvaluationBench', dataset='isnotes',
    # metric='mrr', value=0.1111)
    # na._update_(crm='first_and_two_sents', approach='Test', dataset='isnotes', metric='mrr', value=0.1111)
    na.update()
