"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    Primary: a dataclass to represent one instance of Bridging Anaphora data.
    Secondary: some auxiliary functions doing operations over them.
"""
from typing import List, Tuple, Dict, Union, Iterable, Optional, Set
from dataclasses import dataclass, field
from collections import OrderedDict

# Local Imports
from . import _pathfix
from utils.nlp import to_toks
from utils.misc import get_uniques, BadParameters


@dataclass
class BridgingInstance:
    # The text of the document, broken down as list of sentence. Each sentence is a list of words
    document: List[List[str]]

    # Pos tags, noun phrases in the document computed using the spacy doc
    pos: List[List[str]]

    # Docname corresponds to the ontonotes doc
    docname: str

    # A unique ID assigned to each instance.
    anaphor_id: Union[int, str]

    # Spans are a list of two integers: span start word index, span end word index
    # word index: the number counts the number of words from the start, regardless of sentence structure.
    # in other words, the doc needs to be flattened (utils.nlp.to_toks) before superimposing the spans.
    # the _h variants are the root words' span of the antecedent and anaphors
    antecedent: Optional[List[int]]
    anaphor: List[int]

    # The text of antecedents and anaphors
    # NOTE: antecedent corresponds to the CORRECT span, not the candidate
    antecedent_: Optional[List[str]] = field(default_factory=list)
    antecedent_h: Optional[List[int]] = field(default_factory=list)
    # antecedent_h_: Optional[List[str]] = field(default_factory=list)
    anaphor_: List[str] = field(default_factory=list)
    anaphor_h: List[int] = field(default_factory=list)
    anaphor_h_: List[str] = field(default_factory=list)

    # The anaphors, in some datasets (BASHI) have this metadata
    # NOTE: If the class is negative, we allow antecedents to appear after anaphors.
    anaphor_cls: List[int] = field(default_factory=list)

    """ 
        These fields will be added later, if needed 
        
        Candidate antecedent: "correct antecedents" for other anaphor instances in this document
        Candidate nounphrase: all potential candidates in this document (if specified), else all noun phrases. 
    """

    # Candidates are a collected of all antecedents existing in this document
    candidate_antecedent_ids: List[List[int]] = field(default_factory=list)
    candidate_antecedent_txt: List[List[str]] = field(default_factory=list)

    # Candidate heads deserve similar treatment
    candidate_antecedent_h_ids: List[List[int]] = field(default_factory=list)
    candidate_antecedent_h_txt: List[List[str]] = field(default_factory=list)

    candidate_nounphrase_ids: List[List[int]] = field(default_factory=list)  # Noun phrases in the doc
    candidate_nounphrase_h_ids: List[List[int]] = field(default_factory=list)  # The root word of the noun phrases
    candidate_nounphrase_txt: List[List[str]] = field(default_factory=list)
    candidate_nounphrase_h_txt: List[List[str]] = field(default_factory=list)

    # The index of self.antecedent among self.candidate_antecedents
    correct_antecedent_id: int = field(default_factory=int)
    correct_nounphrase_id: int = field(default_factory=int)

    # Hidden var to be used internally
    sentence_lengths_: List[int] = field(default_factory=list)

    """
        Data specific to dialogue based instances. 
        i.e. all the crac datasets
    """
    # We also encode the speaker information. i.e. which sentence is spoken by which speaker
    speaker_info: Optional[List[int]] = None
    # We also store the raw document, in case we ever need it
    document_raw: Optional[List[List[str]]] = None

    def finalise(self):
        """ Run some sanity checks. Filter candidates based on uniqueness, and if they appear after the anaphor """

        # Ensure there are no duplicates in candidate_nounphrase_ids, or candidates_src
        unique_candidate_indices: List[int] = get_uniques(self.candidate_antecedent_ids)
        self.candidate_antecedent_ids = [self.candidate_antecedent_ids[i] for i in unique_candidate_indices]
        self.candidate_antecedent_h_ids = [self.candidate_antecedent_h_ids[i] for i in unique_candidate_indices]
        self.candidate_antecedent_txt = [self.candidate_antecedent_txt[i] for i in unique_candidate_indices]
        self.candidate_antecedent_h_txt = [self.candidate_antecedent_h_txt[i] for i in unique_candidate_indices]

        unique_nounphrase_indices: List[int] = get_uniques(self.candidate_nounphrase_ids)
        self.candidate_nounphrase_ids = [self.candidate_nounphrase_ids[i] for i in unique_nounphrase_indices]
        self.candidate_nounphrase_h_ids = [self.candidate_nounphrase_h_ids[i] for i in unique_nounphrase_indices]
        self.candidate_nounphrase_txt = [self.candidate_nounphrase_txt[i] for i in unique_nounphrase_indices]
        self.candidate_nounphrase_h_txt = [self.candidate_nounphrase_h_txt[i] for i in unique_nounphrase_indices]

        # Discarding antecedent candidate which appear BEFORE src; IF it is a real instance, not a faux one.
        if not has_negative_class(self.anaphor_cls):
            relevant_candidate_ids, relevant_candidate_txt = [], []
            relevant_candidate_h_ids, relevant_candidate_h_txt = [], []
            for can_id, can_tx, can_id_h, can_tx_h in zip(self.candidate_antecedent_ids,
                                                          self.candidate_antecedent_txt,
                                                          self.candidate_antecedent_h_ids,
                                                          self.candidate_antecedent_h_txt):
                # Patch: if the candidate is the actual antecedent don't remove it.
                if can_id[1] > self.anaphor[0] and not \
                        (can_id[0] == self.antecedent[0] and can_id[1] == self.antecedent[1]):
                    continue
                relevant_candidate_ids.append(can_id)
                relevant_candidate_txt.append(can_tx)
                relevant_candidate_h_ids.append(can_id_h)
                relevant_candidate_h_txt.append(can_tx_h)

            assert len(relevant_candidate_txt) > 0

            self.candidate_antecedent_ids = relevant_candidate_ids
            self.candidate_antecedent_txt = relevant_candidate_txt
            self.candidate_antecedent_h_txt = relevant_candidate_h_txt
            self.candidate_antecedent_h_ids = relevant_candidate_h_ids

        # Discarding nounphrase candidate which appear BEFORE src; IF it is a real instance, not a faux one.
        if not has_negative_class(self.anaphor_cls):
            relevant_candidate_ids, relevant_candidate_txt = [], []
            relevant_candidate_h_ids, relevant_candidate_h_txt = [], []
            for can_id, can_tx, can_id_h, can_tx_h in zip(self.candidate_nounphrase_ids,
                                                          self.candidate_nounphrase_txt,
                                                          self.candidate_nounphrase_h_ids,
                                                          self.candidate_nounphrase_h_txt):
                # Patch: if the candidate is the actual antecedent don't remove it.
                if can_id[1] > self.anaphor[0] and not \
                        (can_id[0] == self.antecedent[0] and can_id[1] == self.antecedent[1]):
                    continue
                relevant_candidate_ids.append(can_id)
                relevant_candidate_txt.append(can_tx)
                relevant_candidate_h_ids.append(can_id_h)
                relevant_candidate_h_txt.append(can_tx_h)

            assert len(relevant_candidate_txt) > 0
            self.candidate_nounphrase_ids = relevant_candidate_ids
            self.candidate_nounphrase_txt = relevant_candidate_txt
            self.candidate_nounphrase_h_txt = relevant_candidate_h_txt
            self.candidate_nounphrase_h_ids = relevant_candidate_h_ids

        # Put in values for self.correct_antecedent_id, self.correct_nounphrase_id
        self.find_correct_candidate_id()

        # Populate sentence lengths. This is required multiple times down the line
        self.sentence_lengths_: List[int] = [len(sent) for sent in self.document]

        # Bunch of sanity checks.
        assert len(self.antecedent) == len(self.anaphor) == 2, f"Anaphor: {len(self.anaphor)}, " \
                                                               f"Antecedent: {len(self.antecedent)}. "
        assert 0 <= self.antecedent[0] <= self.antecedent[1] <= sum(len(sent) for sent in self.document)
        assert 0 <= self.anaphor[0] <= self.anaphor[1] <= sum(len(sent) for sent in self.document)
        assert 0 <= len(self.candidate_antecedent_h_ids) == len(self.candidate_antecedent_h_txt) == \
               len(self.candidate_antecedent_ids) == len(self.candidate_antecedent_txt)
        assert 0 <= len(self.candidate_nounphrase_h_ids) == len(self.candidate_nounphrase_h_txt) == \
               len(self.candidate_nounphrase_ids) == len(self.candidate_nounphrase_txt)

        for candidate_nounphrase in self.candidate_nounphrase_ids:
            assert 0 <= candidate_nounphrase[0] <= candidate_nounphrase[1] <= sum(len(sent) for sent in self.document)

        for candidate_antecedent in self.candidate_antecedent_ids:
            assert 0 <= candidate_antecedent[0] <= candidate_antecedent[1] <= sum(len(sent) for sent in self.document)

    def find_correct_candidate_id(self, fail_silently: bool = False):
        """ Go through the candidates_src, and find the correct one """

        try:
            # noinspection PyTypeChecker
            self.correct_antecedent_id = [tuple(x) for x in self.candidate_antecedent_ids].index(tuple(self.antecedent))
            self.correct_nounphrase_id = [tuple(x) for x in self.candidate_nounphrase_ids].index(tuple(self.antecedent))
        except ValueError:
            if fail_silently:
                self.correct_antecedent_id = -1
                self.correct_nounphrase_id = -1
            else:
                raise AssertionError(f"${self.anaphor_id}: {self.antecedent} not in {self.candidate_antecedent_ids}")

    def get_sentence(self, span: List[int]) -> (int, int):
        """ returns the sentences which contain these spans
        Note that the end sentence is inclusive... meaning the span is included in the end sentence
        """

        if span[0] == span[1] and span[1] < len(to_toks(self.document)):
            span = (span[0], span[1] + 1)

        # A list of two items: number of first sentence, number of last sentence
        start: int = -1
        end: int = -1

        # Get length of each sentence
        # sents_len: List[int] = [len(sent) for sent in self.document]
        sents_lenagg: List[int] = [sum(self.sentence_lengths_[:sent_n + 1])
                                   for sent_n in range(len(self.sentence_lengths_))]

        # Find the anaphor sent
        for sent_n, sent_lenagg in enumerate(sents_lenagg):
            """
            adding the "and" part below so this func can be used for spans that span multiple sentences:
            Consider a span of [23, 32], where sentence 1 ends on token 25, and sentence 2 ends on token 36.
            We should return 1 for the start sentence and 2 for the end sentence.
            But without the "and" below, the start sentence would be assigned to 2, because 23 < 36.
            """
            if span[0] < sent_lenagg and (sent_n == 0 or span[0] >= sents_lenagg[sent_n - 1]):
                start = sent_n
            """
                This <= below was found after hours of toil and hard work in elusive bug squashing.
                    Do not question the gods of empiricism who have given this span their green light.
                    i.e., it works
                    Okay fine I'll tell you why.
                Consider a scenario where sent 33 ends on token 783 (true story, btw).
                And you're asked to give the sentence ID of span [782, 783].
                Now, 782 lies in sent 33 fair and square. But so does 783. 
                And without the = in <=, we would have shifted the entire thing up one sentence - sent # 34. Hence. 🐔
            """
            if span[1] <= sent_lenagg:
                end = sent_n
                break

        assert start >= 0 and end > -1, (start, end)
        return start, end

    def get_sentence_span(self, sent: int) -> (int, int):
        """ Returns the beginning and end of a given sentence number based on the doc """
        assert sent < len(self.document)

        start = len(to_toks(self.document[: sent]))
        end = start + len(self.document[sent])

        return start, end

    def first_relevant_sent(self, src: str) -> int:
        """ Returns the sentence nr. where the FIRST anaphor, or any of the the candidate antecedents appear"""

        assert src in ['antecedent', 'nounphrase']

        # Find the earliest span
        elements: List[int] = [self.anaphor[0]]
        elements += [span[0] for span in getattr(self, f'candidate_{src}_ids')]
        first_element = min(elements)

        # Get length of each sentence
        # doc: List[List[str]] = self.document
        # sents_len: List[int] = [len(sent) for sent in doc]
        # NOTE: sents_n+1 is intentional, and tested even if it seems counterintuitive
        sents_lenagg: List[int] = [sum(self.sentence_lengths_[:sent_n + 1])
                                   for sent_n in range(len(self.sentence_lengths_))]

        # Find the anaphor sent
        for sent_n, sent_lenagg in enumerate(sents_lenagg):
            if first_element < sent_lenagg:
                return sent_n

        raise AssertionError("Should have found the appropriate length by now. Maybe a different doc is passed.")

    def last_relevant_sent(self, src: str) -> int:
        """ Returns the sentence nr. where the LAST anaphor, or any of the the candidate antecedents appear"""

        assert src in ['antecedent', 'nounphrase']

        # Find the last span
        elements: List[int] = [self.anaphor[1]]
        elements += [span[1] for span in getattr(self, f'candidate_{src}_ids')]
        last_element = max(elements)

        # Get length of each sentence
        # doc: List[List[str]] = self.document
        # sents_len: List[int] = [len(sent) for sent in doc]
        # NOTE: sents_n+1 is intentional, and tested even if it seems counterintuitive
        sents_lenagg: List[int] = [sum(self.sentence_lengths_[:sent_n + 1])
                                   for sent_n in range(len(self.sentence_lengths_))]

        # Find the anaphor sent
        for sent_n, sent_lenagg in enumerate(sents_lenagg):
            if last_element <= sent_lenagg:
                return sent_n

        raise AssertionError("Should have found the appropriate length by now. Maybe a different doc is passed.")


@dataclass
class BridgingDocument:
    """
        Instead of bridging instances above,
            one instance here corresponds to one document.

        In each document, we expect the following information to be always present:
            document (list of strings)
            pos (list of strings)
            speaker (if available)
            nounphrases (a list of every demarkated noun phrase)

        Depending on whether an instance is a train or test instance, we also need
            anaphor ids: id of items in nounphrases list which are anaphoric
            antecedent ids: id of items in nounphrases list which are corresponding antecedents
    """

    # The text of the document, broken down as list of sentence. Each sentence is a list of words
    document: List[List[str]]

    # Pos tags, noun phrases in the document computed using the spacy doc
    pos: List[List[str]]

    # Docname corresponds to the ontonotes doc
    docname: str

    # If true, we don't expect to have anaphor and antecedent information
    is_test_data: bool

    nounphrases: List[List[int]]  # Noun phrases in the doc
    nounphrases_h: List[List[int]]  # The root word of the noun phrases
    nounphrases_: List[List[str]] = field(default_factory=list)
    nounphrases_h_: List[List[str]] = field(default_factory=list)

    # We also encode the speaker information. i.e. which sentence is spoken by which speaker
    speaker_info: Optional[List[int]] = None

    # Optional stuff: anaphor and antecedent information
    # The values here are spans over the document
    anaphors: Optional[List[List[int]]] = None
    anaphors_h: Optional[List[List[int]]] = None
    antecedents: Optional[List[List[int]]] = None
    antecedents_h: Optional[List[List[int]]] = None

    # The text corresponding to spans above
    anaphors_: Optional[List[List[str]]] = None
    anaphors_h_: Optional[List[List[str]]] = None
    antecedents_: Optional[List[List[str]]] = None
    antecedents_h_: Optional[List[List[str]]] = None

    # Alternate: pointers to candidates in self.nounphrases*
    anaphors_idx: Optional[List[int]] = None
    antecedents_idx: Optional[List[int]] = None

    """ Hidden content. Not to be used in usual circumstances. """
    # Length of every sentence in document.
    sentence_lengths_: List[int] = field(default_factory=list)

    # candidates cache: a place to store results of 'get_candidates_for' method. So as to not compute every time.
    candidates_cache: Dict[Tuple[int, int], List[List[int]]] = field(default_factory=dict)

    # We also store the raw document, in case we ever need it.
    # If we grammar correct the original doc, this will hold the old values.
    document_raw: Optional[List[List[str]]] = None

    def finalise(self):

        # Things to ensure in the document (regardless of whether we have anaphor, antecedent info or not)
        len_document: int = sum(len(x) for x in self.document)

        # Equal tokens in document and pos
        if not len_document == sum(len(x) for x in self.pos):
            raise AssertionError(f"Length of document is {len_document}"
                                 f" but length of pos is {sum(len(x) for x in self.pos)}")

        # Equal number of sentences
        if self.speaker_info is None:
            if not len(self.document) == len(self.pos):
                raise AssertionError(f"Num. sentences in document: {len(self.document)}, "
                                     f"Num. sentences in pos: {len(self.pos)}")
        else:
            if not len(self.document) == len(self.pos) == len(self.speaker_info):
                raise AssertionError(f"Num. sentences in document: {len(self.document)}, "
                                     f"Num. sentences in pos: {len(self.pos)}, "
                                     f"Num. sentences in speakers: {len(self.speaker_info)}")

        # Ensure every span makes sense (in nounphrases)
        for span in self.nounphrases:
            if len(span) != 2 or not 0 <= span[0] <= span[1] <= len_document:
                raise AssertionError(f"Weird span in nounphrases - {span}. "
                                     f"Length of document is {len_document}.")

        for span in self.nounphrases_h:
            if len(span) != 2 or not 0 <= span[0] <= span[1] <= len_document:
                raise AssertionError(f"Weird span in nounphrases (heads) - {span}. "
                                     f"Length of document is {len_document}.")

        # Ensure there are no duplicates in candidate_nounphrase_ids, or candidates_src
        unique_candidate_indices: List[int] = get_uniques(self.nounphrases)
        self.nounphrases = [self.nounphrases[i] for i in unique_candidate_indices]
        self.nounphrases_ = [self.nounphrases_[i] for i in unique_candidate_indices]
        self.nounphrases_h = [self.nounphrases_h[i] for i in unique_candidate_indices]
        self.nounphrases_h_ = [self.nounphrases_h_[i] for i in unique_candidate_indices]

        # Populate sentence lengths. This is required multiple times down the line
        self.sentence_lengths_: List[int] = [len(sent) for sent in self.document]

        if not self.is_test_data:
            # We are not dealing with "test" data.
            if self.anaphors is None:
                raise AssertionError("Expected anaphors but not found.")
            if self.anaphors_ is None:
                raise AssertionError("Expected anaphors (text) but not found.")
            if self.anaphors_h is None:
                raise AssertionError("Expected anaphors (heads) but not found.")
            if self.anaphors_h_ is None:
                raise AssertionError("Expected anaphors (heads text) but not found.")
            if not len(self.anaphors) == len(self.anaphors_) == len(self.anaphors_h) == len(self.anaphors_h_):
                raise AssertionError("Length mismatch in anaphor lists")
            if self.antecedents is None:
                raise AssertionError("Expected antecedents but not found.")
            if self.antecedents_ is None:
                raise AssertionError("Expected antecedents (text) but not found.")
            if self.antecedents_h is None:
                raise AssertionError("Expected antecedents (heads) but not found.")
            if self.antecedents_h_ is None:
                raise AssertionError("Expected antecedents (heads text) but not found.")
            if not len(self.antecedents) == len(self.antecedents_) \
                   == len(self.antecedents_h) == len(self.antecedents_h_):
                raise AssertionError("Length mismatch in antecedent lists")

        # Ensure every span in anaphors, antecedents makes sense
        if self.anaphors is not None:

            # If there are some anaphors, run this script over all anaphor, antecedent span lists.
            for i, span in enumerate(self.anaphors):
                span_h = self.anaphors_h[i]

                # Span is consistent with the document
                if not 0 <= span[0] <= span_h[0] <= span_h[1] <= span[1] < len_document:
                    raise AssertionError(f"Invalid span in anaphors. anaphor - {span}, anaphor_h - {span_h}. "
                                         f"Length of document is {len_document}")

                # Span is found inside nounphrase
                if not self.anaphors[i] in self.nounphrases:
                    raise AssertionError(f"Anaphor span not in nps. Span - {self.anaphors[i]}")
                if not self.anaphors_[i] in self.nounphrases_:
                    raise AssertionError(f"Anaphor (text) span not in nps. Span - {self.anaphors_[i]}")
                if not self.anaphors_h[i] in self.nounphrases_h:
                    raise AssertionError(f"Anaphor (head) span not in nps. Span - {self.anaphors_h[i]}")
                if not self.anaphors_h_[i] in self.nounphrases_h_:
                    raise AssertionError(f"Anaphor (head) (text) span not in nps. Span - {self.anaphors_h_[i]}")

            for i, span in enumerate(self.antecedents):
                span_h = self.antecedents_h[i]

                # Span is consistent with the document
                if not 0 <= span[0] <= span_h[0] <= span_h[1] <= span[1] < len_document:
                    raise AssertionError(f"Invalid span in antecedents. ante. - {span}, ante._h - {span_h}. "
                                         f"Length of document is {len_document}")

                # Span is found inside nounphrase
                if not self.antecedents[i] in self.nounphrases:
                    raise AssertionError(f"Anaphor span not in nps. Span - {self.antecedents[i]}")
                if not self.antecedents_[i] in self.nounphrases_:
                    raise AssertionError(f"Anaphor (text) span not in nps. Span - {self.antecedents_[i]}")
                if not self.antecedents_h[i] in self.nounphrases_h:
                    raise AssertionError(f"Anaphor (head) span not in nps. Span - {self.antecedents_h[i]}")
                if not self.antecedents_h_[i] in self.nounphrases_h_:
                    raise AssertionError(f"Anaphor (head) (text) span not in nps. Span - {self.antecedents_h_[i]}")

            # Populate anaphors_idx, antecedents_idx
            self.anaphors_idx = [self.nounphrases.index(span) for span in self.anaphors]
            self.antecedents_idx = [self.nounphrases.index(span) for span in self.antecedents]

    def get_sentence(self, span: List[int]) -> (int, int):
        """
            returns the sentences which contain these spans
            Note that the end sentence is inclusive... meaning the span is included in the end sentence
        """

        if span[0] == span[1] and span[1] < len(to_toks(self.document)):
            span = (span[0], span[1] + 1)

        # A list of two items: number of first sentence, number of last sentence
        start: int = -1
        end: int = -1

        # Get length of each sentence
        # sents_len: List[int] = [len(sent) for sent in self.document]
        sents_lenagg: List[int] = [sum(self.sentence_lengths_[:sent_n + 1])
                                   for sent_n in range(len(self.sentence_lengths_))]

        # Find the anaphor sent
        for sent_n, sent_lenagg in enumerate(sents_lenagg):
            """
            adding the "and" part below so this func can be used for spans that span multiple sentences:
            Consider a span of [23, 32], where sentence 1 ends on token 25, and sentence 2 ends on token 36.
            We should return 1 for the start sentence and 2 for the end sentence.
            But without the "and" below, the start sentence would be assigned to 2, because 23 < 36.
            """
            if span[0] < sent_lenagg and (sent_n == 0 or span[0] >= sents_lenagg[sent_n - 1]):
                start = sent_n
            """
                This <= below was found after hours of toil and hard work in elusive bug squashing.
                    Do not question the gods of empiricism who have given this span their green light.
                    i.e., it works
                    Okay fine I'll tell you why.
                Consider a scenario where sent 33 ends on token 783 (true story, btw).
                And you're asked to give the sentence ID of span [782, 783].
                Now, 782 lies in sent 33 fair and square. But so does 783. 
                And without the = in <=, we would have shifted the entire thing up one sentence - sent # 34. Hence. 🐔
            """
            if span[1] <= sent_lenagg:
                end = sent_n
                break

        assert start >= 0 and end > -1, (start, end)
        return start, end

    def get_sentence_span(self, sent: int) -> (int, int):
        """ Returns the beginning and end of a given sentence number based on the doc """
        assert sent < len(self.document)

        start = len(to_toks(self.document[: sent]))
        end = start + len(self.document[sent])

        return start, end

    def first_relevant_sent(self) -> int:
        """ Returns the sentence nr. where the FIRST anaphor, or any of the the candidate antecedents appear"""

        first_element = min(x[0] for x in self.nounphrases)

        # Get length of each sentence
        # doc: List[List[str]] = self.document
        # sents_len: List[int] = [len(sent) for sent in doc]
        # NOTE: sents_n+1 is intentional, and tested even if it seems counterintuitive
        sents_lenagg: List[int] = [sum(self.sentence_lengths_[:sent_n + 1])
                                   for sent_n in range(len(self.sentence_lengths_))]

        # Find the anaphor sent
        for sent_n, sent_lenagg in enumerate(sents_lenagg):
            if first_element < sent_lenagg:
                return sent_n

        raise AssertionError("Should have found the appropriate length by now. Maybe a different doc is passed.")

    def last_relevant_sent(self, span: Optional[List[int]] = None) -> int:
        """ Return the sentence with the last element. If span given, then only consider elements upto that span."""

        if span:
            candidates: List[List[int]] = self.get_candidates_for(span, 'nounphrase')
        else:
            candidates: List[List[int]] = self.nounphrases

        # Find the last span
        last_element = max(x[1] for x in candidates)

        # Get length of each sentence
        sents_lenagg: List[int] = [sum(self.sentence_lengths_[:sent_n + 1])
                                   for sent_n in range(len(self.sentence_lengths_))]

        # Find the anaphor sent
        for sent_n, sent_lenagg in enumerate(sents_lenagg):
            if last_element <= sent_lenagg:
                return sent_n

        raise AssertionError("Should have found the appropriate length by now. Maybe a different doc is passed.")

    def get_candidates_for(self, span: List[int], attr: str = 'nounphrases') -> list:

        # Ensure that the span makes sense
        if not len(span) == 2 and 0 <= span[0] <= span[1] <= sum(self.sentence_lengths_):
            raise BadParameters(f"The span: {span} is invalid. "
                                f"It should be a list of two elements,"
                                f" both of which lie between 0 and {sum(self.sentence_lengths_)}")

        if not attr in ['nounphrases', 'nounphrases_', 'nounphrases_h', 'nounphrases_h_']:
            raise BadParameters(f"attr not recognized.")

        try:
            results = self.candidates_cache[tuple(span)]
        except KeyError:
            # Get all elements from attr based on this
            results = []
            for i, candidate_span in enumerate(self.nounphrases):
                if candidate_span[0] <= span[0] or candidate_span[1] <= span[1]:
                    results.append(i)

            self.candidates_cache[tuple(span)] = results

        # Impose this list on whatever source is specifed in attr
        return [getattr(self, attr)[i] for i in results]

    def get_bridging_instances(self) -> List[BridgingInstance]:
        instances: List[BridgingInstance] = []
        for i, anaphor in enumerate(self.anaphors):
            ba = BridgingInstance(
                document=self.document,
                pos=self.pos,
                docname=self.docname,
                anaphor_id=self.docname + '_' + str(i),
                antecedent=self.antecedents[i],
                anaphor=self.anaphors[i],
                antecedent_=self.antecedents_[i],
                antecedent_h=self.antecedents_h[i],
                anaphor_=self.anaphors_[i],
                anaphor_h=self.anaphors_h[i],
                anaphor_h_=self.anaphors_h_[i],
                anaphor_cls=[0],
                candidate_antecedent_ids=self.get_candidates_for(anaphor, attr='nounphrases'),
                candidate_nounphrase_ids=self.get_candidates_for(anaphor, attr='nounphrases'),
                candidate_antecedent_txt=self.get_candidates_for(anaphor, attr='nounphrases_'),
                candidate_nounphrase_txt=self.get_candidates_for(anaphor, attr='nounphrases_'),
                candidate_antecedent_h_ids=self.get_candidates_for(anaphor, attr='nounphrases_h'),
                candidate_nounphrase_h_ids=self.get_candidates_for(anaphor, attr='nounphrases_h'),
                candidate_antecedent_h_txt=self.get_candidates_for(anaphor, attr='nounphrases_h_'),
                candidate_nounphrase_h_txt=self.get_candidates_for(anaphor, attr='nounphrases_h_'),
                speaker_info=self.speaker_info,
                document_raw=self.document_raw
            )

            ba.finalise()
            instances.append(ba)

        return instances


def spans_id_to_text(doc: List[List[str]], spans: List[List[int]]) -> List[List[str]]:
    """
        Input: [(2, 4), (5, 10)]
        Output: [ 'potato tomato', 'i see a red door']

        i.e., the spans are superimposed on the doc, based on the given docname, and the text is returned.
    """

    # Get a flattened document, i.e. just a list of words... no sentence information.
    doc: List[str] = to_toks(doc)

    # Using the spans to index the flattened doc, and get a list of words for each span.
    word_spans: List[List[str]] = [doc[start: end] for start, end in spans]

    # # Concatenate each list of words to form a continuous text span.
    # text_spans: List[str] = [' '.join(word_span) for word_span in word_spans]

    return word_spans


def get_all_antecedent_spans(relevant_instances: List[BridgingInstance]) -> List[List[int]]:
    """ a list of spans like [(1,5), ... ] corresponding to antecedents of all data. DISCARD Duplicates. """
    return [list(x) for x in {(instance.antecedent[0], instance.antecedent[1]) for instance in relevant_instances}]


def get_all_antecedent_spans_with_heads(relevant_instances: List[BridgingInstance]) \
        -> (List[List[int]], List[List[int]]):
    """
        Input: a list of BridgingInstances

        Output:
            a list of antecedent span' root words span
            a list of antecedent spans

            in this order!

        Removes duplicates.
    """

    # Get a [ (int, int), (int, int) ] of antecedent spans
    antecedents: List[Tuple[int, int]] = [(instance.antecedent[0], instance.antecedent[1])
                                          for instance in relevant_instances]

    # Get a [ [int, int], [int, int] ] of antecedent spans' root words
    antecedents_h: List[List[int]] = [instance.antecedent_h for instance in relevant_instances]

    # Get ID of uniques
    uniques: List[int] = get_uniques(antecedents)

    return [list(antecedents_h[i]) for i in uniques], [list(antecedents[i]) for i in uniques]


def has_negative_class(classes: List[int]) -> bool:
    """ If any item is negative, return True. If all items positive, return False. """
    for cls in classes:
        if cls < 0:
            return True
    return False


def gather_instances_by_docnames(instances: Iterable) -> Dict[str, List[BridgingInstance]]:
    """
        **Key**: instance.docname | **Value**: list of all data with this docname
        If the docname is not a key, make a new key with its value as [], and append this instance.
    """
    indexed_by_docnames: Dict[str, List[BridgingInstance]] = OrderedDict()
    for instance in instances:
        indexed_by_docnames.setdefault(instance.docname, []).append(instance)
    return indexed_by_docnames


def get_dataset_from_docname(docname: str, bashi_docnames: Union[Set, List]):
    """
    Returns the dataset name for use in the bridging fine-tuning 'genre' metadata embedding, based on the instance
    docname.
    The isnotes and bashi docnames are very similar, so the workaround is to provide all the bashi docnames as a
    set or list.
    """
    if docname in bashi_docnames:
        corpus = "bashi"
    elif docname.startswith("nw/wsj/"):
        corpus = "isnotes"
    elif docname.startswith("Trains_"):
        corpus = "arrau"
    elif docname.startswith("light"):
        corpus = "light"
    elif docname.startswith("AMI"):
        corpus = "ami"
    elif docname.startswith("Persuasion"):
        corpus = "persuasion"
    elif docname.startswith("Switchboard"):
        corpus = "switchboard"
    else:
        corpus = ""
    assert not corpus == ""
    return corpus


if __name__ == '__main__':
    _pathfix.suppress_unused_import()
