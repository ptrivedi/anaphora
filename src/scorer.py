from typing import List, Tuple, Dict, Union
from config import ROOT_LOC
import tempfile
import os
import sys

sys.path.append(str(ROOT_LOC) + "/universal-anaphora-scorer")
from coval.ua.reader import get_all_docs, get_doc_markables, process_clusters, get_markable_assignments, get_coref_infos
from coval.ua.markable import Markable
from coval.eval import evaluator


def evaluate_ua_corpus_predicted_anaphors(output_dict, key_file):
    # save predictions to temp file
    lines = []
    for i, (_, doc_dict) in enumerate(output_dict.items()):
        lines += convert_bridg_json_to_ua_doc_dali_bridging(doc_dict, include_header=True if i == 0 else False)
        lines.append("")  # add new line at end of each doc

    with tempfile.NamedTemporaryFile(mode="w+", buffering=1) as tmp:
        for line in lines:
            tmp.write(line)
            tmp.write('\n')
        doc_coref_infos, doc_non_referring_infos, doc_bridging_infos = get_coref_infos(key_file, tmp.name,
                                                                                       keep_singletons=True,
                                                                                       keep_split_antecedent=True,
                                                                                       keep_bridging=True,
                                                                                       keep_non_referring=False,
                                                                                       evaluate_discourse_deixis=False,
                                                                                       use_MIN=False)
        score_ar, score_fbm, score_fbe = evaluator.evaluate_bridgings(doc_bridging_infos)
        recall_ar, precision_ar, f1_ar = score_ar
        recall_fbm, precision_fbm, f1_fbm = score_fbm
        recall_fbe, precision_fbe, f1_fbe = score_fbe
    return f1_fbe


def evaluate_ua_corpus(key_file: str,
                       predictions: Dict[Tuple[str, Tuple[int, int]], Tuple[Tuple[int, int], List[str]]]):
    """
    predictions: dict with keys being (doc_name, (anaphor_start, anaphor_end)) keys, and values being
    ((ant_start, ante_end), words) tuples. Note that prediction span end values are not inclusive, but the annotation
    span end values are inclusive (ie pred_span (1,4) == annotation_span (1,3))
    Scores predictions using the official universal-anaphora-scorer implementation.
    This will load the ua-formatted key file, then map predictions to these gold annotations, then call their eval
    functions.

    1. map bridging instances to "key_bridging_pairs", doc_name, spans can be joined
        bridging_pairs dict with Markable anaphor object keys to  Markable ante values
    2. need to create bridging mention to gold for each document

    for each document:
        get key bridging pairs and mention clusters (clusters of mentions that refer to the same thing... ?)
        map predicted bridging pairs to key bridging pairs

    """
    key_docs = get_all_docs(key_file)
    doc_bridging_infos = {}
    for doc in key_docs:
        key_clusters, key_bridging_pairs = get_doc_markables(doc, key_docs[doc], False, True,
                                                             markable_column=10)
        # key_bridging_pairs is dict with Markable anaphor object keys to Markable ante values
        sys_bridging_pairs = {key: value for key, value in key_bridging_pairs.items()}
        no_prediction_anaphors = []
        for anaphor, gold_ante in key_bridging_pairs.items():
            pred_key = (doc, (anaphor.start, anaphor.end + 1))
            if pred_key in predictions:
                predicted_ante = predictions[pred_key]
                if not predicted_ante[0][0] == gold_ante.start or not predicted_ante[0][1] - 1 == gold_ante.end:
                    predicted_ante = Markable(doc, predicted_ante[0][0], predicted_ante[0][1] - 1, None,
                                              "referring", predicted_ante[1])
                    sys_bridging_pairs[anaphor] = predicted_ante
            else:
                no_prediction_anaphors.append(anaphor)
        for ana_key in no_prediction_anaphors:
            # delete anaphors with no predicted antecedent (these are most likely in validation set and we don't want
            # to count them as wrong)
            del sys_bridging_pairs[ana_key]
            for k in list(key_bridging_pairs.keys()):
                if k.start == ana_key.start and k.end == ana_key.end:
                    del key_bridging_pairs[k]
        (key_clusters, key_non_referrings, key_removed_non_referring,
         key_removed_singletons) = process_clusters(key_clusters, True, True, True)
        sys_mention_key_cluster = get_markable_assignments(key_clusters)
        if len(key_bridging_pairs) > 0:
            doc_bridging_infos[doc] = (key_bridging_pairs, sys_bridging_pairs, sys_mention_key_cluster)
    score_ar, score_fbm, score_fbe = evaluator.evaluate_bridgings(doc_bridging_infos)
    recall_fbe, precision_fbe, f1_fbe = score_fbe
    return f1_fbe


def crac_save_predictions_jsons_to_ua(output_dict, output_file):
    """
        {
            "clusters": [[[0,0],[5,5]],[[2,3],[7,8]], #Coreference
            "bridging_pairs"[[[14,15],[2,3]],....] #Bridging
            "doc_key": "nw",
            "sentences": [["John", "has", "a", "car", "."], ["He", "washed", "the", "car", "yesteday","."],...
            "speakers": [["sp1", "sp1", "sp1", "sp1", "sp1"], ["sp1", "sp1", "sp1", "sp1", "sp1","sp1"],... #Optional
        }
    """
    lines = []
    for i, (_, doc_dict) in enumerate(output_dict.items()):
        lines += convert_bridg_json_to_ua_doc_dali_bridging(doc_dict, include_header=True if i == 0 else False)
        lines.append("")  # add new line at end of each doc

    # Check if the dir exists
    if not output_file.exists():
        output_file.parent.mkdir(parents=True, exist_ok=True)
        output_file.touch(exist_ok=True)

    with open(output_file, 'w+') as f:
        for line in lines:
            f.write(line)
            f.write('\n')


def convert_bridg_json_to_ua_doc_dali_bridging(json_doc, include_header=True):
    # from https://github.com/sopankhosla/codi2021_scripts/blob/main/helper.py
    json_doc['tokens'] = [word for sent in json_doc['sentences'] for word in sent]
    pred_clusters = [tuple(tuple(m) for m in cluster) for cluster in json_doc['clusters']]
    lines = []
    if include_header:
        lines.append("# global.columns = ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC IDENTITY BRIDGING "
                     "DISCOURSE_DEIXIS REFERENCE NOM_SEM")
    lines.append("# newdoc id = " + json_doc['doc_key'])
    markable_id = 1
    entity_id = 1
    a = 1
    men_mark_map = {}
    bridg_strs = [""] * len(json_doc['tokens'])
    coref_strs = [""] * len(json_doc['tokens'])
    for pairs in json_doc['bridging_pairs']:
        for (start, end) in pairs:
            if True:  # start < len(json_doc['tokens']) and end < len(json_doc['tokens']):
                coref_strs[start] += "(EntityID={}|MarkableID=markable_{}".format(entity_id, markable_id)
                men_mark_map[(start, end)] = markable_id
                if start == end:
                    coref_strs[end] += ")"
                else:
                    coref_strs[end] = ")" + coref_strs[end]
            markable_id += 1
        entity_id += 1
    for pairs in json_doc['bridging_pairs']:
        bridg_strs[pairs[0][0]] += "(MarkableID=markable_{}|MentionAnchor=markable_{}".format(
            men_mark_map[(pairs[0][0], pairs[0][1])], men_mark_map[(pairs[1][0], pairs[1][1])])
        if pairs[0][0] == pairs[0][1]:
            bridg_strs[pairs[0][1]] += ")"
        else:
            bridg_strs[pairs[0][1]] = ")" + bridg_strs[pairs[0][1]]
    for _id, token in enumerate(json_doc['tokens']):
        if bridg_strs[_id] == "":
            bridg_strs[_id] = "_"
        sentence = "{}  {}  _  _  _  _  _  _  _  _  {}  {}  _  _  _" \
            .format(_id + 1, token, coref_strs[_id], bridg_strs[_id])
        lines.append(sentence)
    return lines


if __name__ == "__main__":
    key_file = str(ROOT_LOC / "data/raw/crac-arrau/Trains_91.CONLL")
    """predictions: dict with keys being (doc_name, (anaphor_start, anaphor_end)) keys, and values being
    ((ant_start, ante_end), words) tuples."""
    predictions = {('Trains_91/dia8-2', (234, 238)): ((154, 156), ['any', 'boxcar'])}

    print(evaluate_ua_corpus(key_file, predictions))
