"""
    TEMP: For now, this file is limited to getting the model in models/salience.py to train.
    To that end, we are going to use mytorch's generic loop for most of our boilerplate.
"""
import wandb
import click
import torch
import random
import numpy as np
from pathlib import Path
from tqdm.auto import tqdm
from functools import partial
from gensim.models import KeyedVectors
from typing import Union, Callable, Dict
from mytorch.utils.goodies import Timer, update_lr, mt_save, tosave

# Local Imports
from config import LOCATIONS as LOC
from utils.nlp import NUM_SALIENCY_FEATURES, salience_features
from model.salience import WordEmbeddingClassifier, WordEmbeddingClassifierAlter
from dataloader import DataLoader, TrainableInstanceIterator, split_data, WrappedInstances
from evaluate.base import ParametricEvaluationBench, BaseEvaluationBench, RandomEvaluationBench


class Metrics:
    """A collection of static functions that can be called a bit intelligently"""

    @staticmethod
    def train_acc(true: torch.tensor, pos: torch.tensor, neg: Union[float, torch.tensor] = 0) -> float:
        """
            how many times is y_pos > y_neg when y_true is 1; and y_pos < y_neg when y_true is 0
            When working with pointwise data: y_neg is 0 (assuming y_pos is b/w -1 and 1)
            When working with pairwise data: y_neg is specified
        """
        # noinspection PyTypeChecker
        return torch.mean((((pos >= neg) * 1.0).squeeze() == true) * 1.0).item()

    def pointwise(self, pred: torch.tensor, true: torch.tensor):
        return {'metric_acc': self.train_acc(true=true, pos=pred)}

    def pairwise(self, pos: torch.tensor, neg: torch.tensor, labels: torch.tensor):
        return {'metric_acc': self.train_acc(true=labels, pos=pos, neg=neg)}


def get_label_weights_bce(negatives: int) -> np.ndarray:
    """
        weight for label: 0.5 * total instances / instances for this class

        instances for this class 0 / total instances = 1 / (negatives + 1)
        instances for this class 1 / total instances = 1 - ( 1 / (negatives + 1))

        Output is in the sequence: weight for negatives; weight for positives
    """
    total_by_positives_ratio = 1 / (negatives + 1)
    total_by_negatives_ratio = 1 - total_by_positives_ratio

    weight_negatives = 0.5 / total_by_negatives_ratio
    weight_positives = 0.5 / total_by_positives_ratio

    return np.asarray([weight_negatives, weight_positives])


# Copied over and edited from mytorch.
def generic_loop(epochs: int,
                 data: dict,
                 device: Union[str, torch.device],
                 opt: torch.optim,
                 loss_fn: torch.nn,
                 data_fn: Callable,
                 model: torch.nn.Module,
                 train_fn: Callable,
                 eval_bench: BaseEvaluationBench,
                 key_metric: str = 'metric_acc',
                 save: bool = False,
                 save_params: dict = None,
                 save_dir: Path = None,
                 save_above: float = -np.inf,
                 save_args: dict = None,
                 epoch_count: int = 0,
                 epoch_start_hook: Callable = None,
                 epoch_end_hook: Callable = None,
                 batch_start_hook: Callable = None,
                 batch_end_hook: Callable = None,
                 # weight_decay: float = 0.0,
                 clip_grads_at: float = -1.0,
                 lr_schedule=None,
                 eval_fn: Callable = None,
                 enablewandb: bool = False,
                 lr_scheduler=None) -> (list, list, list, list):
    """

        A generic training loop, which based on diff hook fns (defined below), should handle anything given to it.

        The model need not be an nn.Module,
             but should have correctly wired forward and a predict function.

        # Data input
            Data should be a dict like so:
                {"train":{"x":np.arr, "y":np.arr}, "val":{"x":np.arr, "y":np.arr} }
            or more generally,
                {"train": something that can be thrown to data_fn, "valid": something that can be thrown to data_fn}

        # Saving Logic
            If the flag is enabled, give in the dir and it'll save traces and the model (and the model encoder)
                everytime training acc exceeds all prev ones.

        ## If you want to save diff parts of the model,
        Prepare save args like -
            save_args = {'torch_stuff': [tosave('model.torch', clf.state_dict()),
                                         tosave('model_enc.torch', clf.encoder.state_dict())]}
        and pass it to the model alongwith.
        If the arg is empty, it defaults to -
            save_args = {'torch_stuff': [tosave('model.torch', model.state_dict())]}

    :param lr_scheduler: change the LR as you continue training
    :param enablewandb: flag which if true logs summary of every epoch
    :param epochs: number of epochs to train for
    :param data: data dict (structure specified above)
    :param device: torch device to init the tensors with
    :param opt: torch optimizer, with proper param_groups for better lr decay per layer
    :param loss_fn: torch.nn loss fn
    :param model: torch module needed for
            i: grad clipping
            ii: for calling eval() and train() (regarding dropout)
    :param train_fn: a function which takes x & y, returns loss and y_pred
    :param eval_fn: a function which takes y_pred and y_gold tensors are returns a dict of metrics {'nm': va.lue}
    :param eval_bench: an object of class which inherits from BaseEvaluationBench,
        i.e. can call `<obj>.run()` and expect a dict with metrics.
        Should evaluate the model over the ENTIRE test set.
        All of those should be provided to the bench before hand. Not within the loop.
    :param key_metric: one of the metrics returned by the eval_fn and eval_bench. Use this metric to compare runs.
    :param save: [OPTIONAL] bool which wants either doesn't save, or saves at best
    :param save_dir: [OPTIONAL] Path object to which we save stuff (based on save_best)
    :param save_params: [OPTIONAL] a dict of all the params used while running and training the model.
    :param save_above: [OPTIONAL] acts as threshold regarding model saving.
        If the current trn accuracy is less than this, won't.
    :param save_args: [OPTIONAL] reference to the model to be saved
    :param epoch_count: an int which is added with #epochs
            (for better representation of how many epochs have actually passed).
            You can use this for when you run the loop say 3 times, do something else and run it for another 10.
    :param epoch_start_hook: a fn that can be called @ start of every epoch (returns model, opt)
    :param epoch_end_hook: a fn that can be called @ end of every epoch (returns model, opt)
    :param batch_start_hook: a fn that can be called @ start of every batch (returns model, opt)
    :param batch_end_hook: a fn that can be called @ end of every batch (returns model, opt)
    :param clip_grads_at: in case you want gradients clipped, send the max val here
    :param lr_schedule: a schedule that is called @ every batch start.
    :param data_fn: a class to which we can pass X and Y, and get an iterator.
    :param eval_fn: (optional) function which when given pred and true, returns acc
    # :param weight_decay: a L2 ratio (as mentioned in (https://arxiv.org/pdf/1711.05101.pdf)
    :return: traces
    """

    train_loss = []
    train_key_metric: list = []
    eval_key_metric: list = []
    train_metrics: Dict[str, list] = {}
    eval_metrics: Dict[str, list] = {}
    lrs = []
    saved_info = {}

    # Epoch level
    for e in range(epoch_count, epochs + epoch_count):

        per_epoch_loss: list = []
        per_epoch_tr_key_metric: list = []
        per_epoch_tr_metrics: Dict[list] = {}

        # Train
        with Timer() as timer:

            # Enable dropouts
            model.train()

            # TODO: Add hook at start of epoch (how to decide what goes in)
            # all the relevant params should go in. that's what should happen.
            if epoch_start_hook:
                epoch_start_hook()

            # Make data
            trn_dl, val_dl = data_fn(data['train']), data_fn(data['valid'])

            for x, y in tqdm(trn_dl):

                if batch_start_hook:
                    batch_start_hook()

                opt.zero_grad()

                if lr_schedule:
                    lrs.append(update_lr(opt, lr_schedule.get()))

                # Turn inputs to tensors
                _x = {k: torch.tensor(v, dtype=torch.long, device=device) for k, v in x.items()}
                _y = torch.tensor(y, dtype=torch.float, device=device)

                # Forward
                y_pred = train_fn(_x)

                # y_pred is a tuple if we're in pairwise mode
                if type(y_pred) is tuple:
                    y_pos, y_neg = y_pred[0], y_pred[1]
                    loss = loss_fn(pos=y_pos, neg=y_neg, labels=_y)
                    batch_metrics: dict = eval_fn(pos=y_pos, neg=y_neg, labels=_y)
                else:
                    loss = loss_fn(pred=y_pred, true=_y)
                    batch_metrics: dict = eval_fn(y_pred=y_pred, y_true=_y)

                # Logging
                per_epoch_tr_key_metric.append(batch_metrics[key_metric])
                _ = [per_epoch_tr_metrics.setdefault(metric_nm, []).append(metric_vl)
                     for metric_nm, metric_vl in batch_metrics.items()]
                per_epoch_loss.append(loss.item())

                # Backward
                loss.backward()

                if clip_grads_at > 0.0:
                    torch.nn.utils.clip_grad_norm_(model.parameters(), clip_grads_at)

                # for group in opt.param_groups:
                #     for param in group['params']:
                #         param.data = param.data.add(-weight_decay * group['lr'], param.data)

                opt.step()
                if batch_end_hook:
                    batch_end_hook()

        # Val
        with torch.no_grad():

            # Disable dropouts
            model.eval()

            batch_metrics = eval_bench.run()

            per_epoch_vl_key_metric = batch_metrics[key_metric]
            per_epoch_vl_metrics = {k: v for k, v in batch_metrics.items() if k.startswith('metric')}

        # Bookkeeping TODO: fix this
        train_key_metric.append(np.mean(per_epoch_tr_key_metric))
        # noinspection PyTypeChecker
        _ = {train_metrics.setdefault(metric_nm, []).append(np.mean(metric_vl))
             for metric_nm, metric_vl in per_epoch_tr_metrics.items()}
        train_loss.append(np.mean(per_epoch_loss))
        eval_key_metric.append(per_epoch_vl_key_metric)
        _ = {eval_metrics.setdefault(metric_nm, []).append(metric_vl)
             for metric_nm, metric_vl in per_epoch_vl_metrics.items()}

        # LR Scheduling if provided
        if lr_scheduler is not None:
            lr_scheduler.step(train_loss[-1])

        # WandB Logging
        if enablewandb:
            wandb_step = {}
            for eval_metric_nm, eval_metric_vl in eval_metrics.items():
                wandb_step['vl_' + eval_metric_nm.replace('metric_', '')] = eval_metric_vl[-1]
            for train_metric_nm, train_metric_vl in train_metrics.items():
                wandb_step['tr_' + train_metric_nm.replace('metric_', '')] = train_metric_vl[-1]
            wandb_step['loss'] = train_loss[-1]
            wandb.log(wandb_step)

        print("Epoch: %(epo)03d | Loss: %(loss).5f | Tr_c: %(tracc)0.5f | Vl_c: %(vlacc)0.5f | Time: %(time).3f min"
              % {'epo': e,
                 'loss': float(np.mean(per_epoch_loss)),
                 'tracc': float(np.mean(per_epoch_tr_key_metric)),
                 'vlacc': float(np.mean(per_epoch_vl_key_metric)),
                 'time': timer.interval / 60.0})

        # Save block (flag and condition)
        if save and train_key_metric[-1] >= save_above:
            # Update threshold
            save_above = train_key_metric[-1]

            # Adding epoch info along with options
            if save_params:
                save_params['epoch'] = e
            else:
                save_params = {'epoch': e}

            # Prepare save_args if none
            if not save_args:
                save_args = {'torch_stuff': [tosave('model.torch', model.state_dict())]}

            # Call save function and save
            mt_save(save_dir,
                    torch_stuff=None if 'torch_stuff' in save_args.keys() else save_args['torch_stuff'],
                    pickle_stuff=[
                        tosave('traces.pkl', [train_key_metric, train_metrics, eval_key_metric, eval_metrics,
                                              train_loss, lrs]),
                        tosave('unsup_options.pkl', save_params)])
            print(f"Model saved on Epoch {e} at {save_dir} because of highest training acc so far")

            # Log the saved thing
            saved_info['epoch'] = e
            saved_info['accuracy'] = train_key_metric[-1]
            saved_info['directory'] = save_dir

        if epoch_end_hook:
            epoch_end_hook()

    return train_metrics, train_key_metric, eval_metrics, eval_key_metric, train_loss, lrs


@click.command()
@click.option('-e', '--epochs', type=int, default=1000, )
@click.option('-bs', '--batchsize', type=int, default=256)
@click.option('-lr', '--learningrate', type=float, default=0.001)
@click.option('-negs', '--negatives', type=int, default=2)
@click.option('--seed', type=int, default=42)
@click.option('--device', type=str, default='cpu')
@click.option('-wandb', '--enablewandb', is_flag=True, default=False)
@click.option('--pointwise', is_flag=True, default=False)
@click.option('--schedule-lr', '-slr', is_flag=True, default=False, help="Reduce the LR with opt steps")
@click.option('--dataset', '-d', type=str, help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('-o', '--optimizer', type=str, default='adam', help="Give `adam` or `sgd` to change the optimizer.")
@click.option('--dropout', type=float, default=0.0)
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--candidate-reduction-method', '-crm', default='none', type=str,
              help="The chosen method to reduce the number of candidates")
@click.option('-a', '--approach', type=str, default='wordtovec',
              help="The approach we're gonna use to get the model, vocab and everything")
def run(epochs: int, batchsize: int, learningrate: float, negatives: int, seed: int, pointwise: bool, enablewandb: bool,
        candidate_reduction_method: str, device: str, dataset: str, dataset_domain, dataset_domain_filename,
        approach: str, schedule_lr: bool, optimizer: str, dropout: float):
    """
        This snippet is limited to running a w2v based classifier. Frills to be added onto it as time goes.
    """
    # print("ECHO PARAMS")
    # print(click.get_current_context().params)

    # Clamp randomness
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    random.seed(seed)
    np.random.seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    # Get some specifics IF we're using 'wordtovec'
    if approach in ['wordtovec', 'wordtovec-alter']:
        PAD_TOK = '<PAD>'
    else:
        raise NotImplementedError

    """
        - Embeddings and Vocabulary Management
            - pull the w2v vectors from disk
            - init model with the vectors
    """
    gensim_model = KeyedVectors.load_word2vec_format(LOC.wordtovec, binary=True)
    tok2id: Dict[str, int] = {w: i + 1 for i, w in enumerate(gensim_model.vocab.keys())}
    assert PAD_TOK not in tok2id.keys(), f"Selected PAD - {PAD_TOK}: 0 is already in vocab."
    tok2id[PAD_TOK] = 0

    # Offset the vectors so that zero: <PAD> id, and one: <UNK> id
    vectors = np.zeros((gensim_model.vectors.shape[0] + 1, gensim_model.vectors.shape[1]), dtype=np.float32)
    vectors[1:, :] = gensim_model.vectors
    del gensim_model

    """
        Bridging Data Management
            - make a data loader for train data
            - make a data loader for test data
            - pass them to the trainable iterator wrapper (for torch compatible iterator)
    """
    dl = DataLoader(dataset=dataset, dataset_domain=dataset_domain, dataset_domain_filename=dataset_domain_filename)
    include_params = [{'dataset': 'bashi'}, {'dataset': 'crac', 'dataset_domain': 'arrau'}, {'dataset': 'gigaword'}]
    include = [DataLoader(**param) for param in include_params]

    # Make an instance of eval bench to be able to reduce candidates
    random_evalbench = RandomEvaluationBench(
        data_loader=dl,  # Does not matter. We're not using these instances
        candidates_src='nounphrase',
        candidate_reduction_method=candidate_reduction_method)

    # Pass these to the split data thing to get raw lists of instances.
    split_instances: dict = split_data(dl=dl, include=include)

    data_fn = partial(TrainableInstanceIterator, vocab=tok2id, vocab_oov_str='</s>', bs=batchsize,
                      negatives=negatives, salience_method=salience_features, saliency_dim=NUM_SALIENCY_FEATURES,
                      method='pointwise' if pointwise else 'pairwise',
                      candidate_reduction_function=random_evalbench.reduce_candidates)

    """
        Initializing the torch model, the optimizer, the loss functions
    """
    if approach == 'wordtovec':
        model = WordEmbeddingClassifier(torch.from_numpy(vectors), additional_feat_dim=NUM_SALIENCY_FEATURES,
                                        label_weights=get_label_weights_bce(negatives), dropout=dropout).to(device)
    elif approach == 'wordtovec-alter':
        model = WordEmbeddingClassifierAlter(torch.from_numpy(vectors), additional_feat_dim=NUM_SALIENCY_FEATURES,
                                             label_weights=get_label_weights_bce(negatives), dropout=dropout).to(device)
    else:
        raise NotImplementedError(f"NO approach encoded -- {approach}")
    if optimizer == 'adam':
        opt = torch.optim.Adam(model.parameters(), lr=learningrate)
    elif optimizer == 'sgd':
        opt = torch.optim.SGD(model.parameters(), lr=learningrate)
    else:
        raise NotImplementedError(f"No way to make optimizer - {optimizer}")
    loss_fn = model.pointwise_loss if pointwise else model.pairwise_loss
    fwd_fn = model.forward if pointwise else model.pairwise_forward

    if schedule_lr:
        lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer=opt,
            mode='min',
            patience=3,
            factor=0.66,
            verbose=True
        )
    else:
        lr_scheduler = None

    # Also make an evaluation bench which can use our model.
    # #### This needs a vocabularizer function (static)
    # #### and a fake data loader (list of test instances and some metadata)
    vocabularizer = partial(TrainableInstanceIterator.vocabularize_wordembeddings, vocab=tok2id, oov_='</s>')
    wdl = WrappedInstances(dl.dataset, dl.dir, dl.fnames, dl.dataset_domain, dl.dataset_domain_filename,
                           split_instances['test'])
    eval_bench = ParametricEvaluationBench(model=model, model_predict=model, vocabularizer=vocabularizer,
                                           device=device, use_heads=False, data_loader=wdl, batchsize=10,
                                           candidate_reduction_method=candidate_reduction_method,
                                           candidates_src='nounphrase', salience_feat_extractor=salience_features)

    """
        WandB stuff
    """
    if enablewandb:
        wandb.init(project="bridginganaphora-parametric", entity="magnet")
        wandb.config.update(click.get_current_context().params, allow_val_change=True)
        wandb.watch(model, log_freq=500)

    """
        Prepare variables for the training loop
    """
    results = generic_loop(epochs=epochs, data={'train': split_instances['train'], 'valid': split_instances['valid']},
                           device=device, opt=opt, model=model, train_fn=fwd_fn, eval_bench=eval_bench,
                           key_metric='metric_acc', loss_fn=loss_fn, save=False, data_fn=data_fn,
                           eval_fn=Metrics().pointwise if pointwise else Metrics().pairwise, enablewandb=enablewandb,
                           lr_scheduler=lr_scheduler)

    print(results)


if __name__ == '__main__':
    run()
