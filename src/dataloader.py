"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    This file provides a nice interface to load parsed data, with minimal changes to existing code.
    We return an iterable which does not keep all instances in memory and loads and dumps file-by-file.
"""
import os
import copy
import click
import random
import pickle
import numpy as np
from abc import ABC
from pathlib import Path
from copy import deepcopy
from functools import partial
from sklearn.utils import shuffle
from typing import List, Optional, Dict, Iterable, Callable, Union, Tuple

from config import LOCATIONS as LOC
from utils.misc import BadParameters
from utils.nlp import to_toks, to_str
from utils.exceptions import InvalidStateCVDL
from utils.bridging import BridgingInstance, BridgingDocument


class BaseLoader(ABC):

    def __init__(self):
        self.dir: Path = Path()
        self.fnames: List[str] = []
        self._n_: int = -1

    @property
    def dir_suffix(self) -> str:
        parsed_dir = str(self.dir)
        return parsed_dir[parsed_dir.index('parsed') + 7:]

    @staticmethod
    def _count_instances_(fdir: Path, fnames: List[str]) -> List[int]:
        """ Return a list of bridging instances in each file by loading each and storing its length """
        n: List[int] = []
        for fname in fnames:
            with (fdir / fname).open('rb') as f:
                bridging_instances_batch: List[Union[BridgingInstance, BridgingDocument]] = pickle.load(f)

            n.append(len(bridging_instances_batch))

        return n

    def __len__(self) -> int:
        """
            Go through all the files and count the number of instances.
            If you do this before
        """

        if self._n_ < 0:
            self._n_ = sum(self._count_instances_(self.dir, self.fnames))

        return self._n_

    @staticmethod
    def get_fnames(dataset: str, dataset_domain: Optional[str] = None,
                   dataset_domain_filename: Optional[str] = None,
                   get_documents: bool = False, get_test: bool = False,) -> (Path, List[str]):
        """
            Return the dir inside ~/data/parsed, and the associated file names
            If documents are expected: look for documents.pkl; not dump.pkl
            If we are working over test instances, add /test to data/parsed
        """

        if dataset not in ['crac'] and (dataset_domain is not None or dataset_domain_filename is not None):
            raise BadParameters(f"Dataset domain or file name was specified for {dataset}. Not expected.")

        if dataset in ['crac'] and dataset_domain in ['arrau'] and not dataset_domain_filename:
            # noinspection PyTypeChecker
            dataset_domain_filename = {
                'arrau': 'Trains_91.CONLL'
            }[dataset_domain]

        if get_test:
            dataset_dir: Path = LOC.parsed / 'test'
        else:
            dataset_dir: Path =  LOC.parsed

        dataset_dir: Path = dataset_dir / dataset
        if dataset_domain:
            dataset_dir = dataset_dir / dataset_domain
        if dataset_domain_filename:
            dataset_dir = dataset_dir / dataset_domain_filename

        # print(f"Looking for pickled files in {str(dataset_dir)}")
        f_prefix = 'dump_' if not get_documents else 'documents_'
        fnames: List[str] = [x for x in os.listdir(str(dataset_dir)) if x.startswith(f_prefix) and x.endswith('.pkl')]

        return dataset_dir, fnames


class DataLoader(BaseLoader):
    """
        Provide a keyword and we load a dataset.
        The loading is lazy: only load one file at a time.
        We expect that you dont store all instances so as to not run out of memory
            (think of the millions of mined instances).

        # Usage Examples

        **Object as an iterator**:
        # Optional. See other examples for alternative ways to use it.
        dl = DataLoader('isnotes')
        for instance in dl():
            ...

        **Loading a regular dataset**
        dl = DataLoader('isnotes')
        for instance in dl.all():
            ...

        **Loading the default file of a crac dataset**
        dl = DataLoader('crac', 'switchboard')
        dl = DataLoader('crac', dataset_domain='arrau')

        **Loading a specific file of a crac dataset's domain**
        dl = DataLoader('crac', dataset_domain='arrau', dataset_domain_filename='Trains_93.CONLL')

        **Loading documents instead of anaphor instances**
        dl = DataLoader(..., documents=True)

        NOTE: iterating over "few" instances would not count towards the "all" fn call.
            i.e. if you call `all` after calling `few`, you will still start from scratch.

        **Shuffle data (only when iterating through it the first time)**
        dl = DataLoader('bashi', shuffle=True)

    """

    def __init__(self, dataset: str, dataset_domain: Optional[str] = None,
                 dataset_domain_filename: Optional[str] = None,
                 shuffle: bool = False, test: bool = False, documents: bool = False):
        """

        :param dataset: str in ['crac', 'isnotes','bashi' ... ]
        :param dataset_domain: optional, str in ['arrau', 'switchboard' ... ]
        :param dataset_domain_filename: optional, str in ['Trains_91.CONLL' .. ]
        :param shuffle: flag which if True will shuffle the dataset ONLY the first time we iterate through it
            and keep it consistent the next time we run through it
        :param test: flag which if True will look for instances from data/parsed/test instead of data/parsed
        :param documents: flag which if True will yield BridgingDocuments, otherwise, will yield BridgingInstances
        """
        super().__init__()
        self.dataset: str = dataset
        self.dataset_domain: Optional[str] = dataset_domain
        self.dataset_domain_filename: Optional[str] = dataset_domain_filename
        self._shuffle_: bool = shuffle
        self._is_documents: bool = documents
        self._is_test: bool = test

        self.dir: Path
        self.fnames: List[str]

        self.dir, self.fnames = self.get_fnames(self.dataset, self.dataset_domain, self.dataset_domain_filename,
                                                get_test=self._is_test, get_documents=self._is_documents)

        self.dirs_aux: List[Path] = []
        self.fnames_aux: Dict[Path, List[str]] = {}

        # Some book keeping required for playing nice downstream
        if self.dataset == 'crac' and self.dataset_domain == 'arrau' and self.dataset_domain_filename is None:
            self.dataset_domain_filename = str(self.dir).split('/')[-1]

        # Shuffle the fnames if instructed
        if self._shuffle_:
            np.random.shuffle(self.fnames)

        self._n_: int = -1
        self._finished_: bool = False

        # Will be used when we want to store the data in mem
        self.data: Optional[List[Union[BridgingInstance, BridgingDocument]]] = None

    def include(self, dataset: str, dataset_domain: Optional[str] = None,
                dataset_domain_filename: Optional[str] = None, test: bool = False):
        """ If called when we started iterating, raise Error """

        # Get the fname and dir
        dataset_dir, fnames = self.get_fnames(dataset, dataset_domain, dataset_domain_filename,
                                              get_test=test, get_documents=self._is_documents)

        # Check if the dir does not already exist
        assert dataset_dir != self.dir and dataset_dir not in self.dirs_aux

        self.dirs_aux.append(dataset_dir)
        self.fnames_aux[dataset_dir] = fnames

    def all(self):
        """
            Yield one instance at a time
        """

        for fname in self.fnames:
            with (self.dir / fname).open('rb') as f:
                data_batch: List[Union[BridgingInstance, BridgingDocument]] = pickle.load(f)

                if self._shuffle_ and not self._finished_:
                    # Shuffle but only if this is the first iteration and we have been asked to shuffle.
                    np.random.shuffle(data_batch)

                for instance in data_batch:
                    instance.finalise()
                    yield instance

        for fdir, fnames in zip(self.dirs_aux, self.fnames_aux.values()):
            for fname in fnames:
                with (fdir / fname).open('rb') as file_pkl:
                    data_batch: List[Union[BridgingInstance, BridgingDocument]] = pickle.load(file_pkl)
                if self._shuffle_ and not self._finished_:
                    np.random.shuffle(data_batch)

                for instance in data_batch:
                    instance.finalise()
                    yield instance

        self._finished_: bool = True

    def __iter__(self):
        """ This function enables simply calling dl() instead of dl.all(). """
        return self.all()


class CrossValidationDataLoader(BaseLoader):
    """
        Similar to invoke as the one above. But can run cross validation on a dataset.
        Specify the total_folds (defaults to five)

        TODO: specify your own train-test splits

        # Usage

        Iterate through it in a cross validation setup

        ```python
        cvdl = CrossValidationDataLoader('isnotes', total_folds=5, shuffle=True)
        for n_fold in range(5):
            for instance in cvdl.train():
                ...
            for instance in cvdl.valid():
                ...
            # When we have run through train test splits, we switch to the next fold
        ```

        ## Include other data along with
        ```python
        cvdl = CrossValidationDataLoader('isnotes', total_folds=5, shuffle=True)
        cvdl.include('bashi')
        cvdl.include('crac', 'switchboard')

        # As with dataloader above, you can make it yield BridgingDocument instead of BridgingAnaphors by adding the flag
        cvdl = CrossValidationDataLoader(..., documents=True)

        # As with dataloader above, you can make it yield test instances (files from data/parsed/**test**) by
        cvdl = CrossValidationDataLoader(..., test=True)

        cvdl = CrossValidationDataLoader('crac', 'arrau', 'Trains_91.CONLL', total_folds=5, shuffle=True,
                                            include=[{'dataset': 'bashi'},
                                                     {'dataset': 'crac', 'dataset_domain': 'arrau',
                                                        'dataset_domain_filename':'Trains_93.CONLL'})

        # You can then either iterate through all of the data by

        # Or Normally like
        for n_fold in range(5):
            for instance in cvdl.train():   # Train include 80% of Trains_91; all of bashi and Trains_93
                ...
            for instance in cvdl.valid():   # Test includes 20% of Trains_91
                ...
        ```

        # Workings

        During Init, load the first dataset. Load any included dataset. Load all file names.
        Set a flag specifying that train should be called next.

        On the first time train is called:

            Load the fnames from the dataset. Load the num of instances in each file.
            For each fname, maintain a list of indices which belong in test set, and another for the train set..
            When running self.train,
                load a file's instances; and iterate only over the ones we've allocated to be in train.


    """

    def __init__(self, dataset: str, dataset_domain: Optional[str] = None,
                 dataset_domain_filename: Optional[str] = None, include: Iterable[Dict[str, str]] = (),
                 shuffle: bool = False, folds: int = 5, seed: int = 42, test: bool = False, documents: bool = False):
        """

        :param dataset: str in ['crac', 'isnotes','bashi' ... ]
        :param dataset_domain: optional, str in ['arrau', 'switchboard' ... ]
        :param dataset_domain_filename: optional, str in ['Trains_91.CONLL' .. ]
        :param include: a list of dict like  {'dataset': 'crac', 'dataset_domain': 'arrau',
                                    'dataset_domain_filename':'Trains_93.CONLL'}
                        If given, we will include this datasets when calling `train`, but not when calling test.
        :param shuffle: bool flag. if true. we shuffle datasets. not otherwise.
        :param seed: set seed if shuffle, and set it to the value defined.
        :param test: flag which if True will look for instances from data/parsed/test instead of data/parsed
        :param documents: flag which if True will yield BridgingDocuments, otherwise, will yield BridgingInstances
        :param folds: int = 5
        """
        super().__init__()
        self.dataset: str = dataset
        self.dataset_domain: Optional[str] = dataset_domain
        self.dataset_domain_filename: Optional[str] = dataset_domain_filename
        self._shuffle_: bool = shuffle
        self.total_folds: int = folds
        self._is_documents: bool = documents
        self._is_test: bool = test

        self.dirs_aux: List[Path] = []
        self.fnames_aux: Dict[Path, List[str]] = {}

        self.dir, self.fnames = self.get_fnames(self.dataset, self.dataset_domain, self.dataset_domain_filename,
                                                get_test=self._is_test, get_documents=self._is_documents)

        if self.dataset == 'crac' and self.dataset_domain == 'arrau' and self.dataset_domain_filename is None:
            self.dataset_domain_filename = str(self.dir).split('/')[-1]

        # Set State
        self.state_: str = 'init'
        self._n_fold_: int = -1

        for params in include:
            self.include(**params)

        # Set variables to be filled in later
        self.train_indices: Dict[Path, List[int]] = {}
        self.valid_indices: Dict[Path, List[int]] = {}

        if self._shuffle_:
            random.seed(seed)
            np.random.seed(seed)

    def include(self, dataset: str, dataset_domain: Optional[str] = None,
                dataset_domain_filename: Optional[str] = None, test: bool = False):
        """ If called when we started iterating, raise Error """

        if self.state_ != 'init':
            raise InvalidStateCVDL(f"Tried including a new dataset after already iterating. Currently: {self.state_}")

        # Get the fname and dir
        dataset_dir, fnames = self.get_fnames(dataset, dataset_domain, dataset_domain_filename,
                                              get_test=test, get_documents=self._is_documents)

        # Check if the dir does not already exist
        assert dataset_dir != self.dir and dataset_dir not in self.dirs_aux

        self.dirs_aux.append(dataset_dir)
        self.fnames_aux[dataset_dir] = fnames

    def force_next_fold(self):
        """ If the current state is training or validating, we return an error """
        if self.state_ in ['validating', 'training']:
            raise InvalidStateCVDL(f"Force fold change invoked when state was {self.state_}")

        self._prep_next_fold_()
        self.state_ = 'training'

    def train(self):

        # Do some constraints on when train can be called.
        if self.state_ not in ['init', 'validated', 'training', 'trained']:
            raise InvalidStateCVDL(f"Called train. Currently: {self.state_}")

        # If train is called after validated, we have moved to the next fold.
        if self.state_ in ['init', 'validated']:
            # This is the first run of this fold. Re arrange data.
            self._prep_next_fold_()

        # Set state
        self.state_ = 'training'

        for file_path, train_indices in self.train_indices.items():

            # Load the instances
            with file_path.open('rb') as file_pkl:
                data_batch: List[Union[BridgingInstance, BridgingDocument]] = pickle.load(file_pkl)

            # Shuffle indices if needed
            if self._shuffle_:
                np.random.shuffle(train_indices)

            # Based on the indices, yield a instance
            if len(train_indices) == 0:
                indices = range(len(data_batch))
            else:
                indices = train_indices
            for train_index in indices:
                data_batch[train_index].finalise()
                yield data_batch[train_index]

        self.state_ = 'trained'

    def valid(self):

        if self.state_ not in ['trained', 'validated']:
            raise InvalidStateCVDL(f"Called valid without completing train. Currently: {self.state_}")

        # Set state
        self.state_ = 'validating'

        # print(f"Starting the valid split of fold: {self._n_fold_}")

        for file_path, valid_indices in self.valid_indices.items():

            # Load the instances
            with file_path.open('rb') as file_pkl:
                data_batch: List[Union[BridgingInstance, BridgingDocument]] = pickle.load(file_pkl)

            # Shuffle indices if needed
            if self._shuffle_:
                np.random.shuffle(valid_indices)

            # Based on the indices, yield a instance
            for valid_index in valid_indices:
                data_batch[valid_index].finalise()
                yield data_batch[valid_index]

        # Done validating on this split. Set new state.
        self.state_ = 'validated'

    def _prep_next_fold_(self):
        """
            Create indices for one fold. When fold is over, this should be called again.
            Return a dict like { 'fname': ([indices in train], [indices in test]) }

            Empty List implies, use all in train.
            Do not include aux files in valid index at all
        """

        # Increase the fold count
        self._n_fold_ += 1

        if self._n_fold_ >= self.total_folds:
            raise InvalidStateCVDL(f"Called more than the specified folds: {self.total_folds}.")

        # Count the number of instances in self.fnames
        n_primary: List[int] = self._count_instances_(self.dir, self.fnames)

        # If this is the first run, and we do want to shuffle things around.
        if self.state_ == '__init__' and self._shuffle_:
            # Shuffle all file names
            np.random.shuffle(self.fnames)
            _aux_fnames_ = self.fnames_aux.items()
            np.random.shuffle(_aux_fnames_)
            self.fnames_aux = dict(_aux_fnames_)
            self.dirs_aux = list(self.fnames_aux.keys())

        # Divide it into train and valid
        for i, fname in enumerate(self.fnames):
            # For this file
            key: Path = self.dir / fname
            len_valid_split: int = int(n_primary[i] / self.total_folds)
            valid_split_start = len_valid_split * self._n_fold_
            valid_split_end = max(len_valid_split * (self._n_fold_ + 1), n_primary[i]) \
                if self._n_fold_ == self.total_folds - 1 else len_valid_split * (self._n_fold_ + 1)

            valid_indices = list(range(valid_split_start, valid_split_end))
            train_indices = list(range(0, valid_split_start)) + list(range(valid_split_end, n_primary[i]))

            self.train_indices[key] = train_indices
            self.valid_indices[key] = valid_indices

        # Now we include all aux datasets here as well
        for fdir, fnames in zip(self.dirs_aux, self.fnames_aux.values()):
            for fname in fnames:
                self.train_indices[fdir / fname] = []


class WrappedCVDL:
    """
        Wrap CVDL.train or valid like so:
        cvdl = CrossValidationDataLoader('bashi')
        wdl = WrappedCVDL(cvdl.train)
        for x in wdl:
            ...

        NOTE: Can repeat. That is:

        cvdl = CrossValidationDataLoader('bashi')
        wdl = WrappedCVDL(cvdl.train, cvdl)
        for x in wdl:
            ...

        for x in wdl:
            ...

        WILL WORK
    """

    def __init__(self, fn: Callable, obj: CrossValidationDataLoader):
        self.fn: Callable = fn
        self.obj: CrossValidationDataLoader = obj

    def __len__(self):
        try:
            i = 0
            for _ in self.fn():
                i += 1
            return i

        except InvalidStateCVDL:
            # This will most likely happen when we directly call valid of a cvdl without calling a train.

            if self.obj.state_ == 'init':
                # Run the train once and then try again.
                for _ in self.obj.train():
                    ...
                i = 0
                for _ in self.fn():
                    i += 1
                return i
            else:
                raise InvalidStateCVDL("CVDL was expected to be at init when it threw an error. But it wasn't.")

    def __iter__(self):
        return self.fn()

    def __getattr__(self, item):
        return getattr(self.obj, item)


class WrappedInstances(list):
    """
    Overwriting a dict to give it metadata enough to pass though the evaluation classes without throwing errors.
        The usage is tricky.
        Do this and nothing else:

        dl = DataLoader(...whatever...)
        dataset = WrappedInstances(dl.dataset, dl.dir, dl.fnames, dl.dataset_domain, dl.dataset_domain_filename,
                                   split_instances['test'])
    """

    def __init__(self, dataset: str, datasetdir: Path, fnames: List[str], dataset_domain: Optional[str] = None,
                 dataset_domain_filename: Optional[str] = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dataset: str = dataset
        self.dataset_domain: Optional[str] = dataset_domain
        self.dataset_domain_filename: Optional[str] = dataset_domain_filename
        self.dir: Path = datasetdir
        self.fnames: List[str] = fnames


class TrainableInstanceIterator:
    """
    Used for ranking based antecedent selection

        A configurable iterator which returns a batch of:
        - context - (bs, sl)
        - anaphor text - (bs, sl)
        - antecedent candidates - (bs, n_candidates, sl)
        - correct antecedent ID

    TODO: what to do with the tokenizer. Can I make a diff function for different family of tokenizers?
    TODO: make it work with BridgingDocument as well.

    1. Create a flat list of all items (in memory)
    2. Sample from this list
    """

    def __init__(self, datasource: Union[WrappedCVDL, DataLoader, List[BridgingInstance]],
                 vocab: Dict[str, int], vocab_oov_str: str, bs: int, method: str = 'pointwise',
                 candidate_reduction_function: Optional[Callable] = None, candidate_src: str = 'nounphrase',
                 salience_method: Optional[Callable] = None, saliency_dim: int = 0,
                 device: str = 'cpu', use_heads: bool = False, negatives: int = 10, padidx: int = 0):

        # Store param values
        self.bs: int = bs
        self.device: str = device
        self.method: str = method
        self.padidx: int = padidx
        self.negatives: int = negatives
        self.use_heads: bool = use_heads
        self.candidates_src: str = candidate_src
        self.candidate_reduction_method: Optional[Callable] = candidate_reduction_function
        self.salience_method: Optional[Callable] = salience_method
        self.salience_dim: int = saliency_dim

        # Store some more important params
        self.vocab: Dict[str, int] = vocab
        self.oov_: str = vocab_oov_str
        self.instances: List[BridgingInstance] = list(datasource)  # Run the dataloader and get all instances

        # This is specific to WordEmbedding Approaches. Needs to be changed later.
        self.vocabularize = partial(self.vocabularize_wordembeddings, vocab=self.vocab, oov_=self.oov_)

        # The variable in which we store data ripe for being iterated over
        self._prepare_phase_two_: Callable = self._pointwise_prepare_ if self.method == 'pointwise' \
            else self._pairwise_prepare_
        self.data: Dict[str, np.ndarray] = self._prepare_()

    def _init_np_(self, shape: Iterable[int]) -> np.ndarray:
        return np.zeros(shape, dtype=np.long) + self.padidx

    @staticmethod
    def vocabularize_wordembeddings(tokens: List[str], vocab: Dict[str, int], oov_: str) -> List[int]:
        return [vocab.get(tok, vocab[oov_]) for tok in tokens]

    @staticmethod
    def prepare_one_instance(instance: BridgingInstance, anaphor: List[str], anaphor_span: List[int],
                             candidates: List[List[str]], candidates_span: List[List[int]],
                             vocabularize: Callable, salience_feat_extractor: Optional[Callable] = None):
        """
            Vocabularize the spans. And get the salient features if instructed to.
        """


        # Turn every str to ids
        anaphor_: List[int] = vocabularize(anaphor)
        candidates_: List[List[int]] = [vocabularize(neg_candidate) for neg_candidate in candidates]

        if salience_feat_extractor is not None:
            candidates_feats: List[List[int]] = [salience_feat_extractor(span_a=anaphor_span,
                                                                         span_b=neg_candidate,
                                                                         instance=instance)
                                                 for neg_candidate in candidates_span]

            return anaphor_, candidates_, candidates_feats
        else:
            return anaphor_, candidates_, None

    def _prepare_(self) -> Dict[str, np.ndarray]:
        """
            Returns:
                1. np array of (#instances, maxlen) of anaphor word ids
                2. np array of (#instances, maxlen) of correct antecedent word ids
                3. np array of (#instances, <some size>, maxlen) of incorrect antecedent word ids
                4. np array of (#instances, salient feat dim) corresponding to corr antecedent & anaphor
                5. np array of (#instances, <some size>, maxlen) corresponding to neg antecedent & anaphor
        """
        all_anaphors: list = []
        all_pos: list = []
        all_neg: list = []
        all_pos_feats: Optional[list] = []
        all_neg_feats: Optional[list] = []

        for instance in self.instances:

            """ Reduce the candidates for this instance based on the provided method """
            if self.candidate_reduction_method is not None:
                self.candidate_reduction_method(instance)

            """ Extract artefacts to process based on use_heads and candidate_src flags."""
            anaphor = instance.anaphor_ if not self.use_heads else instance.anaphor_h_
            anaphor_span = instance.anaphor if not self.use_heads else instance.anaphor_h

            # Get the candidates
            candidates = getattr(instance, f"candidate_{self.candidates_src}_txt") if not self.use_heads else \
                getattr(instance, f"candidate_{self.candidates_src}_h_txt")
            candidates_span = getattr(instance, f"candidate_{self.candidates_src}_ids") if not self.use_heads else \
                getattr(instance, f"candidate_{self.candidates_src}_h_ids")

            # Don't hurt the actual instances
            candidates = copy.deepcopy(candidates)
            candidates_span = copy.deepcopy(candidates_span)
            pos_candidate = candidates.pop(getattr(instance, f"correct_{self.candidates_src}_id"))
            pos_candidate_span = candidates_span.pop(getattr(instance, f"correct_{self.candidates_src}_id"))

            # Sample negative candidates
            neg_candidate_ids = np.random.choice(range(len(candidates)), self.negatives)
            neg_candidates = [candidates[i] for i in neg_candidate_ids]
            neg_candidates_span = [candidates_span[i] for i in neg_candidate_ids]

            # To prep this instance, concat the pos candidate AT THE START of candidate list
            candidates = [pos_candidate] + neg_candidates
            candidates_span = [pos_candidate_span] + neg_candidates_span

            # Conv. to vocab and get salient features.
            anaphor, neg_cand, neg_cand_feats = self.prepare_one_instance(
                instance=instance, anaphor=anaphor, candidates=candidates,  anaphor_span=anaphor_span,
                candidates_span=candidates_span, vocabularize=self.vocabularize,
                salience_feat_extractor=self.salience_method
            )

            # Unpack the outputs
            pos_cand = neg_cand[0]
            neg_cand = neg_cand[1:]
            if neg_cand_feats is not None:
                pos_cand_feats = neg_cand_feats[0]
                neg_cand_feats = neg_cand_feats[1:]
            else:
                pos_cand_feats = None

            neg_cand = [x for x in neg_cand if x != [0]]

            # If either the anaphor, or the positive candidates are zero vectors (oov), we drop 'em
            if pos_cand == [0] or anaphor == [0] or len(neg_cand) == 0:
                continue

            # Then just append them where needed
            all_anaphors.append(anaphor)
            all_pos.append(pos_cand)
            all_neg.append(neg_cand)
            all_pos_feats.append(pos_cand_feats)
            all_neg_feats.append(neg_cand_feats)

        # Pass them through whichever phase 2 preparation function that has been decided (pointwise/pairwise)
        return self._prepare_phase_two_(all_anaphors, all_pos, all_neg, all_pos_feats, all_neg_feats)

    def _pairwise_prepare_(self, all_anaphors, all_pos, all_neg, all_pos_feats, all_neg_feats) -> Dict[str, np.array]:
        # Filter out None Values from all_pos_feats, all_neg_feats.
        # #### If no saliency_feat_extractor is given, they will become empty lists.
        # #### Which will in turn be handled below gracefully.
        all_pos_feats = list(filter(None, all_pos_feats))
        all_neg_feats = list(filter(None, all_neg_feats))

        # total data points = len(all_pos) + total negative instances
        total: int = sum(len(x) for x in all_neg)
        maxlen_anaphor: int = max(len(x) for x in all_anaphors)
        maxlen_candidates: int = max(max(len(x) for x in all_pos),
                                     max(len(x) for this_neg in all_neg for x in this_neg))

        ana_final: np.ndarray = self._init_np_((total, maxlen_anaphor))
        pos_final: np.ndarray = self._init_np_((total, maxlen_candidates))
        neg_final: np.ndarray = self._init_np_((total, maxlen_candidates))
        label_final: np.ndarray = self._init_np_((total,)) + 1
        if self.salience_method is not None:
            pos_feats_final: Optional[np.ndarray] = self._init_np_((total, self.salience_dim))
            neg_feats_final: Optional[np.ndarray] = self._init_np_((total, self.salience_dim))
        else:
            pos_feats_final = None
            neg_feats_final = None

        # Iterate through each instance
        _start: int = 0
        for i, (_ana, _pos, _negs) in enumerate(zip(all_anaphors, all_pos, all_neg)):
            # Count data point in this instance
            _len: int = len(_negs)

            ana_final[_start: _start+_len, :len(_ana)] = _ana
            pos_final[_start: _start+_len, :len(_pos)] = _pos
            for j, _neg in enumerate(_negs):
                neg_final[_start + j, :len(_neg)] = _neg

            if all_pos_feats and all_neg_feats:
                pos_feats_final[_start: _start + _len] = all_pos_feats[i]
                neg_feats_final[_start: _start + _len] = all_neg_feats[i]

            _start += _len

        if all_pos_feats and all_neg_feats:
            ana_final, pos_final, neg_final, pos_feats_final, neg_feats_final = \
                shuffle(ana_final, pos_final, neg_final, pos_feats_final, neg_feats_final)
            return {'anaphor': ana_final, 'positive': pos_final, 'negative': neg_final, 'label': label_final,
                    'positive_features': pos_feats_final, 'negative_features': neg_feats_final}
        else:
            # We now need to shuffle these three arrays together
            ana_final, pos_final, neg_final = shuffle(ana_final, pos_final, neg_final)
            return {'anaphor': ana_final, 'positive': pos_final, 'negative': neg_final, 'label': label_final}

    def _pointwise_prepare_(self, all_anaphors, all_pos, all_neg, all_pos_feats, all_neg_feats) -> Dict[str, np.array]:
        """
            Combine the pos, neg and anaphor lists to a list of
                <anaphor word ids> <candidate word ids> <0 if candidate is negative, 1 if positive>
        """

        # Filter out None Values from all_pos_feats, all_neg_feats.
        # #### If no saliency_feat_extractor is given, they will become empty lists.
        # #### Which will in turn be handled below gracefully.
        all_pos_feats = list(filter(None, all_pos_feats))
        all_neg_feats = list(filter(None, all_neg_feats))

        # total data points = len(all_pos) + total negative instances
        total: int = len(all_pos) + sum(len(x) for x in all_neg)
        maxlen_anaphor: int = max(len(x) for x in all_anaphors)
        maxlen_candidates: int = max(max(len(x) for x in all_pos),
                                     max(len(x) for this_neg in all_neg for x in this_neg))

        # Declare an anaphor matrix
        ana_final: np.ndarray = self._init_np_((total, maxlen_anaphor))  # Anaphors
        can_final: np.ndarray = self._init_np_((total, maxlen_candidates))  # Candidates
        lab_final: np.ndarray = self._init_np_((total,))  -1 # Labels
        if self.salience_method is not None:
            feats_final: Optional[np.ndarray] = self._init_np_((total, self.salience_dim))
        else:
            feats_final = None

        # Iterate through each instance
        _start: int = 0
        for i, (_ana, _pos, _negs) in enumerate(zip(all_anaphors, all_pos, all_neg)):
            # Count data points in this instance
            _len: int = len(_negs) + 1

            ana_final[_start:_start + _len, :len(_ana)] = _ana
            can_final[_start, :len(_pos)] = _pos
            for j, _neg in enumerate(_negs):
                can_final[_start + 1 + j, :len(_neg)] = _neg

            lab_final[_start] = 1

            if all_pos_feats and all_neg_feats:
                feats_final[_start] = all_pos_feats[i]
                feats_final[_start + 1: _start + 1 + len(all_neg_feats[i])] = all_neg_feats[i]

            _start += _len

        if all_pos_feats and all_neg_feats:
            ana_final, can_final, lab_final, feats_final = shuffle(ana_final, can_final, lab_final, feats_final)
            return {'anaphor': ana_final, 'candidate': can_final, 'label': lab_final, 'features': feats_final}
        else:
            # We now need to shuffle these three arrays together
            ana_final, can_final, lab_final = shuffle(ana_final, can_final, lab_final)
            return {'anaphor': ana_final, 'candidate': can_final, 'label': lab_final}

    def __iter__(self):
        """
            The yield format looks like
            {'anaphor': tensor, 'candidate':tensor}, tensor
                resembling the X, Y sort of a thing
         """
        total: int = self.data['anaphor'].shape[0]
        for i in range(0, total, self.bs):

            batch: dict = {}
            for key in self.data.keys():
                batch[key] = self.data[key][i: min(i + self.bs, total)]
            labels = batch.pop('label')
            yield batch, labels


class TrainableDocumentIterator:
    """
        Used for classification of span into anaphoric nounphrase / or not

        Returns:
            text: (bs, sl)
            class: (bs, 1)
    """

    def __init__(self, datasource: Union[WrappedCVDL, DataLoader, List[BridgingDocument]],
                 vocab_fn: Callable, hf_name: str, bs: int, max_len_bpts: int, max_len_toks: int, device: str = 'cpu'):
        self.bs: int = bs
        self.hf_name: str = hf_name
        self.device: str = device
        self.vocab_fn: Callable = vocab_fn
        self.max_len_bpts: int = max_len_bpts
        self.max_len_toks: int = max_len_toks
        self.data: dict = {}

        self.prepare(datasource)

    @staticmethod
    def _create_offset_dict_(instance: BridgingDocument, sentences: List[List[int]]) -> Dict[int, int]:
        """
            Create a dict where key: current position of a token. Value: the offset (to be subtracted from it).
            E.g. { 30: 30, 45: 5} ->
                every token id including and above 30 needs to have 30 subtracted from it
                every token id including and above 45 needs to have ANOTHER 5 subtracted from it.
        """

        # Create a length aggregate list corresponding to instance.sentence_lengths_
        n_words_before_this_sent: List[int] = [sum(instance.sentence_lengths_[:sent_n])
                                               for sent_n in range(len(instance.sentence_lengths_))]

        offset: Dict[int, int] = {n_words_before_this_sent[sentences[0][0]]: n_words_before_this_sent[sentences[0][0]]}
        for i in range(1, len(sentences)):
            gap_start = sentences[i - 1][1] + 1
            gap_end = sentences[i][0]

            gap_len = n_words_before_this_sent[gap_end] - n_words_before_this_sent[gap_start]
            offset[n_words_before_this_sent[gap_end]] = gap_len

        return offset

    @staticmethod
    def do_offset_(span: List[int], offset: Dict[int, int]) -> List[int]:
        """ [2, 30]; {1:1, 5:10; 29: 14} -> [2-1; 30-1-10-14]. i.e., sub val from span if span >= key """
        span = deepcopy(span)

        offset_k = [k for k in offset.keys() if k <= span[0]]
        offset_v = sum(offset[k] for k in offset_k)
        span[0] -= offset_v
        span[1] -= offset_v

        # for span_pos in range(len(span)):
        #     offset_k = [k for k in offset.keys() if k <= span[span_pos]]
        #     offset_v = sum(offset[k] for k in offset_k)
        #     span[span_pos] -= offset_v

        return span

    @staticmethod
    def impose_sentence_span(document: List[List[str]], spans: List[List[int]]) -> List[List[str]]:
        """ Doc is list of tokenized sentences. spans is [[2, 5], [6, 7]] i.e. concat sent 2,3,4 and 6 and return """
        trimmed: List[List[str]] = []
        for span in spans:
            trimmed += document[span[0]: span[1] + 1]
        return trimmed

    def _convert_span_to_bpt_(self, doc: List[List[str]], span: List[int], offset: Dict[int, int]) -> (int, int):
        """ Get the index of the span inside the doc. Used to find index of antecedent inside doc (hf tokenized) """

        # First, remove the offset from the span, and tokenize the doc
        doc: List[str] = to_toks(doc)
        span = self.do_offset_(span, offset)

        assert span[1] <= len(doc), "The offset was not properly done."

        txt_doc_upto_span_end = ' '.join(doc[:span[1]])
        tok_doc_upto_span_end = self.vocab_fn(txt_doc_upto_span_end, return_tensors='pt', add_special_tokens=True,
                                              max_length=self.max_len_bpts, truncation=True)
        txt_span = ' '.join(doc[span[0]:span[1]])
        """
            EDGE CASE bug fix:
            RoBERTa tokenizes things in a weird way. So the tokenization of
                ```
                the neighborhood: [   133, 3757]
                neighborhood: [    858,  8774, 12514,  8489]
                ```
            So we change 'neighborhood' to ' neighborhood'
            
            Thus, when we're working with Roberta, and when span[1] - span[0] == 1, we add a space before the text span
        """
        if 'roberta' in self.hf_name.lower() and span[1] - span[0] == 1 and ' ' not in txt_span:
            txt_span = ' ' + txt_span
        tok_span = self.vocab_fn(txt_span, return_tensors='pt', add_special_tokens=False,
                                  max_length=self.max_len_bpts, truncation=True)

        end: int = tok_doc_upto_span_end['input_ids'].shape[1] - 1
        start: int = end - tok_span['input_ids'].shape[1]

        return start, end

    def prepare(self, datasource: Iterable[BridgingDocument]):
        """
            Go over the list of BridgingDocument, and for each span in each document
                -> get sentences based on span
                -> get bpt seq. corresponding to the sentences
                -> tokenize the selected sentence text
                -> store the tokenized text and the bpt span and whether the span is an anaphor or not

            Shuffle  all the stored stuff
        """
        # Init arrays where we're gonna store the good stuff
        text_input_ids = []
        text_token_type_ids = []
        text_attention_mask = []
        spans = []
        labels = []

        # Iterate over all documents
        for doc in datasource:
            for i, span in enumerate(doc.nounphrases):

                # Get the span of relevant sentences
                sent_end_id = doc.get_sentence(span)[1]+1
                sent_start_id = -1

                if sent_end_id == 1 or len(to_toks(doc.document[:sent_end_id])) < self.max_len_toks:
                    # If we're only working with the first sent or if the selected sentences fit inside our limit
                    sent_start_id = 0
                else:
                    # Start dropping one sent at a time till you're satisfied with the length
                    for i in range(sent_end_id):
                        if len(to_toks(doc.document[i: sent_end_id])) < self.max_len_toks:
                            sent_start_id = i
                            break

                    if sent_start_id == -1:
                        raise AssertionError("No sentence seems to fit inside our boundaries.")

                # Create offset dir
                offset_dict = self._create_offset_dict_(doc, [[sent_start_id, sent_end_id]])

                # Tokenize the text
                raw_doc = self.impose_sentence_span(doc.document, [[sent_start_id, sent_end_id]])
                doc_bpt = self.vocab_fn(to_str(raw_doc), return_tensors='np', add_special_tokens=True,
                                        max_length=self.max_len_bpts, truncation=True, padding=True)

                # Get the span's bpt counterpart
                span_bpt = self._convert_span_to_bpt_(raw_doc, offset=offset_dict, span=span)

                # Finally, get the label
                label = 1 if span in doc.anaphors else 0

                # Store these things
                text_input_ids.append(doc_bpt['input_ids'])
                text_token_type_ids.append(doc_bpt['token_type_ids'])
                text_attention_mask.append(doc_bpt['attention_mask'])
                spans.append(span_bpt)
                labels.append(label)

        # Convert everything to numpy arrays and shuffle together
        text_input_ids = np.asarray(text_input_ids)
        text_token_type_ids = np.asarray(text_token_type_ids)
        text_attention_mask = np.asarray(text_attention_mask)
        spans = np.asarray(spans)
        labels = np.asarray(labels)

def split_data(dl: DataLoader, ratio: Tuple[int, int, int] = (0.7, 0.15, 0.15),
               include: Optional[List[DataLoader]] = None) -> dict:
    """
        This function takes instances from dataloader in memory,
            and splits them into train, test and valid splits. Based on the provided ratio.

        We can also pass multiple data loaders all of whose instances will be used in the train set.
    """
    if not sum(ratio) == 1.0:
        raise AssertionError(f"Ratio: {ratio} does not add up to one.")

    # Get all instances from the main dataloader
    instances = list(dl)
    n = len(instances)

    # Shuffle the instances
    np.random.shuffle(instances)

    # Divide them up
    train_ratio, valid_ratio, test_ratio = ratio
    train_instances = instances[:int(n * train_ratio)]
    valid_instances = instances[int(n * train_ratio): int(n * (train_ratio + valid_ratio))]
    test_instances = instances[int(n * (train_ratio + valid_ratio)):]

    # If other datasets are provided, append them all to train
    for dl in include:
        train_instances += list(dl)

    # Finally, shuffle the train instances again
    np.random.shuffle(train_instances)

    return {'train': train_instances, 'valid': valid_instances, 'test': test_instances}


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--shuffle', default=False, is_flag=True)
def get_dataloader(dataset, dataset_domain, dataset_domain_filename, shuffle) -> DataLoader:
    return DataLoader(dataset, dataset_domain, dataset_domain_filename, shuffle)


if __name__ == '__main__':
    # dl = get_dataloader()
    # print(dl.dir)

    # cvdl = CrossValidationDataLoader('bashi')
    #
    # list(cvdl.train())
    # list(cvdl.valid())
    # list(cvdl.train())
    # list(cvdl.valid())

    # Testing the Trainable data loader
    dl = DataLoader("isnotes")

    # Pull vectors
    from gensim.models import KeyedVectors

    vectors = KeyedVectors.load_word2vec_format(LOC.wordtovec, binary=True)
    tok2id: Dict[str, int] = {w: i for i, w in enumerate(vectors.vocab.keys())}
    del vectors

    ti = TrainableInstanceIterator(dl, tok2id, vocab_oov_str='</s>', bs=10, negatives=20)
    ti.data.keys(), ti.data['anaphor'].shape[0]

    for x in ti:
        break
