import os
from pathlib import Path

from mytorch.utils.goodies import FancyDict


"""
    ROOT_LOC = '..' if code is called from src
    ROOT_LOC = '.' if code is called from project root
    
    Prefixing with this ensures code works from both places of being called. 
    Python path management is hell.
"""
NLTK_PATH = os.environ.get('NLTK_DATA', "/home/priyansh/Dev/perm/nltk/nltk_data")
ROOT_LOC: Path = Path('..') if str(Path().cwd()).split('/')[-1] == 'src' else Path('.')
LOCATIONS: FancyDict = FancyDict(
    root=ROOT_LOC,
    raw=ROOT_LOC / Path('data/raw'),
    parsed=ROOT_LOC / Path('data/parsed'),
    other=ROOT_LOC / Path('data/other'),
    predictions=ROOT_LOC / Path('data/predictions'),
    knowbert_wsd=ROOT_LOC / Path('data/knowbert_wsd'),
    ontonotes=ROOT_LOC / Path('data/raw/ontonotes/ontonotes-release-5.0/data/files/data/english/annotations'),
    wikitext=ROOT_LOC / Path('data/raw/wikitext/wikitext-103'),
    wordtovec=ROOT_LOC / Path('models/wordtovec/GoogleNews-vectors-negative300.bin'),
    wordnetdir=ROOT_LOC / Path('models/wordnet/wn_synset'),
    knowbert=ROOT_LOC / Path("models/knowbert"),
    pairwisewsdcache=ROOT_LOC / Path("data/other/wsd/pairwise")
)
CRAC_RAWDATA_FILES: FancyDict = FancyDict(
    trains91=LOCATIONS.raw / Path('crac-arrau/Trains_91.CONLL'),
    trains93=LOCATIONS.raw / Path('crac-arrau/Trains_93.CONLL'),
    switchboard=LOCATIONS.raw / Path("crac-switchboard/Switchboard_3_dev.CONLL"),
    light=LOCATIONS.raw / Path('crac-light/light_dev.CONLLUA'),
    ami=LOCATIONS.raw / Path('crac-ami/AMI_dev.CONLLUA'),
    persuasion=LOCATIONS.raw / Path('crac-persuasion/Persuasion_dev.CONLLUA'),
)
