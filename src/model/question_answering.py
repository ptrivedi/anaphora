"""
Implementation of "Bridging Anaphora Resolution as Question Answering" (https://arxiv.org/pdf/2004.07898.pdf) using
mangoes.

import sys, os
parent_dir = os.getcwd()
sys.path.append(parent_dir)
"""
from typing import List, Tuple, Union, Dict, Iterable
import json
from itertools import product
import random
import warnings

import mangoes
import numpy as np
import torch
from tqdm.auto import tqdm

from dataloader import DataLoader, CrossValidationDataLoader, BaseLoader
from model.fine_tune_base import BertFineTuneBase
from utils.bridging import BridgingInstance
from utils.nlp import to_toks, to_str
from utils.exceptions import NoCandidateLeft


class BertQuestionAnswering(BertFineTuneBase):
    def __init__(self, data_loader: DataLoader, model: str, max_len: int, candidates_src: str,
                 candidate_reduction_method: str, tqdm: bool = True,
                 tokenizer: str = "bert-base-uncased", proximity_trim: bool = False, max_score: bool = True,
                 trim_words_before: int = 50, trim_words_after: int = 20):
        """
        This class uses some functionality (namely, the "trim_document" function) from it's superclass. Thus, we give
        some dummy values to the super init function, like all the pool_* arguments (that aren't used for this
        subclass). It might be worth it to redesign the code at some point to not do this, but this works for now.

        :param data_loader: DataLoader object
        :param model: a string indicating which huggingface model to use
        :param max_len: an int suggesting the max nr of words we can consider in a document.
            This is required because the model has a max len (often 512) and our docs are often longer that this.
        :param proximity_trim: Whether to trim the document to the (anaphor_start - 50, anaphor_end + 20) word indices.
            If False, will use normal trimming procedure in trim_document. Note that if the candidate reduction method
            is not "None", it might make sense to just use the normal trimming procedure instead of proximity trimming
            further.
        :param max_score: Determines how the qa model scores candidates. If true, will use the "max_score" method, which
            adds the max end_logit and start_logit output score over the candidate span. If false, will add the
            start_logit score for the first token of the candidate span and the end_logit score for the last token of
            the candidate span.
        :param trim_words_before: if proximity_trim is true (the document is trimmed based on proximity to anaphor),
            this parameter will control how many words before the anaphor are in the trimmed document.
        :param trim_words_after: if proximity_trim is true (the document is trimmed based on proximity to anaphor),
            this parameter will control how many words after the anaphor are in the trimmed document.
        """
        super().__init__(data_loader, model, max_len, candidates_src, candidate_reduction_method, tqdm=tqdm,
                         proximity_trim=proximity_trim, trim_words_before=trim_words_before,
                         trim_words_after=trim_words_after)
        self.mangoes_model = mangoes.modeling.BERTForQuestionAnswering.load(tokenizer, model)
        self.tokenizer_name = tokenizer
        self.model_name = model
        self.tokenizer = self.mangoes_model.tokenizer
        self.model = self.mangoes_model.model
        self.max_score = max_score
        # the trim document function doesn't take into account the question and special tokens, so this is a hacky fix
        # that allocates 16 words for the question/special token
        self.max_len_toks -= 16

    def reset_model(self):
        """ Reloads the pretrained base model. Used for cross validation. """
        self.mangoes_model = mangoes.modeling.BERTForQuestionAnswering.load(self.tokenizer_name, self.model_name)
        self.model = self.mangoes_model.model

    @staticmethod
    def _convert_word_index_to_char(txt_doc: str, word_index: int, trim_case_id: int, offset: Dict[int, int]):
        """
        Convert word index in full document to char index of start of word in possibly shortened document

        txt_doc: str of shortened doc
        full_doc_sentences: list of split sentences of full document
        sentence_spans: list of sentence spans, e.g. [[1, 3], [6, 7]] indicating sent 1, 2, and 6
        word_index: word index in flattened full doc
        trim_case_id: id saying how document was trimmed, see SelfAttentionBench.trim_document()
        offset: sentence word offset after trimming, see output of SelfAttentionBench.trim_document()
            key: current position of a token. Value: the offset (to be subtracted from it).
                E.g. { 30: 30, 45: 5} ->
                    every token id including and above 30 needs to have 30 subtracted from it
                    every token id including and above 45 needs to have ANOTHER 5 subtracted from it.

        returns start character index of start of anaphor/antecedent indicated by word_index
        """
        new_word_index = word_index
        if trim_case_id > 2:
            for k, v in offset.items():
                if word_index >= k:
                    new_word_index -= v
        pretokenized_doc = txt_doc.split(' ')
        start_pos_char = len(' '.join(pretokenized_doc[:new_word_index]))
        return start_pos_char

    def _convert_span_to_qa_bpt_(self, doc: List[List[str]], question_text: str, span: List[int],
                                 offset: Dict[int, int]) -> (int, int):
        """ Get the index of the span inside the doc. Used to find index of antecedent inside doc (hf tokenized), after
         question and special tokens have been added
         """

        # First, remove the offset from the span, and tokenize the doc
        doc: List[str] = to_toks(doc)
        span = self.do_offset_(span, offset)
        assert span[1] <= len(doc), "The offset was not properly done."

        question_tokens = self.tokenizer(question_text, add_special_tokens=False, return_tensors='pt')
        # +1 for [SEP] token ([CLS] token already added in 'tok_doc_upto_span_end' below)
        question_btps_length = question_tokens['input_ids'].shape[1] + 1

        txt_doc_upto_span_end = ' '.join(doc[:span[1]])
        tok_doc_upto_span_end = self.tokenizer(txt_doc_upto_span_end, return_tensors='pt', add_special_tokens=True,
                                               max_length=self.max_len_bpts - question_btps_length, truncation=True)
        txt_span = ' '.join(doc[span[0]:span[1]])
        tok_span = self.tokenizer(txt_span, return_tensors='pt', add_special_tokens=False,
                                  max_length=self.max_len_bpts - question_btps_length, truncation=True)

        end: int = (tok_doc_upto_span_end['input_ids'].shape[1] - 1) + question_btps_length
        start: int = (end - tok_span['input_ids'].shape[1])

        return start, end

    def make_question_answering_datum(self, datum: BridgingInstance, return_candidate_spans: bool = False,
                                      add_answer_variants: bool = False):
        """
        Extracts question answering data example from a BridgingInstance.
        Returns the question text, context text, answer text, and start char position of answer in context text
        """
        if self.proximity_trim:
            sentence_spans, offset_dict, case_id, candidates = \
                self.trim_document_proximity(datum, getattr(datum, f'candidate_{self.candidates_src}_ids'),
                                             src=self.candidates_src)
        else:
            sentence_spans, offset_dict, case_id, candidates = \
                self.trim_document(datum, getattr(datum, f'candidate_{self.candidates_src}_ids'),
                                   src=self.candidates_src)

        # Use the spans to select a doc subset, and flatten it to str
        raw_doc: List[List[str]] = self.impose_sentence_span(datum.document, sentence_spans)
        txt_doc: str = to_str(raw_doc)

        # add 1 for leading space
        q_char_start = self._convert_word_index_to_char(txt_doc, datum.anaphor[0], case_id, offset_dict) + 1
        q_char_end = self._convert_word_index_to_char(txt_doc, datum.anaphor_h[1], case_id, offset_dict)
        question_text = txt_doc[q_char_start:q_char_end] + " of what ?"

        # get char index of start of answer (needed for transformers qa data input)
        a_char_start = self._convert_word_index_to_char(txt_doc, datum.antecedent[0], case_id, offset_dict) + 1
        a_char_ant_end = self._convert_word_index_to_char(txt_doc, datum.antecedent[1], case_id, offset_dict)
        answer_text = txt_doc[a_char_start:a_char_ant_end]
        if add_answer_variants:
            # answer variants include the head of the antecedent, the answer without post-modifiers, and the answer
            # without post-modifiers and the determiner
            answer_text = [answer_text]
            a_char_start = [a_char_start]
            a_char_head_start = self._convert_word_index_to_char(txt_doc, datum.antecedent_h[0], case_id, offset_dict) + 1
            a_char_head_end = self._convert_word_index_to_char(txt_doc, datum.antecedent_h[1], case_id, offset_dict)
            answer_text.append(txt_doc[a_char_head_start:a_char_head_end])
            a_char_start.append(a_char_head_start)
            if datum.antecedent_h[1] < datum.antecedent[1]:
                # if there are postmodifiers
                answer_text.append(txt_doc[a_char_start[0]:a_char_head_end])
                a_char_start.append(a_char_start[0])
            pos_tags = to_toks(datum.pos)
            if pos_tags[datum.antecedent[0]] == "DET":
                # if there is a determiner at the start of the antecedent
                num_determinants = 1
                ant_index = 1
                while pos_tags[datum.antecedent[0] + ant_index] == "DET":
                    ant_index += 1
                    num_determinants += 1
                a_char_no_det_start = self._convert_word_index_to_char(txt_doc, datum.antecedent[0] + num_determinants,
                                                                       case_id, offset_dict) + 1
                text = txt_doc[a_char_no_det_start:a_char_head_end]
                if not text == answer_text[1] and len(text) > 0:
                    answer_text.append(text)
                    a_char_start.append(a_char_no_det_start)

        if return_candidate_spans:
            if len(candidates) == 0:
                candidates: List[List[int]] = [list(x) for x in getattr(datum, f'candidate_{self.candidates_src}_ids')]
            span_ants: List[Tuple[int, int]] = [self._convert_span_to_qa_bpt_(raw_doc, question_text, list(candidate),
                                                                              offset_dict)
                                                if not candidate[0] == candidate[1] == -1 else [-1, -1]
                                                for candidate in candidates]
            return question_text, txt_doc, answer_text, a_char_start, span_ants
        if len(candidates) == 0:
            candidates: List[List[int]] = [list(x) for x in getattr(datum, f'candidate_{self.candidates_src}_ids')]
        if datum.antecedent not in candidates:
            return -1, -1, -1, -1
        return question_text, txt_doc, answer_text, a_char_start

    def train(self, train_data: BaseLoader, output_dir: str, eval_data: DataLoader = None,
              freeze_bert_base: bool = False, qa_head_learn_rate: float = None,  **training_args):
        """ Finetune the question answering heads on the bridging instances

        train_data: Baseloader subclass. can take a DataLoader or a CrossValidationDataLoader loader as input.
            if cv, eval dataset will be taken from train_data.valid(). Currently the iterable_dataset function is not
            supported when using the CrossValidationDataLoader data class, but this shouldn't be a problem, because
            if the data is so large it doesn't fit into memory, we probably shouldn't be doing cv on it.
        freeze_bert_base: boolean, whether to freeze the base layers of the bert model or not
        qa_head_learn_rate: float, learn rate of head layers (if None, will use learn rate in training_args (ie same
            learn rate as base layers).
        training_args: keyword arguments for training. Includes "learning_rate", "weight_decay", "num_train_epochs",
            "seed", "per_device_train_batch_size", "per_device_eval_batch_size", "dataloader_num_workers". See
            https://huggingface.co/transformers/main_classes/trainer.html#transformers.TrainingArguments for
            more/defaults.
        """
        train_questions = []
        train_contexts = []
        train_answers = []
        train_answer_starts = []
        if isinstance(train_data, DataLoader):
            train_iterator = train_data.all()
            if eval_data is not None:
                eval_iterator = eval_data.all()
            else:
                eval_iterator = None
        elif isinstance(train_data, CrossValidationDataLoader):
            train_iterator = train_data.train()
            eval_iterator = train_data.valid()
        else:
            raise ValueError("Unrecognized train_data input argument: should be a DataLoader object or "
                             "CrossValidationDataLoader object")
        for datum in train_iterator:
            self.reduce_candidates(instance=datum)
            try:
                if len(getattr(datum, f'candidate_{self.candidates_src}_ids')) == 0:
                    raise NoCandidateLeft
                question_text, txt_doc, answer_text, start_pos_char = \
                    self.make_question_answering_datum(datum, add_answer_variants=True)
                if not question_text == -1 and len(getattr(datum, f'candidate_{self.candidates_src}_ids')) > 0:
                    for i in range(len(answer_text)):
                        train_questions.append(question_text)
                        train_contexts.append(txt_doc)
                        train_answers.append(answer_text[i])
                        train_answer_starts.append(start_pos_char[i])
            except NoCandidateLeft:
                warnings.warn(f"No candidate left error was raised for - {datum.anaphor_id}")
        if len(train_questions) == 0:
            print("Candidate pruning has resulted in 0 trainable instance sin this fold.")
            return
        if eval_iterator is not None:
            eval_questions = []
            eval_contexts = []
            eval_answers = []
            eval_answer_starts = []
            for datum in eval_iterator:
                self.reduce_candidates(instance=datum)
                try:
                    if len(getattr(datum, f'candidate_{self.candidates_src}_ids')) == 0:
                        raise NoCandidateLeft
                    question_text, txt_doc, answer_text, start_pos_char = \
                        self.make_question_answering_datum(datum, add_answer_variants=True)
                    if not question_text == -1 and len(getattr(datum, f'candidate_{self.candidates_src}_ids')) > 0:
                        for i in range(len(answer_text)):
                            eval_questions.append(question_text)
                            eval_contexts.append(txt_doc)
                            eval_answers.append(answer_text[i])
                            eval_answer_starts.append(start_pos_char[i])
                except NoCandidateLeft:
                    warnings.warn(f"No candidate left error was raised for - {datum.anaphor_id}")

        if eval_iterator is None or len(eval_questions) == 0:
            eval_questions = None
            eval_contexts = None
            eval_answers = None
            eval_answer_starts = None
        self.mangoes_model.train(output_dir=output_dir, train_question_texts=train_questions,
                                 eval_question_texts=eval_questions, train_context_texts=train_contexts,
                                 eval_context_texts=eval_contexts,
                                 train_answer_texts=train_answers, eval_answer_texts=eval_answers,
                                 train_start_indices=train_answer_starts, eval_start_indices=eval_answer_starts,
                                 max_seq_length=self.max_len_bpts, max_query_length=128,
                                 freeze_base=freeze_bert_base, task_learn_rate=qa_head_learn_rate,
                                 compute_metrics=None, **training_args)
        # not sure if this is needed, but just to make sure reference is updated with new weights
        self.model = self.mangoes_model.model

    def predict(self, datum: BridgingInstance, src: str,
                candidate_span_ids: List[List[int]], candidate_span_txt: List[List[str]],
                candidate_span_h_ids: List[List[int]], candidate_span_h_txt: List[List[str]]) \
            -> Union[List[int], np.ndarray]:
        """
            1. make qa example
            2. predict
            3. return scores in a way that can be integrated with base class
        """
        question_text, \
            context, \
            answer_text, \
            a_char_start, \
            span_ants = self.make_question_answering_datum(datum, return_candidate_spans=True)

        prediction = self.mangoes_model.generate_outputs(question_text, context)
        if self.max_score:
            candidate_scores = [(torch.max(prediction["start_logits"][0][cand_start:cand_end + 1]) +
                                 torch.max(prediction["end_logits"][0][cand_start:cand_end + 1])).item()
                                if (cand_start != -1 and cand_end != -1) else -1000
                                for cand_start, cand_end in span_ants]
        else:
            candidate_scores = [(prediction["start_logits"][0][cand_start] + prediction["end_logits"][0][cand_end]).item()
                                if (cand_start != -1 and cand_end != -1) else -1000
                                for cand_start, cand_end in span_ants]

        return np.array(candidate_scores, dtype=np.float)

    def run_cross_validation(self, dl: CrossValidationDataLoader, qa_head_learn_rate: float = None,
                             freeze_bert_base: bool = False,
                             training_output_dir: str = "../models/qa_model_ckpts/",
                             save_models: bool = False, save_dir: str = "../models/qa_model/",
                             print_training_metrics: bool = False,
                             **training_args
                             ):
        """
        qa_head_learn_rate: float, learn rate of head layers (if None, will use learn rate in training_args (ie same
            learn rate as base layers).
        dl: CrossValidationDataLoader
        freeze_bert_base: boolean, whether to freeze the base layers of the bert model or not
        training_output_dir: str, where to output training logs
        save_models: bool, whether to save cv fine-tuned models
        save_dir: str, where to save cv fine-tuned models
        training_args: keyword arguments for training. Includes "learning_rate", "weight_decay", "num_train_epochs",
            "seed", "per_device_train_batch_size", "per_device_eval_batch_size", "dataloader_num_workers".  See
            https://huggingface.co/transformers/main_classes/trainer.html#transformers.TrainingArguments for
            more/defaults.
        """
        return super().run_cross_validation(dl, freeze_bert_base, training_output_dir, save_models, save_dir,
                                            print_training_metrics, qa_head_learn_rate=qa_head_learn_rate,
                                            **training_args)


def grid_search(cvdl: CrossValidationDataLoader, learning_rate: float = 3e-3, freeze_base: bool = True,
                num_epochs: int = 5, seed: int = 42, model_name: str = "bert-base-uncased"):
    """
    will run grid search over cross validation data loader, similar to base.py's cross_validate function.
    Each parameter group will return a dict with param values and metrics.
    This function is mainly for trimming techniques, and also model head score aggregation methods.
    Other tuning (learning rate, whether to freeze base or not), is probably easier to tune manually and then use for
    all datasets. Here I've made the defaults to these parameters equal to what I manually found worked best.
    """
    params = {"candidate_reduction_method": ["first_and_two_sents", "first_and_one_sent", "first_and_current_sent",
                                             "two_sents", "none"]}

    param_names = [k for k in params.keys()]
    param_values = [v for v in params.values()]
    param_combos: list = [comb for comb in product(*param_values)]
    print(f"{len(param_combos)} parameter combinations")
    results: List[dict] = []

    for param_combo in tqdm(param_combos, desc='Cross Validation'):
        model = BertQuestionAnswering(**{k: v for k, v in zip(param_names, param_combo)}, model=model_name,
                                      tokenizer="bert-base-uncased"
                                      if model_name == "bert-base-uncased" else "bert-base-cased", max_len=100,
                                      candidates_src='nounphrase', data_loader=cvdl)
        combo_metrics = model.run_cross_validation(cvdl, freeze_bert_base=freeze_base, learning_rate=learning_rate,
                                                   num_train_epochs=num_epochs, per_device_train_batch_size=8,
                                                   per_device_eval_batch_size=8, logging_steps=250)
        results.append(combo_metrics)
        cvdl.state_ = 'init'
        cvdl._n_fold_ = -1
        if cvdl._shuffle_:
            random.seed(seed)
            np.random.seed(seed)
    return results


if __name__ == "__main__":
    seed = 42
    model_name = "bert-base-uncased"

    dl = CrossValidationDataLoader('crac', 'arrau', 'Trains_91.CONLL', folds=5, shuffle=True, seed=seed)
    results = grid_search(dl, num_epochs=5, seed=seed, model_name=model_name)
    with open("qa_gridsearch_trains91.json", 'w') as fout:
        json.dump(results, fout)
    dl = CrossValidationDataLoader('crac', 'switchboard', folds=5, shuffle=True, seed=seed)
    results = grid_search(dl, num_epochs=5, seed=seed, model_name=model_name)
    with open("qa_gridsearch_switchboard.json", 'w') as fout:
        json.dump(results, fout)
    dl = CrossValidationDataLoader('crac', 'arrau', 'Trains_91.CONLL', folds=5, shuffle=True, seed=seed)
    results = grid_search(dl, num_epochs=5, seed=seed, model_name=model_name)
    with open("qa_gridsearch_trains91.json", 'w') as fout:
        json.dump(results, fout)
    dl = CrossValidationDataLoader('isnotes', folds=5, shuffle=True, seed=seed)
    results = grid_search(dl, num_epochs=5, seed=seed, model_name=model_name)
    with open("qa_gridsearch_isnotes.json", 'w') as fout:
        json.dump(results, fout)
    dl = CrossValidationDataLoader('bashi', folds=5, shuffle=True, seed=seed)
    results = grid_search(dl, num_epochs=5, seed=seed, model_name=model_name)
    with open("qa_gridsearch_bashi.json", 'w') as fout:
        json.dump(results, fout)
    dl = CrossValidationDataLoader('crac', 'arrau', 'Trains_93.CONLL', folds=5, shuffle=True, seed=seed)
    results = grid_search(dl, num_epochs=5, seed=seed, model_name=model_name)
    with open("qa_gridsearch_trains93.json", 'w') as fout:
        json.dump(results, fout)

    exit(0)

    candidate_reduction_method = "first_and_two_sents"
    proximity_document_trim = False
    trim_words_before = 200
    trim_words_after = 20
    max_score = False
    freeze_base = True
    learning_rate = 3e-3
    qa_model = BertQuestionAnswering(model="bert-base-uncased", tokenizer="bert-base-uncased", max_len=400,
                                     candidates_src='nounphrase', candidate_reduction_method=candidate_reduction_method,
                                     data_loader=None, proximity_trim=proximity_document_trim,
                                     max_score=max_score, trim_words_before=trim_words_before,
                                     trim_words_after=trim_words_after)

    qa_model.run_cross_validation(dl, freeze_base, learning_rate=learning_rate, num_train_epochs=5,
                                  per_device_train_batch_size=8, per_device_eval_batch_size=8, logging_steps=10)
    """ 6.238325119018555,
    # instantiate
    # dl = CrossValidationDataLoader('crac', 'switchboard',  folds=5, shuffle=True)
                               # include=[{'dataset': 'crac', 'dataset_domain': 'arrau','dataset_domain_filename':'Trains_93.CONLL'}])
    qa_model = BertQuestionAnswering(model="../models/qa_model/", tokenizer="bert-base-cased", max_len=512,
                                     candidates_src='nounphrase', data=bridging_instances)
    """
