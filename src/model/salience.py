"""
    This file will contain model which try to exploit salience along with other features
        when trying predict the anaphora stuff.
"""
import torch
import numpy as np
import torch.nn as nn
from typing import Dict
# from evaluate.contextualmodels import


class WordEmbeddingClassifier(nn.Module):

    def __init__(self, embeddings: np.array, additional_feat_dim: int, device: str = 'cpu',
                 label_weights: np.ndarray = None, dropout: float = 0.0):
        super().__init__()

        vocab_size, emb_dim = embeddings.shape
        self.additional_feat_dim: int = additional_feat_dim
        self.device: str = device

        # Manage loss internally
        if label_weights is not None:
            self.bceloss = torch.nn.BCELoss(weight=torch.tensor(label_weights, dtype=torch.float))
        else:
            self.bceloss = torch.nn.BCELoss()
        self.marginloss = torch.nn.MarginRankingLoss()

        self.embedding = nn.Embedding.from_pretrained(embeddings).to(device)
        self.dropout = nn.Dropout(dropout)
        self.clf_1 = nn.Linear(2*emb_dim+self.additional_feat_dim, emb_dim, bias=True).to(device)
        # self.clf_1 = nn.Linear(emb_dim+self.additional_feat_dim, emb_dim, bias=True).to(device)
        self.clf_2 = nn.Linear(emb_dim, 1, bias=False).to(device)

    @staticmethod
    def masked_mean(t: torch.Tensor, dim: int) -> torch.Tensor:
        return torch.sum(t, dim=dim)/torch.sum((t != 0).float(), dim=dim)

    def forward(self, x: Dict[str, torch.Tensor]):
        x_ana, x_can, x_add = x['anaphor'], x['candidate'], x['features']
        """
            x_ana: bs, sl
            x_ant: bs, sl
            x_add: bs, additional_feat_dim
        """

        x_ana_emb: torch.Tensor = self.embedding(x_ana)                             # bs, sl, emb_dim
        x_ana_pool: torch.Tensor = self.masked_mean(x_ana_emb, dim=1)                     # bs, emb_dim

        x_ant_emb: torch.Tensor = self.embedding(x_can)                             # bs, sl, emb_dim
        x_ant_pool: torch.Tensor = self.masked_mean(x_ant_emb, dim=1)                     # bs, emb_dim

        # Concatenate the other features
        # TODO: norm the other features
        x_cat: torch.Tensor = torch.cat([x_ana_pool, x_ant_pool, x_add], dim=1)     # bs, 2*emb_dim+additional_feat_dim

        x_clf_1: torch.Tensor = torch.tanh(self.dropout(self.clf_1(x_cat)))         # bs, emb_dim
        x_clf_2: torch.Tensor = torch.sigmoid(self.clf_2(x_clf_1))     # bs, 1

        return x_clf_2

    def pairwise_forward(self, x: Dict[str, torch.Tensor]):
        x_ana, x_pos,  x_neg = x['anaphor'], x['positive'], x['negative']
        x_pfeats, x_nfeats = x['positive_features'], x['negative_features']

        pos_score = self.forward({'anaphor': x_ana, 'candidate': x_pos, 'features': x_pfeats})
        neg_score = self.forward({'anaphor': x_ana, 'candidate': x_neg, 'features': x_nfeats})

        return pos_score, neg_score

    def pointwise_loss(self, pred, true):
        return self.bceloss(pred.squeeze(), true)

    def pairwise_loss(self, pos, neg, labels):
        return self.marginloss(pos, neg, labels)


class WordEmbeddingClassifierAlter(nn.Module):

    def __init__(self, embeddings: np.array, additional_feat_dim: int, device: str = 'cpu',
                 label_weights: np.ndarray = None, dropout: float = 0.0):
        super().__init__()

        vocab_size, emb_dim = embeddings.shape
        self.additional_feat_dim: int = additional_feat_dim
        self.device: str = device

        # Manage loss internally
        if label_weights is not None:
            self.bceloss = torch.nn.BCELoss(weight=torch.tensor(label_weights, dtype=torch.float))
        else:
            self.bceloss = torch.nn.BCELoss()
        self.marginloss = torch.nn.MarginRankingLoss()

        self.embedding = nn.Embedding.from_pretrained(embeddings).to(device)
        self.dropout = nn.Dropout(dropout)
        self.clf_1 = nn.Linear(emb_dim, emb_dim//2, bias=True).to(device)
        # self.clf_1 = nn.Linear(emb_dim+self.additional_feat_dim, emb_dim, bias=True).to(device)
        # self.clf_2 = nn.Linear(emb_dim, 1, bias=False).to(device)
        self.sim = torch.nn.CosineSimilarity(dim=1, eps=1e-08)

    @staticmethod
    def masked_mean(t: torch.Tensor, dim: int) -> torch.Tensor:
        return torch.sum(t, dim=dim)/torch.sum((t != 0).float(), dim=dim)

    def forward(self, x: Dict[str, torch.Tensor]):
        x_ana, x_can, x_add = x['anaphor'], x['candidate'], x['features']
        """
            x_ana: bs, sl
            x_ant: bs, sl
            x_add: bs, additional_feat_dim
        """

        x_ana_emb: torch.Tensor = self.embedding(x_ana)                             # bs, sl, emb_dim
        x_ana_pool: torch.Tensor = self.masked_mean(x_ana_emb, dim=1)                     # bs, emb_dim
        x_enc_ana: torch.Tensor = torch.tanh(self.dropout(self.clf_1(x_ana_pool)))         # bs, emb_dim

        x_ant_emb: torch.Tensor = self.embedding(x_can)                             # bs, sl, emb_dim
        x_ant_pool: torch.Tensor = self.masked_mean(x_ant_emb, dim=1)                     # bs, emb_dim
        x_enc_ant: torch.Tensor = torch.tanh(self.dropout(self.clf_1(x_ant_pool)))         # bs, emb_dim

        x_clf_2: torch.Tensor = self.sim(x_enc_ana, x_enc_ant)

        return x_clf_2

    def pairwise_forward(self, x: Dict[str, torch.Tensor]):
        x_ana, x_pos,  x_neg = x['anaphor'], x['positive'], x['negative']
        x_pfeats, x_nfeats = x['positive_features'], x['negative_features']

        pos_score = self.forward({'anaphor': x_ana, 'candidate': x_pos, 'features': x_pfeats})
        neg_score = self.forward({'anaphor': x_ana, 'candidate': x_neg, 'features': x_nfeats})

        return pos_score, neg_score

    def pointwise_loss(self, pred, true):
        return self.bceloss(pred.squeeze(), true)

    def pairwise_loss(self, pos, neg, labels):
        return self.marginloss(pos, neg, labels)
