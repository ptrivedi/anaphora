from typing import List, Tuple, Union, Dict, Iterable
import warnings
from itertools import product
import random
import json
from pathlib import Path

from tqdm.auto import tqdm
import numpy as np
import mangoes
import click

try:
    import _pathfix
except ImportError:
    from . import _pathfix
from dataloader import DataLoader, CrossValidationDataLoader, BaseLoader
from model.fine_tune_base import BertFineTuneBase
from utils.bridging import get_dataset_from_docname, BridgingDocument
from config import CRAC_RAWDATA_FILES as raw_data_files
from config import LOCATIONS as LOC
from utils.exceptions import NoCandidateLeft
from utils.wordsense import KnowbertSynsetDisambiguation, PairwiseSynsetDisambiguation
from scorer import evaluate_ua_corpus_predicted_anaphors, crac_save_predictions_jsons_to_ua
from run import update_metrics


class BertBridgingFineTune(BertFineTuneBase):
    def __init__(self, data_loader: DataLoader, model: str, max_len: int, candidates_src: str,
                 candidate_reduction_method: str, tqdm: bool = True, tokenizer: str = "bert-base-uncased",
                 use_metadata: bool = False,
                 genres: List[str] = ("isnotes", "bashi", "arrau", "switchboard", "light", "ami", "persuasion"),
                 max_segment_len: int = 256, max_segments: int = 64, proximity_trim: bool = False,
                 trim_words_before: int = 50, trim_words_after: int = 20, device: str = None,
                 graph_emb_dim: int = None, use_heads: bool = False, coref_dropout=0.3, max_top_spans=300,
                 max_top_antecendents=50, max_span_width=200, ffnn_hidden_size=512, mention_pos_weight=100.0,
                 antecedent_mask_max_len=400, non_dummy_weight=5.0, dummy_score=0.0, metadata_feature_size=20,
                 coref_depth=2):
        """
        This class uses some functionality (namely, the "trim_document" function) from it's superclass. Thus, we give
        some dummy values to the super init function, like all the pool_* arguments (that aren't used for this
        subclass). It might be worth it to redesign the code at some point to not do this, but this works for now.

        :param data_loader: DataLoader object
        :param model: a string indicating which huggingface model to use
        :param max_len: an int suggesting the max nr of words we can consider in a document.
            This is required because the model has a max len (often 512) and our docs are often longer that this.
        :param use_metadata: whether or not to use speaker data and genre data
        :param genres: possible genres if using genre metadata. Leave blank to not use genres (ie use the same genre for
            all instances)
        :param max_segment_len: max length of segments. Each input document is broken into segments.
        :param max_segments: max number of segments per document.
        :param proximity_trim: Whether to trim the document to the (anaphor_start - 50, anaphor_end + 20) word indices.
            If False, will use normal trimming procedure in trim_document. Note that if the candidate reduction method
            is not "None", it might make sense to just use the normal trimming procedure instead of proximity trimming
            further.
        :param trim_words_before: if proximity_trim is true (the document is trimmed based on proximity to anaphor),
            this parameter will control how many words before the anaphor are in the trimmed document.
        :param trim_words_after: if proximity_trim is true (the document is trimmed based on proximity to anaphor),
            this parameter will control how many words after the anaphor are in the trimmed document.
        :param graph_emb_dim: int
            size of external knowledge embeddings. Defaults to None (no embeddings)
        :param use_heads: bool flag which if turned true will ensure that every model only uses spans' head words to
            build span embedding.
        :param ffnn_hidden_size: int size of hidden layers in head
        :param mention_pos_weight: float. weight to give to positive mention examples in mention loss.
        :param antecedent_mask_max_len: int
            possible antecedents that are more than antecedent_mask_max_len words away from a particular anaphor will be
            masked for that anaphor.
        :non_dummy_weight: float
            weight given in loss function to antecedents that are not dummy antecedents
        :dummy_score: float
            score threshold for dummy antecedent (non-anaphor prediction) while training
        """
        super().__init__(data_loader, model, max_len, candidates_src, candidate_reduction_method, tqdm=tqdm,
                         proximity_trim=proximity_trim, trim_words_before=trim_words_before,
                         trim_words_after=trim_words_after)
        self.device = device
        self.use_heads = use_heads
        self.coref_dropout = coref_dropout
        self.max_top_spans = max_top_spans
        self.max_top_antecendents = max_top_antecendents
        self.max_span_width = max_span_width
        self.ffnn_hidden_size = ffnn_hidden_size
        self.mention_pos_weight = mention_pos_weight
        self.antecedent_mask_max_len = antecedent_mask_max_len
        self.non_dummy_weight = non_dummy_weight
        self.dummy_score = dummy_score
        self.metadata_feature_size = metadata_feature_size
        self.coref_depth = coref_depth
        self.mangoes_model = mangoes.modeling.BERTForCoreferenceResolution.load(tokenizer, model, device=device,
                                                                                use_metadata=use_metadata,
                                                                                graph_emb_dim=graph_emb_dim,
                                                                                max_span_width=max_span_width,
                                                                                max_top_antecendents=max_top_antecendents,
                                                                                max_top_spans=max_top_spans,
                                                                                coref_dropout=coref_dropout,
                                                                                ffnn_hidden_size=ffnn_hidden_size,
                                                                                mention_pos_weight=mention_pos_weight,
                                                                                antecedent_mask_max_len=antecedent_mask_max_len,
                                                                                non_dummy_weight=non_dummy_weight,
                                                                                dummy_score=dummy_score,
                                                                                metadata_feature_size=metadata_feature_size,
                                                                                coref_depth=coref_depth
                                                                                )
        assert self.mangoes_model.model.graph_emb_dim == graph_emb_dim
        self.metrics_ = ["metric_anaphor_recall", "metric_anaphor_precision", "metric_anaphor_f1",
                         "metric_antecedent_accuracy", "metric_ent_f1", "metric_mention_fn_rate",
                         "metric_coarse_fn_rate"]
        self.tokenizer_name = tokenizer
        self.model_name = model
        self.tokenizer = self.mangoes_model.tokenizer
        self.model = self.mangoes_model.model
        self.use_metadata = use_metadata
        self.graph_emb_dim = graph_emb_dim
        if self.graph_emb_dim is not None and self.graph_emb_dim == 200:
            self.wsd = KnowbertSynsetDisambiguation(use_heads=True)
        elif self.graph_emb_dim is not None and self.graph_emb_dim == 400:
            self.wsd = PairwiseSynsetDisambiguation()
        self.genres = genres
        self.max_segment_len = max_segment_len
        self.max_segments = max_segments
        if self.genres is not None:
            self.genre_map = {genre: i for i, genre in enumerate(genres)}
        if self.use_metadata or self.graph_emb_dim is not None:
            self.bashi_docnames = self.get_bashi_docnames()

    @staticmethod
    def get_bashi_docnames():
        # the bridging instances don't include the dataset they are from, so if using multiple dataset, we need to infer
        # the dataset from the docname. Here we presave the bashi docnames and use them when differentiating between
        # the bashi and isnotes instances
        dl = DataLoader('bashi', shuffle=True)
        bashi_docnames = set()
        for datum in dl.all():
            bashi_docnames.add(datum.docname)
        return bashi_docnames

    def reset_model(self):
        """ Reloads the pretrained base model. Used for cross validation. """
        self.mangoes_model = mangoes.modeling.BERTForCoreferenceResolution.load(self.tokenizer_name, self.model_name,
                                                                                use_metadata=self.use_metadata,
                                                                                graph_emb_dim=self.graph_emb_dim,
                                                                                max_span_width=self.max_span_width,
                                                                                max_top_antecendents=self.max_top_antecendents,
                                                                                max_top_spans=self.max_top_spans,
                                                                                coref_dropout=self.coref_dropout,
                                                                                ffnn_hidden_size=self.ffnn_hidden_size,
                                                                                mention_pos_weight=self.mention_pos_weight,
                                                                                antecedent_mask_max_len=self.antecedent_mask_max_len,
                                                                                non_dummy_weight=self.non_dummy_weight,
                                                                                dummy_score=self.dummy_score,
                                                                                metadata_feature_size=self.metadata_feature_size,
                                                                                coref_depth=self.coref_depth
                                                                                )

        self.model = self.mangoes_model.model

    def train(self, train_data: BaseLoader, output_dir: str, eval_data: DataLoader = None,
              freeze_bert_base: bool = None, task_learn_rate: float = None, **training_args):
        """ Finetune the classification heads on the bridging instances

        train_data: Baseloader subclass. can take a DataLoader or a CrossValidationDataLoader loader as input.
            if cv, eval dataset will be taken from train_data.valid(). Currently the iterable_dataset function is not
            supported when using the CrossValidationDataLoader data class, but this shouldn't be a problem, because
            if the data is so large it doesn't fit into memory, we probably shouldn't be doing cv on it.
        freeze_bert_base: boolean, whether to freeze the base layers of the bert model or not
        task_learn_rate: float, learn rate of head layers (if None, will use learn rate in training_args (ie same
            learn rate as base layers).
        training_args: keyword arguments for training. Includes "learning_rate", "weight_decay", "num_train_epochs",
            "seed", "per_device_train_batch_size", "per_device_eval_batch_size", "dataloader_num_workers". See
            https://huggingface.co/transformers/main_classes/trainer.html#transformers.TrainingArguments for
            more/defaults.
        """
        if isinstance(train_data, DataLoader):
            train_iterator = train_data.all()
            if eval_data is not None:
                eval_iterator = eval_data.all()
            else:
                eval_iterator = None
        elif isinstance(train_data, CrossValidationDataLoader):
            train_iterator = train_data.train()
            eval_iterator = train_data.valid()
        else:
            raise ValueError("Unrecognized train_data input argument: should be a DataLoader object or "
                             "CrossValidationDataLoader object")
        train_documents = []
        train_gold_starts = []
        train_gold_ends = []
        train_ant_ind = []
        train_mention_labels = []
        if self.use_metadata:
            train_speaker_ids = []
            train_genres = []
        else:
            train_speaker_ids = None
            train_genres = None
        train_graph_emb = None
        for datum in train_iterator:
            if self.use_metadata:
                text, gold_starts, gold_ends, speaker_ids, genre = self.make_bert_datum(datum, self.use_metadata)
            else:
                text, gold_starts, gold_ends = self.make_bert_datum(datum, self.use_metadata)
            train_documents.append(text)
            train_gold_starts.append(gold_starts)
            train_gold_ends.append(gold_ends)
            antecedents = [-1 for _ in gold_starts]
            mention_labels = [0 for _ in gold_starts]
            for i in range(len(datum.anaphors_idx)):
                mention_labels[datum.anaphors_idx[i]] = 1
                mention_labels[datum.antecedents_idx[i]] = 1
                if 0 <= datum.nounphrases[datum.anaphors_idx[i]][1] - datum.nounphrases[datum.antecedents_idx[i]][1] < \
                        self.antecedent_mask_max_len:
                    antecedents[datum.anaphors_idx[i]] = datum.antecedents_idx[i]
            train_ant_ind.append(antecedents)
            train_mention_labels.append(mention_labels)
            if self.use_metadata:
                train_speaker_ids.append(speaker_ids)
                train_genres.append(genre)
        if len(train_documents) == 0:
            print("Candidate/document pruning has resulted in 0 trainable instances in this fold.")
            return
        if eval_iterator is not None:
            eval_documents = []
            eval_gold_starts = []
            eval_gold_ends = []
            eval_ant_ind = []
            eval_mention_labels = []
            if self.use_metadata:
                eval_speaker_ids = []
                eval_genres = []
            else:
                eval_speaker_ids = None
                eval_genres = None
            eval_graph_emb = None
            for datum in eval_iterator:
                if self.use_metadata:
                    text, gold_starts, gold_ends, speaker_ids, genre = self.make_bert_datum(datum, self.use_metadata)
                else:
                    text, gold_starts, gold_ends = self.make_bert_datum(datum, self.use_metadata)
                eval_documents.append(text)
                eval_gold_starts.append(gold_starts)
                eval_gold_ends.append(gold_ends)
                antecedents = [-1 for _ in gold_starts]
                mention_labels = [0 for _ in gold_starts]
                for i in range(len(datum.anaphors_idx)):
                    mention_labels[datum.anaphors_idx[i]] = 1
                    mention_labels[datum.antecedents_idx[i]] = 1
                    if 0 <= datum.nounphrases[datum.anaphors_idx[i]][1] - \
                            datum.nounphrases[datum.antecedents_idx[i]][1] < self.antecedent_mask_max_len:
                        antecedents[datum.anaphors_idx[i]] = datum.antecedents_idx[i]
                eval_ant_ind.append(antecedents)
                eval_mention_labels.append(mention_labels)
                if self.use_metadata:
                    eval_speaker_ids.append(speaker_ids)
                    eval_genres.append(genre)
        if eval_iterator is None or len(eval_documents) == 0:
            eval_documents = None
            eval_gold_starts = None
            eval_gold_ends = None
            eval_ant_ind = None
            eval_speaker_ids = None
            eval_genres = None
            eval_graph_emb = None
            eval_mention_labels = None
        self.mangoes_model.train(output_dir=output_dir, max_segment_len=self.max_segment_len,
                                 max_segments=self.max_segments, freeze_base=freeze_bert_base,
                                 task_learn_rate=task_learn_rate, train_documents=train_documents,
                                 train_gold_starts=train_gold_starts, train_gold_ends=train_gold_ends,
                                 train_antecedent_indices=train_ant_ind, train_speaker_ids=train_speaker_ids,
                                 train_genres=train_genres, train_graph_embs=train_graph_emb,
                                 train_mention_labels=train_mention_labels,
                                 eval_documents=eval_documents,
                                 eval_gold_starts=eval_gold_starts, eval_gold_ends=eval_gold_ends,
                                 eval_antecedent_indices=eval_ant_ind, eval_speaker_ids=eval_speaker_ids,
                                 eval_genres=eval_genres, eval_graph_embs=eval_graph_emb,
                                 eval_mention_labels=eval_mention_labels,
                                 compute_metrics=None,
                                 **training_args)
        # not sure if this is needed, but just to make sure reference is updated with new weights
        self.model = self.mangoes_model.model

    @staticmethod
    def filter_valid_mentions(gold_starts, gold_ends):
        """
        Since we trim the document to be able to fit in the BERT model, we sometime filter candidates out and replace
        their start and end indices with -1.
        Since we can't send -1 starts/ends to the model, we have to take these out, but we need to retain the original
        candidate ordering.

        This function returns the starts and end with the -1 filtered out, and a mapping of original index -> filtered
        index for the valid mentions
        """
        candidate_index_offset = {}  # output score index: original candidate index
        valid_starts = []
        valid_ends = []
        for i in range(len(gold_starts)):
            if gold_starts[i] >= 0:
                candidate_index_offset[i] = len(valid_starts)
                valid_starts.append(gold_starts[i])
                valid_ends.append(gold_ends[i])
        return valid_starts, valid_ends, candidate_index_offset

    def predict_document(self, datum: BridgingDocument) -> np.ndarray:
        """
            1. make example
            2. predict
            3. return scores in a way that can be integrated with base class
            returns np array of size (gold_starts)
                for each mention, the index of the predicted antecedent in gold_starts (or -1 if not an anaphor)
        """
        if self.use_metadata:
            text, gold_starts, gold_ends, speaker_ids, genre = self.make_bert_datum(datum, self.use_metadata)
        else:
            text, gold_starts, gold_ends = self.make_bert_datum(datum, self.use_metadata)
        # since we can't send -1 starts/ends to the model, we have to take these out, but we need to retain the
        # original candidate ordering. index_offset: dict of original index -> filtered index
        valid_starts, valid_ends, candidate_index_offset = self.filter_valid_mentions(gold_starts, gold_ends)
        results = self.mangoes_model.generate_outputs(text, valid_starts, valid_ends,
                                                      speaker_ids if self.use_metadata else None,
                                                      genre if self.use_metadata else None,
                                                      max_segments=self.max_segments,
                                                      max_segment_len=self.max_segment_len,
                                                      graph_embs=None)
        return results

    @staticmethod
    def score_anaphors(pred_anaphors_idx: List[int], true_anaphors_idx: List[int]):
        pred_anaphors = set(pred_anaphors_idx)
        true_anaphors = set(true_anaphors_idx)
        fp = 0
        fn = 0
        tp = 0
        for ana in true_anaphors:
            if ana in pred_anaphors:
                tp += 1
            else:
                fn += 1
        for ana in pred_anaphors:
            if ana not in true_anaphors:
                fp += 1
        return fp, fn, tp

    @staticmethod
    def score_antecedents(pred_anaphors_idx, pred_antecedents_idx, true_anaphors_idx, true_antecedents_idx):
        true_anaphors_set = set(true_anaphors_idx)
        tp = 0
        correct_antecedents = 0
        for i in range(len(pred_anaphors_idx)):
            if pred_anaphors_idx[i] in true_anaphors_set:
                tp += 1
                if pred_antecedents_idx[i] == true_antecedents_idx[true_anaphors_idx.index(pred_anaphors_idx[i])]:
                    correct_antecedents += 1
        return tp, correct_antecedents

    @staticmethod
    def mention_scorer_metric(predictions, true_anaphor_idx):
        # predictions is numpy array of antecedent index predictions for each nounphrase, with codes for no antecedent
        #   predictions: -2 for nounphrase was filtered by mention scorer, -1 for dummy antecedent prediction
        men_fn = 0
        for anaphor_idx in true_anaphor_idx:
            if predictions[anaphor_idx] == -2:
                men_fn += 1
        return men_fn

    @staticmethod
    def coarse_to_fine_scorer_metric(results, true_anaphor_idx, true_antecedents_idx):
        # results is output dict from generate outputs
        # this function calculates how many times the coarse to fine scorer in the model falsely filters the correct
        # antecedent
        coarse_fn = 0
        for i in range(len(true_anaphor_idx)):
            if results["pred_antecedents"][
                true_anaphor_idx[i]] > -2:  # the anaphor makes it past the initial mention scorer filtering
                # find index in top_span_indices
                ana_top_span_index = (results["top_span_indices"] == true_anaphor_idx[i]).nonzero().item()
                ant_top_span_index = (results["top_span_indices"] == true_antecedents_idx[i]).nonzero()
                if len(ant_top_span_index) == 0:
                    coarse_fn += 1
                # check for real antecedent in top_antecedents
                elif ant_top_span_index.item() not in set(results["top_antecedents"][ana_top_span_index].tolist()):
                    coarse_fn += 1
        return coarse_fn

    def evaluate_documents(self, document_iterator: Iterable[BridgingDocument], save_output: bool = False,
                           testing: bool = False):
        """
        similar to .run() can input any instance iterator (lots of code duplication from .run() but moving fast now)

        returns summary dict
        """
        # dict with keys: (doc_name, (ana_start, ana_end)) tuples, values ((ant_start, ante_end), words) tuples
        universal_anaphora_predictions = {}
        output_dict = {}

        anaphor_fp = 0
        anaphor_fn = 0
        anaphor_tp = 0
        total_anaphors = 0
        mention_fn = 0  # counter for when mention scorer falsely filters the anaphor
        coarse_fn = 0  # counter for when coarse to fine scorer falsely filters the correct anaphor
        correct_antecedent = 0
        for i, datum in document_iterator:
            results = self.predict_document(datum)
            predictions = results["pred_antecedents"]
            anaphor_idx = []
            antecedent_idx = []
            for j in range(len(datum.nounphrases)):
                if predictions[j] > -1:
                    anaphor_idx.append(j)
                    antecedent_idx.append(predictions[j])
                    universal_anaphora_predictions[(datum.docname, tuple(datum.nounphrases[j]))] = \
                        (datum.nounphrases[predictions[j]], datum.nounphrases_[predictions[j]])
            output_dict[datum.docname] = {}
            output_dict[datum.docname]["sentences"] = datum.document
            output_dict[datum.docname]["doc_key"] = datum.docname
            output_dict[datum.docname]["bridging_pairs"] = []
            output_dict[datum.docname]["clusters"] = []
            anaphors = [(datum.nounphrases[idx][0], datum.nounphrases[idx][1] - 1) for idx in anaphor_idx]
            antecedents = [(datum.nounphrases[idx][0], datum.nounphrases[idx][1] - 1) for idx in antecedent_idx]
            output_dict[datum.docname]["bridging_pairs"] = [[ana, ant] for ana, ant in zip(anaphors, antecedents)]
            if datum.anaphors is not None:
                mention_fn += self.mention_scorer_metric(predictions, datum.anaphors_idx)
                coarse_fn += self.coarse_to_fine_scorer_metric(results, datum.anaphors_idx, datum.antecedents_idx)
                total_anaphors += len(datum.anaphors_idx)
                ana_fp, ana_fn, ana_tp = self.score_anaphors(anaphor_idx, datum.anaphors_idx)
                anaphor_fp += ana_fp
                anaphor_fn += ana_fn
                anaphor_tp += ana_tp
                tp, t_ante = self.score_antecedents(anaphor_idx, antecedent_idx, datum.anaphors_idx,
                                                    datum.antecedents_idx)
                assert tp == ana_tp
                correct_antecedent += t_ante

        # anaphor detection metrics
        if not testing:
            mention_fn_rate = mention_fn / float(total_anaphors)
            coarse_fn_rate = coarse_fn / float(total_anaphors)
            recall = anaphor_tp / float(anaphor_tp + anaphor_fn) if (anaphor_tp + anaphor_fn) > 0 else 0
            precision = anaphor_tp / float(anaphor_tp + anaphor_fp) if (anaphor_tp + anaphor_fp) > 0 else 0
            f1 = (2 * recall * precision / (recall + precision) if (recall + precision) > 0 else 0)
            metrics = {"metric_anaphor_recall": recall, "metric_anaphor_precision": precision, "metric_anaphor_f1": f1,
                       "metric_antecedent_accuracy": correct_antecedent / float(anaphor_tp) if anaphor_tp > 0 else 0,
                       "metric_mention_fn_rate": mention_fn_rate, "metric_coarse_fn_rate": coarse_fn_rate}

        if not testing and "metric_ent_f1" in self.metrics_:
            raw_file = ""
            if self.data_loader.dataset_domain == "switchboard":
                raw_file = raw_data_files.switchboard
            elif self.data_loader.dataset_domain == "light":
                raw_file = raw_data_files.light
            elif self.data_loader.dataset_domain == "arrau":
                if "91" in self.data_loader.dataset_domain_filename:
                    raw_file = raw_data_files.trains91
                elif "93" in self.data_loader.dataset_domain_filename:
                    raw_file = raw_data_files.trains93
            elif self.data_loader.dataset_domain == "ami":
                raw_file = raw_data_files.ami
            elif self.data_loader.dataset_domain == "persuasion":
                raw_file = raw_data_files.persuasion
            if raw_file == "":
                print("Raw data file not found, skipping ent_f1 metric.")
            else:
                metrics["metric_ent_f1"] = evaluate_ua_corpus_predicted_anaphors(output_dict=output_dict,
                                                                                 key_file=raw_file)

        if save_output:
            if self.data_loader.dataset == "crac":
                output_dir: Path = LOC.parsed / "submission" / self.data_loader.dataset_domain
                # Figure out the file name
                file_name = None
                if self.data_loader.dataset_domain == "switchboard":
                    file_name = 'switchboard.CONLL'
                elif self.data_loader.dataset_domain == "light":
                    file_name = 'light.CONLLUA'
                elif self.data_loader.dataset_domain == 'persuasion':
                    file_name = 'persuasion.CONLLUA'
                elif self.data_loader.dataset_domain == 'ami':
                    file_name = 'ami.CONLLUA'
                elif self.data_loader.dataset_domain == "arrau":
                    file_name = self.data_loader.dataset_domain_filename
                else:
                    warnings.warn("Unknown dataset, skipping prediction saving")
                if file_name is not None:
                    crac_save_predictions_jsons_to_ua(output_dict=output_dict, output_file=output_dir / file_name)
            else:
                warnings.warn("Saving predictions is only implemented for crac datasets currently.")
        if not testing:
            metrics["coref_dropout"] = self.coref_dropout
            metrics["max_top_spans"] = self.max_top_spans
            metrics["max_top_antecendents"] = self.max_top_antecendents
            metrics["max_span_width"] = self.max_span_width
            metrics["max_segment_len"] = self.max_segment_len
            metrics["max_segments"] = self.max_segments
            return metrics

    def make_bert_datum(self, datum: BridgingDocument, use_metadata: bool = False):
        """
        Extracts bert coref data example from a BridgingInstance.
        returns the text, gold mention starts and ends (with -1 for candidates that are in sentences that were trimmed
        from the document), and speaker info if applicable.
        """
        if self.use_heads:
            candidate_ids = getattr(datum, f'nounphrases_h')
        else:
            candidate_ids = getattr(datum, f'nounphrases')
        raw_doc: List[List[str]] = datum.document

        gold_starts = [x[0] for x in candidate_ids]
        # -1 because the model takes *inclusive* end indices
        gold_ends = [x[1] - 1 for x in candidate_ids]
        if use_metadata:
            if datum.speaker_info is not None:
                speaker_ids_trimmed = datum.speaker_info
            else:
                speaker_ids_trimmed = [0 for _ in raw_doc]
            assert len(speaker_ids_trimmed) == len(raw_doc)
            for i in range(len(speaker_ids_trimmed)):
                speaker_ids_trimmed[i] = [speaker_ids_trimmed[i]] * len(raw_doc[i])
            genre = self.genre_map[get_dataset_from_docname(datum.docname, self.bashi_docnames)]
            return raw_doc, gold_starts, gold_ends, speaker_ids_trimmed, genre
        return raw_doc, gold_starts, gold_ends

    def run_cross_validation(self, dl: CrossValidationDataLoader, task_learn_rate: float = None,
                             freeze_bert_base: bool = False,
                             training_output_dir: str = "../models/bft_model_ckpts/",
                             save_models: bool = False, save_dir: str = "../models/bft_model/",
                             print_training_metrics: bool = False, eval_dummy_scores: Tuple[float] = None,
                             fast_estimate: bool = False,
                             **training_args
                             ):
        """
        task_learn_rate: float, learn rate of head layers (if None, will use learn rate in training_args (ie same
            learn rate as base layers).
        dl: CrossValidationDataLoader
        freeze_bert_base: boolean, whether to freeze the base layers of the bert model or not
        training_output_dir: str, where to output training logs
        save_models: bool, whether to save cv fine-tuned models
        save_dir: str, where to save cv fine-tuned models
        eval_dummy_score: tuple of dummy score values in model to evaluate with
        fast_estimate: bool, if running many grid search parameters, this gives a quick estimate of the performance by
            only running one fold of the cross validation.
        training_args: keyword arguments for training. Includes "learning_rate", "weight_decay", "num_train_epochs",
            "seed", "per_device_train_batch_size", "per_device_eval_batch_size", "dataloader_num_workers".  See
            https://huggingface.co/transformers/main_classes/trainer.html#transformers.TrainingArguments for
            more/defaults.
        """
        if eval_dummy_scores is None:
            eval_dummy_scores = [self.mangoes_model.model.dummy_score]
        all_metrics = [{} for _ in range(len(eval_dummy_scores))]
        for j in range(dl.total_folds):
            print(f"\nFold {j}:")
            self.train(dl, output_dir=training_output_dir + f"/fold_{j}/", freeze_bert_base=freeze_bert_base,
                       **training_args)
            if print_training_metrics:
                dl._state_ = "init"
                dl._n_fold_ -= 1
                iterator: Iterable = enumerate(dl.train())
                metrics = self.evaluate_documents(iterator)
                print("train: ")
                print(metrics)
            for k in range(len(eval_dummy_scores)):
                iterator: Iterable = enumerate(dl.valid())
                self.mangoes_model.model.dummy_score = eval_dummy_scores[k]
                metrics = self.evaluate_documents(iterator)
                print("validation: ")
                print(metrics)
                if j == 0:
                    all_metrics[k] = metrics
                    for m_name in self.metrics_:
                        all_metrics[k][m_name] = [all_metrics[k][m_name]]
                else:
                    for m_name in self.metrics_:
                        all_metrics[k][m_name].append(metrics[m_name])

            if save_models:
                self.mangoes_model.save(save_dir + f"/fold_{j}")
            self.reset_model()
            if fast_estimate:
                break
        for k in range(len(eval_dummy_scores)):
            all_metrics[k]["summary"] = {}
            for m_name in self.metrics_:
                print(f"{m_name}: {np.mean(all_metrics[k][m_name])}")
                all_metrics[k]["mean_" + m_name] = np.mean(all_metrics[k][m_name])
                all_metrics[k]["summary"]["mean_" + m_name] = np.mean(all_metrics[k][m_name])
            all_metrics[k]["freeze_base"] = freeze_bert_base
            all_metrics[k]["model"] = self.model_name
        return all_metrics

    def train_and_predict(self, train_dl: DataLoader, test_dl: DataLoader, model_dir: str, learning_rate: float = 3e-4,
                          freeze_base: bool = True, num_epochs: int = 20):
        self.train(train_data=train_dl, output_dir=model_dir, freeze_bert_base=freeze_base, learning_rate=learning_rate,
                   num_train_epochs=num_epochs)
        self.mangoes_model.save(output_directory=model_dir + "/final_model/")
        self.evaluate_documents(document_iterator=enumerate(test_dl.all()), save_output=True, testing=True)


def grid_search(cvdl: CrossValidationDataLoader, freeze_base: bool = True,
                seed: int = 42, model_name: str = "bert-base-uncased", device=None,
                metadata=True, graph_emb=None, all_data=False):
    """
    will run grid search over cross validation data loader, similar to base.py's cross_validate function.
    Each parameter group will return a dict with param values and metrics.
    This function is mainly for trimming techniques, and also model head score aggregation methods.
    Other tuning (learning rate, whether to freeze base or not), is probably easier to tune manually and then use for
    all datasets. Here I've made the defaults to these parameters equal to what I manually found worked best.
    """
    params = {"candidate_reduction_method": ["none"], "use_metadata": [True, False] if metadata else [False],
              "coref_dropout": [0.3, 0.5], "max_top_spans": [50, 100], "max_top_antecendents": [100],
              "max_segment_len": [128, 256, 512], "ffnn_hidden_size": [256, 512], "mention_pos_weight": [100., 200.],
              }
    num_epochs = [5]
    learning_rate = [3e-4]
    eval_dummy_scores = [0, -100]
    learning_params = {"num_train_epochs": num_epochs, "learning_rate": learning_rate}
    if cvdl.dataset == "crac" and cvdl.dataset_domain == "persuasion":
        params = {"candidate_reduction_method": ["none"], "use_metadata": [True, False] if metadata else [False],
                  "coref_dropout": [0.3],
                  "max_segment_len": [512], "ffnn_hidden_size": [512],
                  "mention_pos_weight": [1000.0], "antecedent_mask_max_len": [800],
                  "non_dummy_weight": [8.]}
        num_epochs = [5, 10, 20]
        learning_rate = [3e-4]
        eval_dummy_scores = [0.0]
        learning_params = {"num_train_epochs": num_epochs, "learning_rate": learning_rate}
    elif cvdl.dataset == "crac" and cvdl.dataset_domain == "light":
        params = {"candidate_reduction_method": ["none"], "use_metadata": [True, False] if metadata else [False],
                  "coref_dropout": [0.3],
                  "max_segment_len": [512], "ffnn_hidden_size": [512],
                  "mention_pos_weight": [10.0], "antecedent_mask_max_len": [800],
                  "non_dummy_weight": [11.]}
        num_epochs = [5, 10, 20]
        learning_rate = [3e-4]
        eval_dummy_scores = [0.0]
        learning_params = {"num_train_epochs": num_epochs, "learning_rate": learning_rate}
    elif cvdl.dataset == "crac" and cvdl.dataset_domain == "switchboard":
        params = {"candidate_reduction_method": ["none"], "use_metadata": [True, False] if metadata else [False],
                  "coref_dropout": [0.3],
                  "max_segment_len": [512], "ffnn_hidden_size": [512],
                  "mention_pos_weight": [1000.0], "antecedent_mask_max_len": [800],
                  "non_dummy_weight": [15.]}
        num_epochs = [5, 10, 20]
        learning_rate = [3e-4]
        eval_dummy_scores = [0.0]
        learning_params = {"num_train_epochs": num_epochs, "learning_rate": learning_rate}
    elif cvdl.dataset == "crac" and cvdl.dataset_domain == "ami":
        params = {"candidate_reduction_method": ["none"], "use_metadata": [True, False] if metadata else [False],
                  "coref_dropout": [0.3],
                  "max_segment_len": [512], "ffnn_hidden_size": [512],
                  "mention_pos_weight": [500.0], "antecedent_mask_max_len": [800],
                  "non_dummy_weight": [15.]}
        num_epochs = [5, 10, 20]
        learning_rate = [3e-4]
        eval_dummy_scores = [0.0]
        learning_params = {"num_train_epochs": num_epochs, "learning_rate": learning_rate}
    param_names = [k for k in params.keys()]
    param_values = [v for v in params.values()]
    param_combos: list = [comb for comb in product(*param_values)]
    learning_param_names = [k for k in learning_params.keys()]
    learning_param_values = [v for v in learning_params.values()]
    learning_param_combos: list = [comb for comb in product(*learning_param_values)]
    print(f"{len(param_combos) * len(learning_param_combos)} parameter combinations")
    results: List[dict] = []
    best_result: dict = {}
    for crm in params["candidate_reduction_method"]:
        best_result[crm] = {}
    for param_combo in tqdm(param_combos, desc='Cross Validation'):
        for learn_param_combo in tqdm(learning_param_combos, desc='Cross Validation'):
            model = BertBridgingFineTune(**{k: v for k, v in zip(param_names, param_combo)},
                                         model=model_name, max_len=800,
                                         tokenizer="bert-base-uncased"
                                         if model_name == "bert-base-uncased" else "bert-base-cased",
                                         candidates_src='nounphrase', data_loader=cvdl, device=device,
                                         genres=["isnotes", "bashi", "arrau", "switchboard", "light", "ami",
                                                 "persuasion"],
                                         graph_emb_dim=graph_emb)
            combo_metrics = model.run_cross_validation(cvdl,
                                                       **{k: v for k, v in zip(learning_param_names,
                                                                               learn_param_combo)},
                                                       freeze_bert_base=freeze_base, logging_strategy="no",
                                                       save_strategy="no", print_training_metrics=False,
                                                       fast_estimate=True, eval_dummy_scores=eval_dummy_scores)
            # combo_metrics is list of metrics
            for result_idx in range(len(combo_metrics)):
                param_dict = {k: v for k, v in zip(param_names, param_combo)}
                param_dict.update({k: v for k, v in zip(learning_param_names, learn_param_combo)})
                param_dict["eval_dummy_score"] = eval_dummy_scores[result_idx]
                if len(best_result[param_dict["candidate_reduction_method"]]) == 0 or \
                        combo_metrics[result_idx]['mean_metric_ent_f1'] > \
                        best_result[param_dict["candidate_reduction_method"]]['mean_metric_ent_f1']:
                    best_result[param_dict["candidate_reduction_method"]] = combo_metrics[result_idx]["summary"]
                _ = combo_metrics[result_idx].pop("summary")
                combo_metrics[result_idx].update({k: v for k, v in zip(param_names, param_combo)})
                combo_metrics[result_idx].update({k: v for k, v in zip(learning_param_names, learn_param_combo)})
                combo_metrics[result_idx]["eval_dummy_score"] = eval_dummy_scores[result_idx]
                results.append(combo_metrics[result_idx])
            cvdl.state_ = 'init'
            cvdl._n_fold_ = -1
            if cvdl._shuffle_:
                random.seed(seed)
                np.random.seed(seed)
    return results, best_result


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
@click.option('--model_name', '-mod', type=str, default="bert-base-uncased",
              help="The name of the base model to fine tune.")
@click.option('--learning_rate', '-lr', type=float, default=3e-3,
              help="Learning rate to use.")
@click.option('--seed', '-s', type=int, default=42,
              help="Seed to use to split cross validation dataset.")
@click.option('--device', '-dv', type=str, default=None,
              help="The device to use: cpu, cuda, cuda:0, ...")
@click.option('--metadata/--no-metadata', default=True,
              help="Whether to use speaker and genre data")
@click.option('--all_data/--not_all_data', default=False,
              help="Whether to use all the data to train.")
@click.option('--freeze_base/--not_freeze_base', default=True,
              help="Whether to freeze the base layer while training.")
@click.option('--graph_emb', type=int, default=None,
              help="Size of external knowledge embedding, set to None to not use.")
@click.option('--mined_data/--no_mined_data', default=False,
              help="Whether to freeze the base layer while training.")
def main(dataset: str, dataset_domain: str, dataset_domain_filename: str, model_name: str, learning_rate: float,
         seed: int, device: str, metadata: bool, all_data: bool, freeze_base: bool, graph_emb: int, mined_data: bool):
    dl = CrossValidationDataLoader(dataset, dataset_domain, dataset_domain_filename, folds=5 if not all_data else 4,
                                   shuffle=True, seed=seed, documents=True)
    if all_data:
        if not dataset == "crac" or not dataset_domain == "ami":
            dl.include("crac", "ami")
        if not dataset == "crac" or not dataset_domain == "light":
            dl.include("crac", "light")
        if not dataset == "crac" or not dataset_domain == "persuasion":
            dl.include("crac", "persuasion")
        if not dataset == "crac" or not dataset_domain == "switchboard":
            dl.include("crac", "switchboard")
        if not dataset == "crac" or not dataset_domain == "arrau" or not dataset_domain_filename == "Trains_91.CONLL":
            dl.include("crac", "arrau", "Trains_91.CONLL")
        if not dataset == "crac" or not dataset_domain == "arrau" or not dataset_domain_filename == "Trains_93.CONLL":
            dl.include("crac", "arrau", "Trains_93.CONLL")
    elif mined_data:
        dl.include("wikitext")
        dl.include("gigaword")
    results, best_results = grid_search(dl, model_name=model_name, seed=seed,
                                        device=device, metadata=metadata, freeze_base=freeze_base, graph_emb=graph_emb,
                                        all_data=all_data)
    dataset_domain = dataset_domain if dataset_domain is not None else ""
    dataset_domain_filename = dataset_domain_filename if dataset_domain_filename is not None else ""
    model_name = model_name.replace("/", "-")
    output_file = f"bft_{dataset}_{dataset_domain}_{dataset_domain_filename}_{model_name}_" \
                  f"{'alldata' if all_data else ''}_{'freeze' if freeze_base else ''}_" \
                  f"{'mined' if mined_data else ''}_{'graph' if graph_emb is not None else ''}.json"
    with open(output_file, 'w') as fout:
        json.dump(results, fout)

    metrics = {}
    approach_name = "BERT higher-order"
    if all_data:
        approach_name += " - alldata"
    if mined_data:
        approach_name += " - mined"
    if graph_emb is not None:
        approach_name += " +wsd"
    for crm in best_results.keys():
        metrics[crm] = {}
        metrics[crm][approach_name] = best_results[crm]
        update_metrics(metrics, key=crm, dataset_dir=dl.dir)


if __name__ == "__main__":
    _pathfix.suppress_unused_import()
    main()
    exit(0)
    """
    train_dl = DataLoader('crac', 'ami', shuffle=True)
    train_dl.include("crac", "light")
    train_dl.include("crac", "persuasion")
    train_dl.include("crac", "switchboard")
    train_dl.include("crac", "arrau", "Trains_91.CONLL")
    train_dl.include("crac", "arrau", "Trains_93.CONLL")

    test_dl = DataLoader('crac', "light", test=False, documents=True)
    """
    train_dl = DataLoader('crac', 'light', shuffle=True, documents=True)
    params = {"coref_dropout": 0.0, "max_top_spans": 200, "max_top_antecendents": 100,
              "max_segment_len": 512, "ffnn_hidden_size": 512, "mention_pos_weight": 5000.0, "dummy_score": -5,
              "non_dummy_weight": 15.0}

    model = BertBridgingFineTune(candidate_reduction_method="none", use_metadata=False,
                                 antecedent_mask_max_len=800,
                                 model="bert-base-uncased", max_len=800,
                                 tokenizer="bert-base-uncased",
                                 candidates_src='nounphrase', data_loader=train_dl,
                                 genres=["isnotes", "bashi", "arrau", "switchboard", "light", "ami", "persuasion"],
                                 graph_emb_dim=None, **params)

    model.train(train_dl, output_dir="test", freeze_bert_base=True, num_train_epochs=10, learning_rate=3e-4,
                logging_strategy="steps", logging_steps=5)
    metrics = model.evaluate_documents(enumerate(train_dl.all()))
    print(metrics)
    print(params)
    """
    model.train_and_predict(train_dl=train_dl, test_dl=test_dl, model_dir="light_model")
    """

    """
    seed = 10
    model_name = "bert-base-uncased"

    dl = CrossValidationDataLoader('crac', 'persuasion', folds=5, shuffle=True, seed=seed, documents=True)
    ft = BertBridgingFineTune(dl, "bert-base-uncased", 400, "nounphrase", "none", use_metadata=True, coref_dropout=0.3,
                              max_top_spans=200, max_top_antecendents=100, ffnn_hidden_size=128, mention_pos_weight=10.0, antecedent_mask_max_len=300, non_dummy_weight=10.0)
    out = ft.train(dl, "test/", num_train_epochs=5, logging_strategy="steps",  save_strategy="no", logging_steps=2, learning_rate=3e-4, freeze_bert_base=True)
    dl._state_ = "init"
    dl._n_fold_ -= 1
    iterator: Iterable = enumerate(dl.train())
    metrics = ft.evaluate_documents(iterator)
    print("train: ")
    print(metrics)
    print(out)
    print(ft.mangoes_model.model.coref_dropout)
    iterator: Iterable = enumerate(dl.valid())
    metrics = ft.evaluate_documents(iterator)
    print(metrics)
    exit(0)
    """

    """
    for instance in dl.train():
        scores = ft.predict_document(instance)
        print(scores)
        exit(0)

        text = instance.document
        golds = instance.candidate_nounphrase_ids + [instance.anaphor]
        correct = instance.correct_nounphrase_id
        out = make_coref_inputs(text, tokenizer, [x[0] for x in golds], [x[1]-1 for x in golds], )
        print(out["input_ids"].shape)
        print(out["attention_mask"].sum())
        print(correct)
        print(instance.candidate_nounphrase_ids[correct])
        print(out["gold_starts"][correct])
        print(instance.candidate_nounphrase_txt[correct])
        print(instance.antecedent_)
        print(out["input_ids"][0][out["gold_starts"][correct]:out["gold_ends"][correct]+1])
        print(tokenizer.decode(out["input_ids"][0][out["gold_starts"][correct]:out["gold_ends"][correct]+1]))

        model.train(output_dir="test", freeze_base=True, task_learn_rate=0.003, train_documents=[text],
                    train_gold_starts=[[x[0] for x in golds]], train_gold_ends=[[x[1]-1 for x in golds]],
                    train_antecedent_indices=[correct], num_train_epochs=5, logging_strategy="epoch")
        exit(0)
        """
