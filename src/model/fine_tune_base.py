
from typing import List, Tuple, Union, Dict, Iterable
import warnings
import abc

import numpy as np

from evaluate.contextualmodels import SelfAttentionBench
from utils.bridging import BridgingInstance
from utils.exceptions import NoCandidateLeft
from dataloader import DataLoader, CrossValidationDataLoader, BaseLoader
from config import CRAC_RAWDATA_FILES as raw_data_files
from scorer import evaluate_ua_corpus


class BertFineTuneBase(SelfAttentionBench):
    def __init__(self, data_loader: DataLoader, model: str, max_len: int, candidates_src: str,
                 candidate_reduction_method: str, tqdm: bool = True, proximity_trim: bool = False,
                 trim_words_before: int = 50, trim_words_after: int = 20):
        super().__init__(data_loader, model, max_len, candidates_src, candidate_reduction_method, tqdm=tqdm,
                         proximity_trim=proximity_trim, trim_words_before=trim_words_before,
                         trim_words_after=trim_words_after)

    def _get_tokenizer(self, name):
        """ Instantiates and returns tokenizer object """
        pass

    def _get_model(self, name):
        """ Instantiates and returns model object """
        pass

    def _get_model_inputs(self, txt):
        """ Get model inputs from tokenizer """
        pass

    def _get_attention_weights(self, tokenized_document):
        """ Get attention weights from model for input document"""
        pass

    def evaluate(self, instance_iterator: Iterable[BridgingInstance]):
        """
        similar to .run() can input any instance iterator (lots of code duplication from .run() but moving fast now)

        returns summary dict
        """
        true: List[int] = []
        pred_list: List[np.ndarray] = []
        max_n_candidates: int = -1
        num_candidates: List[int] = []
        universal_anaphora_predictions = {}

        for i, datum in instance_iterator:
            self.reduce_candidates(instance=datum)
            num_candidates.append(len(getattr(datum, f'candidate_{self.candidates_src}_ids')))
            try:
                if num_candidates[-1] == 0:
                    # if the candidate reduction results in zero candidates, create dummy prediction, which will count
                    # as a wrong prediction because correct_{self.candidates_src}_id = -1 in this case
                    raise NoCandidateLeft
                else:
                    _pred = self.predict(datum=datum, src=self.candidates_src,
                                         candidate_span_ids=getattr(datum, f'candidate_{self.candidates_src}_ids'),
                                         candidate_span_txt=getattr(datum, f'candidate_{self.candidates_src}_txt'),
                                         candidate_span_h_ids=getattr(datum, f'candidate_{self.candidates_src}_h_ids'),
                                         candidate_span_h_txt=getattr(datum, f'candidate_{self.candidates_src}_h_txt'))
            except NoCandidateLeft:
                warnings.warn(f"No candidate left error was raised for - {datum.anaphor_id}")
                _pred = np.array([0], dtype=np.float)

            true.append(getattr(datum, f'correct_{self.candidates_src}_id'))
            pred_list.append(_pred)
            if "ent_f1" in self.metrics_:
                if num_candidates[-1] == 0:
                    # if the filtering ends with 0 candidates, create dummy prediction
                    pred_ante_span = (-1, -1)
                    pred_antecedent_words = []
                else:
                    predicted_index = np.argmax(_pred, axis=0)
                    pred_ante_span = getattr(datum, f'candidate_{self.candidates_src}_ids')[predicted_index]
                    pred_antecedent_words = getattr(datum, f'candidate_{self.candidates_src}_txt')[predicted_index]
                universal_anaphora_predictions[(datum.docname, tuple(datum.anaphor))] = \
                    (pred_ante_span, pred_antecedent_words)

            max_n_candidates = _pred.shape[0] if _pred.shape[0] > max_n_candidates else max_n_candidates

        # Convert True and Pred to np mats/arrays
        true: np.ndarray = np.asarray(true)
        pred: np.ndarray = self._to_numpy_(pred_list, max_len=max_n_candidates)  # 2D mat: n_data, n_candidates

        # Get metrics out.
        metrics: List[Union[float, np.ndarray]] = [metric_callable(true=true, pred=pred)
                                                   for metric_callable in self.metrics]
        if "ent_f1" in self.metrics_:
            raw_file = ""
            if self.data_loader.dataset_domain == "switchboard":
                raw_file = raw_data_files.switchboard
            elif self.data_loader.dataset_domain == "light":
                raw_file = raw_data_files.light
            elif self.data_loader.dataset_domain == "arrau":
                if "91" in self.data_loader.dataset_domain_filename:
                    raw_file = raw_data_files.trains91
                elif "93" in self.data_loader.dataset_domain_filename:
                    raw_file = raw_data_files.trains93
            elif self.data_loader.dataset_domain == "ami":
                raw_file = raw_data_files.ami
            elif self.data_loader.dataset_domain == "persuasion":
                raw_file = raw_data_files.persuasion
            if raw_file == "":
                print("Raw data file not found, skipping ent_f1 metric.")
            else:
                metrics.insert(self.metrics_.index("ent_f1"), evaluate_ua_corpus(raw_file,
                                                                                 universal_anaphora_predictions))
        summary = {'candidates_src': self.candidates_src,
                   'candidate_reduction_method': self.candidate_reduction_method_,
                   'candidates_num': '%(mean)1.3f +- %(std)1.3f' % {'mean': np.mean(num_candidates),
                                                                    'std': np.std(num_candidates)},
                    **{'metric_' + m_name: m_val for m_name, m_val in zip(self.metrics_, metrics)}}

        return summary

    @abc.abstractmethod
    def train(self, train_data: BaseLoader, output_dir: str, freeze_bert_base: bool, **training_args):
        raise NotImplementedError

    @abc.abstractmethod
    def reset_model(self):
        raise NotImplementedError

    def run_cross_validation(self, dl: CrossValidationDataLoader, freeze_bert_base: bool = False,
                             training_output_dir: str = "../models/fine_tune_checkpoints/",
                             save_models: bool = False, save_dir: str = "../models/fine_tune/",
                             print_training_metrics: bool = False,
                             **training_args):
        """
        dl: CrossValidationDataLoader
        freeze_bert_base: boolean, whether to freeze the base layers of the bert model or not
        training_output_dir: str, where to output training logs
        save_models: bool, whether to save cv fine-tuned models
        save_dir: str, where to save cv fine-tuned models
        training_args: keyword arguments for training. Includes "learning_rate", "weight_decay", "num_train_epochs",
            "seed", "per_device_train_batch_size", "per_device_eval_batch_size", "dataloader_num_workers",
            in addition to any fine-tune method specific arguments. See
            https://huggingface.co/transformers/main_classes/trainer.html#transformers.TrainingArguments for
            more/defaults.
        """
        all_metrics = {}
        for j in range(dl.total_folds):
            print(f"\nFold {j}:")
            self.train(dl, output_dir=training_output_dir + f"/fold_{j}/", freeze_bert_base=freeze_bert_base,
                       **training_args)
            if print_training_metrics:
                dl._state_ = "init"
                dl._n_fold_ -= 1
                iterator: Iterable = enumerate(dl.train())
                metrics = self.evaluate_documents(iterator)
                print("train: ")
                print(metrics)

            iterator: Iterable = enumerate(dl.valid())
            metrics = self.evaluate_documents(iterator)
            if print_training_metrics:
                print("validation: ")
                print(metrics)
            if j == 0:
                all_metrics = metrics
                for m_name in self.metrics_:
                    all_metrics[m_name] = [all_metrics[m_name]]
            else:
                for m_name in self.metrics_:
                    all_metrics[m_name].append(metrics[m_name])
            if save_models:
                self.mangoes_model.save(save_dir + f"/fold_{j}")
            self.reset_model()
        all_metrics["summary"] = {}
        for m_name in self.metrics_:
            print(f"{m_name}: {np.mean(all_metrics[m_name])}")
            all_metrics["mean_" + m_name] = np.mean(all_metrics[m_name])
            all_metrics["summary"]["mean_" + m_name] = np.mean(all_metrics[m_name])
        all_metrics["freeze_base"] = freeze_bert_base
        all_metrics["model"] = self.model_name
        return all_metrics
