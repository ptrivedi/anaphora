"""
    This file will work over the raw ISNotes corpora. Here are some relevant details.

    # References

        Hou, Yufang; Markert, Katja; Strube, Michael (2013)
            Global inference for bridging anaphora resolution.
        In Proceedings of the 2013 Conference of the North American Chapter of the Association for Computational
            Linguistics: Human Language Technologies,Atlanta, Georgia, 9-14 June 2013.

        https://www.h-its.org/software/isnotes-corpus/

    # Description

        We use the dataset we created in Markert et al.(2012) with almost 11,000 NPs annotated for information status
            including 663 bridging NPs and their antecedents in 50 texts taken from the WSJ portion
            of the OntoNotes corpus (Weischedel et al., 2011). Bridging anaphora can be any noun phrase.

"""
import os
import spacy
from tqdm import tqdm
from pathlib import Path
from xml.dom import minidom
from typing import List, Dict
from spacy.tokens import Token


# Local Import
import _pathfix
from config import LOCATIONS as LOC
from preproc.base import OntonotesBasedParser, get_noun_chunks, get_pos_tags, get_span_head
from utils.bridging import to_toks, BridgingInstance


class ISNotesParser(OntonotesBasedParser):
    """
        Raw dataset format:
            one xml file corresponding to one Ontonotes file
            get each 'markable' using xml's elementname 'markable'
            xml obj should contain all necessary attributes like span start, span end,
    """

    def __init__(self):
        super().__init__(ontonotes_method='onf', pretokenized=True)

        self.name: str = 'isnotes'
        self.isnotes_dir: Path = LOC.raw / self.name / 'ISNotes1.0-master' / 'ISAnnotationWithTrace'
        self.known_bridge_types: Dict[str, int] = {}

    @staticmethod
    def _convert_filename_isnotes_to_ontonotes_(fname: str) -> str:
        """ Input: wsj_1443_entity_level.xml,  Output: nw/wsj/14/wsj_1443 """
        file_num = fname[4:8]
        return f'nw/wsj/{file_num[:2]}/wsj_{file_num}'

    @staticmethod
    def _parse_markable_span_(raw_span: str) -> List[int]:
        """
            Expected inputs look like
                - word_736..word_751
                - word_736

            Output is a list of two integers.
                Offset the span start by -1
        """
        anaphor_span: List[int] = [int(x.replace('word_', '')) for x in raw_span.strip().split('..')]

        if len(anaphor_span) == 1:
            anaphor_span.append(anaphor_span[0])

        assert len(anaphor_span) == 2

        if anaphor_span[0] > 0:
            anaphor_span[0] -= 1

        return anaphor_span

    def parse_raw(self) -> None:
        """ Get all files, for each file, fetch doc; parse the xml and get all the markables out """

        file_names: List[str] = os.listdir(self.isnotes_dir)
        file_names: List[str] = sorted(file_names)

        # Go through all BASHI files
        for file_name in tqdm(file_names):

            # Turn the isnotes filename to something that resembles bashi's
            current_doc_nm = self._convert_filename_isnotes_to_ontonotes_(file_name)

            # Get the document, pos, noun phrases, and their heads
            current_doc: List[List[str]] = self.get_ontonotes_document(current_doc_nm)
            current_doc_: spacy.tokens.Doc = self.nlp(to_toks(current_doc))
            current_doc_pos: List[List[str]] = get_pos_tags(current_doc_)

            # XML Parse the document
            raw: minidom.Document = minidom.parse(str(self.isnotes_dir / file_name))

            # Store all the markables here
            markables: Dict[str, List[int]] = {}

            # Run the loop one, and find all mentions
            for phrase in raw.getElementsByTagName('markable'):

                mention_id: str = phrase.attributes['id'].value.strip()
                mention_span = self._parse_markable_span_(phrase.attributes['span'].value)

                # Append it to to the dict
                markables[mention_id] = mention_span

            # The noun phrases are just all markables
            current_doc_nps = list(markables.values())
            current_doc_nps_h = [get_span_head(span, doc=current_doc_) for span in current_doc_nps]

            # Run the loop again, this time only to find bridging data
            for phrase in raw.getElementsByTagName('markable'):

                # Ignore the phrase if its not a bridging instance
                if ('mediated_type' not in phrase.attributes) or \
                        (phrase.attributes['mediated_type'].value.strip() != 'bridging'):
                    continue

                anaphor_span: List[int] = self._parse_markable_span_(phrase.attributes['span'].value)
                anaphor_type: str = phrase.attributes['bridge_type'].value
                anaphor_cls: int = self.known_bridge_types.setdefault(anaphor_type, len(self.known_bridge_types))
                anaphor_h: List[int] = get_span_head(anaphor_span, doc=current_doc_)

                antecedent_span: List[int] = markables[phrase.attributes['bridged_from'].value]
                antecedent_h: List[int] = get_span_head(antecedent_span, doc=current_doc_)

                instance = BridgingInstance(document=current_doc,
                                            docname=current_doc_nm,
                                            antecedent=antecedent_span,
                                            antecedent_h=antecedent_h,
                                            anaphor_id=self.len,
                                            anaphor_cls=[anaphor_cls],
                                            anaphor=anaphor_span,
                                            anaphor_h=anaphor_h,
                                            pos=current_doc_pos,
                                            candidate_nounphrase_ids=current_doc_nps,
                                            candidate_nounphrase_h_ids=current_doc_nps_h)

                self.add(instance)


if __name__ == '__main__':
    _pathfix.suppress_unused_import()

    isnotes = ISNotesParser()
    isnotes.run()
