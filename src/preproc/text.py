"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    This file contains code (classes) to create `faux` bridging data from unstructured text corpora.
    We follow the suggestion of [Hou, 2018], and use two patterns:
        - X of Y
        - Y's X
        where both X and Y are noun pairs.

    Other things we take care of
        - rare words

    How do we represent the data:
        - list of bridging data (utils.bridging.BridgingInstance)

    What corpora do we mine from?
        - Gigaword: [Rush et al, 2015] (GigaWordMiner)
        - Wikitext 103

    # References

    Hou, Yufang.
        "Enhanced word representations for bridging anaphora resolution."
        arXiv preprint arXiv:1803.04790 (2018).

    Rush, Alexander M., Sumit Chopra, and Jason Weston.
        "A neural attention model for abstractive sentence summarization."
        arXiv preprint arXiv:1509.00685 (2015).
"""
# Global imports
import sys
from abc import ABC
from pathlib import Path
from typing import List, Tuple

import spacy
from datasets import load_dataset, Dataset
from spacy.tokens import Token
from tqdm import tqdm

# Local imports
import _pathfix
from config import LOCATIONS as LOC
from utils.bridging import BridgingInstance
from preproc.base import BaseBridgingParser, get_pos_tags, get_noun_chunks, get_span_head


class BaseBridgingAnaphoraMiner(BaseBridgingParser, ABC):
    """
        Abstract class which has the patterns implemented.

    """
    def __init__(self):
        super().__init__(pretokenized=False)

        # If new pattern is used, add it here!
        self.patterns = [self._pattern_a_, self._pattern_b_]
        self.pattern_names = [-1, -2]       # if class is negative, the finalization of data will be lax

    @staticmethod
    def _is_valid_sent_(x: str) -> bool:
        x = x.strip()
        if len(x) == 0:
            return False
        if x[0] == '=' and x[-1] == '=':
            return False
        return True

    def _wiki_pull_from_disk_(self, path: Path, fnames: List[str]) -> list:
        texts = []
        for idx, label in enumerate(fnames):
            with open(path / label, 'r', encoding='utf-8') as f:
                texts.append([sent.strip() for sent in tqdm(f.readlines()) if self._is_valid_sent_(sent)])
        return texts

    @staticmethod
    def _break_spacy_doc_(doc: spacy.tokens.Doc) -> List[List[str]]:
        return [[tok.text for tok in sent] for sent in doc.sents]

    @staticmethod
    def _get_neighbors_(doc: spacy.tokens.Doc, tok: spacy.tokens.Token, max_toks: int = 4) -> List[spacy.tokens.Token]:
        """ Get upto `max` integers after the current token, respecting the length of the total doc. """
        return [tok.nbor(i) for i in range(1, min(len(doc) - tok.i, max_toks))]

    def _pattern_a_(self, doc: spacy.tokens.Doc) -> List[Tuple[spacy.tokens.Span, spacy.tokens.Span]]:
        """
            # Target
            X of Y | X and Y are noun phrases. E.g. employee of Starbucks

            # Algorithm
            Iterate over noun chunks. For each chunk
                find next chunk
                find next few words
                if 'of' in next few words, X is current chunk (src); Y is next chunk (antecedent)

            # Output Format
            [
                ( spacy span of src, spacy span of antecedent),
                ...
            ]
        """
        # The results we're gonna throw back
        results: List[Tuple[spacy.tokens.Span, spacy.tokens.Span]] = []

        # Make the noun chunk list subscriptable, instead of just a generator
        noun_chunks: List[spacy.tokens.Span] = [span for span in doc.noun_chunks]

        # Iterate over all but last one chunk
        for i, curr_np in enumerate(noun_chunks[:-1]):
            next_np: spacy.tokens.Span = noun_chunks[i + 1]
            next_wds: List[spacy.tokens.Token] = self._get_neighbors_(doc=doc, tok=curr_np[-1])

            for next_wd in next_wds:

                # If (i) 'of' is in the neighborhood, and (ii) the next noun phrase begins right after this 'or'
                # TODO: what if `<noun phrase> of the <noun phrase>`?

                if next_wd.text == 'of' and next_np.start == next_wd.i + 1:
                    # Curr is src (employee), Next is antecedent (Starbucks)
                    results.append((curr_np, next_np))

                    # Expect only one pair, not more
                    continue

        return results

    @staticmethod
    def _pattern_b_(doc: spacy.tokens.Doc) -> List[Tuple[spacy.tokens.Span, spacy.tokens.Span]]:
        """
            # Target
            Y's X | X and Y are noun phrases. E.g. Starbucks' employee

            # Algorithm
            Iterate over noun chunks. For each chunk
                check if `'s` appears as a token and that its POS tag is 'PART'
                If so,
                    - left side of it is Y,
                    - right side of it is X

            # Output Format
            [
                ( spacy span of src, spacy span of antecedent),
                ...
            ]
        """
        # The results we're gonna throw back
        results: List[Tuple[spacy.tokens.Span, spacy.tokens.Span]] = []

        for i, noun_phrase in enumerate(doc.noun_chunks):
            for token in noun_phrase:
                if token.text in ["'", "'s"] and token.pos_ == 'PART':
                    # E.g. the big red van's four wheels
                    antecedent = doc[noun_phrase.start: token.i]  # the big red van
                    anaphor = doc[token.i + 1: noun_phrase.end]  # four wheels

                    # Add this to results
                    results.append((anaphor, antecedent))

                    # only expect one instance in one noun chunk
                    continue

        return results


class GigawordBridgingAnaphoraMiner(BaseBridgingAnaphoraMiner):

    def __init__(self):
        super().__init__()
        self.name: str = 'gigaword'

    def parse_raw(self) -> None:
        """
            Go through each doc, and annotate it with NPs following the syntactic patterns.
                - **Pattern A**: X of Y (X and Y are NPs)
                - **Pattern B**: Y's X (X and Y are NPs)

            Structure:
                - fn _pattern_a_ returns a list: [(span, span), (span, span) ...] corresponding to one document.
                - these lists are then turned into a bridging instance
                - each bridging instance is labeled with a unique class based on the pattern which found them
        """
        # docs: Dataset = load_dataset('gigaword', split='train[:1000]+validation[:1000]')
        docs: Dataset = load_dataset('gigaword', split='train+validation')
        print("Loaded Gigaword:train+validation", docs)
        docs: List[str] = [doc['document'] for doc in docs]

        for i, doc in enumerate(tqdm(self.nlp.pipe(docs, batch_size=500), total=len(docs))):

            # doc: spacy.tokens.Doc = self.nlp(raw['document'])

            # Get annotations from all patterns
            annotations_all: List[list] = [patternfn(doc) for patternfn in self.patterns]

            # Get some information for creating anaphora data
            docname: str = f"gigaword.train+validation.{i}"
            document: List[List[str]] = self._break_spacy_doc_(doc)

            for cls, annotations_grp in zip(self.pattern_names, annotations_all):
                for anaphor_, antecedent_ in annotations_grp:

                    # Make some info to be added in the instance
                    pos: List[List[str]] = get_pos_tags(doc)
                    nps: List[List[int]] = get_noun_chunks(fname=docname, doc=doc)
                    nps_h: List[List[int]] = [get_span_head(span, doc=doc) for span in nps]
                    antecedent_h = get_span_head([antecedent_.start, antecedent_.end], doc)
                    anaphor_h = get_span_head([anaphor_.start, anaphor_.end], doc)

                    instance = BridgingInstance(document=document,
                                                docname=docname,
                                                anaphor_cls=[cls],
                                                anaphor_h = anaphor_h,
                                                anaphor=[anaphor_.start, anaphor_.end],
                                                antecedent=[antecedent_.start, antecedent_.end],
                                                antecedent_h=antecedent_h,
                                                anaphor_id=self.len,
                                                pos=pos,
                                                candidate_nounphrase_ids=nps,
                                                candidate_nounphrase_h_ids=nps_h)

                    self.add(instance)

    print("Done mining data. Now to post process them and call it a day.")


class WikitextBridgingAnaphoraMiner(BaseBridgingAnaphoraMiner):

    def __init__(self):
        super().__init__()
        self.name: str = 'wikitext'
        self.WIKITEXT_FILES: List[str] = ['wiki.train.tokens', 'wiki.valid.tokens']

    def parse_raw(self) -> None:
        """
            Go through each doc, and annotate it with NPs following the syntactic patterns.
                - **Pattern A**: X of Y (X and Y are NPs)
                - **Pattern B**: Y's X (X and Y are NPs)

            Structure:
                - fn _pattern_a_ returns a list: [(span, span), (span, span) ...] corresponding to one document.
                - these lists are then turned into a bridging instance
                - each bridging instance is labeled with a unique class based on the pattern which found them
        """

        # Get Two elements: List of train docs, list of valid docs. **EXCLUDE TEST**!
        splits = self._wiki_pull_from_disk_(LOC.wikitext, fnames=self.WIKITEXT_FILES)

        # Iterate over all docs
        for splitname, splitdocs in zip(self.WIKITEXT_FILES, splits):
            print(f"Starting a spacy pipe over ")
            for i, doc in enumerate(tqdm(self.nlp.pipe(splitdocs, batch_size=500), total=len(splitdocs))):

                # Get annotations from all patterns
                annotations_all: List[list] = [patternfn(doc) for patternfn in self.patterns]

                # Get some information for creating anaphora data
                docname: str = f"{splitname}.{i}"
                document: List[List[str]] = self._break_spacy_doc_(doc)

                for cls, annotations_grp in zip(self.pattern_names, annotations_all):
                    for anaphor_, antecedent_ in annotations_grp:

                        # Make some info to be added in the instance
                        pos: List[List[str]] = get_pos_tags(doc)
                        nps: List[List[int]] = get_noun_chunks(fname=docname, doc=doc)
                        nps_h: List[List[int]] = [get_span_head(span, doc=doc) for span in nps]
                        antecedent_h = get_span_head([antecedent_.start, antecedent_.end], doc)

                        instance = BridgingInstance(document=document,
                                                    docname=docname,
                                                    anaphor_cls=[cls],
                                                    anaphor=[anaphor_.start, anaphor_.end],
                                                    anaphor_h=get_span_head([anaphor_.start, anaphor_.end], doc=doc),
                                                    antecedent=[antecedent_.start, antecedent_.end],
                                                    antecedent_h=antecedent_h,
                                                    anaphor_id=self.len + 1,
                                                    pos=pos,
                                                    candidate_nounphrase_ids=nps,
                                                    candidate_nounphrase_h_ids=nps_h)

                        self.add(instance)

        print("Done mining data. Now to post process them and call it a day.")


if __name__ == '__main__':
    _pathfix.suppress_unused_import()

    args = sys.argv[1:]
    if 'wikitext' in args:
        wikitext = WikitextBridgingAnaphoraMiner()
        wikitext.run()
    if 'gigaword' in args:
        gigaword = GigawordBridgingAnaphoraMiner()
        gigaword.run()
