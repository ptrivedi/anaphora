"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr
    A base class containing some common functionalities to be used across multiple dataset parsers
"""
import re
import spacy
import pickle
import jsonlines
from pathlib import Path
from dataclasses import asdict
from spacy.tokens import Token
from abc import ABC, abstractmethod
from typing import Dict, List, Union

# Local imports
from config import LOCATIONS as LOC
from utils.bridging import BridgingInstance, BridgingDocument, spans_id_to_text, gather_instances_by_docnames, \
    get_all_antecedent_spans_with_heads
from utils.nlp import AnnotationBlock, AnnotationBlockStack, find_one, to_toks, NullTokenizer
from utils.misc import NoRegexMatchError

"""
    Some common functions used elsewhere
"""


def get_pos_tags(doc: spacy.tokens.Doc) -> List[List[str]]:
    """ Get pos tags for each token, respecting the sentence boundaries, i.e. each sent is a list """
    return [[token.pos_ for token in sent] for sent in doc.sents]


def get_noun_chunks(fname: str, doc: Union[List[List[str]], spacy.tokens.Doc]) -> List[List[int]]:
    """
        This fn gets a bunch of mentions out of a doc, calling them entities.
        This includes
            - spans of noun chunks

        Returns [[10, 18], [span start, span end of a candidate mention], ..., [134, 139]]
    """
    return [[nc.start, nc.end] for nc in doc.noun_chunks]


def get_span_head(span: List[int], doc: spacy.tokens.Doc) -> List[int]:
    """ Get the spacy Span, get its root and return its word index (_, _+1) """
    """ Get the spacy Span, get its root and return its word index (_, _+1) """
    root = doc[span[0]: span[1]].root
    stop_list = ['det', 'num', 'adp', 'DET', 'NUM', 'ADP']
    if root.pos_ in stop_list:  # det can't be a root. So a hack to make sure that det can't be a det.
        if span[1] - span[0] == 1:
            # return the same thing
            assert root.i >= 0
            return [root.i, root.i + 1]
        elif span[1] - span[0] == 2:
            # return the other word
            root = doc[span[0] + 1]
            assert root.i >= 0
            return [root.i, root.i + 1]
        elif doc[span[0]].pos_ in stop_list:
            # remove the span[0] and call get_span_head
            return get_span_head([span[0] + 1, span[1]], doc)
        elif doc[span[1]].pos_ in stop_list:
            # remove the last one and call get_span_head again
            return get_span_head([span[0], span[1] - 1], doc)
        else:
            # root is somewhere in somewhere between span.
            assert root.i >= 0

            return [root.i, root.i + 1]
    else:
        return [root.i, root.i + 1]


class BaseBridgingParser(ABC):

    def __init__(self, pretokenized: bool):
        self.name: str = ''
        self.name_addendum: str = ''
        self.n_dumps: int = 0
        self.dump_at: int = 100000
        self.last_doc_name: str = ''
        self.last_doc_instances: List[BridgingInstance] = []
        self.instances: List[BridgingInstance] = []

        self.nlp = spacy.load('en_core_web_sm')

        if pretokenized:
            # This tokenizer DOES not tokenize documents.
            # Use this is the document is already tokenized.
            self.nlp.tokenizer = NullTokenizer(self.nlp.vocab)

    @property
    def len(self):
        return len(self.instances) + len(self.last_doc_instances) + (self.dump_at*self.n_dumps)

    def batch_end(self) -> None:
        """
            This goes over all data in self.data
            Gathers candidates_src by grouping them by docnames
            Finalises each instance
            Writes them to disk
            Removes them from memory
        """
        # Compute candidate data corresponding to each "document"
        self.gather_candidate_antecedents()

        # Sanity check each instance
        self.finalise_instances()

        # Write all of it to disk
        self.write_to_disk()

        self.n_dumps += 1

        # Finally, purge the data
        self.instances: List[BridgingInstance] = []

    def run(self):
        """ The function is not to be overwritten. """

        # Get a list of bridging data, corresponding to the raw data
        # A lot of data are now either in self.data, or self.last_doc_instances
        # But we have gone over the entire raw resource and made all data
        self.parse_raw()

        # Add the last doc data to the main list
        self.instances += self.last_doc_instances

        # Run batch end again.
        self.batch_end()

    def add(self, instance: BridgingInstance) -> None:
        """
            Instances are only added to the main list (self.data) once the doc changes.
            If this instance is from the same doc
                - add it to a temporary list i.e. self.last_doc_instances
            Else
                - empty the temporary list to the main list
                - if the main list is longer than a certain threshold, empty it to disk
                - reset the temporary list with this instance
                - reset the last doc name with this instance's doc name
        """

        if instance.docname == self.last_doc_name:
            # Instance belongs to the same doc as prev one(s)
            # Simply add it to the last_doc_instances and forget about it
            self.last_doc_instances.append(instance)
        else:
            # A new doc has started.

            # Add all data of the last doc to the main list
            self.instances += self.last_doc_instances

            # Clear out self.last_doc_instances
            self.last_doc_instances: List[BridgingInstance] = [instance]
            self.last_doc_name: str = instance.docname

            # If we have already accumulated enough data, dump them to disk
            if len(self.instances) >= self.dump_at:
                self.batch_end()

    def gather_candidate_antecedents(self) -> None:
        """
            Calculate candidate spans for each src.
                - get all BridgingInstances for a given docname.
                - get all their TEXT spans, and ID spans.
                - add the candidate TEXT and ID spans to EACH BridgingAnaphor instance.
        """

        gathered_instances = gather_instances_by_docnames(self.instances)

        for docname, instances in gathered_instances.items():

            # Recall that duplicate antecedent candidates_src are discarded by _get_all_antecedent_spans_
            candidate_span_h_ids, candidate_span_ids = get_all_antecedent_spans_with_heads(instances)

            # Add other antecedents in nps as well.
            for instance in instances:
                instance.candidate_nounphrase_ids += candidate_span_ids
                instance.candidate_nounphrase_h_ids += candidate_span_h_ids

            # ################################# #
            # Note: We do not root out overlaps #
            # ################################# #

            # Get corresponding text for each phrase as well.
            candidate_span_txt: List[List[str]] = spans_id_to_text(doc=instances[0].document,
                                                                   spans=candidate_span_ids)
            candidate_span_h_txt: List[List[str]] = spans_id_to_text(doc=instances[0].document,
                                                                     spans=candidate_span_h_ids)

            # Add them to the data
            for instance in instances:
                instance.candidate_antecedent_ids = candidate_span_ids
                instance.candidate_antecedent_txt = candidate_span_txt
                instance.candidate_antecedent_h_ids = candidate_span_h_ids
                instance.candidate_antecedent_h_txt = candidate_span_h_txt

                # Also add text variants of all noun phrases
                instance.candidate_nounphrase_txt = spans_id_to_text(doc=instance.document,
                                                                     spans=instance.candidate_nounphrase_ids)
                instance.candidate_nounphrase_h_txt = spans_id_to_text(doc=instance.document,
                                                                       spans=instance.candidate_nounphrase_h_ids)

                # Commented out because we dont want NPS to have candidates_src but the other way around.
                # instance.nps += [list(item) for item in candidate_span_ids]
                # instance.candidate_nounphrase_h_ids += candidate_span_h_ids

            # # Check if all candidates_src have been selected at least once in a given doc
            # assert len(candidate_span_ids) == len(set(instance.correct_antecedent_id for instance in data)), \
            #     "Looks like some candidates_src were never selected."

    @abstractmethod
    def parse_raw(self) -> None:
        """
            This function is expected to
                - read the raw file
                - create a bunch of BridgingInstance
        :return: Nothing
        """
        ...

    def finalise_instances(self):
        """ Finalise all bridging data """

        # Ensure there are no duplicate anaphor IDs
        anaphor_ids = {}
        for instance in self.instances:
            if not instance.anaphor_id in anaphor_ids:
                anaphor_ids[instance.anaphor_id] = 0
            else:
                raise AssertionError("Found multiple instances with the same anaphor ID.")

        # Annotate the data with the "text" of the src & antecedent based on their
        for instance in self.instances:
            flattened_doc = to_toks(instance.document)
            instance.anaphor_ = flattened_doc[instance.anaphor[0]: instance.anaphor[1]]
            instance.anaphor_h_ = flattened_doc[instance.anaphor_h[0]: instance.anaphor_h[1]]
            instance.antecedent_ = flattened_doc[instance.antecedent[0]: instance.antecedent[1]]

        for datum in self.instances:
            datum.finalise()

    def write_to_disk(self):
        """ Write to config.LOC.parsed / self.name / dump.jsonlines, and dump.pickle """

        outputdir: Path = LOC.parsed / self.name
        if self.name_addendum:
            outputdir: Path = outputdir / self.name_addendum
        outputdir.mkdir(parents=True, exist_ok=True)

        # Pickle outputs
        with (outputdir / f'dump_{self.n_dumps}.pkl').open('wb+') as f:
            pickle.dump(self.instances, f)

        # JSONines output
        with (outputdir / f'dump_{self.n_dumps}.json').open('w+', encoding='utf8') as f:
            with jsonlines.Writer(f) as writer:
                writer.write_all([asdict(datum) for datum in self.instances])

        print(f"Successfully wrote {len(self.instances)} data of {self.name} to {outputdir}, {self.n_dumps}th time")


class BaseDocumentParser(ABC):

    def __init__(self, pretokenized: bool, is_test: bool = False):
        self.name: str = ''
        self.name_addendum: str = ''
        self.n_dumps: int = 0
        self.dump_at: int = 100000
        self.parsed: List[BridgingDocument] = []
        self.is_test: bool = is_test    # If True: do not expect anaphor/antecedent information.

        self.nlp = spacy.load('en_core_web_sm')

        if pretokenized:
            # This tokenizer DOES not tokenize documents.
            # Use this is the document is already tokenized.
            self.nlp.tokenizer = NullTokenizer(self.nlp.vocab)

    @property
    def len(self):
        return len(self.parsed) + (self.dump_at * self.n_dumps)

    def batch_end(self) -> None:
        """
            This goes over all data in self.data
            Finalises each instance
            Writes them to disk
            Removes them from memory

            This is expected to be called only when
                (i) we're at the end of raw files or
                (ii) we have more instances in memory than self.dumps_at
        """

        # Sanity check each instances
        self.finalise_instances()

        # Write all of it to disk
        self.write_to_disk()

        self.n_dumps += 1

        # Finally purge the data
        self.parsed: List[BridgingDocument] = []

    def run(self):
        """ The function is not to be overwritten. The main function to called from outside."""

        # Get a list of bridging data, corresponding to the raw data
        # A lot of data are now either in self.data, or self.last_doc_instances
        # But we have gone over the entire raw resource and made all data
        self.parse_raw()

        # Run batch end finally when done dealing with all files.
        self.batch_end()

    def add(self, document: BridgingDocument) -> None:
        """ The only way a document should be added to self.parsed """
        self.parsed.append(document)

        if len(self.parsed) >= self.dump_at:
            self.batch_end()

    @abstractmethod
    def parse_raw(self) -> None:
        """
            This is the function which ought to read raw files and invoke self.add for every new document
        :return: Nothing
        """
        ...

    def finalise_instances(self) -> None:
        """
        For every instance currently in self.documents,
            - annotate it with "text" of spans
            - finalise it
        """

        for obj in self.parsed:
            tokens = to_toks(obj.document)
            obj.nounphrases_ = [tokens[span[0]: span[1]] for span in obj.nounphrases]
            obj.nounphrases_h_ = [tokens[span[0]: span[1]] for span in obj.nounphrases_h]

            if obj.anaphors is not None:
                obj.anaphors_ = [tokens[span[0]: span[1]] for span in obj.anaphors]
                obj.anaphors_h_ = [tokens[span[0]: span[1]] for span in obj.anaphors_h]

            if obj.antecedents is not None:
                obj.antecedents_ = [tokens[span[0]: span[1]] for span in obj.antecedents]
                obj.antecedents_h_ = [tokens[span[0]: span[1]] for span in obj.antecedents_h]

            obj.finalise()

    def write_to_disk(self):
        """
            Write to
                config.LOC.parsed / self.name  / document.pickle, document.jsonlines
            or (if self.is_test)
                config.LOC.parsed / test / self.name / document.pickle, document.jsonlines
        """
        outputdir: Path = LOC.parsed / self.name
        if self.name_addendum:
            outputdir: Path = outputdir / self.name_addendum
        outputdir.mkdir(parents=True, exist_ok=True)

        # Pickle outputs
        with (outputdir / f'documents_{self.n_dumps}.pkl').open('wb+') as f:
            pickle.dump(self.parsed, f)

        # JSONines output
        with (outputdir / f'documents_{self.n_dumps}.json').open('w+', encoding='utf8') as f:
            with jsonlines.Writer(f) as writer:
                writer.write_all([asdict(datum) for datum in self.parsed])

        print(f"Successfully wrote {len(self.parsed)} data of {self.name} to {outputdir}, {self.n_dumps}th time")


class OntonotesBasedParser(BaseBridgingParser, ABC):
    """
        Abilities: get_ontonotes_document
        Abilities: get_ontonotes_sentence
        Abilities: get_ontonotes_word
        Abilities: _fetch_ontonotes_word_
    """

    def __init__(self, ontonotes_method: str, pretokenized: bool = True):
        super().__init__(pretokenized)

        self.cached_docs: Dict[str, List[List[str]]] = {}
        self.re_sqbkt: str = r"<.[^>]*>"
        self.re_word_in_parse_tree = re.compile(r"(?<=\s)[^()]+(?=\))")
        self.ontonotes_special_words: Dict[str, str] = {
            '-AMP-': '&'
        }

        """
            Preparing methods to parse raw ontonotes.
        """
        if ontonotes_method == 'name':
            self._get_ontonotes_document_ = self._get_ontonotes_document_by_name_
        elif ontonotes_method == 'onf':
            self._get_ontonotes_document_ = self._get_raw_ontonotes_document_by_onf_
        else:
            raise ValueError(f"Unknown ontonotes method: {ontonotes_method}")

    def get_ontonotes_document(self, fname: str) -> List[List[str]]:
        """ Dict lookup if already loaded, else pull from disk using _get_ontonotes_document_"""
        if fname not in self.cached_docs:
            doc = self._get_ontonotes_document_(fname)

            # Replace all known ontonotes special characters in the doc with real ones
            for i_sent, sent in enumerate(doc):
                for i_tok, _ in enumerate(sent):
                    for sp_word, en_word in self.ontonotes_special_words.items():
                        doc[i_sent][i_tok] = doc[i_sent][i_tok].replace(sp_word, en_word)

            self.cached_docs[fname] = doc

        return self.cached_docs[fname]

    def get_ontonotes_sentence(self, fname: str, sentenceid: int) -> List[str]:
        """ Get doc, and get string """
        return self.get_ontonotes_document(fname)[sentenceid]

    def get_ontonotes_word(self, fname: str, sentenceid: int, tokenid: int) -> str:
        """ Get doc, and get string, split it and get word """
        return self.get_ontonotes_sentence(fname, sentenceid)[tokenid]

    def get_absolute_length_upto(self, fname: str, sentenceid: int, tokenid: int) -> int:
        """ Count all the words upto: current sent ID, token ID """
        sum_sents: int = sum(len(sent) for sent in self.get_ontonotes_document(fname)[:sentenceid])
        return sum_sents + tokenid

    def get_absolute_length(self, fname) -> int:
        """ Count all the words in the doc """
        return sum(len(sent) for sent in self.get_ontonotes_document(fname))

    # Overwriting from base parser
    def get_noun_chunks(self, fname: str, doc: Union[List[List[str]], spacy.tokens.Doc]) -> List[List[int]]:
        """ Overwriting the base parsers' fn. Here, we call our nice in house function. """
        return self._get_pos_spans_from_ontonotes_onf_(fname, doc, tags=['NP'])

    def _get_pos_spans_from_ontonotes_onf_(self, fname: str, doc: List[List[str]], tags: List[str]) \
            -> List[List[int]]:
        """
            filename looks like nw/wsj/02/wsj_0242. parsed_doc is instance.document; tags is PENN POS Tag

            Overview of algo:
                maintain a list of open parenthesis (aka `stack` (corresponding to the specified POS tags)
                for each line, note the word, check if it matches the given doc's words
                go through each token in the line (split by space)
                    if the token starts with (
                        -> add one to open bracket counter in all elements of `stack`
                        -> if the pos tag is specified in args `tags`
                            -> create a new element and add it to `stack`
                    if the token starts with )
                        -> add one to closed bracket counter in all elements of `stack`
                        -> if open == closed in any element: consider it _finished_
                            i.e. all words, its spans are now extracted from `stack` and stored away.

            Edge cases:
                edgecase_no_word:
                    line like: (S-ADV (NP-SBJ (-NONE- *PRO*-1))

                    does not correspond to a word in doc

                    do: keep track of brackets
                    dont: increment sentence pointer; or add new element in `stack`; or add the word in open elements.

                edgecase_incomplete_line:
                    line is (NP (NNS
                    next line years)))))))))))))))))))))

                    here, both lines are to be considered together. difficult to model shit.

                    do: append next line to this line and continue as normal. set a flag to skip next line altogether.
        """

        with Path(LOC.ontonotes / (fname + '.onf')).open('r', encoding='utf8') as f:
            rawlines: List[str] = f.readlines()

        sent_start_index: int = 0  # Indicates the index of the first word of this sent
        sent_curr_index: int = -1
        sent_id: int = 0
        stack = AnnotationBlockStack()
        finished_blocks: List[AnnotationBlock] = []
        in_tree: bool = False

        # Set flags for edge cases
        is_edgecase_incomplete_line: bool = False
        is_edgecase_no_word: bool

        # Go over all lines
        for i, line in enumerate(rawlines):

            # Skip the first line
            if i == 0:
                continue

            if is_edgecase_incomplete_line:
                is_edgecase_incomplete_line = False
                continue

            is_edgecase_no_word = False

            # Get previous and next lines
            prev_line: str = rawlines[i - 1].strip()
            next_line: str = rawlines[i + 1].replace('\n', '').strip() if i < len(rawlines) - 1 else ''

            line = line.replace('\n', '').strip()

            # Tree Start block looks like
            #
            # Tree:
            # -----
            # This block implies that we're now dealing with the parse tree for a given sent
            if line == 'Tree:' and prev_line == '' and next_line == '-----':
                in_tree = True
                continue

            # Do something if we're inside a tree
            if in_tree and line.replace('-', '') != '':

                # Find the word we're talking about
                try:
                    word = find_one(self.re_word_in_parse_tree, line)
                except NoRegexMatchError:
                    # There is no word here.
                    # Further check this: line does not end in )

                    if not line.endswith(')'):
                        """ 
                            Edge case 2 detection
                             
                                append next line into this. 
                                skip the next iteration
                                oh and also recompute the current word
                        """
                        is_edgecase_incomplete_line = True
                        line = line + ' ' + next_line
                        word = find_one(self.re_word_in_parse_tree, line)

                    else:
                        raise ValueError(f"We expect edge case 2, but we got more problems: {line}")

                """
                    Edge case 1 detection
                        check if this word does not correspond to the doc and 
                        we should not update sent_curr_index
                """
                if (word[-1].isdigit() or word.startswith('*')) and '-NONE-' in line:
                    is_edgecase_no_word = True

                else:

                    # Increment the num of words in this sent
                    sent_curr_index += 1

                    # Make sure that our word indices line up with the doc
                    assert doc[sent_id][sent_curr_index] == word, print(f"{i}: {line}", sent_id, sent_curr_index)

                    # Add this word in all open blocks
                    stack.register_word(word)

                # Regardless of the edge case, we need to add/remove parenthesis from our stack. ]
                # only, we don't create new ones.

                for tok in line.split():

                    # If some span starts here
                    if tok.startswith('('):

                        # Let's append a bracket for all open blocks in the stack
                        stack.add()

                        if not is_edgecase_no_word:
                            # Code to register new blocks
                            if tok[1:] in tags:
                                for tag in tags:
                                    if tag == tok[1:]:
                                        stack.append(AnnotationBlock(
                                            start=sent_curr_index + sent_start_index,
                                            words=[word],
                                            tag=tag,
                                            open_inside=1
                                        ))

                    # No new block in this token but some brackets may be closing
                    else:

                        # Count the number of closing brackets
                        n_closed = tok.count(')')

                        _finished_blocks: List[AnnotationBlock] = stack.sub(n_closed,
                                                                            span=sent_curr_index + sent_start_index)

                        if _finished_blocks:
                            finished_blocks += _finished_blocks

            # Tree End block looks like
            #
            # Leaves:
            # This indicates that we're done with the "TREE" part of the sentence but are not done with the sent yet
            if line == '' and next_line == 'Leaves:':
                in_tree = False
                sent_curr_index = -1
                continue

            # Sentence end block looks like ----
            # This marks the end of sentence
            if line == '-' * 120:
                sent_start_index += len(doc[sent_id])

                assert len(stack.blocks) == 0
                sent_id += 1

                continue

        """
            Now to sanity check all blocks, we're gonna see if 
                - BridgingInstance.document's span in onf block and,
                - the words inside onf block, 
                are aligned.
        """

        tok_doc: List[str] = to_toks(doc)
        for block in finished_blocks:
            assert ' '.join(block.words) == ' '.join(tok_doc[block.start: block.end]), block

        """
            Finally, lets drop all the blocks, and retain necessary information
        """
        block_span: List[List[int]] = [block.span for block in finished_blocks]

        return block_span

    @staticmethod
    def _get_raw_ontonotes_document_by_onf_(fname: str) -> List[List[str]]:
        """ filename looks like nw/wsj/02/wsj_0242. """

        with (LOC.ontonotes / (fname + '.onf')).open('r', encoding='utf8') as f:
            rawlines = f.readlines()

        doc: List[List[str]] = []
        sent: List[str] = []
        in_leaves: bool = False

        # Go over all lines
        for i, line in enumerate(rawlines):

            line = line.replace('\n', '')

            # Look for a block that looks like
            # Leaves:
            # -------
            # This block implies a new sentence's start
            if line == 'Leaves:' and rawlines[i + 1].replace('-', '') == '\n':
                in_leaves = True
                sent = []
                continue

            # A long line: --------- ... length 120 implies a sentence has ended
            # Add the sent var into the doc var
            if line == '-' * 120:
                in_leaves = False
                doc.append(sent)
                continue

            # We are inside the Leaves: section. Then, if 4th char is digit: its a word not a coref annotation.
            if in_leaves and len(line) > 4 and line[4].isdigit():
                sent.append(line.strip().split()[1])

        # Finally, maybe sent is not empty
        if sent:
            doc.append(sent)

        # Clear out the often empty first line
        if not doc[0]:
            doc.pop(0)

        return doc

    def _get_ontonotes_document_by_name_(self, fname: str) -> List[List[str]]:
        """ filename looks like nw/wsj/02/wsj_0242. """

        with (LOC.ontonotes / (fname + '.name')).open('r', encoding='utf8') as f:
            rawlines = f.readlines()

        # Clean up the square brackets, remove \n's
        clean = [re.sub(self.re_sqbkt, '', line).replace('\n', '') for line in rawlines]
        clean = [line for line in clean if line != '']

        return [sent.split() for sent in clean]
