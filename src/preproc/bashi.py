"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr
    This file will work over the raw BASHI corpora. Here are some relevant details.

    # References

        Ina Rösiger (2018)
            BASHI: A corpus of Wall Street Journal articles annotated with bridging links
        Proceedings of LREC. Miyazaki, Japan 2018.

        https://www.ims.uni-stuttgart.de/en/research/resources/corpora/bashi/

    # Description

        BASHI is a corpus consisting of 50 Wall Street Journal (WSJ) articles
        which adds bridging anaphors and their antecedents to the other gold
        annotations that have been created as part of the OntoNotes project.
        Bridging anaphors are context-dependent expressions that do not refer
        to the same entity as their antecedent, but to a related entity.

        The corpus contains 57,709 tokens and 410 bridging pairs and is available for download in an offset-based format
         and a CoNLL-12 style bridging column that can be merged with the other annotation layers in OntoNotes.

        The corpus will be made available for download in two different formats: in an offset-based format
        and in the widely-used, tabular CoNLL-2012 format.

"""
import re
import spacy
from tqdm import tqdm
from pathlib import Path
from spacy.tokens import Token
from typing import List, Dict, Optional

# Local Import
import _pathfix
from utils.nlp import find_one
from config import LOCATIONS as LOC
from preproc.base import OntonotesBasedParser, get_pos_tags, get_noun_chunks, get_span_head
from utils.bridging import BridgingInstance, to_toks


class BashiParser(OntonotesBasedParser):
    """
        # Raw Dataset Format
        1. A line looks like: 'nw/wsj/21/wsj_2151 0 7 - - -'
        1. Syntax: docname, ??, word id, annotation A, annotation B, annotation C
        1. Explanation: there are **three kinds of annotations** in BASHI: definite, indefinite, and comparative
        1. An annotation looks like "(bridging$8$4-6-7" in one of the three slots in a conll line
            1. Here $8 is the ID of the current annotation.
            1. $4-6-7 refers to the antecedent span:  4th sentence, 6th to 7th word
        1. Another way it could look like: "bridging$8)"
            1. This is simply marking the end of the annotation ID 8

        **note**: we offset the span end with 1.
            This is so because when start_id = 20, end_id = 20 in their files
                => we want the 20th word, i.e. words[20: 21]

            Likewise, when they say start_id = 20, end_id = 21. they want two words- 20th and 21st.
            So in python, this translates to words[20: 22]

        # Exceptions
        1. Some data have multiple antecedents. We ignore them all
        1. Some data are opened, but not closed. We ignore them.
        1. Some data have been closed twice, we ignore the second close.
        See self.hacks for more information

        # Parsed Dataset Format (see utils -> bridging.py -> BridgingInstance)
        for each src-antecedent pair:
            doc: list of words
            src: int span start, int span end
            antecedent: int span start, int span end
            bridging type: int ∈ {0,1,2}

        # Parsing Algorithm
        for each conll formatted line with some bridging information:
            from doc name, get entire doc
            note antecedent span numbers
            note src span start nr.
            maintain a set of open spans

            if bridging src is the 'end' sequence:
                pop the open span, note the span end nr.

        TODO: handle case: bridging-multiple
        TODO: handle case: handle case where both one src ends and another begins
        TODO: verify if an antecedent is an src as well
    """

    def __init__(self):
        super().__init__(ontonotes_method='name')

        self.name: str = 'bashi'
        self.bashi_raw_loc: Path = LOC.raw / self.name / 'bashi-bridging-column.conll'

        self.re_docname_in_beginline = re.compile(r'(?<=\s\()[^\(\)]*(?=\))')
        self.re_begin_anaphor_id = re.compile(r'(?<=\$)(\d+)(?=\$)')
        self.re_end_anaphor_id = re.compile(r'(?<=\$)(\d+)(?=\))')
        self.re_annotation_antecedents = re.compile(r'\d+-\d+-\d+')

        # These hacks are needed because there are actual errors in the dataset :/
        self.hacks: Dict[str, List[str]] = {
            # These annotations have been closed twice
            'closed_twice': ['nw/wsj/06/wsj_061670'],
            # These annotations are opened but never closed
            'never_closed': ['nw/wsj/15/wsj_1504243', 'nw/wsj/18/wsj_1875349', 'nw/wsj/18/wsj_1875362'],
            # These annotations' src and antecedent spans overlap
            'span_overlap': ['nw/wsj/09/wsj_0956167', 'nw/wsj/20/wsj_2013431']
        }

    def parse_raw(self) -> None:
        """ Go line by line, fetch documents. Maintain list of active annotations. Finalise when done. """

        bashi_raw: List[str] = self.bashi_raw_loc.open('r', encoding='utf8').readlines()
        current_doc_nm: str = ''
        current_doc_ln: int = 0
        current_doc: List[List[str]] = []
        current_doc_pos: List[List[str]] = []
        current_doc_nps: List[List[int]] = []
        current_doc_nps_h: List[List[int]] = []
        current_doc_: Optional[spacy.tokens.Token] = None
        current_sent_nr: int = 0
        current_sent_ln: int = 0
        annotation_stack: Dict[str, BridgingInstance] = {}  # Key is docname + anaphor_id

        for i, line in enumerate(tqdm(bashi_raw)):

            # Remove all '\n's
            line = line.replace('\n', '')

            if line.startswith('#begin'):
                """ Beginning of new doc """
                current_sent_nr, current_doc_ln, current_sent_ln = 0, 0, 0
                current_doc_nm = find_one(self.re_docname_in_beginline, line)
                current_doc = self.get_ontonotes_document(current_doc_nm)
                current_doc_ = self.nlp(to_toks(current_doc))
                current_doc_pos = get_pos_tags(current_doc_)
                current_doc_nps = get_noun_chunks(fname=current_doc_nm, doc=current_doc)
                current_doc_nps_h = [get_span_head(span, doc=current_doc_) for span in current_doc_nps]

            elif line.startswith('#end'):
                """ End of a doc """

                # Some annotations are simply not closed in the dataset. We drop these data
                for key in self.hacks['never_closed']:
                    if key in annotation_stack:
                        annotation_stack.pop(key)

                assert annotation_stack == {}, f"There are still unclosed annotations, but doc ended. " \
                                               f"Namely: {[key for key in annotation_stack.keys()]}"
                assert current_doc_ln == self.get_absolute_length(current_doc_nm), f"Docname: {current_doc_nm}."

            elif line == '':
                """ End of a sentence, Beginning of a new one after it. """
                current_sent_nr += 1
                current_doc_ln += current_sent_ln
                current_sent_ln = 0

            else:
                """ 
                    A regular conll line. E.g. "nw/wsj/21/wsj_2151 0 7 - - -"
                    
                    If there is no bridging annotation, continue.
                    Else:
                        for each of three positions:
                            if active annotation **starts**:
                                create new BridgingInstance
                                note document text
                                note src id, class, and span start
                                note antecedent spans | assert antecedent happens before the current src, no overlap
        
                                add BridgingInstance to dataset
                            if active annotation **ends**:
                                note src id
                """
                items = line.split()
                current_sent_ln += 1

                # Check if the docname is the same
                assert items[0] == current_doc_nm, f"Expected doc: '{current_doc_nm}', found '{items[0]}'"

                # If there is any annotation here
                if items[3] == items[4] == items[5] == '-':
                    continue

                """
                    There is at least one annotation. It may look like either of:
                        * (bridging$8$6-6-7 (bridging$8$6-6-7       
                        * - (bridging$9$12-8-10
                        * - bridging$9)
                        * bridging$8) bridging$8)
                        * (bridging$20$39-0-1) - (bridging$20$39-0-1)

                
                    If the annotation is a end annotation, and it appears in multiple positions
                        - e.g. `- (bridging$8$6-6-7 (bridging$8$6-6-7`    
                        - treat it as one annotation
                        - check if both annotations have the same antecedent
                    
                    If its an end annotation
                        - e.g. `- bridging$8) bridging$8)` or `- - bridging$9)`
                        - check if it has multiple classes. If so, both classes must end at this very line. 
                        
                    If its both a start and end annotation:
                        - e.g. `(bridging$20$39-0-1) - -`
                        - treat it as one annotation
                        - end it there as well.
                """

                instance: Optional[BridgingInstance] = None
                existing_instance_classes: List[int] = []
                new_instance: bool = False

                for cls, annotation in enumerate(items[3:]):

                    if annotation == '-':
                        continue

                    if annotation.startswith('(bridging$'):
                        # Its a beginning annotation

                        # Parse src info
                        anaphor_id = int(find_one(self.re_begin_anaphor_id, annotation))

                        # Check hack: span overlap. If so, ignore instance
                        if current_doc_nm + str(anaphor_id) in self.hacks['span_overlap']:
                            continue

                        anaphor_start = self.get_absolute_length_upto(current_doc_nm, current_sent_nr, int(items[2]))

                        # Parse antecedent info
                        ante_info = find_one(self.re_annotation_antecedents, annotation).split('-')
                        ante_sent, ante_start, ante_end = [int(x) for x in ante_info]
                        ante_start = self.get_absolute_length_upto(current_doc_nm, ante_sent, ante_start)
                        ante_end = self.get_absolute_length_upto(current_doc_nm, ante_sent, ante_end) + 1
                        ante_h = get_span_head([ante_start, ante_end], doc=current_doc_)

                        # Check if there is already a new instance in a previous position:
                        if instance:

                            # An instance already exists (there are multiple annotations on this line)
                            assert instance.antecedent == [ante_start, ante_end]
                            assert instance.anaphor_id == anaphor_id
                            instance.anaphor_cls.append(cls)

                            # Now, check if the instance ends here as well.
                            # This will happen in cases like: `(bridging$20$39-0-1) - -`
                            # The annotation will end with a ')'

                            if annotation.endswith(')'):
                                # Since this is not the first time we encountered this annotation,
                                # Check if the previous one ended as well or not.
                                assert len(instance.anaphor) == 2

                        else:
                            # Its a new instance
                            new_instance = True
                            instance = BridgingInstance(document=current_doc,
                                                        docname=current_doc_nm,
                                                        antecedent=[ante_start, ante_end],
                                                        antecedent_h=ante_h,
                                                        anaphor_id=anaphor_id,
                                                        anaphor_cls=[cls],
                                                        anaphor=[anaphor_start],
                                                        pos=current_doc_pos,
                                                        candidate_nounphrase_ids=current_doc_nps,
                                                        candidate_nounphrase_h_ids=current_doc_nps_h)

                            # Now, check if the instance ends here as well.
                            # This will happen in cases like: `(bridging$20$39-0-1) - -`
                            # The annotation will end with a ')'

                            if annotation.endswith(')'):

                                # Mark this word as the end of the span as well.
                                instance.anaphor.append(anaphor_start + 1)
                                instance.anaphor_h = get_span_head(instance.anaphor, doc=current_doc_)
                                self.add(instance)

                            else:

                                # The annotation does not end here.
                                annotation_stack[current_doc_nm + str(anaphor_id)] = instance

                        """
                            Now, check if the instance ends here as well.
                            This will happen in cases like: `(bridging$20$39-0-1) - -`
                        """

                    elif annotation.startswith('bridging$'):

                        # Its an ending annotation

                        if new_instance:
                            raise NotImplementedError("In this line, both an annotation ended, and another started.")

                        anaphor_id = int(find_one(self.re_end_anaphor_id, annotation))
                        anaphor_end = self.get_absolute_length_upto(current_doc_nm, current_sent_nr, int(items[2])) + 1

                        # Check if there is already an old instance fetched due to a previous annotation
                        # e.g. Line is `- bridging$8) bridging$8)`, and we are on the last item

                        if instance:
                            assert instance.anaphor_id == anaphor_id
                        else:

                            # Skip if its a flawed annotation
                            if (current_doc_nm + str(anaphor_id) in self.hacks['closed_twice']) and \
                                    (current_doc_nm + str(anaphor_id) not in annotation_stack):
                                continue

                            if current_doc_nm + str(anaphor_id) in self.hacks['span_overlap']:
                                continue

                            # Its the first or only end annotation
                            instance = annotation_stack[current_doc_nm + str(anaphor_id)]
                            existing_instance_classes = instance.anaphor_cls[:]

                            # Lets append the src's span end in it
                            instance.anaphor.append(anaphor_end)
                            instance.anaphor_h = get_span_head(instance.anaphor, doc=current_doc_)

                        # In both cases, we now have an instance. Now lets pop the current class
                        existing_instance_classes.pop(existing_instance_classes.index(cls))

                        # If all classes are popped: we have now completed all beginnings and ends of this annotation
                        # Let's put it out of annotation_stack, and into self.data
                        if not existing_instance_classes:
                            self.add(annotation_stack.pop(current_doc_nm + str(anaphor_id)))

                    elif annotation.startswith('bridging-multiple$') or annotation.startswith('(bridging-multiple$'):
                        # warnings.warn(f"Found a bridging-multiple on line {i}")
                        ...

                    else:
                        raise ValueError(f"Unknown annotation: {annotation}")

                # If it was a beginning annotation, the variable will be []
                # If it was an existing one, the variable will first have 1-3 values, but they should be all popped.
                assert existing_instance_classes == [], f"Instance {instance.docname}:{instance.anaphor_id} " \
                                                        f"had multiple start annotations, " \
                                                        f"but not enough end annotations.\nNamely: {annotation_stack}"

        # We have now gone through all the lines, and hopefully have mined all the data we need.


if __name__ == "__main__":
    _pathfix.suppress_unused_import()

    bashi = BashiParser()
    bashi.run()
