"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    This file is used to convert our bridging data into a legacy format, to be used in another codebase.
    By and large, you probably don't have to worry about it.

    **Fields expected in legacy jsonlines**
        'doc_key',
        'sentences',
        'bridging',
        'entities',
        'pos',
        'clusters',
        'parse_bit',
        'art_bridging_pairs_good',
        'art_bridging_pairs_bad',
        'art_bridging_pairs_ugly'

    **Fields expected in legacy jsonlines but missing**
        - parse_bit
        - art_bridging_pairs_good
        - art_bridging_pairs_bad
        - art_bridging_pairs_ugly
        - clusters
"""
import os
import pickle
import sys
from pathlib import Path
from typing import List, Dict, Tuple

import jsonlines
import spacy
from tqdm import tqdm

# Local imports
import _pathfix
from config import LOCATIONS as LOC
from utils.bridging import BridgingInstance, to_toks
from utils.nlp import NullTokenizer


class LegacyConverter:

    def __init__(self, dataset_name: str):

        # Init a nice numpy instance
        self.nlp = spacy.load('en_core_web_sm')
        self.data_dir: Path = LOC.parsed / dataset_name
        self.nlp.tokenizer = NullTokenizer(self.nlp.vocab)

        # The output, in legacy format
        self.n_dumps: int = 0

    @staticmethod
    def remove_real_spans(entities: List[Tuple[int, int]], antecedents: List[List[int]]) -> List[Tuple[int, int]]:
        """
            Remove any element FROM `entities`
                which also appears in `antecedents`
        """
        for antecedent in antecedents:
            t_antecedent: Tuple[int, int] = (antecedent[0], antecedent[1])
            if t_antecedent in entities:
                entities.pop(entities.index(t_antecedent))

        return entities

    def run(self):

        # Pull BridgingInstances
        filenames = [fname for fname in os.listdir(self.data_dir) if fname.startswith('dump_')
                     and fname.endswith('.pkl')]

        for filename in filenames:

            # Pull data from disk
            with (self.data_dir / filename).open('rb') as f:
                instances: List[BridgingInstance] = pickle.load(f)

            # Flush the legacy variable
            legacy: Dict[str, dict] = {}

            for instance in tqdm(instances):

                if instance.docname not in legacy:
                    # If docname does not exist in legacy, i.e. the first time we're seeing this doc

                    # Pass the doc via spacy
                    # noinspection PyTypeChecker
                    doc = self.nlp(to_toks(instance.document))
                    assert len(doc) == len(to_toks(instance.document))

                    # Get some surface level properties, easily
                    legacy[instance.docname]: dict = {}
                    legacy[instance.docname]['sentences']: List[List[str]] = instance.document
                    legacy[instance.docname]['doc_key']: str = instance.docname
                    legacy[instance.docname]['pos']: List[List[str]] = instance.pos

                    # Get all noun chunk spans.
                    legacy[instance.docname]['entities']: List[List[int]] = instance.candidate_nounphrase_ids
                    legacy[instance.docname]['entity_heads']: List[List[int]] = instance.candidate_nounphrase_h_ids

                    # Add src, antecedent info
                    legacy[instance.docname]['bridging'] = [[instance.antecedent, instance.anaphor]]

                else:
                    # The doc has been seen but we haven't seen the particular src, antecedent pairs
                    legacy[instance.docname]['bridging'].append([instance.antecedent, instance.anaphor])

            """ 
                Finally write them to disk, as jsonlines object
            """
            self.dump_to_disk(legacy)

    def dump_to_disk(self, legacy: Dict[str, dict]) -> None:

        # JSONines output
        with (self.data_dir / f'legacy_{self.n_dumps}.json').open('w+', encoding='utf8') as f:
            with jsonlines.Writer(f) as writer:
                writer.write_all([datum for datum in legacy.values()])

        print(f"Successfully wrote {len(legacy)} data to {self.data_dir}legacy_{self.n_dumps}.json")
        self.n_dumps += 1


if __name__ == "__main__":
    _pathfix.suppress_unused_import()

    dataset = sys.argv[1]
    converter = LegacyConverter(dataset)
    converter.run()
