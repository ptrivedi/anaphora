"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    # Overview

    This file is geared towards parsing datasets released with the
    [**CODI CRAC 2021** Shared Task](https://competitions.codalab.org/competitions/30312)

    Specifically, there is going to be data from 5 domains (2 released as of 05/04/2021) namely:
        - ARRAU (Trains_91): Dev set available now via LDC. Please mail this form to LDC to request the dataset.
        - Switchboard: Dev set available now via LDC. Please mail this form to LDC to request the dataset.
        - AMI: Coming Soon!
        - Persuasion: Coming Soon!
        - Light: Coming Soon!

    # Format

    Thankfully, these datasets are in a very comparable CONLL-esque format called **Universal Anaphora Format**
    Details about this format can be found on:
        https://github.com/UniversalAnaphora/UniversalAnaphora/blob/main/UA_CONLL_U_Plus_proposal_v1.0.md

    # How to get Data

    Refer to the ./README.md file's manual downloads section
"""
import re
import copy
import click
from pathlib import Path
from typing import List, Dict, Optional

# Local Import
import _pathfix
from config import LOCATIONS as LOC
from utils.nlp import AnnotationBlock, AnnotationBlockStack
from utils.bridging import BridgingInstance, BridgingDocument, to_toks
from preproc.base import BaseBridgingParser, BaseDocumentParser, get_span_head, get_pos_tags


class CracParser(BaseBridgingParser):
    """
        # Usage Examples

            parser = CracParser(name='arrau', fname='Trains_91.CONLL', ignore_pseudo=False)
            parser.run()

            parser = CracParser(name='arrau', ignore_pseudo=False)  # fname defaults to 'Trains_91.CONLL' for arrau
            parser.run()

            parser = CracParser(name='switchboard')
            parser.run()

        # Format Cliff notes

            - akin to the CONLL format
            - newline is new sentence
            - lines beginning with # are comments and contain metadata
            - lines beginning with numbers are actual lines
            - entities/markables/noun phrases are defined in the IDENTITY field.
                - noun phrases are also defined in the SPLIT field (TODO: model these in)
            - bridging anaphors are defined in the BRIDGING field.
                - markable reference: `MentionAnchor`

        # Parsing Details.
        ## Noun phrases (candidates)

            1. We consider all marked entities/markables as candidates. This annotation is present in the ENTITY field
                 `(EntityID=1-DD|MarkableID=dd_markable_535|Min=8,23|SemType=dn`
            1. **We ignore all singletons**. They are represented as:
                `EntityID=1-Pseudo|MarkableID=markable_469|Min=16,17|SemType=quantifier`
                `Pseudo` signifies that they're singletons (but only for the coref tasks;
                they are valid antecedents for bridging anaphora task)
            1. As before, we only include the markables which have been referenced as antecedents
                for different bridging anaphors in the candidate-antecedents set.
            1. And we ensure that candidate-nounphrase set is superset of candidates-antecedents set.
            1. We ignore all available METADATA for consistency's sake. But we can consider adding them later.

    """

    def __init__(self, name: str, fname: Optional[str] = '', ignore_pseudo: bool = False, is_test: bool = False):
        """
        :param name: The name of the dataset; ['arrau', 'switchboard' ...]
        :param fname: The actual filename; in case of 'arrau' if we want to parse any other specific file
        :param ignore_pseudo: ignore all pseudo entities as markables/nounphrases.
        """
        super().__init__(pretokenized=True)

        default_fnames: Dict[str, str] = {'switchboard': 'Switchboard_3_dev.CONLL',
                                          'arrau': 'Trains_91.CONLL',
                                          'light': 'light_dev.CONLLUA',
                                          'persuasion': 'Persuasion_dev.CONLLUA',
                                          'ami': 'AMI_dev.CONLLUA'}

        default_fnames_test: Dict[str, str] = {'switchboard': 'swbd_test_gold_men.CONLLUA',
                                               'arrau': 'Trains_91.CONLL',
                                               'light': 'light_test_gold_men.CONLLUA',
                                               'persuasion': 'Persuasion_test_gold_men.CONLLUA',
                                               'ami': 'AMI_test_gold_men.CONLLUA'}
        if is_test:
            self.name: str = 'test/crac'
        else:
            self.name: str = 'crac'

        self.is_test: bool = is_test
        self.crac_src: str = name
        if not fname:
            self.fname: str = default_fnames[self.crac_src] if not self.is_test else default_fnames_test[self.crac_src]
        else:
            self.fname: str = fname
        self.ignore_pseudo: bool = ignore_pseudo

        if self.crac_src in ['arrau']:
            self.name_addendum = Path(self.crac_src) / self.fname
        else:
            self.name_addendum = self.crac_src

        # noinspection RegExpRedundantEscape
        self.re_nested_annotation_blocks = re.compile(r'\([^\)\(\n]*\)?|\)')

    @staticmethod
    def _parse_annotation_(annotation: str) -> Dict[str, str]:
        """
        INPUT
            (EntityID=1-Pseudo|MarkableID=markable_469|Min=16,17|SemType=quantifier)
        OUTPUT
            {'EntityID': '1-Pseudo', 'MarkableID': 'markable_469', 'Min': '16,17', 'SemType': 'quantifier'}
        """

        if annotation[0] == '(':
            annotation = annotation[1:]

        if annotation[-1] == ')':
            annotation = annotation[:-1]

        parsed: Dict[str, str] = {}
        for key_value in annotation.split('|'):
            k = key_value.split('=')[0]
            v = key_value.split('=')[1]
            parsed[k] = v

        return parsed

    @staticmethod
    def _arrau_speaker_identification_posthoc(sentence: List[str], crac_src: str, fname: str) -> int:

        sp_names: dict = {
            'arrau': {
                'Trains_91.CONLL': ['s', 'm'],
                'Trains_93.CONLL': ['s', 'u'],
                'Trains_93.CONLL_alter': ['s:', 'u:']
            }
        }

        if ':' not in ' '.join(sentence):
            return -1

        if sentence[1] == ':':
            if sentence[0].lower() in sp_names[crac_src][fname]:
                return sp_names[crac_src][fname].index(sentence[0].lower())
            else:
                return -1
        elif fname == 'Trains_93.CONLL' and sentence[0].lower() in sp_names[crac_src][fname + '_alter']:
            return sp_names[crac_src][fname + '_alter'].index(sentence[0].lower())
        else:
            return -1

    def _arrau_clean_documents_(self, document: List[List[str]]) -> (List[List[str]], List[int]):
        # Init some vars
        sentences_by_speakers = []
        speakers: List[int] = []
        sentences_by_current_speaker = []

        for sent in document:
            # For each sent, see if the sent introduces a new speaker
            new_speaker: int = self._arrau_speaker_identification_posthoc(sent, self.crac_src, self.fname)

            if new_speaker >= 0:
                # Append the already known sentences to the main list and reset the current speaker list
                if sentences_by_current_speaker:
                    sentences_by_speakers.append(sentences_by_current_speaker)
                sentences_by_current_speaker = [sent]
                speakers.append(new_speaker)
            else:
                sentences_by_current_speaker.append(sent)

        # Finally, append the remaining "start of new sentence"
        sentences_by_speakers.append(sentences_by_current_speaker)
        final_sentences = []
        for s in sentences_by_speakers:
            final_sentences.append([j for i in s for j in i])

        # Some sanity checks
        if not len(speakers) == len(final_sentences):
            raise AssertionError(f"Len mismatch, speakers and final sentences")

        if not tuple(to_toks(final_sentences)) == tuple(to_toks(document)):
            raise AssertionError(f"One or more elements of raw and processed documents differ")
        for index in range(len(to_toks(document))):
            assert to_toks(final_sentences)[index] == to_toks(document)[index]

        return final_sentences, speakers

    @staticmethod
    def _switchboard_clean_documents(document: List[List[str]]) -> (List[List[str]], List[int]):
        """ No cleaning, every alternate sentence is spoken by alternate speaker """
        speakers = [i % 2 for i in range(len(document))]
        return document, speakers

    def get_markables(self, documents: Dict[str, List[List[str]]],
                      documents_: Dict[str, List[List[Dict[str, str]]]], annotation_key: str, tag_name: str,
                      ignore_pseudo: bool, meta_name: Optional[str] = None) -> Dict[str, List[AnnotationBlock]]:
        """
            :param documents: A dict of list of list of tokens. Key is doc name
            :param documents_: same as above but instead of token str, we have a dict of all annotations from raw file
            :param annotation_key: which annotation key are we extracting for. E.g. IDENTITY (for markables)
            :param tag_name: we might want to have some text information saved up. which field.
                e.g. in  'MarkableID' in `(EntityID=1-Pseudo|MarkableID=markable_469|Min=16,17|SemType=quantifier)`
            :param meta_name: if given also store some more text information from the specified field. same as above.
            :param ignore_pseudo: don't include it if Pseudo is present in the annotation.
                e.g. Pseudo in EntityID in  `(EntityID=1-Pseudo|MarkableID=markable_469|Min=16,17|SemType=quantifier)`
            :return { docname - 1:
                        { markable tag name - 1: [span start int, span end int],
                          markable tag name - 2: [span start int, span end int],
                          ...
                        },
                      docname - 2:
                        { markable tag name - 3: [span start int, span end int],
                          markable tag name - 4: [span start int, span end int],
                          ...
                        }
                      ...
                    }

           # Markable Detection

               **START**
               an annotation like `(EntityID=1-Pseudo|MarkableID=markable_469|Min=16,17|SemType=quantifier`
               in the 'IDENTITY' field (for e.g.) of a token marks the start of a markable.

               **END**
               Its end is found by the bracket close.

               We assume they can be nested.

           # Algorithm
                We use AnnotationBlock and AnnotationBlock stack used initially in
                    `src/preproc/base class OntonotesBasedParser def _get_pos_spans_from_ontonotes_onf_`

                `( ....`
                Every time we find an open bracket as specified above,
                    we create a new AnnotationBlock and append it to an AnnotationBlockStack

                We also append `1` to all open (i.e. in the stack) AnnotationBlock's 'open' counter in the stack.

                `)`
                We append `1` to all open (i.e. in the stack) AnnotationBlock's 'closed' counter in the stack.
                When open and closed equate, i.e. this block is closed. \
                We pop it from stack and save it in a permanent-ish list.

                Else
                On each subsequent word, we append the word and add its ID to all elements of the AnnotationBlockStack.
        """
        markables_: Dict[str, List[AnnotationBlock]] = {}
        for docname, document, document_ in zip(documents.keys(), documents.values(), documents_.values()):

            stack: AnnotationBlockStack = AnnotationBlockStack()
            finished_blocks: List[AnnotationBlock] = []
            cumulative_token_id_till_sent_start: int = 0

            for sentence_id, (sentence, sentence_) in enumerate(zip(document, document_)):
                for token_id, (token, token_) in enumerate(zip(sentence, sentence_)):

                    assert token == token_['FORM'], f"token: `{token}` and token_: `{token_['FORM']}` do not match!"

                    annotation: str = token_[annotation_key]

                    # In all cases
                    # Add word to all active blocks
                    stack.register_word(token)

                    # Find all annotation blocks
                    # E.g. of blocks
                    # ( ..... -> [(....]
                    # (...(...)(.... -> [(... | (...) | (...]
                    # ) -> [)]
                    # )) -> [) | )]
                    # ( .... ) -> [ (...) ]
                    # Content inside the block ... -> EntityID=5-Pseudo....
                    # Each of these blocks will be processed and replaced in the actual str `annotation` with `_`
                    for annotation_block in self.re_nested_annotation_blocks.finditer(annotation):
                        _span_b, _span_e = annotation_block.span()
                        annotation_block_: str = annotation_block.group()

                        # Case A: annotation has an open bracket
                        if '(' in annotation_block_:

                            if ignore_pseudo and 'pseudo' in annotation_block_.split('|')[0].lower():
                                # Ignore all pseudo ones (they are singletons)
                                continue

                            # Update active blocks with an open bracket
                            stack.add(1)

                            # Parse contents within a block, so we can note down markable IDs and such
                            parsed_annotation_block: Dict[str, str] = self._parse_annotation_(annotation_block_)

                            new_instance: AnnotationBlock = AnnotationBlock(
                                start=cumulative_token_id_till_sent_start + token_id,
                                words=[token],
                                open_inside=1,
                                tag=parsed_annotation_block[tag_name]
                            )

                            if meta_name:
                                new_instance.metadata = parsed_annotation_block[meta_name]

                            stack.append(new_instance)

                        # Case B: (not mutually exclusive): annotation has a closed bracket
                        if ')' in annotation_block_:

                            assert annotation_block_.count(')') == 1, "There are more than one `(` in this block!"

                            # Update active blocks with closed brackets. Catch all those that are _finished_.
                            _finished_blocks: list = stack.sub(1, span=cumulative_token_id_till_sent_start + token_id)

                            if _finished_blocks:
                                finished_blocks += _finished_blocks

                        # UNCONDITIONALLY replace this span with '_' in the main string
                        annotation = annotation[:_span_b] + '_' * (_span_e - _span_b) + annotation[_span_e:]

                # Done Iterating over this sentence. Note the number of tokens in it.
                cumulative_token_id_till_sent_start += len(sentence)

            # Done iterating over all sentences of this document

            # Check if stack is empty
            assert len(stack.blocks) == 0

            # Add the _finished_ ones to the main dict
            markables_[docname] = finished_blocks

        # Sanity check the markables
        for docname, doc_markables in markables_.items():
            doc_toks: List[str] = to_toks(documents[docname])
            for markable in doc_markables:
                assert doc_toks[markable.start: markable.end] == markable.words

        return markables_

    def parse_raw(self):
        """
            The actual code to parse one file.

            One file contains multiple documents

            # Algorithm

            1. Run through the file once creating
                - a List[List[str]] doc for every document mentioned.
                    i.e. where a token belongs in a sentence list, and a sentence list belongs in a document list.
                - a corresponding List[List[Dict[str, str]]] list corresponding to the above where
                    each token str is replaced by a dict containing all the fields mentioned in the file.
        """
        data_dir: Path = LOC.raw if not self.is_test else LOC.raw / 'test'

        with (data_dir / f"crac-{self.crac_src}" / self.fname).open('r') as f:
            raw = f.readlines()

        """
            Init some variables we'll use in the doc.
            
            sentence is a list of tokens in one sentence
            document is a list of sentences in one document
            documents is a docname: doc sentences dict for each document in the file
            
            their `var_name`_ variants replace the token str with a dict containing all metadata for this token.
        """
        documents: Dict[str, List[List[str]]] = {}
        documents_: Dict[str, List[List[Dict[str, str]]]] = {}
        documents_speakers: Dict[str, List[int]] = {}
        known_speakers: Dict[str, int] = {}
        current_speaker = 10 if self.crac_src == 'light' else -1  # light begins with 'setting' which is new speaker

        raw_document: List[List[str]] = []
        document_speakers: List[int] = []
        document_: List[List[Dict[str, str]]] = []

        sentence: List[str] = []
        sentence_: List[Dict[str, str]] = []

        docname: str = ''

        """
            Parse the file to fill the documents and documents_ var.
        """
        # Keys to token annotations are mentioned in the first line, sort of like an csv file.
        keys: List[str] = raw[0].strip().split('=')[1].split()
        last_state: int = -1

        # Iterate after the first line.
        for i, line in enumerate(raw[1:]):

            # Strip and split the line into its tokens
            tokens = line.strip().split()

            if not tokens:
                # New sentence line.
                # Example: '\n', i.e. an empty line

                assert sentence, "Expected sentence to have some tokens. It is empty"

                raw_document.append(sentence)
                document_.append(sentence_)
                document_speakers.append(current_speaker)

                sentence, sentence_ = [], []

                last_state = 0

            elif tokens[0] == '#':
                # Meta data line.
                # Example: '# sent_id = D93_9_1-1\n'

                if self.crac_src in ['light', 'persuasion', 'ami'] and 'speaker' in tokens:
                    # A special metadata line: speaker information
                    # E.g. "# speaker = orc"
                    current_speaker = known_speakers.setdefault(tokens[-1], len(known_speakers))

                if 'newdoc' in tokens:
                    # We have started a document. Dump the old one and store the key for the new one

                    if raw_document:
                        documents[docname] = raw_document
                        documents_[docname] = document_
                        documents_speakers[docname] = document_speakers

                    raw_document, document_, document_speakers = [], [], []
                    docname = tokens[-1]
                    current_speaker = 10

                last_state = 1

            elif tokens[0].isdigit():
                # Actual word.
                # Example: '3     hello  _  _  _  _  _  _  _  _  _  _  _  _  \n'
                # It will have either 14 or 15 things.

                assert len(keys) - 1 <= len(tokens) <= len(keys), f"Line {i} has {len(tokens)} annotations."

                if len(tokens) == len(keys) - 1:
                    tokens.append('-')

                parsed = {k: v for k, v in zip(keys, tokens)}
                parsed['RAW'] = line

                sentence.append(parsed['FORM'])
                sentence_.append(parsed)

                last_state = 2

        # Finally check the last state and append things based on that
        if not last_state == 2:
            raise ValueError("The first CRAC parsing loop ended unexpectedly")

        # Assuming we last added a word to the sent.
        # #### Time to put that sent in the doc, the last doc in the global list of docs.
        assert sentence, "Expected sentence to have some tokens. It is empty"

        raw_document.append(sentence)
        document_.append(sentence_)
        document_speakers.append(current_speaker)
        documents[docname] = raw_document
        documents_[docname] = document_
        documents_speakers[docname] = document_speakers

        """
            Get all noun phrases (which may be antecedents for anaphors)
        """
        markables_: Dict[str, List[AnnotationBlock]] = self.get_markables(documents=documents,
                                                                          documents_=documents_,
                                                                          annotation_key='IDENTITY',
                                                                          tag_name='MarkableID',
                                                                          ignore_pseudo=self.ignore_pseudo)

        # Strip these annotation blocks of all metadata, and only keep spans and markable names
        markables: Dict[str, Dict[str, AnnotationBlock]] = {docname: {block.tag: block for block in blocks}
                                                            for docname, blocks in markables_.items()}

        """ 
            Get all bridging annotations (along with the markables to which they refer 
        """
        bridging_anaphors: Dict[str, List[AnnotationBlock]] = self.get_markables(documents=documents,
                                                                                 documents_=documents_,
                                                                                 annotation_key='BRIDGING',
                                                                                 tag_name='MarkableID',
                                                                                 meta_name='MentionAnchor',
                                                                                 ignore_pseudo=False)

        for docname, raw_document, document_ in zip(documents.keys(), documents.values(), documents_.values()):

            # Collect information to be put into BridgingInstances
            document_ = self.nlp(to_toks(raw_document))
            document_pos = get_pos_tags(document_)
            document_nps = [[markable.start, markable.end] for markable in markables[docname].values()]
            document_nps_h = [get_span_head(span, doc=document_) for span in document_nps]

            if self.crac_src == 'arrau' and self.fname.startswith("Trains"):
                document, speakers = self._arrau_clean_documents_(raw_document)
            elif self.crac_src == 'switchboard':
                document, speakers = self._switchboard_clean_documents(raw_document)
            elif self.crac_src in ['light', 'persuasion', 'ami']:
                document = raw_document
                speakers = documents_speakers[docname]
            else:
                raise NotImplementedError

            for i, bridging_block in enumerate(bridging_anaphors[docname]):

                # Find the span of antecedent
                antecedent_annotation: AnnotationBlock = markables[docname][bridging_block.metadata]

                # Find the information necessary for creating a bridging instance
                anaphor_id: str = docname + str(i)
                # NOTE: This is important! We have decided to ignore the antecedents which appear after the anaphors.
                anaphor_cls: List[int] = [0]
                anaphor_span: List[int] = [bridging_block.start, bridging_block.end]
                anaphor_span_h: List[int] = get_span_head(anaphor_span, doc=document_)
                antecedent_span: List[int] = [antecedent_annotation.start, antecedent_annotation.end]
                antecedent_span_h: List[int] = get_span_head(antecedent_span, doc=document_)

                if anaphor_span == antecedent_span:
                    raise AssertionError("Same anaphor and antecedent spans!")

                # Remove the actual anaphor from the candidate list
                id_of_anaphor_in_candidates = document_nps.index(anaphor_span)
                candidate_nounphrase_ids = copy.deepcopy(document_nps)
                candidate_nounphrase_ids.pop(id_of_anaphor_in_candidates)
                candidate_nounphrase_h_ids = copy.deepcopy(document_nps_h)
                candidate_nounphrase_h_ids.pop(id_of_anaphor_in_candidates)

                instance = BridgingInstance(
                    document=document,
                    docname=docname,
                    pos=document_pos,
                    antecedent=antecedent_span,
                    antecedent_h=antecedent_span_h,
                    anaphor_id=anaphor_id,
                    anaphor_cls=anaphor_cls,
                    anaphor=anaphor_span,
                    anaphor_h=anaphor_span_h,
                    candidate_nounphrase_ids=candidate_nounphrase_ids,
                    candidate_nounphrase_h_ids=candidate_nounphrase_h_ids,
                    document_raw=raw_document,
                    speaker_info=speakers
                )

                # Store this instance
                self.add(instance)


class CracDocumentParser(BaseDocumentParser):
    """
        A class which returns not BridgingInstance (s) but BridgingDocument (s).
        These objects correspond not to each anaphoric nounphrase but instead to entire document
            which may or may not contain anaphoric spans.
    """

    def __init__(self, name: str, fname: Optional[str] = '', ignore_pseudo: bool = False, is_test: bool = False):
        """
        :param name: The name of the dataset; ['arrau', 'switchboard' ...]
        :param fname: The actual filename; in case of 'arrau' if we want to parse any other specific file
        :param ignore_pseudo: ignore all pseudo entities as markables/nounphrases.
        :param is_test: if flag is True, we dont expect to find bridging annotations.
            But we still create instances for each document with all its nounphrases.
        """
        super().__init__(pretokenized=True, is_test=is_test)
        self.cp = CracParser(name=name, fname=fname, ignore_pseudo=ignore_pseudo, is_test=is_test)
        self.name = self.cp.name
        self.fname = self.cp.fname
        self.crac_src = self.cp.crac_src
        self.name_addendum = self.cp.name_addendum

    # noinspection PyProtectedMember
    def parse_raw(self) -> None:
        """
            The actual code to parse one file.

            One file contains multiple documents

            # Algorithm

            1. Run through the file once creating
                - a List[List[str]] doc for every document mentioned.
                    i.e. where a token belongs in a sentence list, and a sentence list belongs in a document list.
                - a corresponding List[List[Dict[str, str]]] list corresponding to the above where
                    each token str is replaced by a dict containing all the fields mentioned in the file.

            # i.e. find all markables/nounphrases associated with all documents
            For each document, make a new instance (do not self.add() them)

            2. If we have this information (not self.is_test), find all bridging markables and note their antecedents
            3. For each bridging markable, annotate the corresponding document instance with anaphor and antecedent info
        """

        data_dir: Path = LOC.raw if not self.is_test else LOC.raw / 'test'

        with (data_dir / f"crac-{self.cp.crac_src}" / self.cp.fname).open('r') as f:
            raw = f.readlines()

        """
        Init some variables we'll use in the doc.
        
        sentence is a list of tokens in one sentence
        document is a list of sentences in one document
        documents is a docname: doc sentences dict for each document in the file
        
        their `var_name`_ variants replace the token str with a dict containing all metadata for this token.
        """
        documents: Dict[str, List[List[str]]] = {}
        documents_: Dict[str, List[List[Dict[str, str]]]] = {}
        documents_speakers: Dict[str, List[int]] = {}
        known_speakers: Dict[str, int] = {}
        # light begins with 'setting' which is new speaker
        current_speaker = 10 if self.cp.crac_src == 'light' else -1

        raw_document: List[List[str]] = []
        document_speakers: List[int] = []
        document_: List[List[Dict[str, str]]] = []

        sentence: List[str] = []
        sentence_: List[Dict[str, str]] = []

        docname: str = ''

        """
            Parse the file to fill the documents and documents_ var.
        """
        # Keys to token annotations are mentioned in the first line, sort of like an csv file.
        keys: List[str] = raw[0].strip().split('=')[1].split()
        last_state: int = -1

        # Iterate after the first line.
        for i, line in enumerate(raw[1:]):

            # Strip and split the line into its tokens
            tokens = line.strip().split()

            if not tokens:
                # New sentence line.
                # Example: '\n', i.e. an empty line

                assert sentence, "Expected sentence to have some tokens. It is empty"

                raw_document.append(sentence)
                document_.append(sentence_)
                document_speakers.append(current_speaker)

                sentence, sentence_ = [], []

                last_state = 0

            elif tokens[0] == '#':
                # Meta data line.
                # Example: '# sent_id = D93_9_1-1\n'

                if self.cp.crac_src in ['light', 'persuasion', 'ami'] and 'speaker' in tokens:
                    # A special metadata line: speaker information
                    # E.g. "# speaker = orc"
                    current_speaker = known_speakers.setdefault(tokens[-1], len(known_speakers))

                if 'newdoc' in tokens:
                    # We have started a document. Dump the old one and store the key for the new one

                    if raw_document:
                        documents[docname] = raw_document
                        documents_[docname] = document_
                        documents_speakers[docname] = document_speakers

                    raw_document, document_, document_speakers = [], [], []
                    docname = tokens[-1]
                    current_speaker = 10

                last_state = 1

            elif tokens[0].isdigit():
                # Actual word.
                # Example: '3     hello  _  _  _  _  _  _  _  _  _  _  _  _  \n'
                # It will have either 14 or 15 things.

                assert len(keys) - 1 <= len(tokens) <= len(keys), f"Line {i} has {len(tokens)} annotations."

                if len(tokens) == len(keys) - 1:
                    tokens.append('-')

                parsed = {k: v for k, v in zip(keys, tokens)}
                parsed['RAW'] = line

                sentence.append(parsed['FORM'])
                sentence_.append(parsed)

                last_state = 2

        # Finally check the last state and append things based on that
        if not last_state == 2:
            raise ValueError("The first CRAC parsing loop ended unexpectedly")

        # Assuming we last added a word to the sent.
        # #### Time to put that sent in the doc, the last doc in the global list of docs.
        assert sentence, "Expected sentence to have some tokens. It is empty"

        raw_document.append(sentence)
        document_.append(sentence_)
        document_speakers.append(current_speaker)
        documents[docname] = raw_document
        documents_[docname] = document_
        documents_speakers[docname] = document_speakers

        """
            Get all noun phrases (which may be antecedents for anaphors)
        """
        markables_: Dict[str, List[AnnotationBlock]] = self.cp.get_markables(documents=documents,
                                                                             documents_=documents_,
                                                                             annotation_key='IDENTITY',
                                                                             tag_name='MarkableID',
                                                                             ignore_pseudo=self.cp.ignore_pseudo)

        # Strip these annotation blocks of all metadata, and only keep spans and markable names
        markables: Dict[str, Dict[str, AnnotationBlock]] = {docname: {block.tag: block for block in blocks}
                                                            for docname, blocks in markables_.items()}

        if not self.is_test:
            """ 
                Get all bridging annotations (along with the markables to which they refer 
            """
            bridging_anaphors: Dict[str, List[AnnotationBlock]] = self.cp.get_markables(documents=documents,
                                                                                        documents_=documents_,
                                                                                        annotation_key='BRIDGING',
                                                                                        tag_name='MarkableID',
                                                                                        meta_name='MentionAnchor',
                                                                                        ignore_pseudo=False)
        else:
            bridging_anaphors = {}

        for docname, raw_document, document_ in zip(documents.keys(), documents.values(), documents_.values()):

            # Collect information to be put into BridgingInstances
            document_ = self.nlp(to_toks(raw_document))
            document_nps = [[markable.start, markable.end] for markable in markables[docname].values()]
            document_nps_h: List[List[int]] = [get_span_head(span, doc=document_) for span in document_nps]

            if self.cp.crac_src == 'arrau' and self.cp.fname.startswith("Trains"):
                document, speakers = self.cp._arrau_clean_documents_(raw_document)

            elif self.cp.crac_src == 'switchboard':
                document, speakers = self.cp._switchboard_clean_documents(raw_document)
            elif self.cp.crac_src in ['light', 'persuasion', 'ami']:
                document = raw_document
                speakers = documents_speakers[docname]
            else:
                raise NotImplementedError

            # Pass the updated doc through NLP again and re calc. POS tags
            document_ = self.nlp(to_toks(document))
            document_pos: List[str] = to_toks(get_pos_tags(document_))

            # At this point document and document_pos have the same num. tokens
            # We have to ensure that document_pos follows the same sentence boundaries as document
            document_pos_conformed = []
            tok_idx = 0
            for sent in document:
                sent_pos = []
                for _ in sent:
                    sent_pos.append(document_pos[tok_idx])
                    tok_idx += 1

                document_pos_conformed.append(sent_pos)

            instance = BridgingDocument(
                document=document,
                pos=document_pos_conformed,
                docname=docname,
                nounphrases=document_nps,
                nounphrases_h=document_nps_h,
                is_test_data=self.is_test,
                speaker_info=speakers,
                document_raw=raw_document
            )

            found_anaphors: bool = False

            # If we have bridging anaphors, add them to the instance
            for i, bridging_block in enumerate(bridging_anaphors.get(docname, [])):

                found_anaphors = True

                # Find the span of antecedent
                antecedent_annotation: AnnotationBlock = markables[docname][bridging_block.metadata]

                anaphor_span: List[int] = [bridging_block.start, bridging_block.end]
                anaphor_span_h: List[int] = get_span_head(anaphor_span, doc=document_)
                antecedent_span: List[int] = [antecedent_annotation.start, antecedent_annotation.end]
                antecedent_span_h: List[int] = get_span_head(antecedent_span, doc=document_)

                if instance.anaphors is None:
                    instance.anaphors = []
                    instance.anaphors_ = []
                    instance.anaphors_h = []
                    instance.anaphors_h_ = []
                    instance.antecedents = []
                    instance.antecedents_ = []
                    instance.antecedents_h = []
                    instance.antecedents_h_ = []

                instance.anaphors.append(anaphor_span)
                instance.anaphors_h.append(anaphor_span_h)
                instance.antecedents.append(antecedent_span)
                instance.antecedents_h.append(antecedent_span_h)

            # If we are not in test mode but we still did not find any anaphors, we ignore this instance
            if not self.is_test and not found_anaphors:
                continue

            # Store this instance
            self.add(instance)


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. arrau, switchboard etc")
@click.option('--filename', '-f', type=str,
              help="In case of arrau, specify which file you want parsed. Defaults to 91.")
@click.option('--test', is_flag=True, default=False,
              help="If True, we look for file in data/raw/test/crac-* and don't expect anaphor, antecedent info")
@click.option('--documents', '-doc', is_flag=True, default=False,
              help="If True, we render BridgingDocument and not BridgingInstance. The new shiny format!")
def run(dataset: str, filename: str, documents: bool, test: bool):
    if documents:
        parser = CracDocumentParser(name=dataset, fname=filename, ignore_pseudo=False, is_test=test)
    else:
        parser = CracParser(name=dataset, fname=filename, ignore_pseudo=False, is_test=test)

    parser.run()


if __name__ == '__main__':
    _pathfix.suppress_unused_import()

    run()
