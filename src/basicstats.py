"""
    Authored by: Priyansh Trivedi, priyansh.trivedi@inria.fr

    This file, given some dataset (list of bridging instances), will compute some basic stats over it.
    Namely:

    1. Total number of instances
    1. Average length of span of anaphor
    1. Average length of span of antecedent
    1. Average number of candidates per anaphor
        - candidate: nounphrase
        - candidate: antecedent
    1. Average distance between the anaphor and antecedent
        - in words
        - in sentences
    1. Number of anaphors per antecedents
    1. Number of anaphors per nounphrase
    1. Average Number of words in a document
    1. Average Number of sentences in a document
    1. Average length of sentences in a document

"""
# Global Imports
import json
import click
import spacy
import numpy as np
from typing import List, Dict, Optional

# Local Imports
from utils.bridging import BridgingInstance, gather_instances_by_docnames
from utils.nlp import NullTokenizer
from utils.misc import get_uniques, check_overlap
from dataloader import DataLoader
from utils.nlp import to_toks


def _get_unique_candidates_(instances: List[BridgingInstance], source: str) -> List[List[int]]:
    """
        Get all unique candidates across this list of instances.
        Source can be either nounphrases or antecedents
    """
    assert source in ['nounphrase', 'antecedent']

    all_candidates: List[List[int]] = []
    for instance in instances:
        candidates: List[int] = getattr(instance, f"candidate_{source}_ids")
        all_candidates += candidates

    return [all_candidates[i] for i in get_uniques(all_candidates)]


def _count_words_between_(span_a: List[int], span_b: List[int]) -> int:
    """ if overlap -> len is zero. """

    overlap: bool = len(set(range(span_a[0], span_a[1])).intersection(range(span_b[0], span_b[1]))) > 0
    if overlap:
        return 0

    return max(span_a[0] - (span_b[1] - 1), span_b[0] - (span_a[1] - 1))


def _count_sents_between_(span_a: List[int], span_b: List[int], instance: BridgingInstance) -> int:
    senta: List[int] = instance.get_sentence(span_a)
    span_b: List[int] = instance.get_sentence(span_b)

    return abs(senta[0] - span_b[0])


def _check_if_antecedent_is_ner_(instance: BridgingInstance, nlp: spacy) -> bool:
    """assumes antecedent is ner, even if there is one char overlap"""
    nlp_doc_ = nlp(to_toks(instance.document))
    correct_span = instance.antecedent
    for ent in nlp_doc_.ents:
        start_loc = ent.start_char
        end_loc = ent.end_char
        if check_overlap([start_loc, end_loc], correct_span):
            return True
    return False


def compute(dataloader: DataLoader, name: str):
    # Loading space model
    nlp = spacy.load('en_core_web_sm')
    nlp.tokenizer = NullTokenizer(nlp.vocab)

    all = [instance for instance in dataloader.all()]

    # Total Number of instances and dataset name
    summary: dict = {'name': str(name), 'instances': len(dataloader)}

    # Average length of span of anaphor
    len_anaphor: List[int] = [instance.anaphor[1] - instance.anaphor[0] for instance in all]
    summary['anaphor len'] = [np.mean(len_anaphor), np.std(len_anaphor)]

    # Average length of span of antecedent
    len_antecedent: List[int] = [instance.antecedent[1] - instance.antecedent[0] for instance in all]
    summary['antecedent len'] = [np.mean(len_antecedent), np.std(len_antecedent)]

    # Average length of span of candidates
    # Group docs by docname. Only count one instance per docname
    # - antecedents
    # - nounphrases

    data_by_docname: Dict[str, List[BridgingInstance]] = gather_instances_by_docnames(all)
    unique_candidate_antecedents_per_doc: List[List[List[int]]] = [_get_unique_candidates_(v, 'antecedent')
                                                                   for v in data_by_docname.values()]
    unique_candidate_nounphrases_per_doc: List[List[List[int]]] = [_get_unique_candidates_(v, 'nounphrase')
                                                                   for v in data_by_docname.values()]

    len_candidate_antecedents_per_doc: List[int] = [span[1] - span[0]
                                                    for instances in unique_candidate_antecedents_per_doc
                                                    for span in instances]
    len_candidate_nounphrases_per_doc: List[int] = [span[1] - span[0]
                                                    for instances in unique_candidate_nounphrases_per_doc
                                                    for span in instances]

    summary['candidate antecedent len'] = [np.mean(len_candidate_antecedents_per_doc),
                                           np.std(len_candidate_antecedents_per_doc)]
    summary['candidate nounphrase len'] = [np.mean(len_candidate_nounphrases_per_doc),
                                           np.std(len_candidate_nounphrases_per_doc)]

    # Average number of candidates per anaphor
    # - antecedents
    # - nounphrases

    num_candidate_nounphrases = [len(instance.candidate_nounphrase_ids) for instance in all]
    summary['candidate nounphrase num'] = [np.mean(num_candidate_nounphrases), np.std(num_candidate_nounphrases)]

    # Average length between anaphor and antecedent spans
    words_between_spans = [_count_words_between_(instance.anaphor, instance.antecedent)
                           for instance in all]
    sents_between_spans = [_count_sents_between_(instance.anaphor, instance.antecedent, instance)
                           for instance in all]

    summary['words between'] = [np.mean(words_between_spans), np.std(words_between_spans)]
    summary['sents between'] = [np.mean(sents_between_spans), np.std(sents_between_spans)]

    summary['number of times antecedent is ner'] = [_check_if_antecedent_is_ner_(instance, nlp)
                                                    for instance in all].count(True)

    # Number of anaphors per antecedents
    # - aggregate instances per doc
    # - get all unique real antecedents per doc
    # - count how many anaphors point to this.

    num_anaphors_per_antecedents = []
    num_anaphors_per_nounphrases = []

    for i, instances in enumerate(data_by_docname.values()):

        # candidates for this doc
        antecedents_ = {str(list(k)): 0 for k in unique_candidate_antecedents_per_doc[i]}
        nounphrases_ = {str(list(k)): 0 for k in unique_candidate_nounphrases_per_doc[i]}

        # count how many instances point to the candidates above
        for instance in instances:
            antecedents_[str(instance.antecedent)] += 1
            nounphrases_[str(instance.antecedent)] += 1

        num_anaphors_per_antecedents += list(antecedents_.values())
        num_anaphors_per_nounphrases += list(nounphrases_.values())

    summary['anaphor per antecedent'] = [np.mean(num_anaphors_per_antecedents), np.std(num_anaphors_per_antecedents)]
    summary['anaphor per nounphrase'] = [np.mean(num_anaphors_per_nounphrases), np.std(num_anaphors_per_nounphrases)]

    # ####
    # Document statistics
    # ####

    all = [instance for instance in dataloader.all()]

    # Average Number of words in a document
    num_words = [len(to_toks(instance.document)) for instance in all]
    summary['doc words'] = [np.mean(num_words), np.std(num_words)]

    # Average Number of sentences in a document
    num_sents: List[int] = [len(instance.document) for instance in all]
    summary['doc sents'] = [np.mean(num_sents), np.std(num_sents)]

    # Average length of sentences in a document
    sent_lens: List[int] = [len(sent) for instance in all for sent in instance.document]
    summary['doc sents len'] = [np.mean(sent_lens), np.std(sent_lens)]

    return summary


@click.command()
@click.option('--dataset', '-d', type=str,
              help="The name of dataset e.g. crac, bashi, isnotes etc")
@click.option('--dataset-domain', '-dd', type=str, default=None,
              help="The name of a specific domain within the dataset e.g. arrau, switchboard")
@click.option('--dataset-domain-filename', '-ddf', type=str, default=None,
              help="The name of a specific file within the dataset domain e.g. Trains_91.CONLL")
def run(dataset: str, dataset_domain: Optional[str] = None, dataset_domain_filename: Optional[str] = None):
    """
        Pull the data
        Compute the stats
        Dump the stats
    """

    dataloader = DataLoader(dataset=dataset,
                            dataset_domain=dataset_domain,
                            dataset_domain_filename=dataset_domain_filename)

    summary: dict = compute(dataloader, name=dataset)
    print(summary)

    # Dump the dict
    with (dataloader.dir / 'stats.json').open('w+') as f:
        json.dump(summary, f)

    # Print the dict
    for k, v in summary.items():
        if type(v) is list and len(v) == 2:
            summary[k] = '%2.1f' % v[0] + ' +/- ' + '%2.1f' % v[1]
        else:
            summary[k] = v

    print(summary)


if __name__ == '__main__':
    run()
